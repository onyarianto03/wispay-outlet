import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

class FormNote extends StatelessWidget {
  const FormNote({
    Key? key,
    required this.children,
  }) : super(key: key);

  final List<Widget> children;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const CustomText(
          title: 'Keterangan:',
          size: FontSize.medium,
          textType: TextType.Bold,
          color: WispayColors.cBlack333,
          margin: EdgeInsets.only(bottom: Spacing.small),
        ),
        const SizedBox(height: Spacing.xSmall),
        BaseCard(
          blurRadius: 0,
          spreadRadius: 0,
          margin: EdgeInsets.zero,
          color: WispayColors.cSecondary.withOpacity(0.2),
          child: Column(
            children: List.generate(
              children.length,
              (index) => Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(title: '${index + 1}.'),
                  const SizedBox(width: Spacing.small),
                  Expanded(child: children[index]),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
