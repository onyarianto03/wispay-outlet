// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:auto_size_text/auto_size_text.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';

class CustomText extends StatelessWidget {
  const CustomText({
    Key? key,
    required this.title,
    this.color,
    this.textType,
    this.size,
    this.decoration,
    this.textStyle,
    this.textAlign,
    this.margin,
    this.maxLines,
    this.overflow,
    this.minFontSize,
    this.lineHeight,
  }) : super(key: key);

  final String title;
  final Color? color;
  final TextType? textType;
  final double? size;
  final TextDecoration? decoration;
  final TextStyle? textStyle;
  final TextAlign? textAlign;
  final EdgeInsets? margin;
  final int? maxLines;
  final TextOverflow? overflow;
  final double? minFontSize;
  final double? lineHeight;

  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;
    return Container(
      margin: margin,
      child: AutoSizeText(
        title,
        minFontSize: minFontSize ?? 9,
        textAlign: textAlign,
        maxLines: maxLines,
        overflow: overflow,
        style: textTheme.bodyText1
            ?.copyWith(
              color: color,
              fontSize: size ?? FontSize.defaultSize,
              fontWeight: fontWeightHelper(textType),
              decoration: decoration,
              height: lineHeight,
            )
            .merge(textStyle),
      ),
    );
  }
}
