// Flutter imports:
import 'package:flutter/material.dart';

enum TextType {
  Bold,
  SemiBold,
  Medium,
  Regular,
  Light,
}

FontWeight fontWeightHelper(type) {
  switch (type) {
    case TextType.Bold:
      return FontWeight.w700;
    case TextType.SemiBold:
      return FontWeight.w600;
    case TextType.Regular:
      return FontWeight.w400;
    default:
      return FontWeight.w400;
  }
}
