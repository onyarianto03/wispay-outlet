import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';

class RadioButton extends StatelessWidget {
  const RadioButton({
    Key? key,
    required this.value,
    required this.groupValue,
    required this.onChanged,
    required this.title,
    required this.isActive,
    this.isError = false,
    this.margin,
  }) : super(key: key);

  final BaseModel value;
  final BaseModel groupValue;
  final String title;
  final ValueChanged<BaseModel?>? onChanged;
  final EdgeInsets? margin;
  final bool isActive;
  final bool isError;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onChanged?.call(value),
      child: Container(
        margin: margin,
        decoration: BoxDecoration(
          border: Border.all(
            color: isActive
                ? WispayColors.cPrimary
                : isError
                    ? WispayColors.cRed2
                    : WispayColors.cBlackBBB,
            width: isActive || isError ? 1.8 : 1,
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Row(
          children: [
            Radio(
                value: value,
                activeColor: WispayColors.cPrimary,
                groupValue: groupValue,
                visualDensity: const VisualDensity(vertical: -1),
                onChanged: (BaseModel? value) {
                  onChanged?.call(value);
                }),
            CustomText(title: title)
          ],
        ),
      ),
    );
  }
}
