// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:shimmer/shimmer.dart';

class ShimmerWidget extends StatelessWidget {
  final double height;
  final double? width;
  final ShapeBorder shape;

  ShimmerWidget.rectangular({
    Key? key,
    required this.height,
    this.width = double.infinity,
    BorderRadiusGeometry borderRadius = BorderRadius.zero,
  })  : shape = RoundedRectangleBorder(borderRadius: borderRadius),
        super(key: key);

  const ShimmerWidget.circle({
    Key? key,
    required this.height,
    required this.width,
    this.shape = const CircleBorder(),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Colors.grey[200]!,
      highlightColor: Colors.grey[100]!,
      child: Container(
        height: height,
        width: width,
        decoration: ShapeDecoration(
          shape: shape,
          color: Colors.grey[500]!,
        ),
      ),
    );
  }
}
