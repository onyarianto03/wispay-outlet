// Flutter imports:
import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/utils/input_price_formatter.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

class TrxDetailItem extends StatelessWidget {
  const TrxDetailItem({
    Key? key,
    required this.title,
    this.value,
    this.valueColor = WispayColors.cBlack333,
    this.titleColor = WispayColors.cBlack666,
    this.isBoldTitle = false,
    this.isBoldValue = false,
    this.marginBottom = Spacing.small,
    this.multiLine = false,
    this.isInput = false,
    this.crossAxisAlignment = CrossAxisAlignment.start,
    this.textEditingController,
  }) : super(key: key);

  final String title;
  final String? value;
  final Color? valueColor;
  final Color? titleColor;
  final bool isBoldTitle;
  final bool isBoldValue;
  final double marginBottom;
  final bool multiLine;
  final bool isInput;
  final CrossAxisAlignment crossAxisAlignment;
  final TextEditingController? textEditingController;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: marginBottom),
      width: double.maxFinite,
      child: multiLine
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  title: title,
                  color: titleColor,
                  textType: isBoldTitle ? TextType.Bold : TextType.Regular,
                ),
                CustomText(
                  title: value ?? '',
                  textType: isBoldValue ? TextType.Bold : TextType.SemiBold,
                  color: valueColor,
                ),
              ],
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: crossAxisAlignment,
              children: [
                Expanded(
                  flex: 2,
                  child: CustomText(
                    title: title,
                    color: titleColor,
                    textType: isBoldTitle ? TextType.Bold : TextType.Regular,
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: isInput
                      ? CustomTextInput(
                          value: value ?? '',
                          keyboardType: TextInputType.number,
                          textEditingController: textEditingController,
                          inputFormatters: [InputPriceFormatter()],
                        )
                      : CustomText(
                          textAlign: TextAlign.right,
                          title: value ?? '',
                          color: valueColor,
                          textType: isBoldValue ? TextType.Bold : TextType.SemiBold,
                          maxLines: 2,
                        ),
                ),
              ],
            ),
    );
  }
}
