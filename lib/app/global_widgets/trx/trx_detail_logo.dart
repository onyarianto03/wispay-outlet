// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

class TrxDetailLogo extends StatelessWidget {
  const TrxDetailLogo({Key? key, required this.productName, this.productImage}) : super(key: key);

  final String productName;
  final String? productImage;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CustomImage(
          source: productImage ?? '',
          width: 40,
          height: 40,
          fit: BoxFit.contain,
        ),
        const SizedBox(width: Spacing.small),
        Expanded(
          child: CustomText(
            title: productName,
            textType: TextType.Bold,
            size: FontSize.medium,
            color: WispayColors.cBlack,
            maxLines: 2,
            minFontSize: 12,
            overflow: TextOverflow.ellipsis,
          ),
        )
      ],
    );
  }
}
