// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';

class TopTapBar extends StatelessWidget {
  const TopTapBar({
    Key? key,
    required this.controller,
    required this.tabs,
  }) : super(key: key);

  final TabController controller;
  final List<Widget> tabs;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: WispayColors.cBlack666,
            width: 0.5,
          ),
        ),
      ),
      child: TabBar(
        labelColor: WispayColors.cPrimary,
        unselectedLabelColor: WispayColors.cBlack666,
        controller: controller,
        labelStyle: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w700,
        ),
        indicatorColor: WispayColors.cPrimary,
        indicatorWeight: 2,
        tabs: tabs,
      ),
    );
  }
}
