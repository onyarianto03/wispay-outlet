import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/poin.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

class CardProduct extends StatelessWidget {
  final String? image;
  final String? name;
  final String? price;
  final String? point;
  final GestureTapCallback? onTap;

  const CardProduct({Key? key, this.image, this.name, this.onTap, this.point, this.price}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: BaseCard(
        blurRadius: 20,
        spreadRadius: 0,
        margin: EdgeInsets.zero,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(Spacing.small),
              clipBehavior: Clip.hardEdge,
              child: CustomImage(
                source: image ?? "",
                width: 60,
                height: 60,
              ),
            ),
            const SizedBox(width: Spacing.defaultSpacing),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    title: name ?? "",
                    textType: TextType.SemiBold,
                    maxLines: 2,
                  ),
                  const SizedBox(height: Spacing.xSmall),
                  Row(
                    children: [
                      CustomText(
                        title: NumberHelper.formatMoney(price),
                        color: WispayColors.cPrimary,
                        size: FontSize.small,
                      ),
                      const SizedBox(width: Spacing.large),
                      Poin(poin: point),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
