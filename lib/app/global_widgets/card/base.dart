import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BaseCard extends StatelessWidget {
  const BaseCard({
    Key? key,
    required this.child,
    this.borderRadius,
    this.margin = const EdgeInsets.all(Spacing.defaultSpacing),
    this.padding = const EdgeInsets.all(Spacing.defaultSpacing),
    this.blurRadius = Spacing.xSmall,
    this.spreadRadius = Spacing.xSmall,
    this.color = Colors.white,
    this.isEnabled = true,
    this.decorationImage,
    this.withShadow = true,
    this.height,
  }) : super(key: key);

  final Widget child;
  final double? borderRadius;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final double blurRadius;
  final double spreadRadius;
  final Color color;
  final bool isEnabled;
  final ImageProvider<Object>? decorationImage;
  final bool withShadow;
  final double? height;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      height: height,
      padding: padding,
      width: double.infinity,
      clipBehavior: Clip.hardEdge,
      foregroundDecoration: !isEnabled
          ? BoxDecoration(
              color: WispayColors.cWhite.withOpacity(0.5),
              borderRadius: BorderRadius.circular(borderRadius ?? 8.r),
            )
          : null,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(borderRadius ?? 8.r),
        image: decorationImage == null
            ? null
            : DecorationImage(
                image: decorationImage!,
                fit: BoxFit.cover,
              ),
        boxShadow: withShadow
            ? [
                BoxShadow(
                  color: WispayColors.cBlack.withOpacity(0.06),
                  blurRadius: blurRadius,
                  spreadRadius: spreadRadius,
                ),
              ]
            : null,
      ),
      child: child,
    );
  }
}
