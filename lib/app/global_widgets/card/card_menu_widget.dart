// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';

// Project imports:
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

class CardMenuWidget extends StatelessWidget {
  const CardMenuWidget({
    Key? key,
    required this.menu,
    this.onPress,
  }) : super(key: key);

  final List<CardMenu> menu;
  final VoidCallback? onPress;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      decoration: BoxDecoration(
        color: WispayColors.cWhite,
        boxShadow: [buildShadow()],
        borderRadius: BorderRadius.circular(10.r),
      ),
      child: ListView.separated(
        separatorBuilder: (context, index) => const Divider(height: 32),
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (_, idx) => _buildItem(idx),
        itemCount: menu.length,
      ),
    );
  }

  Widget _buildItem(int idx) {
    return InkWell(
      onTap: () {
        if (menu[idx].onPress != null) {
          menu[idx].onPress!();
        } else if (menu[idx].route != null) {
          Get.toNamed(menu[idx].route!, arguments: menu[idx]);
        } else {
          onPress?.call();
        }
      },
      child: Row(
        children: [
          menu[idx].icon != '' ? Image.asset(menu[idx].icon, width: 24, height: 24) : const SizedBox(),
          SizedBox(width: menu[idx].icon != '' ? Spacing.defaultSpacing : 0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildTitle(idx),
                Visibility(
                  child: CustomText(
                    title: menu[idx].subTitle ?? "",
                    size: FontSize.small.sp,
                    color: menu[idx].subTitleColor,
                  ),
                  visible: menu[idx].subTitle != null,
                )
              ],
            ),
          ),
          menu[idx].customRightIcon != null
              ? menu[idx].customRightIcon!
              : (menu[idx].route != null || menu[idx].onPress != null)
                  ? const Icon(Icons.arrow_forward_ios_rounded, size: 16)
                  : const SizedBox(),
        ],
      ),
    );
  }

  Row _buildTitle(int idx) {
    return Row(
      children: [
        CustomText(
          title: menu[idx].title,
          size: FontSize.medium,
          textType: TextType.SemiBold,
          color: WispayColors.cBlack333,
        ),
        const SizedBox(width: Spacing.defaultSpacing),
        Visibility(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 1),
            decoration: BoxDecoration(
              color: WispayColors.cSuccess,
              borderRadius: BorderRadius.circular(16.r),
            ),
            child: const CustomText(
              title: 'Baru',
              color: WispayColors.cBlack333,
              size: FontSize.xSmall,
              textType: TextType.SemiBold,
            ),
          ),
          visible: menu[idx].isNew,
        )
      ],
    );
  }
}
