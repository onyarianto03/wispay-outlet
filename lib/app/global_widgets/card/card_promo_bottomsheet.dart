import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class CardPromoBottomSheet extends StatelessWidget {
  const CardPromoBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(Spacing.small),
      decoration: BoxDecoration(
        color: Colors.white,
        image: const DecorationImage(
          image: Assets.iconsMiscWave1,
          alignment: Alignment.centerRight,
        ),
        borderRadius: BorderRadius.circular(8),
        boxShadow: [buildShadow(spreadRadius: 0, blurRadius: 20)],
      ),
      child: Row(
        children: [
          Assets.iconsKupon.image(width: 24),
          const CustomText(
            title: 'Tambah promo/voucher',
            textType: TextType.SemiBold,
            margin: EdgeInsets.only(left: Spacing.small),
            color: WispayColors.cBlack666,
          ),
          const Spacer(),
          const Icon(
            Icons.chevron_right,
            size: 35,
            color: WispayColors.cBlack666,
          )
        ],
      ),
    );
  }
}
