// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/shimmer/shimmer_widget.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';

class ProductCategoryItem extends StatelessWidget {
  const ProductCategoryItem({
    Key? key,
    required this.image,
    required this.title,
    this.isLoading,
    this.onTap,
  }) : super(key: key);

  final String image;
  final String title;
  final bool? isLoading;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        highlightColor: Colors.transparent,
        splashFactory: InkRipple.splashFactory,
        splashColor: Colors.grey[50],
        radius: 50,
        onTap: onTap ?? () {},
        child: Column(
          children: [
            BaseCard(
              margin: EdgeInsets.zero,
              padding: EdgeInsets.all(Spacing.medium.r),
              blurRadius: 20,
              spreadRadius: 0,
              borderRadius: 16,
              child: CustomImage(source: image, width: 35.w, height: 40.h, borderRadius: 0),
            ),
            const SizedBox(height: Spacing.small),
            _buildText(isLoading, title),
          ],
        ),
      ),
    );
  }

  Widget _buildText(isLoading, title) {
    if (isLoading) return ShimmerWidget.rectangular(height: 20.h);
    return SizedBox(
      height: 35.w,
      child: CustomText(
        title: title,
        size: FontSize.defaultSize,
        color: WispayColors.cBlack666,
        textAlign: TextAlign.center,
      ),
    );
  }
}
