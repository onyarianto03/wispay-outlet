// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';

class RoundedTabBar extends StatelessWidget {
  const RoundedTabBar({Key? key, required this.tabs, this.controller}) : super(key: key);

  final TabController? controller;
  final List<Widget> tabs;

  @override
  Widget build(BuildContext context) {
    return ButtonsTabBar(
      controller: controller,
      contentPadding: EdgeInsets.symmetric(
        horizontal: Spacing.large.w,
      ),
      radius: 50,
      labelStyle: const TextStyle(color: WispayColors.cPrimary),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(
          color: WispayColors.cPrimary,
          width: 1.5,
        ),
        color: WispayColors.cPrimary.withAlpha(20),
      ),
      unselectedLabelStyle: const TextStyle(
        color: WispayColors.cBlack666,
      ),
      unselectedDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: Colors.white,
        border: Border.all(
          color: WispayColors.cBorderE,
          width: 1.5,
        ),
      ),
      tabs: tabs,
    );
  }
}
