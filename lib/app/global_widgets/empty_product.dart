import 'package:flutter/cupertino.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class EmptyProduct extends StatelessWidget {
  const EmptyProduct({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(Assets.iconsEmptyList.path, width: 100, height: 100),
          const CustomText(
            title: "Produk tidak ditemukan",
            size: FontSize.medium,
            textType: TextType.SemiBold,
            margin: EdgeInsets.only(top: Spacing.small),
          ),
        ],
      ),
    );
  }
}
