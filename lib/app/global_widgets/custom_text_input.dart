// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttercontactpicker/fluttercontactpicker.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/gsm_helper.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class CustomTextInput extends StatelessWidget {
  const CustomTextInput({
    Key? key,
    this.hintText = 'Placeholder',
    this.obscureText = false,
    this.validator,
    this.textEditingController,
    required this.value,
    this.onClear,
    this.isEnabled = true,
    this.hintStyle = const TextStyle(color: WispayColors.cDisabledColor),
    this.keyboardType,
    this.initialValue,
    this.contentPadding,
    this.showContact = false,
    this.label,
    this.textInputAction,
    this.autofocus = false,
    this.onTap,
    this.isGSM = false,
    this.inputFormatters,
    this.onChanged,
    this.prefixIcon,
    this.helperText,
    this.maxLines = 1,
    this.margin,
    this.autovalidateMode,
    this.filled = false,
    this.fillColor,
    this.withCustomSuffixIcon = false,
    this.suffixIcon,
    this.helperTextStyle,
    this.maxLength,
    this.focusNode,
    this.onContactPicked,
  }) : super(key: key);

  final String hintText;
  final bool obscureText;
  final String? Function(String?)? validator;
  final TextEditingController? textEditingController;
  final String value;
  final VoidCallback? onClear;
  final bool isEnabled;
  final TextStyle? hintStyle;
  final TextInputType? keyboardType;
  final String? initialValue;
  final EdgeInsets? contentPadding;
  final bool showContact;
  final String? label;
  final TextInputAction? textInputAction;
  final bool autofocus;
  final GestureTapCallback? onTap;
  final bool isGSM;
  final List<TextInputFormatter>? inputFormatters;
  final ValueChanged<String>? onChanged;
  final Widget? prefixIcon;
  final String? helperText;
  final int? maxLines;
  final EdgeInsetsGeometry? margin;
  final AutovalidateMode? autovalidateMode;
  final bool filled;
  final Color? fillColor;
  final bool withCustomSuffixIcon;
  final Widget? suffixIcon;
  final TextStyle? helperTextStyle;
  final int? maxLength;
  final FocusNode? focusNode;
  final ValueChanged<String?>? onContactPicked;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (label != null)
            CustomText(
              title: label!,
              textType: TextType.SemiBold,
              size: FontSize.small,
              margin: const EdgeInsets.only(bottom: Spacing.xSmall),
            ),
          TextFormField(
            focusNode: focusNode,
            autovalidateMode: autovalidateMode,
            onChanged: onChanged,
            inputFormatters: inputFormatters,
            textAlignVertical: TextAlignVertical.center,
            keyboardType: isGSM ? TextInputType.phone : keyboardType,
            maxLines: maxLines,
            autofocus: autofocus,
            controller: textEditingController,
            initialValue: initialValue,
            onTap: onTap,
            textInputAction: textInputAction,
            validator: validator,
            enabled: isEnabled,
            maxLength: maxLength,
            decoration: InputDecoration(
              prefixIcon: prefixIcon,
              contentPadding: const EdgeInsets.symmetric(horizontal: Spacing.small, vertical: Spacing.small),
              hintText: hintText,
              helperText: helperText,
              helperStyle: helperTextStyle ?? const TextStyle(fontSize: FontSize.small),
              suffixIcon: withCustomSuffixIcon ? suffixIcon : _suffixIcon(),
              errorStyle: const TextStyle(fontSize: FontSize.small, color: Colors.red),
              isDense: true,
              enabled: isEnabled,
              filled: filled || !isEnabled,
              fillColor: fillColor ?? WispayColors.cDisabledColor.withOpacity(0.35),
              hintStyle: hintStyle,
            ),
          ),
        ],
      ),
    );
  }

  Padding _suffixIcon() {
    return Padding(
      padding: const EdgeInsets.only(right: Spacing.defaultSpacing),
      child: IntrinsicHeight(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            if (isGSM) GSMHelper.getOperatorLogo(value) ?? const SizedBox(),
            value.isEmpty || isEnabled == false
                ? const SizedBox()
                : InkWell(
                    onTap: () {
                      textEditingController?.clear();
                      onClear?.call();
                    },
                    child: Assets.iconsDel.image(width: 25)),
            showContact
                ? const VerticalDivider(
                    thickness: 1,
                    endIndent: Spacing.small,
                    indent: Spacing.small,
                  )
                : const SizedBox(),
            showContact
                ? InkWell(
                    onTap: () async {
                      try {
                        final PhoneContact contact =
                            await FlutterContactPicker.pickPhoneContact(askForPermission: true);

                        if (contact.phoneNumber?.number != null) {
                          final newVal =
                              NumberHelper.formatPhoneRemove62(contact.phoneNumber!.number!).replaceAll('-', '');
                          onContactPicked?.call(newVal);
                        }
                      } catch (error) {
                        print(error);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(left: Spacing.xSmall),
                      child: Image.asset(Assets.iconsKontak.path, height: 20, width: 20),
                    ),
                  )
                : const SizedBox(),
          ],
        ),
      ),
    );
  }
}
