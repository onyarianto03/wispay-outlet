// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';

class Dropdown extends StatelessWidget {
  final GestureTapCallback? onTap;
  final String? label;
  final String value;
  final bool withImage;
  final String imageUrl;
  final bool enabled;
  final String? topLabel;
  final String? errorText;
  final double imageWidth;
  final double imageHeight;

  const Dropdown({
    Key? key,
    this.onTap,
    this.label,
    this.withImage = false,
    this.enabled = true,
    this.imageUrl = '',
    this.topLabel,
    required this.value,
    this.errorText,
    this.imageHeight = 25,
    this.imageWidth = 25,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          child: CustomText(
            title: topLabel ?? "",
            textType: TextType.SemiBold,
            size: FontSize.small,
            margin: const EdgeInsets.only(bottom: Spacing.xSmall),
          ),
          visible: topLabel != null && topLabel!.isNotEmpty,
        ),
        Container(
          height: 48,
          padding: const EdgeInsets.symmetric(
            horizontal: Spacing.defaultSpacing,
            vertical: Spacing.small,
          ),
          width: double.infinity,
          foregroundDecoration: BoxDecoration(
            color: enabled ? Colors.transparent : WispayColors.cDisabledColor.withOpacity(0.5),
            borderRadius: BorderRadius.circular(8.r),
          ),
          decoration: BoxDecoration(
            border: Border.all(
              color: errorText != null ? Colors.red : WispayColors.cBlackBBB,
              width: 1,
            ),
            color: Colors.white,
            borderRadius: BorderRadius.circular(8.r),
          ),
          child: InkWell(
            onTap: enabled ? onTap : null,
            child: Row(
              children: [
                Visibility(
                  child: CustomImage(
                    source: imageUrl,
                    width: imageWidth,
                    height: imageHeight,
                    borderRadius: 4,
                    fit: BoxFit.contain,
                  ),
                  visible: withImage && value.isNotEmpty,
                ),
                SizedBox(width: withImage && value.isNotEmpty ? Spacing.small : 0),
                CustomText(
                  title: value.isEmpty ? label ?? '' : value,
                  size: FontSize.medium,
                  color: value.isNotEmpty ? WispayColors.cBlack : WispayColors.cBlack666,
                ),
                const Spacer(),
                const Icon(
                  Icons.keyboard_arrow_down,
                  color: WispayColors.cBlack333,
                ),
              ],
            ),
          ),
        ),
        Visibility(
          child: CustomText(
            title: errorText ?? '',
            size: FontSize.small,
            color: Colors.red,
            margin: const EdgeInsets.only(top: Spacing.xSmall, left: Spacing.small),
          ),
          visible: errorText != null,
        )
      ],
    );
  }
}
