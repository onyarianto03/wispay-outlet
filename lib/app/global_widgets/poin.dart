import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class Poin extends StatelessWidget {
  const Poin({
    Key? key,
    this.poin,
  }) : super(key: key);

  final String? poin;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Assets.iconsCoin.image(width: 12),
        const SizedBox(width: Spacing.xSmall),
        CustomText(
          title: NumberHelper.formatMoneyWithoutSymbol(poin) + ' Poin',
          size: FontSize.xSmall,
        ),
      ],
    );
  }
}
