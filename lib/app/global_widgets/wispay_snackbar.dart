// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';

class WispaySnackbar {
  static Future<void> showError(String message, {String title = 'Error', VoidCallback? onClose}) async {
    if (message == 'Unauthorized') {
      return;
    }
    Get.snackbar(
      '',
      '',
      titleText: CustomText(
        title: title,
        color: Colors.white,
        size: FontSize.defaultSize,
        textType: TextType.Bold,
      ),
      messageText: CustomText(
        title: message,
        size: FontSize.defaultSize,
        textStyle: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      backgroundColor: Colors.red[500],
      duration: const Duration(milliseconds: 1500),
      shouldIconPulse: false,
      isDismissible: false,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      reverseAnimationCurve: Curves.easeInOut,
      margin: const EdgeInsets.all(5),
      icon: const Icon(Icons.error, color: Colors.white),
    );

    Future.delayed(const Duration(milliseconds: 1000), () {
      onClose?.call();
      Get.back();
    });
  }

  static Future<void> showSuccess(String message, {String title = 'Success', VoidCallback? onClose}) async {
    Get.snackbar(
      '',
      '',
      titleText: CustomText(
        title: title,
        color: Colors.white,
        size: FontSize.defaultSize,
        textType: TextType.Bold,
      ),
      messageText: CustomText(
        title: message,
        size: FontSize.defaultSize,
        textStyle: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      backgroundColor: Colors.green[500],
      duration: const Duration(milliseconds: 2000),
      shouldIconPulse: false,
      isDismissible: false,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      reverseAnimationCurve: Curves.easeInOut,
      margin: const EdgeInsets.all(5),
      // icon: const Icon(Icons.check, color: Colors.white),
    );

    Future.delayed(const Duration(milliseconds: 1000), () {
      onClose?.call();
      Get.back();
    });
  }
}
