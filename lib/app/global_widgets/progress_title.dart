// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:auto_size_text/auto_size_text.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

class ProgressTitle extends StatelessWidget {
  const ProgressTitle({
    Key? key,
    this.progressText = '',
    this.title = '',
    this.description = '',
    this.percent = 0.0,
    this.descriptionColor,
  }) : super(key: key);

  final String progressText;
  final String title;
  final String description;
  final double percent;
  final Color? descriptionColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CircularPercentIndicator(
          radius: 55.0,
          lineWidth: 4.0,
          percent: percent,
          center: CustomText(
            title: progressText,
            color: WispayColors.cBlack333,
            textType: TextType.Bold,
            size: FontSize.medium,
          ),
          progressColor: WispayColors.cGreen,
        ),
        const SizedBox(width: Spacing.defaultSpacing),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: title,
              textType: TextType.Bold,
              size: FontSize.large,
              color: WispayColors.cBlack333,
            ),
            AutoSizeText.rich(
              TextSpan(
                text: 'Next: ',
                style: const TextStyle(
                  color: WispayColors.cBlack666,
                  fontSize: FontSize.small,
                ),
                children: [
                  TextSpan(
                    text: description,
                    style: TextStyle(
                      color: descriptionColor ?? WispayColors.cBlack666,
                    ),
                  )
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}
