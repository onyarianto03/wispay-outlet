// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/bottomsheet_title.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

class RegencyBottomSheet extends StatelessWidget {
  const RegencyBottomSheet({
    Key? key,
    required this.data,
    required this.title,
    this.onSelect,
  }) : super(key: key);

  final List<String> data;
  final String title;
  final void Function(String)? onSelect;

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      initialChildSize: 0.6,
      maxChildSize: 0.95,
      expand: false,
      builder: (ctx, sc) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
          child: Column(
            children: [
              BottomSheetTitle(
                title: 'Pilih $title',
              ),
              const SizedBox(
                height: Spacing.defaultSpacing,
              ),
              SizedBox(
                height: 40,
                child: TextField(
                  style: const TextStyle(fontSize: FontSize.medium),
                  decoration: InputDecoration(
                    prefixIcon: const Icon(Icons.search),
                    isDense: true,
                    hintText: 'Cari $title...',
                    hintStyle: const TextStyle(
                      color: WispayColors.cGrey99,
                      fontSize: FontSize.small,
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: Spacing.defaultSpacing,
              ),
              Expanded(
                child: ListView.separated(
                  shrinkWrap: true,
                  controller: sc,
                  separatorBuilder: (context, index) => const Divider(
                    thickness: 1,
                  ),
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    final item = data[index];
                    return InkWell(
                      onTap: () {
                        onSelect!(item);
                        Get.back();
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: Spacing.small,
                        ),
                        child: CustomText(
                          title: item,
                          size: FontSize.defaultSize,
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
