// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

class BottomSheetTitle extends StatelessWidget {
  const BottomSheetTitle({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: Spacing.defaultSpacing),
        Stack(
          fit: StackFit.loose,
          children: [
            Align(
              alignment: Alignment.center,
              child: CustomText(
                title: title,
                color: WispayColors.cBlack,
                textType: TextType.SemiBold,
                size: FontSize.xLarge,
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: GestureDetector(
                child: const Icon(Icons.close),
                onTap: () => Get.back(),
              ),
            ),
          ],
        ),
        const SizedBox(height: Spacing.small),
        const Divider(thickness: 1)
      ],
    );
  }
}
