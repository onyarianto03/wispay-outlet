// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/bottomsheet_title.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/card/card_promo_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class WispayBottomSheet {
  static void showBottomSheet(
    Widget content, {
    isDismissible = true,
    isScrollControlled = false,
    enterBottomSheetDuration,
    VoidCallback? onclose,
  }) {
    Get.bottomSheet(
      content,
      backgroundColor: WispayColors.cWhite,
      isDismissible: isDismissible,
      isScrollControlled: isScrollControlled,
      enterBottomSheetDuration: enterBottomSheetDuration,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
    ).then((value) => onclose?.call());
  }

  static void show({
    String title = 'Title',
    bool withTitle = true,
    required List<Widget> children,
    bool isDismissible = true,
    bool enableDrag = true,
    bool isScrollControlled = true,
    bool enableBackButton = true,
    double height = 1,
    VoidCallback? onclose,
  }) {
    Get.bottomSheet(
      WillPopScope(
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.start,
          spacing: Spacing.defaultSpacing,
          children: [
            withTitle ? BottomSheetTitle(title: title) : const SizedBox(height: Spacing.defaultSpacing),
            ...children,
          ],
        ).paddingSymmetric(horizontal: Spacing.defaultSpacing),
        onWillPop: () async => enableBackButton,
      ),
      isScrollControlled: isScrollControlled,
      useRootNavigator: true,
      backgroundColor: WispayColors.cWhite,
      isDismissible: isDismissible,
      enableDrag: enableDrag,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
    ).then((value) => onclose?.call());
  }

  static void scrollable({
    String title = 'Title',
    bool withTitle = true,
    bool isDismissible = true,
    bool withSearch = true,
    double maxChildSize = 0.95,
    double initialChildSize = 0.6,
    double minChildSize = 0.25,
    ValueChanged<String>? onSearch,
    required final ScrollableWidgetBuilder itemBuilder,
    VoidCallback? onclose,
  }) {
    Get.bottomSheet(
      StatefulBuilder(
        builder: (_, setState) => DraggableScrollableSheet(
          initialChildSize: initialChildSize,
          maxChildSize: maxChildSize,
          minChildSize: minChildSize,
          expand: false,
          builder: (_, sc) {
            return Column(
              children: [
                if (withTitle) BottomSheetTitle(title: title),
                if (withSearch || withTitle) const SizedBox(height: Spacing.defaultSpacing),
                if (withSearch)
                  SizedBox(
                    height: 40,
                    child: TextField(
                      autofocus: false,
                      onChanged: (terms) {
                        setState(() {
                          onSearch?.call(terms);
                        });
                      },
                      style: const TextStyle(fontSize: FontSize.medium),
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.search),
                        isDense: true,
                        hintText: 'Cari $title',
                        hintStyle: const TextStyle(
                          color: WispayColors.cGrey99,
                          fontSize: FontSize.small,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                  ),
                Expanded(child: itemBuilder(_, sc)),
              ],
            );
          },
        ).paddingSymmetric(horizontal: Spacing.defaultSpacing),
      ),
      backgroundColor: WispayColors.cWhite,
      isDismissible: isDismissible,
      isScrollControlled: true,
      ignoreSafeArea: false,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
    ).then((_) => onclose?.call());
  }

  static void scrollableList<T>({
    String title = 'Title',
    bool withTitle = true,
    bool isDismissible = true,
    bool withSearch = true,
    ValueChanged<String>? onSearch,
    ValueChanged<int>? onSelect,
    required List<T> items,
    required T selectedItem,
    bool withImage = false,
    double imageWidth = 25,
    double imageHeight = 25,
    String? imageKey = 'image',
    VoidCallback? onClose,
    VoidCallback? onReachedEnd,
    double initialChildSize = 0.6,
  }) {
    String _renderItems(List<T> items, int idx) {
      if (items is List<String>) {
        final _d = items[idx] as String;
        return _d;
      }
      final _d = items[idx] as BaseModel;
      return _d.name ?? "";
    }

    bool _selectedItem(T item, T selectedItem) {
      if (item is String) {
        return item == selectedItem as String;
      }
      final _d = item as BaseModel;
      final _b = selectedItem as BaseModel;
      return _d.id == _b.id;
    }

    String _renderImage(T item) {
      String url = '';

      if (item is BaseModel) {
        url = item.image?.url ?? item.logo?.url ?? "";
      }
      return url;
    }

    Get.bottomSheet(
      StatefulBuilder(
        builder: (_, StateSetter setState) => DraggableScrollableSheet(
          initialChildSize: initialChildSize,
          maxChildSize: 0.95,
          expand: false,
          minChildSize: initialChildSize,
          builder: (_, sc) {
            return Column(
              children: [
                BottomSheetTitle(title: title),
                Visibility(child: const SizedBox(height: Spacing.defaultSpacing), visible: withSearch),
                Visibility(
                  child: SizedBox(
                    height: 40,
                    child: TextField(
                      onChanged: (terms) {
                        setState(() {
                          onSearch?.call(terms);
                        });
                      },
                      textInputAction: TextInputAction.search,
                      style: const TextStyle(fontSize: FontSize.medium),
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.search),
                        isDense: true,
                        hintText: 'Cari $title',
                        hintStyle: const TextStyle(
                          color: WispayColors.cGrey99,
                          fontSize: FontSize.small,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                  ),
                  visible: withSearch,
                ),
                const SizedBox(height: Spacing.small),
                Expanded(
                  child: NotificationListener(
                    onNotification: (notification) {
                      if (notification is ScrollEndNotification) {
                        if (notification.metrics.pixels == notification.metrics.maxScrollExtent) {
                          setState(() {});
                          onReachedEnd?.call();
                          return true;
                        }
                      }
                      return true;
                    },
                    child: ListView.separated(
                      shrinkWrap: true,
                      controller: sc,
                      itemCount: items.length,
                      physics: const BouncingScrollPhysics(),
                      separatorBuilder: (context, index) => const Divider(thickness: 1),
                      itemBuilder: (context, index) => ListTile(
                        dense: true,
                        contentPadding: EdgeInsets.zero,
                        onTap: () => onSelect!(index),
                        visualDensity: const VisualDensity(vertical: -2),
                        leading: withImage
                            ? CustomImage(
                                source: _renderImage(items[index]),
                                width: imageWidth,
                                fit: BoxFit.contain,
                                height: imageHeight,
                                borderRadius: 4,
                              )
                            : null,
                        trailing: Visibility(
                          child: const Icon(
                            Icons.check_circle,
                            color: WispayColors.cGreen2,
                            size: 20,
                          ),
                          visible: _selectedItem(items[index], selectedItem),
                        ),
                        title: CustomText(
                          title: _renderItems(items, index),
                          color: WispayColors.cBlack333,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
        ).paddingSymmetric(horizontal: Spacing.defaultSpacing),
      ),
      backgroundColor: WispayColors.cWhite,
      isDismissible: isDismissible,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
    ).then((_) => onClose?.call());
  }

  static void showConfirm({
    String title = 'Title',
    bool withTitle = true,
    required List<Widget> children,
    bool isDismissible = true,
    bool withPromo = true,
    String totalTitle = 'Total Bayar',
    String confirmBtnText = 'Lanjut Bayar',
    String totalPrice = 'Rp 0',
    String? point,
    VoidCallback? onConfirm,
  }) {
    Get.bottomSheet(
      Wrap(
        crossAxisAlignment: WrapCrossAlignment.start,
        spacing: Spacing.defaultSpacing,
        children: [
          withTitle ? BottomSheetTitle(title: title) : const SizedBox(),
          ...children,
          SizedBox(height: withPromo ? Spacing.defaultSpacing : 0),
          withPromo
              ? const CardPromoBottomSheet()
              : const SizedBox(
                  height: 0,
                ),
          SizedBox(height: withPromo ? Spacing.defaultSpacing : 0),
          const Divider(thickness: 1),
          SizedBox(height: withPromo ? Spacing.small : 0),
          Row(
            children: [
              Expanded(
                flex: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(title: totalTitle),
                    CustomText(
                      title: totalPrice,
                      size: FontSize.large,
                      color: WispayColors.cPrimary,
                      textType: TextType.Bold,
                    ),
                    point != null && point != '0.0' && point != '0'
                        ? Row(
                            children: [
                              Assets.iconsCoin.image(width: 20, height: 20),
                              const SizedBox(width: Spacing.small),
                              CustomText(title: '$point Poin'),
                            ],
                          )
                        : const SizedBox(),
                  ],
                ),
              ),
              Expanded(
                flex: 3,
                child: CustomButton(
                  title: confirmBtnText,
                  onPress: onConfirm,
                ),
              )
            ],
          ),
          const SizedBox(height: Spacing.defaultSpacing),
        ],
      ).paddingSymmetric(horizontal: Spacing.defaultSpacing),
      isScrollControlled: true,
      backgroundColor: WispayColors.cWhite,
      isDismissible: isDismissible,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
    );
  }
}
