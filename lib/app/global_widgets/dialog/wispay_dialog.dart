// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/bottomsheet_title.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class WispayDialog {
  static void showLoading({VoidCallback? onClose}) {
    Get.dialog(
      Center(
        child: SizedBox(
          width: 100,
          height: 100,
          child: Lottie.asset(Assets.lottieLoading),
        ),
      ),
      name: 'Loading Dialog',
    ).then((_) => onClose?.call);
  }

  static void showLoadingPrinter() {
    Get.dialog(
      Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                color: WispayColors.cWhite,
                borderRadius: BorderRadius.circular(20.r),
              ),
              child: Lottie.asset(Assets.lottiePrintLoader),
            ),
            const SizedBox(height: Spacing.defaultSpacing),
            const CustomText(
              title: 'Kami sedang menyiapkan hasil print Anda...',
              color: WispayColors.cWhite,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
      name: 'Loading Printer Dialog',
    );
  }

  static void fullScreenLoading() {
    Get.dialog(
      Container(
        color: Colors.white,
        child: Center(
          child: SizedBox(
            width: 100,
            height: 100,
            child: Lottie.asset(Assets.lottieLoading),
          ),
        ),
      ),
    );
  }

  static void showConfirmDialog({
    VoidCallback? onConfirm,
    VoidCallback? onCancel,
    required String title,
  }) {
    Get.dialog(
      Center(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: WispayColors.cWhite,
          ),
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          width: Get.width * 0.8,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              CustomText(
                title: title,
                size: FontSize.medium,
                textAlign: TextAlign.center,
                textType: TextType.SemiBold,
                color: WispayColors.cBlack,
              ),
              const SizedBox(height: Spacing.defaultSpacing),
              Row(
                children: [
                  Expanded(
                    child: CustomButton(
                      title: 'Tidak',
                      onPress: () {
                        Get.back();
                        onCancel?.call();
                      },
                    ),
                  ),
                  const SizedBox(width: Spacing.defaultSpacing),
                  Expanded(
                    child: WispayOutlinedButton(
                      title: 'Yakin',
                      onPress: () {
                        Get.back();
                        onConfirm?.call();
                      },
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  static void showQR(BuildContext context, String phonNumber, String? name) {
    Get.dialog(
      Center(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: WispayColors.cWhite,
          ),
          padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
          width: Get.width * 0.8,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const BottomSheetTitle(title: 'Kode QR Saya'),
              CustomImage(
                source: 'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${phonNumber}',
                width: 150,
              ),
              CustomText(
                title: name ?? "",
                color: WispayColors.cBlack,
                textType: TextType.SemiBold,
                size: FontSize.medium,
                margin: const EdgeInsets.only(top: Spacing.defaultSpacing),
              ),
              const SizedBox(height: Spacing.defaultSpacing)
            ],
          ),
        ),
      ),
      name: 'QR Code',
    );
  }

  static void closeDialog() {
    Get.back();
  }

  static void showPaymentDialog() {
    Get.dialog(
      Container(
        color: WispayColors.cWhite,
        child: Center(
          child: Stack(
            children: [
              Align(
                alignment: Alignment.center,
                child: Lottie.asset(
                  Assets.lottiePayment,
                  width: Get.width * 0.8,
                  height: Get.width * 0.8,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: Get.height * 0.62),
                child: Align(
                  alignment: Alignment.center,
                  child: Column(
                    children: const [
                      CustomText(
                        title: 'Transaksi Anda sedang kami proses',
                        textType: TextType.SemiBold,
                        size: 16,
                        color: WispayColors.cBlack333,
                      ),
                      SizedBox(height: Spacing.small),
                      CustomText(
                        title: 'Mohon tunggu beberapa saat',
                        size: FontSize.defaultSize,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      transitionCurve: Curves.fastLinearToSlowEaseIn,
      transitionDuration: const Duration(milliseconds: 300),
    );
  }
}
