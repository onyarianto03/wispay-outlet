import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';

class RoundedOnlyIconButton extends StatelessWidget {
  const RoundedOnlyIconButton({
    Key? key,
    this.onPressed,
    this.borderRadius = 50,
    required this.icon,
  }) : super(key: key);

  final void Function()? onPressed;
  final double borderRadius;
  final Icon icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: borderRadius,
      height: borderRadius,
      decoration: BoxDecoration(
        color: WispayColors.cPrimary,
        borderRadius: BorderRadius.circular(borderRadius),
        gradient: LinearGradient(
          colors: [WispayColors.cPrimary, WispayColors.cPrimary.withOpacity(0.65)],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          stops: const [0.3, 0.9],
        ),
      ),
      child: IconButton(
        onPressed: onPressed,
        color: Colors.white,
        icon: icon,
      ),
    );
  }
}
