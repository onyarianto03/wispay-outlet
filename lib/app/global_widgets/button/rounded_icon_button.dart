// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';

class RoundedIconButton extends StatelessWidget {
  const RoundedIconButton({
    Key? key,
    required this.icon,
    this.title = 'Title',
    this.showBadge = false,
    this.badgeCount = 0,
    this.onPressed,
  }) : super(key: key);

  final String icon;
  final String title;
  final bool showBadge;
  final int badgeCount;
  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Column(
          children: [
            Material(
              color: Colors.transparent,
              child: InkWell(
                splashColor: WispayColors.cPrimary.withAlpha(50),
                highlightColor: Colors.transparent,
                borderRadius: BorderRadius.circular(30),
                onTap: onPressed,
                child: SizedBox(
                  width: 30.w,
                  height: 30.h,
                  child: Image.asset(icon),
                ),
              ),
            ),
            const SizedBox(height: Spacing.small),
            CustomText(
              title: title,
              textType: TextType.SemiBold,
              size: FontSize.small,
            ),
          ],
        ),
        if (showBadge)
          Positioned(
            right: 1,
            top: -10,
            child: Container(
              padding: const EdgeInsets.all(2),
              child: const CustomText(
                title: '99+',
                size: FontSize.small,
                color: WispayColors.cWhite,
                textAlign: TextAlign.center,
              ),
              height: 20,
              width: 20,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(300.r),
                color: Colors.red,
              ),
            ),
          ),
      ],
    );
  }
}
