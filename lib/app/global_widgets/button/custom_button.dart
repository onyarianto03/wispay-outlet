part of 'button.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key? key,
    this.title = "Button",
    this.onPress,
    this.isEnabled = true,
    this.isLoading = false,
    this.type = ButtonType.Primary,
    this.leftIcon,
    this.size = ButtonSize.md,
    this.width = double.infinity,
  }) : super(key: key);

  final String title;
  final VoidCallback? onPress;
  final bool isEnabled;
  final bool isLoading;
  final ButtonType type;
  final Widget? leftIcon;
  final ButtonSize? size;
  final double? width;

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(8),
      child: InkWell(
        splashColor: WispayColors.cPrimary,
        onTap: isEnabled ? onPress : null,
        borderRadius: BorderRadius.circular(8),
        child: Container(
          width: width,
          height: size == ButtonSize.md ? 40 : 30,
          padding: const EdgeInsets.symmetric(
            vertical: Spacing.medium - 2,
            horizontal: Spacing.defaultSpacing - 2,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            gradient: LinearGradient(
              colors: isEnabled ? _getColors(type) : _disabledColors(type),
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              stops: const [0.3, 0.9],
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              if (leftIcon != null) leftIcon!,
              if (leftIcon != null) const SizedBox(width: Spacing.small),
              Center(
                child: !isLoading
                    ? CustomText(
                        title: title,
                        textStyle: const TextStyle(letterSpacing: 0.4),
                        size: FontSize.defaultSize,
                        color: isEnabled ? _getTextColor(type) : _disabledTextColors(type),
                        textType: TextType.Bold,
                      )
                    : const SpinKitThreeBounce(color: Colors.white, size: 15),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Color> _getColors(ButtonType type) {
    switch (type) {
      case ButtonType.Primary:
        return [
          WispayColors.cPrimary,
          WispayColors.cPrimary.withOpacity(0.7),
        ];
      case ButtonType.Secondary:
        return [WispayColors.cPrimary.withOpacity(0.10), WispayColors.cPrimary.withOpacity(0.05)];
      default:
        return [
          WispayColors.cPrimary,
          WispayColors.cPrimary.withOpacity(0.7),
        ];
    }
  }

  Color _getTextColor(ButtonType type) {
    Color c = Colors.white;
    switch (type) {
      case ButtonType.Primary:
        c = Colors.white;
        break;
      case ButtonType.Secondary:
        c = WispayColors.cPrimary;
        break;
    }

    return c;
  }

  Color _disabledTextColors(ButtonType type) {
    Color c = Colors.white;
    switch (type) {
      case ButtonType.Primary:
        c = Colors.white;
        break;
      case ButtonType.Secondary:
        c = WispayColors.cBlack.withOpacity(0.4);
        break;
    }

    return c;
  }

  List<Color> _disabledColors(ButtonType type) {
    switch (type) {
      case ButtonType.Primary:
        return [
          WispayColors.cPrimary,
          WispayColors.cPrimary.withOpacity(0.7),
        ];
      case ButtonType.Secondary:
        return [
          const Color.fromARGB(255, 38, 39, 43).withOpacity(0.10),
          const Color.fromARGB(255, 16, 16, 16).withOpacity(0.05)
        ];
      default:
        return [
          WispayColors.cPrimary,
          WispayColors.cPrimary.withOpacity(0.7),
        ];
    }
  }
}
