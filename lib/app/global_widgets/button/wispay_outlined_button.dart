part of 'button.dart';

class WispayOutlinedButton extends StatelessWidget {
  const WispayOutlinedButton({
    Key? key,
    this.title = "Button",
    this.onPress,
    this.isEnabled = true,
    this.isLoading = false,
    this.leftIcon,
    this.width,
    this.size = ButtonSize.md,
    this.height,
  }) : super(key: key);

  final String title;
  final VoidCallback? onPress;
  final bool isEnabled;
  final bool isLoading;
  final Widget? leftIcon;
  final double? width;
  final double? height;
  final ButtonSize? size;

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(8),
      child: InkWell(
        splashColor: WispayColors.cPrimary,
        onTap: isEnabled ? onPress : null,
        borderRadius: BorderRadius.circular(8),
        child: Container(
          width: width,
          height: size == ButtonSize.md ? 40 : 30,
          padding: const EdgeInsets.symmetric(
            vertical: Spacing.medium - 2,
            horizontal: Spacing.defaultSpacing - 2,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: WispayColors.cWhite,
            border: Border.all(color: WispayColors.cPrimary),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (leftIcon != null) leftIcon!,
              if (leftIcon != null)
                const SizedBox(
                  width: Spacing.small,
                ),
              Center(
                child: !isLoading
                    ? CustomText(
                        title: title,
                        textStyle: const TextStyle(letterSpacing: 0.4),
                        size: FontSize.defaultSize,
                        color: WispayColors.cPrimary,
                        textType: TextType.Bold,
                      )
                    : const SpinKitThreeBounce(color: WispayColors.cPrimary, size: 15),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
