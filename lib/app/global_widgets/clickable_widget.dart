// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:wispay_outlet/app/core/theme/common_theme.dart';

class ClickableWidget extends StatelessWidget {
  const ClickableWidget({
    Key? key,
    required this.child,
    this.onTap,
    this.withShadow = true,
    this.borderRadius = 10,
    this.color = Colors.white,
  }) : super(key: key);

  final GestureTapCallback? onTap;
  final Widget child;
  final double borderRadius;
  final bool? withShadow;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(boxShadow: withShadow == true ? [buildShadow()] : null),
      child: Material(
        borderRadius: BorderRadius.circular(borderRadius),
        color: color,
        child: InkWell(
          borderRadius: BorderRadius.circular(borderRadius),
          splashFactory: InkRipple.splashFactory,
          onTap: onTap ?? () => {},
          child: child,
        ),
      ),
    );
  }
}
