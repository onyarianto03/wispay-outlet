// Dart imports:
import 'dart:async';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:pin_code_fields/pin_code_fields.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';

class PinInputWidget extends StatelessWidget {
  const PinInputWidget({
    Key? key,
    this.errorController,
    this.onComplete,
    this.controller,
    required this.onChange,
    this.focusNode,
    this.autoDisponseController = true,
  }) : super(key: key);

  final StreamController<ErrorAnimationType>? errorController;
  final ValueChanged<String>? onComplete;
  final ValueChanged<String> onChange;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final bool autoDisponseController;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: PinCodeTextField(
        autoFocus: true,
        onCompleted: onComplete,
        animationCurve: Curves.bounceIn,
        errorAnimationController: errorController,
        length: 6,
        onChanged: onChange,
        keyboardType: TextInputType.number,
        pinTheme: _pinTheme(),
        enableActiveFill: true,
        autoDismissKeyboard: true,
        showCursor: false,
        obscureText: true,
        obscuringWidget: _buildPin(),
        animationType: AnimationType.fade,
        appContext: context,
        controller: controller,
        autoDisposeControllers: autoDisponseController,
        focusNode: focusNode,
      ),
    );
  }

  PinTheme _pinTheme() {
    return PinTheme(
      shape: PinCodeFieldShape.circle,
      fieldHeight: 18,
      activeFillColor: WispayColors.cBlack333,
      inactiveColor: WispayColors.cDisabledColor,
      selectedColor: WispayColors.cDisabledColor,
      activeColor: WispayColors.cBlack333,
      inactiveFillColor: WispayColors.cDisabledColor,
      selectedFillColor: WispayColors.cDisabledColor,
    );
  }

  Container _buildPin() {
    return Container(
      width: 15,
      height: 15,
      decoration: BoxDecoration(
        color: WispayColors.cBlack333,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: WispayColors.cBlack333, width: 10),
      ),
    );
  }
}
