// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class InfoWidget extends StatelessWidget {
  const InfoWidget({
    Key? key,
    this.text,
    this.maxLines,
    this.margin,
    this.rightWidget = const SizedBox(),
    this.textWidget,
    this.withWidget = false,
  }) : super(key: key);

  final String? text;
  final int? maxLines;
  final EdgeInsetsGeometry? margin;
  final Widget rightWidget;
  final Widget? textWidget;
  final bool withWidget;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      width: double.infinity,
      padding: const EdgeInsets.symmetric(vertical: Spacing.small, horizontal: Spacing.defaultSpacing),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: WispayColors.cSecondary.withOpacity(0.2),
        image: DecorationImage(
          image: AssetImage(Assets.iconsInfoBg.path),
          alignment: Alignment.topLeft,
          fit: BoxFit.contain,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          withWidget
              ? Expanded(child: textWidget!)
              : Expanded(
                  child: CustomText(
                    title: text ?? "",
                    color: WispayColors.cBlack333,
                    size: FontSize.small,
                    maxLines: maxLines,
                  ),
                ),
          rightWidget,
        ],
      ),
    );
  }
}
