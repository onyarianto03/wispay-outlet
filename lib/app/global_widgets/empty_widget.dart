// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class EmptyWidget extends StatelessWidget {
  const EmptyWidget({
    Key? key,
    this.image,
    required this.title,
    required this.subTitle,
    this.imageWidth,
  }) : super(key: key);

  final String? image;
  final String title;
  final String subTitle;
  final double? imageWidth;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(image ?? Assets.imagesHistory.path, width: imageWidth ?? Get.width * 0.5),
        const SizedBox(height: Spacing.large),
        CustomText(
          title: title,
          textType: TextType.SemiBold,
          color: WispayColors.cBlack333,
          size: FontSize.medium,
        ),
        const SizedBox(height: Spacing.small),
        CustomText(
          title: subTitle,
          textAlign: TextAlign.center,
        )
      ],
    );
  }
}
