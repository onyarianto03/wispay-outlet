import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';

class ControllableInput extends StatefulWidget {
  const ControllableInput({
    Key? key,
    required this.value,
    this.onChangeValue,
  }) : super(key: key);

  final String value;
  final void Function(int)? onChangeValue;

  @override
  State<ControllableInput> createState() => _ControllableInputState();
}

class _ControllableInputState extends State<ControllableInput> {
  @override
  void initState() {
    _value = int.parse(widget.value);
    super.initState();
  }

  int _value = 0;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 40,
          child: CustomButton(
            isEnabled: _value > 1,
            title: '-',
            size: ButtonSize.sm,
            onPress: () {
              setState(() => _value--);
              widget.onChangeValue != null ? widget.onChangeValue!(_value) : null;
            },
          ),
        ),
        Container(
          width: 45,
          height: 30,
          margin: const EdgeInsets.symmetric(horizontal: Spacing.small),
          padding: const EdgeInsets.symmetric(vertical: Spacing.small),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: WispayColors.cDisabledColor,
          ),
          child: Center(
            child: CustomText(
              title: '$_value',
              textType: TextType.SemiBold,
            ),
          ),
        ),
        SizedBox(
          width: 40,
          child: CustomButton(
            title: '+',
            size: ButtonSize.sm,
            onPress: () {
              setState(() => _value++);
              widget.onChangeValue != null ? widget.onChangeValue!(_value) : null;
            },
          ),
        ),
      ],
    );
  }
}
