import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class CustomImage extends StatelessWidget {
  const CustomImage({
    Key? key,
    required this.source,
    this.width,
    this.height,
    this.borderRadius = Spacing.small,
    this.fit = BoxFit.cover,
    this.defaultImage,
  }) : super(key: key);

  final String source;
  final double? width;
  final double? height;
  final double borderRadius;
  final BoxFit fit;
  final Image? defaultImage;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(child: _buildImage(source), borderRadius: BorderRadius.circular(borderRadius));
  }

  Widget _buildImage(String souurce) {
    Image _defaultImage = defaultImage ?? Assets.iconsDefaultIcon.image(width: width, height: height);
    final bool isNetworkImage = source.startsWith('http');
    if (source.isNotEmpty && isNetworkImage) {
      return CachedNetworkImage(
        imageUrl: source,
        placeholder: (context, url) => _defaultImage,
        errorWidget: (context, url, error) => _defaultImage,
        width: width,
        height: height,
        fit: fit,
      );
    } else if (source.isNotEmpty && !isNetworkImage) {
      return Image.asset(
        source,
        width: width,
        height: height,
      );
    }

    return _defaultImage;
  }
}
