import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/middlewares/login_middleware.dart';

import '../core/middlewares/home_middleware.dart';
import '../modules/account/bindings/account_binding.dart';
import '../modules/account/views/account_view.dart';
import '../modules/account_verify/bindings/account_verify_binding.dart';
import '../modules/account_verify/upload_ktp/bindings/upload_ktp_binding.dart';
import '../modules/account_verify/upload_ktp/views/upload_ktp_view.dart';
import '../modules/account_verify/views/account_verify_view.dart';
import '../modules/auth/login/bindings/login_binding.dart';
import '../modules/auth/login/views/login_view.dart';
import '../modules/auth/onboarding/bindings/onboarding_binding.dart';
import '../modules/auth/onboarding/views/onboarding_view.dart';
import '../modules/auth/otp_verification/bindings/otp_verification_binding.dart';
import '../modules/auth/otp_verification/views/otp_verification_view.dart';
import '../modules/auth/pin_input/bindings/pin_input_binding.dart';
import '../modules/auth/pin_input/views/payment_view.dart';
import '../modules/auth/pin_input/views/pin_input_view.dart';
import '../modules/auth/register/bindings/register_binding.dart';
import '../modules/auth/register/register_pin/bindings/register_pin_binding.dart';
import '../modules/auth/register/register_pin/views/register_pin_view.dart';
import '../modules/auth/register/views/register_view_form.dart';
import '../modules/categories/bindings/categories_binding.dart';
import '../modules/categories/views/categories_view.dart';
import '../modules/cetak_bukti/bindings/cetak_bukti_binding.dart';
import '../modules/cetak_bukti/views/cetak_bukti_view.dart';
import '../modules/commision_setting/bindings/commision_setting_binding.dart';
import '../modules/commision_setting/views/commision_setting_view.dart';
import '../modules/data_outlet/bindings/data_outlet_binding.dart';
import '../modules/data_outlet/views/data_outlet_view.dart';
import '../modules/help/bindings/help_binding.dart';
import '../modules/help/views/article_list_view.dart';
import '../modules/help/views/article_view.dart';
import '../modules/help/views/help_center_view.dart';
import '../modules/help/views/help_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/tabs_view.dart';
import '../modules/inbox/bindings/inbox_binding.dart';
import '../modules/inbox/views/inbox_detail_view.dart';
import '../modules/inbox/views/inbox_view.dart';
import '../modules/my_point/bindings/my_point_binding.dart';
import '../modules/my_point/views/detail_voucher_view.dart';
import '../modules/my_point/views/history_point_view.dart';
import '../modules/my_point/views/my_point_view.dart';
import '../modules/my_point/views/my_voucher_view.dart';
import '../modules/my_point/views/set_address_view.dart';
import '../modules/products/BPJS/bindings/bpjs_binding.dart';
import '../modules/products/BPJS/views/bpjs_view.dart';
import '../modules/products/PLN/bindings/pln_binding.dart';
import '../modules/products/PLN/views/pln_view.dart';
import '../modules/products/asuransi_gadget/bindings/asuransi_gadget_binding.dart';
import '../modules/products/asuransi_gadget/views/asuransi_gadget_form_view.dart';
import '../modules/products/asuransi_gadget/views/asuransi_gadget_view.dart';
import '../modules/products/asuransi_gadget/views/confirm_view.dart';
import '../modules/products/asuransi_gadget/views/detail_premi_view.dart';
import '../modules/products/asuransi_gadget/views/select_gadget_view.dart';
import '../modules/products/asuransi_gadget/views/select_premi_view.dart';
import '../modules/products/e-wallet/bindings/e_wallet_binding.dart';
import '../modules/products/e-wallet/views/e_wallet_view.dart';
import '../modules/products/e-wallet/views/ewallet_form_view.dart';
import '../modules/products/e-wallet/views/ewallet_linkaja_view.dart';
import '../modules/products/emoney/bindings/emoney_binding.dart';
import '../modules/products/emoney/views/emoney_view.dart';
import '../modules/products/modem/bindings/modem_binding.dart';
import '../modules/products/modem/views/modem_view.dart';
import '../modules/products/multifinance/bindings/multifinance_binding.dart';
import '../modules/products/multifinance/views/multifinance_view.dart';
import '../modules/products/omni/bindings/omni_binding.dart';
import '../modules/products/omni/views/omni_view.dart';
import '../modules/products/pascabayar/bindings/pascabayar_binding.dart';
import '../modules/products/pascabayar/views/pascabayar_view.dart';
import '../modules/products/pdam/bindings/pdam_binding.dart';
import '../modules/products/pdam/views/pdam_view.dart';
import '../modules/products/physical_voucer/bindings/physical_voucer_binding.dart';
import '../modules/products/physical_voucer/views/physical_voucer_view.dart';
import '../modules/products/physical_voucer/views/physical_voucher_form.dart';
import '../modules/products/pulsa_data/bindings/pulsa_data_binding.dart';
import '../modules/products/pulsa_data/views/pulsa_data_view.dart';
import '../modules/products/sell_out/bindings/sell_out_binding.dart';
import '../modules/products/sell_out/views/sell_out_view.dart';
import '../modules/products/send_money/bindings/send_money_binding.dart';
import '../modules/products/send_money/views/confirm_bank_view.dart';
import '../modules/products/send_money/views/payment_instructions_view.dart';
import '../modules/products/send_money/views/payment_method_view.dart';
import '../modules/products/send_money/views/send_money_nominal_view.dart';
import '../modules/products/send_money/views/send_money_view.dart';
import '../modules/products/sim_card/bindings/sim_card_binding.dart';
import '../modules/products/sim_card/views/sim_card_view.dart';
import '../modules/products/streaming/bindings/streaming_binding.dart';
import '../modules/products/streaming/views/streaming_view.dart';
import '../modules/products/telkom/bindings/telkom_binding.dart';
import '../modules/products/telkom/views/telkom_indihome_view.dart';
import '../modules/products/telkom/views/telkom_telpon_view.dart';
import '../modules/products/telkom/views/telkom_view.dart';
import '../modules/products/voucher_game/bindings/voucher_game_binding.dart';
import '../modules/products/voucher_game/views/voucher_game_view.dart';
import '../modules/profile_account/bindings/profile_account_binding.dart';
import '../modules/profile_account/views/profile_account_view.dart';
import '../modules/promo/bindings/promo_binding.dart';
import '../modules/promo/views/promo_view.dart';
import '../modules/referral/bindings/referral_binding.dart';
import '../modules/referral/views/referral_view.dart';
import '../modules/saldo/bindings/saldo_binding.dart';
import '../modules/saldo/views/cara_bayar_topup_view.dart';
import '../modules/saldo/views/how_to_topup_view.dart';
import '../modules/saldo/views/request_topup_view.dart';
import '../modules/saldo/views/saldo_view.dart';
import '../modules/saldo/views/topup_history_view.dart';
import '../modules/sales/bindings/sales_binding.dart';
import '../modules/sales/views/sales_view.dart';
import '../modules/transaction/bindings/transaction_binding.dart';
import '../modules/transaction/bindings/transaction_detail_binding.dart';
import '../modules/transaction/views/transaction_detail_view.dart';
import '../modules/transaction/views/transaction_view.dart';
import '../modules/transfer/bindings/transfer_binding.dart';
import '../modules/transfer/views/transfer_form_view.dart';
import '../modules/transfer/views/transfer_status_view.dart';
import '../modules/transfer/views/transfer_view.dart';
import '../modules/waiting_payment/bindings/waiting_payment_binding.dart';
import '../modules/waiting_payment/views/waiting_payment_view.dart';

// Package imports:

// Project imports:

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const TabView(),
      binding: HomeBinding(),
      middlewares: [HomeMiddleware()],
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
      middlewares: [LoginMiddleware()],
    ),
    GetPage(
      name: _Paths.OTP_VERIFICATION,
      page: () => const OtpVerificationView(),
      binding: OtpVerificationBinding(),
    ),
    GetPage(
      name: _Paths.PIN_INPUT,
      page: () => const PinInputView(),
      binding: PinInputBinding(),
    ),
    GetPage(
      name: _Paths.CATEGORIES,
      page: () => const CategoriesView(),
      binding: CategoriesBinding(),
    ),
    GetPage(
      name: _Paths.TRANSACTION,
      page: () => TransactionView(),
      binding: TransactionBinding(),
    ),
    GetPage(
      name: _Paths.INBOX,
      page: () => const InboxView(),
      binding: InboxBinding(),
    ),
    GetPage(
      name: _Paths.INBOX_DETAIL,
      page: () => const InboxDetailView(),
      binding: InboxBinding(),
    ),
    GetPage(
      name: _Paths.ACCOUNT,
      page: () => const AccountView(),
      binding: AccountBinding(),
    ),
    GetPage(
      name: _Paths.SALDO,
      page: () => SaldoView(),
      binding: SaldoBinding(),
    ),
    GetPage(
      name: _Paths.HOW_TO_TOPUP,
      page: () => const HowToTopupView(),
      binding: SaldoBinding(),
    ),
    GetPage(
      name: _Paths.REQUEST_TOPUP,
      page: () => const RequestTopupView(),
      binding: SaldoBinding(),
    ),
    GetPage(
      name: _Paths.HOW_TO_PAY_TOPUP,
      page: () => const CaraBayarTopupView(),
      binding: SaldoBinding(),
    ),
    GetPage(
      name: _Paths.HISTORY_TOPUP,
      page: () => const TopupHistoryView(),
      binding: SaldoBinding(),
    ),
    GetPage(
      name: _Paths.PLN,
      page: () => const PlnView(),
      binding: PlnBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_TRANSACTION,
      page: () => const TransactionDetailView(),
      binding: TransactionDetailBinding(),
    ),
    GetPage(
      name: _Paths.COMMISION_SETTING,
      page: () => const CommisionSettingView(),
      binding: CommisionSettingBinding(),
    ),
    GetPage(
      name: _Paths.DATA_OUTLET,
      page: () => const DataOutletView(),
      binding: DataOutletBinding(),
    ),
    GetPage(
      name: _Paths.HELP,
      page: () => const HelpView(),
      binding: HelpBinding(),
    ),
    GetPage(
      name: _Paths.HELP_CENTER,
      page: () => const HelpCenterView(),
      binding: HelpBinding(),
    ),
    GetPage(
      name: _Paths.ARTICLE_LIST,
      page: () => const ArticleListView(),
      binding: HelpBinding(),
    ),
    GetPage(
      name: _Paths.ARTICLE,
      page: () => const ArticleView(),
      binding: HelpBinding(),
    ),
    GetPage(
      name: _Paths.MY_POINT,
      page: () => const MyPointView(),
      binding: MyPointBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_VOUCHER,
      page: () => const DetailVoucherView(),
      binding: MyPointBinding(),
    ),
    GetPage(
      name: _Paths.SET_DELIVERY_ADDRESS,
      page: () => const SetAddressView(),
      binding: MyPointBinding(),
    ),
    GetPage(
      name: _Paths.MY_VOUCHER,
      page: () => const MyVoucherView(),
      binding: MyPointBinding(),
    ),
    GetPage(
      name: _Paths.HISTORY_POINT,
      page: () => const HistoryPointView(),
      binding: MyPointBinding(),
    ),
    GetPage(
      name: _Paths.REFERRAL,
      page: () => const ReferralView(),
      binding: ReferralBinding(),
    ),
    GetPage(
      name: _Paths.ACCOUNT_VERIFY,
      page: () => const AccountVerifyView(),
      binding: AccountVerifyBinding(),
      children: [
        GetPage(
          name: _Paths.UPLOAD_KTP,
          page: () => const UploadKtpView(),
          binding: UploadKtpBinding(),
        ),
      ],
    ),
    GetPage(
      name: _Paths.TRANSFER,
      page: () => const TransferView(),
      binding: TransferBinding(),
    ),
    GetPage(
      name: _Paths.TRANSFER_FORM,
      page: () => const TransferFormView(),
      binding: TransferBinding(),
    ),
    GetPage(
      name: _Paths.TRANSFER_STATUS,
      page: () => const TransferStatusView(),
      binding: TransferBinding(),
    ),
    GetPage(
      name: _Paths.PAYMENT,
      page: () => const PaymentView(),
      binding: PinInputBinding(),
    ),
    GetPage(
      name: _Paths.REGISTER,
      page: () => const RegisterView(),
      binding: RegisterBinding(),
      children: [
        GetPage(
          name: _Paths.REGISTER_PIN,
          page: () => const RegisterPinView(),
          binding: RegisterPinBinding(),
        ),
      ],
    ),
    GetPage(
      name: _Paths.SALES,
      page: () => const SalesView(),
      binding: SalesBinding(),
    ),
    GetPage(
      name: _Paths.MULTIFINANCE,
      page: () => const MultifinanceView(),
      binding: MultifinanceBinding(),
    ),
    GetPage(
      name: _Paths.VOUCHER_GAME,
      page: () => const VoucherGameView(),
      binding: VoucherGameBinding(),
    ),
    GetPage(
      name: _Paths.EMONEY,
      page: () => const EmoneyView(),
      binding: EmoneyBinding(),
    ),
    GetPage(
      name: _Paths.PROFILE_ACCOUNT,
      page: () => const ProfileAccountView(),
      binding: ProfileAccountBinding(),
    ),
    GetPage(
      name: _Paths.PASCABAYAR,
      page: () => const PascabayarView(),
      binding: PascabayarBinding(),
    ),
    GetPage(
      name: _Paths.PHYSICAL_VOUCER,
      page: () => const PhysicalVoucerView(),
      binding: PhysicalVoucerBinding(),
      children: [
        GetPage(
          name: _Paths.PHYSICAL_VOUCER_FORM,
          page: () => const ProductList(),
          binding: PhysicalVoucerBinding(),
        ),
      ],
    ),
    GetPage(
      name: _Paths.PDAM,
      page: () => const PdamView(),
      binding: PdamBinding(),
    ),
    GetPage(
      name: _Paths.ASURANSI_GADGET,
      page: () => const AsuransiGadgetView(),
      binding: AsuransiGadgetBinding(),
      children: [
        GetPage(
          name: _Paths.ASURANSI_GADGET_SELECT_WIDGET,
          page: () => const SelectGadgetView(),
          binding: AsuransiGadgetBinding(),
        ),
        GetPage(
          name: _Paths.ASURANSI_GADGET_FORM,
          page: () => const AsuransiGadgetFormView(),
          binding: AsuransiGadgetBinding(),
        ),
        GetPage(
          name: _Paths.ASURANSI_GADGET_PREMI,
          page: () => const SelectPremiView(),
          binding: AsuransiGadgetBinding(),
        ),
        GetPage(
          name: _Paths.ASURANSI_GADGET_DETAIL_PREMI,
          page: () => const DetailPremiView(),
          binding: AsuransiGadgetBinding(),
        ),
        GetPage(
          name: _Paths.ASURANSI_GADGET_CONFIRM,
          page: () => const ConfirmView(),
          binding: AsuransiGadgetBinding(),
        ),
      ],
    ),
    GetPage(
      name: _Paths.PULSA_DATA,
      page: () => const PulsaDataView(),
      binding: PulsaDataBinding(),
    ),
    GetPage(
      name: _Paths.BPJS,
      page: () => const BpjsView(),
      binding: BpjsBinding(),
    ),
    GetPage(
      name: _Paths.MODEM,
      page: () => const ModemView(),
      binding: ModemBinding(),
    ),
    GetPage(
      name: _Paths.SIM_CARD,
      page: () => const SimCardView(),
      binding: SimCardBinding(),
    ),
    GetPage(
      name: _Paths.TELKOM,
      page: () => const TelkomView(),
      binding: TelkomBinding(),
    ),
    GetPage(
      name: _Paths.TELKOM_INDIHOME,
      page: () => const TelkomIndihomeView(),
      binding: TelkomBinding(),
    ),
    GetPage(
      name: _Paths.TELKOM_TELPON,
      page: () => const TelkomTelponView(),
      binding: TelkomBinding(),
    ),
    GetPage(
      name: _Paths.STREAMING,
      page: () => const StreamingView(),
      binding: StreamingBinding(),
    ),
    GetPage(
      name: _Paths.SEND_MONEY,
      page: () => const SendMoneyView(),
      binding: SendMoneyBinding(),
    ),
    GetPage(
      name: _Paths.SEND_MONEY_NOMINAL,
      page: () => const SendMoneyNominalView(),
      binding: SendMoneyBinding(),
    ),
    GetPage(
      name: _Paths.SEND_MONEY_PAYMENT_METHOD,
      page: () => const PaymentMethodView(),
      binding: SendMoneyBinding(),
    ),
    GetPage(
      name: _Paths.SEND_MONEY_CONFIRM_BANK,
      page: () => const ConfirmBankView(),
      binding: SendMoneyBinding(),
    ),
    GetPage(
      name: _Paths.SEND_MONEY_PAYMENT_INSTRUCTIONS,
      page: () => const PaymentInstructionsView(),
      binding: SendMoneyBinding(),
    ),
    GetPage(
      name: _Paths.OMNI,
      page: () => const OmniView(),
      binding: OmniBinding(),
    ),
    GetPage(
      name: _Paths.SELL_OUT,
      page: () => const SellOutView(),
      binding: SellOutBinding(),
    ),
    GetPage(
      name: _Paths.E_WALLET,
      page: () => const EWalletView(),
      binding: EWalletBinding(),
      children: [
        GetPage(
          name: _Paths.E_WALLET_LINKAJA,
          page: () => const EwalletLinkajaView(),
          binding: EWalletBinding(),
        ),
        GetPage(
          name: _Paths.E_WALLET_FORM,
          page: () => const EwalletFormView(),
          binding: EWalletBinding(),
        ),
      ],
    ),
    GetPage(
      name: _Paths.CETAK_BUKTI,
      page: () => const CetakBuktiView(),
      binding: CetakBuktiBinding(),
    ),
    GetPage(
      name: _Paths.PROMO,
      page: () => const PromoView(),
      binding: PromoBinding(),
    ),
    GetPage(
      name: _Paths.WAITING_PAYMENT,
      page: () => const WaitingPaymentView(),
      binding: WaitingPaymentBinding(),
    ),
    GetPage(
      name: _Paths.ONBOARDING,
      page: () => const OnboardingView(),
      binding: OnboardingBinding(),
    ),
  ];
}
