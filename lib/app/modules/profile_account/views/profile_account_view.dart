// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import '../controllers/profile_account_controller.dart';

class ProfileAccountView extends GetView<ProfileAccountController> {
  const ProfileAccountView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Data Akun'),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(Icons.chevron_left, size: 35),
            onPressed: () => Get.back(),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
          child: Obx(
            () => Form(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              key: controller.getFormKey,
              child: Stack(
                children: [
                  ListView(
                    reverse: true,
                    shrinkWrap: true,
                    children: [
                      const SizedBox(height: Spacing.large),
                      _avatar(controller.homeController),
                      const SizedBox(height: Spacing.large),
                      CustomTextInput(
                        value: '',
                        label: 'Nama Lengkap',
                        isEnabled: false,
                        hintText: controller.homeController.profile.value.name ?? '',
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      CustomTextInput(
                        value: '',
                        label: 'Nomor Hp',
                        isEnabled: false,
                        hintText: decrypt(controller.homeController.profile.value.phone),
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      CustomTextInput(
                        label: 'Alamat Email',
                        value: controller.email.value,
                        textEditingController: controller.tController,
                        validator: controller.emailValidator,
                        onClear: controller.onClear,
                        hintText: 'email',
                        isEnabled: !controller.isLoading.value,
                      ),
                      const SizedBox(height: Spacing.large * 3),
                    ].reversed.toList(),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
                      child: CustomButton(
                        title: 'Simpan Perubahan',
                        onPress: controller.onSave,
                        isLoading: controller.isLoading.value,
                        isEnabled: (controller.email.value != decrypt(controller.homeController.profile.value.email) ||
                                controller.picker.isPicked.value) &&
                            controller.email.value != '' &&
                            !controller.isLoading.value,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _avatar(CategoriesController c) {
    return Row(
      children: [
        Obx(
          () => ClipOval(
            child: controller.picker.isPicked.value
                ? Image.file(
                    controller.picker.imageFile.value,
                    width: 40,
                    height: 40,
                    fit: BoxFit.cover,
                  )
                : CustomImage(
                    source: c.profile.value.avatar?.url ?? "",
                    defaultImage: Assets.iconsProfilePlaceholder.image(width: 40, height: 40),
                    width: 40,
                    height: 40,
                  ),
          ),
        ),
        const SizedBox(width: Spacing.medium),
        InkWell(
          onTap: () => controller.picker.showUploadBottomsheet(),
          child: const CustomText(
            title: 'Tambah foto akun',
            color: WispayColors.cPrimary,
            decoration: TextDecoration.underline,
          ),
        )
      ],
    );
  }
}
