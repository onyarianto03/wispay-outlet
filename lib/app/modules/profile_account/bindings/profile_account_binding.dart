// Package imports:
import 'package:get/get.dart';

// Project imports:
import '../controllers/profile_account_controller.dart';

class ProfileAccountBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ProfileAccountController>(
      () => ProfileAccountController(),
    );
  }
}
