// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:form_field_validator/form_field_validator.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/controllers/image_picker_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';

class ProfileAccountController extends GetxController {
  ImagePickerController picker = Get.find();
  TextEditingController tController = TextEditingController();
  CategoriesController homeController = Get.find();
  final AccountRepository _accountRepository = AccountRepository(Get.find());
  final _formKey = GlobalKey<FormState>();

  final email = ''.obs;
  final isLoading = false.obs;
  get getFormKey => _formKey;

  final emailValidator = MultiValidator([
    RequiredValidator(errorText: 'Email wajib diisi'),
    EmailValidator(errorText: 'Format email tidak valid'),
  ]);

  @override
  void onInit() {
    tController.text = decrypt(homeController.profile.value.email);
    email.value = decrypt(homeController.profile.value.email);
    tController.addListener(onChangeEmail);
    super.onInit();
  }

  @override
  void onClose() {}
  void onChangeEmail() {
    email.value = tController.text;
  }

  void onClear() => tController.clear();

  void onSave() async {
    bool isValid = _formKey.currentState!.validate();
    FocusManager.instance.primaryFocus?.unfocus();

    if (isValid) {
      var _data = {
        "email": encrypt(email.value),
      };
      isLoading.value = true;
      if (email.value != decrypt(homeController.profile.value.email)) {
        await _accountRepository.updateProfile(data: RequestBodyModel.fromJson(_data));
      }
      if (picker.isPicked.value) {
        await _accountRepository.updateAvatar(filePath: picker.imageFile.value.path);
      }
      homeController.getProfile().then((val) {
        isLoading.value = false;
        Get.back();
      });
    }
  }
}
