// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/transfer/controllers/transfer_form_controller.dart';
import 'package:wispay_outlet/app/modules/transfer/widgets/status_card.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class TransferStatusView extends GetView<TransferFormController> {
  const TransferStatusView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(''),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.close_sharp),
          onPressed: () => Get.offAllNamed(Routes.HOME),
        ),
      ),
      body: SafeArea(
        child: Container(
          color: WispayColors.cWhite,
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.symmetric(
            horizontal: Spacing.defaultSpacing,
            vertical: Spacing.large,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const CustomText(
                title: 'Pengiriman Berhasil!',
                size: FontSize.medium,
                textType: TextType.Bold,
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: Spacing.defaultSpacing,
              ),
              Image.asset(
                Assets.iconsTrxStatusSuccess.path,
                width: MediaQuery.of(context).size.width * 0.3,
                height: MediaQuery.of(context).size.width * 0.3,
                fit: BoxFit.cover,
              ),
              const SizedBox(
                height: Spacing.defaultSpacing,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const CustomText(
                    title: 'Saldo sebesar  ',
                    textAlign: TextAlign.center,
                    color: WispayColors.cBlack,
                  ),
                  CustomText(
                    title: NumberHelper.formatMoney(controller.nominal.value.replaceAll('.', '')),
                    textAlign: TextAlign.center,
                    textType: TextType.SemiBold,
                    color: WispayColors.cBlack,
                  ),
                  const CustomText(
                    title: ' berhasil dikirim!',
                    textAlign: TextAlign.center,
                    color: WispayColors.cBlack,
                  ),
                ],
              ),
              const SizedBox(height: Spacing.large),
              TransferStatusCard(
                phone: controller.phone.value,
                nominal: controller.nominal.value,
              ),
              const Spacer(),
              CustomButton(
                title: 'Kembali',
                onPress: () => Get.offAllNamed(Routes.HOME),
              ),
              const SizedBox(height: Spacing.defaultSpacing),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  CustomText(title: 'Butuh Bantuan? '),
                  CustomText(
                    title: 'Klik Di sini',
                    color: WispayColors.cPrimary,
                    decoration: TextDecoration.underline,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
