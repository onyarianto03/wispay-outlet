// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/utils/input_price_formatter.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/modules/transfer/controllers/transfer_form_controller.dart';
import 'package:wispay_outlet/app/modules/transfer/widgets/transfer_form_header_widget.dart';

class TransferFormView extends GetView<TransferFormController> {
  const TransferFormView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(controller.getPageTitle()),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(Icons.chevron_left, size: 35),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: SafeArea(
          child: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: Spacing.defaultSpacing,
              vertical: Spacing.large,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const TransferFormHeader(),
                const SizedBox(height: Spacing.large),
                Expanded(
                  child: Obx(
                    () => Column(
                      children: [
                        Form(
                          key: controller.phoneKey,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: _input(
                            title: 'Transfer ke:',
                            value: controller.phone.value,
                            hintText: 'Masukkan Nomor HP',
                            c: controller.phoneController,
                            keyboardType: TextInputType.phone,
                            validator: (_) => controller.errorPhone.value,
                            onContactPicked: (value) => controller.onPickedContact(value),
                            showContact: true,
                            helperText: controller.outletReceiver.value != null
                                ? 'Penerima : ${controller.outletReceiver.value?.name}'
                                : '',
                          ),
                        ),
                        Form(
                          key: controller.nominalKey,
                          child: _input(
                            title: 'Nominal:',
                            value: controller.nominal.value,
                            hintText: 'Masukkan Nominal',
                            c: controller.nominalController,
                            keyboardType: TextInputType.number,
                            validator: controller.nominalValidator,
                            formatter: [InputPriceFormatter()],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                CustomButton(
                  title: 'Lanjutkan',
                  onPress: () {
                    FocusManager.instance.primaryFocus?.unfocus();
                    controller.submit();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _input({
    required String title,
    required String value,
    String? hintText,
    required TextEditingController c,
    TextInputType? keyboardType,
    required String? Function(String?)? validator,
    bool showContact = false,
    List<TextInputFormatter>? formatter,
    String? helperText,
    bool autofocus = false,
    ValueChanged<String?>? onContactPicked,
  }) {
    return CustomTextInput(
      label: title,
      value: value,
      helperText: helperText,
      onContactPicked: onContactPicked,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: validator,
      hintText: hintText!,
      isEnabled: !controller.isSubmitting.value,
      onClear: c.clear,
      autofocus: autofocus,
      textEditingController: c,
      keyboardType: keyboardType,
      showContact: showContact,
      inputFormatters: formatter,
      helperTextStyle: const TextStyle(color: WispayColors.cSuccess, fontWeight: FontWeight.w600),
      margin: const EdgeInsets.only(bottom: Spacing.xxSmall),
    );
  }
}
