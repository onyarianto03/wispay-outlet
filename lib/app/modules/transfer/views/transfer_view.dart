// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/card_menu_widget.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import '../controllers/transfer_controller.dart';

class TransferView extends GetView<TransferController> {
  const TransferView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Transfer Saldo'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: Spacing.defaultSpacing,
            vertical: Spacing.large,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CustomText(
                title: 'Transfer ke:',
                textType: TextType.SemiBold,
                margin: EdgeInsets.only(bottom: Spacing.defaultSpacing),
              ),
              CardMenuWidget(menu: kTransferMenuOutlet),
              const SizedBox(height: Spacing.defaultSpacing),
              CardMenuWidget(menu: kTransferMenuCustomer),
            ],
          ),
        ),
      ),
    );
  }
}
