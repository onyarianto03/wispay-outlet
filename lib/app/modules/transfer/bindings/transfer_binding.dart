// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/modules/transfer/controllers/transfer_form_controller.dart';
import 'package:wispay_outlet/app/modules/transfer/controllers/transfer_status_controller.dart';
import '../controllers/transfer_controller.dart';

class TransferBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TransferStatusController>(
      () => TransferStatusController(),
    );
    Get.lazyPut<TransferFormController>(
      () => TransferFormController(),
    );
    Get.lazyPut<TransferController>(
      () => TransferController(),
    );
  }
}
