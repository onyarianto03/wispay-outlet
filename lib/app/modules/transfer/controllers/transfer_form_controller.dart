// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';

// Project imports:
// ignore: unused_import
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/data/models/profile/profile_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/data/repositories/wallet_repository.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transfer/widgets/confirm_modal.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class TransferFormController extends GetxController {
  late TextEditingController phoneController;
  late TextEditingController nominalController;

  final CategoriesController _categoriesController = Get.find();

  final String? target = Get.arguments?.nextScreenTitle;
  final WalletRepository _walletRepository = WalletRepository(Get.find());
  final phoneKey = GlobalKey<FormState>();
  final nominalKey = GlobalKey<FormState>();

  var phone = ''.obs;
  var nominal = ''.obs;
  var isSubmitting = false.obs;
  final outletReceiver = Rxn<ProfileModel>();
  final transferData = const TransactionDataModel().obs;

  final isEnableButton = false.obs;
  final errorPhone = Rxn<String>('Nomor HP Tujuan wajib diisi');

  final phoneValidator = RequiredValidator(errorText: 'Nomor HP Tujuan wajib diisi');
  final nominalValidator = RequiredValidator(errorText: 'Nominal wajib diisi');

  @override
  void onInit() {
    super.onInit();
    phoneController = TextEditingController()..addListener(onChangePhone);
    nominalController = TextEditingController()..addListener(onChangeNominal);

    debounce<String>(phone, (value) {
      if (phoneValidator.isValid(value)) {
        _getOutletByPhoneNumber();
      }
    }, time: const Duration(milliseconds: 500));
  }

  void onPickedContact(String? phone) {
    if (phone != null) {
      phoneController.text = phone;
    }
  }

  @override
  void onClose() {
    super.onClose();
    phoneController.removeListener(onChangePhone);
    nominalController.removeListener(onChangeNominal);
  }

  void onChangePhone() {
    errorPhone.value = phoneValidator.call(phoneController.text);
    phone.value = phoneController.text;
    outletReceiver.value = null;
  }

  void onChangeNominal() {
    nominal.value = nominalController.text;
  }

  void _transferToOutlet(pin) async {
    final _data = RequestBodyModel(
      amount: nominal.value.replaceAll('.', ''),
      to: encrypt(NumberHelper.formatPhoneToIndonesian(phone.value)),
    );
    final _response = await _walletRepository.transferToOutlet(data: _data, pin: pin);
    if (_response.success != false) {
      WispayDialog.closeDialog();
      transferData.value = _response.data!;
      _gotoStatus();
    } else {
      WispaySnackbar.showError(_response.message!, onClose: () => WispayDialog.closeDialog());
    }
  }

  void _transferToPartner(pin) async {
    final _data = RequestBodyModel(
      amount: nominal.value.replaceAll('.', ''),
      phone: encrypt(NumberHelper.formatPhoneToIndonesian(phone.value)),
    );
    final _response = await _walletRepository.transferToPartner(data: _data, pin: pin);
    if (_response.success != false) {
      WispayDialog.closeDialog();
      transferData.value = _response.data!;
      _gotoStatus();
    } else {
      WispaySnackbar.showError(_response.message!, onClose: () => WispayDialog.closeDialog());
    }
  }

  void _gotoStatus() {
    Get.offNamed(Routes.TRANSFER_STATUS);
  }

  void _next(String pin) async {
    WispayDialog.showLoading();
    if (target == 'PARTNER') {
      _transferToPartner(pin);
    } else {
      _transferToOutlet(pin);
    }
  }

  onConfirm() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'isPayment': true, 'phone': _categoriesController.profile.value.phone},
    );
  }

  submit() async {
    phoneKey.currentState!.validate();
    final isValid = nominalKey.currentState!.validate() && phoneKey.currentState!.validate();
    if (isValid) {
      WispayBottomSheet.show(title: 'Transfer Saldo', children: [
        TransferConfirmModal(
          phoneValue: phone.value,
          nominalValue: nominal.value,
          receiverName: outletReceiver.value?.name ?? "",
          onConfirm: onConfirm,
        ),
      ]);
    }
  }

  void _getOutletByPhoneNumber() async {
    if (target == 'PARTNER') {
      _inquiryPartner();
      return;
    } else {
      _inquiryOutlet();
      return;
    }
  }

  void _inquiryPartner() async {
    final _data = RequestBodyModel(
      amount: nominal.value.replaceAll('.', ''),
      phone: encrypt(NumberHelper.formatPhoneToIndonesian(phone.value)),
    );
    final _response = await _walletRepository.inquiryTransferPartner(data: _data);
    if (_response.success == false) {
      errorPhone.value = 'Nomor pelanggan tidak ditemukan';
      isEnableButton.value = false;
      phoneKey.currentState!.validate();
    } else {
      outletReceiver.value = _response.data!;
      isEnableButton.value = true;
    }
  }

  void _inquiryOutlet() async {
    final _data = RequestBodyModel(phone: encrypt(NumberHelper.formatPhoneToIndonesian(phone.value)));
    final _res = await _walletRepository.getOutletByPhone(data: _data);

    if (_res.success == false) {
      errorPhone.value = 'Nomor handphone tidak ditemukan';
      isEnableButton.value = false;
      phoneKey.currentState!.validate();
    } else {
      outletReceiver.value = _res.data!;
      isEnableButton.value = true;
    }
  }

  String getPageTitle() {
    String result = 'Transfer ke Outlet';
    if (target == 'PARTNER') {
      result = 'Transfer ke Pelanggan';
    }

    return result;
  }
}
