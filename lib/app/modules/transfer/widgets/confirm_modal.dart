// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get_utils/src/extensions/widget_extensions.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/number_helper.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';

class TransferConfirmModal extends StatelessWidget {
  const TransferConfirmModal({
    Key? key,
    required this.phoneValue,
    required this.nominalValue,
    required this.onConfirm,
    required this.receiverName,
  }) : super(key: key);

  final String phoneValue;
  final String nominalValue;
  final String receiverName;
  final void Function() onConfirm;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        TrxDetailItem(title: 'Penerima', value: receiverName),
        TrxDetailItem(title: 'Nomor HP', value: phoneValue),
        TrxDetailItem(title: 'Nominal', value: NumberHelper.formatMoney(nominalValue.replaceAll('.', ''))),
        const Divider(thickness: 1).marginOnly(top: Spacing.small, bottom: Spacing.small),
        CustomButton(title: 'Transfer', onPress: onConfirm),
        const SizedBox(height: Spacing.medium),
      ],
    );
  }
}
