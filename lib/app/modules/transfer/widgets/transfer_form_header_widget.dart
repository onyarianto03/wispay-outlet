// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/number_helper.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class TransferFormHeader extends GetView<CategoriesController> {
  const TransferFormHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseCard(
      margin: EdgeInsets.zero,
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      decorationImage: Assets.iconsMiscWave1,
      spreadRadius: 0,
      blurRadius: 10,
      child: Column(
        children: [
          const CustomText(title: 'Sisa Saldo', size: FontSize.small),
          const SizedBox(height: Spacing.small),
          Obx(
            () => CustomText(
              title: NumberHelper.formatMoney(controller.profile.value.outletWallet?.total),
              size: FontSize.xLarge,
              color: WispayColors.cPrimary,
              textType: TextType.SemiBold,
            ),
          ),
        ],
      ),
    );
  }
}
