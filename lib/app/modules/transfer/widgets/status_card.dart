// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/transfer/controllers/transfer_form_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class TransferStatusCard extends GetView<TransferFormController> {
  const TransferStatusCard({
    Key? key,
    required this.phone,
    required this.nominal,
  }) : super(key: key);

  final String phone;
  final String nominal;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      decoration: BoxDecoration(
        boxShadow: const [shadow],
        color: Colors.white,
        image: DecorationImage(
          image: AssetImage(Assets.iconsMiscWave2.path),
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.circular(10.r),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Obx(() => _infoTransfer(title: 'Nomor Transaksi', value: controller.transferData.value.code ?? "")),
          _infoTransfer(
            title: 'Tanggal',
            value: DateHelper.formatDate(controller.transferData.value.createdAt, format: 'DD MMM yyyy HH:mm:ss'),
          ),
          _infoTransfer(title: 'Nama Penerima', value: controller.outletReceiver.value?.name ?? ""),
          _infoTransfer(title: 'Nomor HP', value: phone),
          _infoTransfer(
            title: 'Saldo Terkirim',
            boldTitle: true,
            value: NumberHelper.formatMoney(nominal.replaceAll('.', '')),
            marginBottom: 0,
          ),
        ],
      ),
    );
  }

  Widget _infoTransfer({
    required String title,
    required String value,
    bool boldTitle = false,
    double marginBottom = Spacing.defaultSpacing,
  }) {
    return Row(
      children: [
        CustomText(
          title: title,
          textType: boldTitle ? TextType.SemiBold : TextType.Regular,
          color: boldTitle ? WispayColors.cBlack : WispayColors.cBlack666,
        ),
        const Spacer(),
        CustomText(
          title: value,
          textType: TextType.SemiBold,
          color: WispayColors.cBlack,
        ),
      ],
    ).marginOnly(bottom: marginBottom);
  }
}
