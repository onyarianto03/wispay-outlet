import 'package:get/get.dart';

import 'package:wispay_outlet/app/modules/saldo/controllers/request_topup_controller.dart';
import 'package:wispay_outlet/app/modules/saldo/controllers/topup_history_controller.dart';

import '../controllers/saldo_controller.dart';

// Package imports:

// Project imports:

class SaldoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TopupHistoryController>(
      () => TopupHistoryController(),
    );
    Get.lazyPut<RequestTopupController>(
      () => RequestTopupController(),
    );
    Get.lazyPut<SaldoController>(
      () => SaldoController(),
    );
  }
}
