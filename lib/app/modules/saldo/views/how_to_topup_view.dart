import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/timeline.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class HowToTopupView extends GetView {
  const HowToTopupView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cara Top Up'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.asset(
                        Assets.imagesCaraTopup.path,
                        width: MediaQuery.of(context).size.width,
                        fit: BoxFit.fill,
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      const CustomText(
                        title: 'Cara Top-Up Saldo:',
                        size: FontSize.medium,
                        textType: TextType.Bold,
                        color: WispayColors.cBlack,
                      ),
                      Timeline(
                        lineColor: WispayColors.cPrimary,
                        itemGap: 20,
                        lineGap: 0,
                        indicatorSize: 24,
                        gutterSpacing: 16,
                        children: [
                          _timeline('Hubungi agen sales WisPay di kotamu.'),
                          _timeline('Masukkan nominal saldo yang ingin ditop-up.'),
                          _timeline('Kirim permintaan top-up saldo.'),
                          _timeline('Bayar secara tunai sesuai jumlah saldo yang diterima.'),
                          _timeline('Untuk memastikan saldo bertambah, cek di menu Saldo Saya.'),
                        ],
                        indicators: _indicator(5),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: Spacing.defaultSpacing),
              CustomButton(
                title: 'Buat Permintaan Top Up',
                onPress: () => Get.toNamed(Routes.REQUEST_TOPUP),
              )
            ],
          ),
        ),
      ),
    );
  }

  // I don't know why, but I can't use CustomText in Timeline
  Text _timeline(String title) {
    return Text(
      title,
      style: const TextStyle(
        fontSize: FontSize.defaultSize,
      ),
    );
  }

  List<Widget> _indicator(int total) {
    List<Widget> indicators = [];

    for (int i = 0; i < total; i++) {
      indicators.add(Container(
        alignment: Alignment.center,
        width: 24,
        height: 24,
        decoration: const BoxDecoration(
          color: WispayColors.cPrimary,
          shape: BoxShape.circle,
        ),
        child: CustomText(
          title: '${i + 1}',
          color: WispayColors.cWhite,
          size: FontSize.defaultSize,
        ),
      ));
    }

    return indicators;
  }
}
