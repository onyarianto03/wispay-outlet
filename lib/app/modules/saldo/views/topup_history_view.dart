import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/empty_widget.dart';
import 'package:wispay_outlet/app/global_widgets/top_tab_bar.dart';
import 'package:wispay_outlet/app/modules/saldo/controllers/topup_history_controller.dart';
import 'package:wispay_outlet/app/modules/saldo/widget/card_history_topup.dart';

import '../widget/skeleton_widget.dart';

class TopupHistoryView extends GetView<TopupHistoryController> {
  const TopupHistoryView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Permintaan Top-Up'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: DefaultTabController(
          length: 2,
          child: Column(
            children: [
              Container(
                color: WispayColors.cWhite,
                child: TopTapBar(
                  controller: controller.tabController,
                  tabs: const [
                    Tab(text: 'Saat Ini'),
                    Tab(text: 'Selesai'),
                  ],
                ),
              ),
              Expanded(
                child: TabBarView(
                  controller: controller.tabController,
                  children: [
                    _buildTabView(),
                    _buildTabView(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _buildTabView() {
    return Obx(() {
      final isEmptyData = controller.historyTopUp.isEmpty && !controller.isLoading.value;

      return RefreshIndicator(
        onRefresh: () => controller.onRefresh(),
        child: isEmptyData
            ? const EmptyWidget(
                title: 'Belum Ada Riwayat',
                subTitle: 'Silakan hubungi agen sales kotamu untuk top-up saldo dan mulai berjualan.',
              )
            : ListView.separated(
                controller: controller.scrollController,
                padding: const EdgeInsets.symmetric(vertical: Spacing.defaultSpacing),
                itemCount: controller.isLoading.value ? 5 : controller.historyTopUp.length,
                separatorBuilder: (context, index) => const SizedBox(height: Spacing.defaultSpacing),
                itemBuilder: (context, index) {
                  if (controller.isLoading.value) {
                    return const Padding(
                      padding: EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                      child: SkeletonWidget(),
                    );
                  }
                  return CardHistoryTopUp(item: controller.historyTopUp[index]);
                },
              ),
      );
    });
  }
}
