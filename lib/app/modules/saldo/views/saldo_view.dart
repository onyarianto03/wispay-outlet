// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/filter_date_controller.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/empty_widget.dart';
import 'package:wispay_outlet/app/global_widgets/rounded_tab_bar.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/app/views/filter_date_view.dart';
import '../controllers/saldo_controller.dart';
import '../widget/skeleton_widget.dart';
import '../widget/card_saldo.dart';
import '../widget/saldo_header.dart';

class SaldoView extends GetView<SaldoController> {
  SaldoView({Key? key}) : super(key: key);

  final FilterDateController fc = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF7F9FA),
      appBar: AppBar(
        title: const Text('Saldo Saya'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.chevron_left, size: 35),
          onPressed: () {
            Get.back();
          },
        ),
        actions: [
          Center(
            child: InkWell(
              onTap: () => Get.toNamed(Routes.HOW_TO_TOPUP),
              child: const CustomText(
                title: 'Cara Top-Up',
                margin: EdgeInsets.only(right: Spacing.defaultSpacing),
                decoration: TextDecoration.underline,
                color: WispayColors.cPrimary,
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: Obx(
        () => Visibility(
          child: FloatingActionButton(
            mini: true,
            backgroundColor: WispayColors.cPrimary,
            onPressed: () => controller.backToTop(),
            child: const Icon(Icons.arrow_upward),
          ),
          visible: controller.showFloatingButton.value,
        ),
      ),
      body: Obx(
        () {
          final isEmptyData = controller.listHistory.isEmpty && !controller.isLoading.value;
          return NotificationListener(
            onNotification: (notification) {
              if (notification is ScrollEndNotification) {
                if (notification.metrics.pixels == notification.metrics.maxScrollExtent) {
                  controller.getMoreHistory();
                  return true;
                }
              }
              return true;
            },
            child: NestedScrollView(
              controller: controller.scrollController,
              headerSliverBuilder: (_, __) {
                return [
                  SliverToBoxAdapter(
                    child: Container(
                      color: Colors.white,
                      padding: const EdgeInsets.all(Spacing.defaultSpacing),
                      child: SaldoHeader(saldo: controller.home.profile.value.outletWallet?.total ?? '0'),
                    ),
                  ),
                  const SliverToBoxAdapter(child: SizedBox(height: Spacing.defaultSpacing)),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                      child: FilterDate(onChangeDate: (value) => controller.handleSelectDate(value)),
                    ),
                  ),
                  const SliverToBoxAdapter(child: SizedBox(height: Spacing.defaultSpacing)),
                  if (!isEmptyData || controller.isLoading.value == true)
                    SliverToBoxAdapter(child: _buildButtonTabBar())
                ];
              },
              body: isEmptyData
                  ? const Padding(
                      padding: EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                      child: EmptyWidget(
                        title: 'Belum Ada Riwayat',
                        subTitle: 'Silakan hubungi agen sales kotamu untuk top-up saldo dan mulai berjualan.',
                      ),
                    )
                  : TabBarView(
                      controller: controller.tabController,
                      children: [
                        _listItem(),
                        _listItem(),
                        _listItem(),
                      ],
                    ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildButtonTabBar() {
    return RoundedTabBar(
      tabs: const [
        Tab(text: 'Semua'),
        Tab(text: 'Pembayaran'),
        Tab(text: 'Penerimaan'),
      ],
      controller: controller.tabController,
    );
  }

  _listItem() {
    final data = controller.listHistory;
    return Obx(
      () => ListView.separated(
        physics: const NeverScrollableScrollPhysics(),
        padding: const EdgeInsets.all(Spacing.defaultSpacing),
        itemCount: controller.isLoading.value ? 5 : data.length,
        separatorBuilder: (ctx, idx) => const SizedBox(height: Spacing.small),
        itemBuilder: (ctx, idx) {
          if (controller.isLoading.value) {
            return const SkeletonWidget();
          }
          return CardSaldo(
            saldoItem: data[idx],
          );
        },
      ),
    );
  }
}
