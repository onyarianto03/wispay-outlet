import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/utils/input_price_formatter.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/top_tab_bar.dart';
import 'package:wispay_outlet/app/modules/saldo/controllers/request_topup_controller.dart';
import 'package:wispay_outlet/app/modules/saldo/widget/bank_provider_dropdown.dart';
import 'package:wispay_outlet/app/modules/saldo/widget/sales_card.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class RequestTopupView extends GetView<RequestTopupController> {
  const RequestTopupView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Top-Up Saldo Saya'),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.chevron_left,
              size: 35,
            ),
            onPressed: () {
              Get.back();
            },
          ),
          actions: [
            Center(
              child: IconButton(
                icon: const Icon(
                  Icons.history,
                  size: 24,
                ),
                onPressed: () => Get.toNamed(Routes.HISTORY_TOPUP),
              ),
            ),
            const SizedBox(width: Spacing.small),
          ],
        ),
        body: SafeArea(
          child: DefaultTabController(
            length: 2,
            child: Column(
              children: [
                Container(
                  color: WispayColors.cWhite,
                  child: TopTapBar(
                    controller: controller.tabController,
                    tabs: const [
                      Tab(text: 'Via Sales'),
                      Tab(text: 'Transfer Bank'),
                    ],
                  ),
                ),
                Expanded(
                  child: TabBarView(
                    controller: controller.tabController,
                    children: [
                      Obx(
                        () => Form(
                          autovalidateMode: AutovalidateMode.disabled,
                          key: controller.formSalesKey,
                          child: Padding(
                            padding: const EdgeInsets.all(Spacing.defaultSpacing),
                            child: Column(
                              children: [
                                SalesCard(
                                  name: controller.catController.profile.value.upline?.name ?? "",
                                  phoneNumber: decrypt(controller.catController.profile.value.upline?.phone),
                                  image: controller.catController.profile.value.upline?.avatar!.url ?? "",
                                ),
                                const SizedBox(height: Spacing.defaultSpacing),
                                CustomTextInput(
                                  hintText: 'Nominal Top Up',
                                  value: controller.nominalSales.value,
                                  validator: controller.nominalValidator,
                                  textEditingController: controller.nominalSalesController,
                                  isEnabled: !controller.isSubmitting.value &&
                                      controller.catController.profile.value.upline != null,
                                  onClear: controller.nominalSalesController?.clear,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                                    InputPriceFormatter(),
                                  ],
                                  keyboardType: TextInputType.number,
                                ),
                                const Spacer(),
                                CustomButton(
                                  title: controller.catController.profile.value.upline != null
                                      ? 'Konfirmasi'
                                      : 'Sambungkan Ke Sales',
                                  isEnabled: controller.nominalSales.value.length > 4 ||
                                      controller.catController.profile.value.upline == null,
                                  onPress: controller.catController.profile.value.upline == null
                                      ? () => Get.toNamed(Routes.SALES)
                                      : controller.confirmTopUpSales,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Obx(
                        () => Form(
                          autovalidateMode: AutovalidateMode.disabled,
                          key: controller.formAdminKey,
                          child: Padding(
                            padding: const EdgeInsets.all(Spacing.defaultSpacing),
                            child: Column(
                              children: [
                                const BankProviderTopUpDropdown(),
                                const SizedBox(height: Spacing.defaultSpacing),
                                CustomTextInput(
                                  hintText: 'Nominal Top Up',
                                  value: controller.nominalAdmin.value,
                                  validator: controller.nominalValidator,
                                  textEditingController: controller.nominalAdminController,
                                  isEnabled: !controller.isSubmitting.value,
                                  onClear: controller.nominalAdminController?.clear,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                                    InputPriceFormatter()
                                  ],
                                  keyboardType: TextInputType.number,
                                ),
                                const Spacer(),
                                CustomButton(
                                  title: 'Bayar',
                                  isEnabled: controller.nominalAdmin.value.length > 4,
                                  onPress: () => controller.confirmTopUpAdmin(),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
