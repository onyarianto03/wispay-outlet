// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

class HistorySaldoView extends GetView {
  const HistorySaldoView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HistorySaldoView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'HistorySaldoView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
