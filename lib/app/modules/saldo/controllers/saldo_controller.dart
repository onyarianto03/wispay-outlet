// Package imports:
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/saldo/saldo_item.dart';
import 'package:wispay_outlet/app/data/models/utility/filter_date_model.dart';
import 'package:wispay_outlet/app/data/repositories/wallet_repository.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';

class SaldoController extends GetxController with SingleGetTickerProviderMixin {
  final WalletRepository _walletRepo = WalletRepository(Get.find());
  late TabController tabController = TabController(length: 3, vsync: this);
  late ScrollController scrollController = ScrollController();
  final CategoriesController home = Get.find();

  final _formKey = GlobalKey<FormState>();
  final _activeTab = 0.obs;
  final selectedDate = ''.obs;
  final inout = ''.obs;

  get getFormKey => _formKey;

  final listHistory = <SaldoItem>[].obs;
  final isLastPage = false.obs;
  final isLoading = false.obs;
  final showFloatingButton = false.obs;
  final _fixSroll = false.obs;

  String? _startDate;
  String? _endDate;
  String? _date;
  int _page = 1;

  @override
  void onInit() {
    super.onInit();
    getHistory();

    ever<int>(_activeTab, (value) {
      _page = 1;
      isLastPage.value = false;
      if (value != 0) {
        inout.value = SALDO_WALLET_INOUT[value - 1];
      } else {
        inout.value = '';
      }
      getHistory();
    });

    ever<List<SaldoItem>>(listHistory, (value) {
      if (value.length > 4) {
        _fixSroll.value = false;
      } else {
        _fixSroll.value = true;
      }
    });

    tabController.addListener(_tabListener);
    scrollController.addListener(_scrollListener);
  }

  @override
  void onClose() {
    super.onClose();
    tabController.removeListener(_tabListener);
  }

  void _tabListener() {
    if (!tabController.indexIsChanging) {
      _activeTab.value = tabController.index;
    }
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  void getHistory() async {
    isLoading.value = true;
    var _filter = {
      'in_out': inout.value,
      'created_at[from]': _startDate,
      'created_at[to]': _endDate,
      'date': _date,
    };
    var _response = await _walletRepo.getHistory(filter: FilterModel.fromJson(_filter));
    if (_response.success != false) {
      listHistory.value = _response.data!;
    }
    isLoading.value = false;
  }

  void handleSelectDate(FilterDateModel valueDate) async {
    if (valueDate.dateRange != null) {
      final r = valueDate.dateRange;
      final String _s = DateHelper.formatDate(r!.start.toString(), format: 'dd-MM-yyyy');
      final String _e = DateHelper.formatDate(r.end.toString(), format: 'dd-MM-yyyy');
      _startDate = _s;
      _endDate = _e;
    } else if (valueDate.dateRange == null) {
      _startDate = null;
      _endDate = null;
    }

    _date = valueDate.date;
    getHistory();
  }

  void _scrollListener() async {
    if (_fixSroll.value) scrollController.jumpTo(0);

    if (scrollController.position.maxScrollExtent == scrollController.offset) {
      showFloatingButton.value = true;
    } else {
      showFloatingButton.value = false;
    }
  }

  void getMoreHistory() async {
    if (isLastPage.value == true) return;
    _page++;
    var _filter = {
      'created_at[from]': _startDate,
      'created_at[to]': _endDate,
      'date': _date,
      'page': _page,
      'in_out': inout.value,
    };

    final result = await _walletRepo.getHistory(filter: FilterModel.fromJson(_filter));
    if (result.success != false) {
      if (result.data!.isEmpty) {
        isLastPage(true);
      } else {
        listHistory.addAll(result.data!);
      }
    }
  }

  void backToTop() {
    scrollController.animateTo(0,
        duration: Duration(milliseconds: listHistory.length * 10), curve: Curves.fastOutSlowIn);
  }
}
