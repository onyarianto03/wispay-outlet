import 'package:get/get.dart';
import 'package:wispay_outlet/app/data/models/topup/detail_request_topup_model.dart';
import 'package:wispay_outlet/app/data/repositories/topup_repository.dart';

class DetailTopupRequestController extends GetxController {
  final TopupRepository _topupRepo = TopupRepository(Get.find());

  final data = const DetailRequestTopupModel().obs;
  final topupId = Get.arguments?['id'];

  @override
  void onClose() {}

  Future<bool> getDetail(int id) async {
    final _response = await _topupRepo.getTopupRequestDetail(id);
    if (_response.data != null) {
      data.value = _response.data as DetailRequestTopupModel;
      return true;
    }

    return false;
  }
}
