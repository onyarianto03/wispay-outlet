import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/strings.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/topup/detail_request_topup_model.dart';
import 'package:wispay_outlet/app/data/repositories/topup_repository.dart';

class TopupHistoryController extends GetxController with SingleGetTickerProviderMixin {
  final TopupRepository _topupRepository = TopupRepository(Get.find());
  late ScrollController scrollController = ScrollController();

  late TabController tabController = TabController(
    length: 2,
    vsync: this,
  );
  final _activeTab = 0.obs;
  final historyTopUp = <DetailRequestTopupModel>[].obs;
  final isLoading = false.obs;
  final isLastPage = false.obs;

  int _page = 1;

  @override
  void onInit() {
    super.onInit();
    tabController.addListener(_tabListener);
    getList();
    ever<int>(_activeTab, (_) {
      _page = 1;
      isLastPage.value = false;
      getList();
    });
    scrollController.addListener(_scrollListener);
  }

  @override
  void onClose() {}

  void _tabListener() {
    if (!tabController.indexIsChanging) {
      _activeTab.value = tabController.index;
      FocusManager.instance.primaryFocus?.unfocus();
    }
  }

  Future<void> getList() async {
    isLoading.value = true;
    final _filter = _generateFilter();
    final _response = await _topupRepository.getTopupRequestList(filter: _filter);
    if (_response.success != false) {
      historyTopUp.value = _response.data!;
    }

    isLoading.value = false;
  }

  void _scrollListener() async {
    if (scrollController.position.pixels == scrollController.position.maxScrollExtent) {
      if (isLastPage.value == true) return;

      _page++;
      final _filter = _generateFilter();

      final result = await _topupRepository.getTopupRequestList(filter: _filter);
      if (result.success != false) {
        if (result.data!.isEmpty) {
          isLastPage(true);
        } else {
          historyTopUp.addAll(result.data!);
        }
      }
    }
  }

  FilterModel _generateFilter() {
    var _filter = FilterModel(status: PENDING, page: _page);
    if (_activeTab.value == 1) {
      _filter = FilterModel(statusList: [ACCEPT, DECLINE], page: _page);
    } else {
      _filter = FilterModel(status: PENDING, page: _page);
    }

    return _filter;
  }

  Future<void> onRefresh() async {
    _page = 1;
    isLastPage.value = false;
    await getList();
  }
}
