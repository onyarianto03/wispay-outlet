import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/models/topup/detail_request_topup_model.dart';
import 'package:wispay_outlet/app/data/models/wispay_bank_model/wispay_bank_model.dart';
import 'package:wispay_outlet/app/data/repositories/topup_repository.dart';
import 'package:wispay_outlet/app/data/repositories/utility_repository.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/home/controllers/home_controller.dart';
import 'package:wispay_outlet/app/modules/saldo/controllers/detail_topup_request_controller.dart';

import 'package:wispay_outlet/app/routes/app_pages.dart';
import '../widget/confirm_modal.dart';

class RequestTopupController extends GetxController with SingleGetTickerProviderMixin {
  final HomeController homeController = Get.find();
  final CategoriesController catController = Get.find();
  final DetailTopupRequestController _detailTopupController = Get.find();
  late TabController tabController = TabController(
    length: 2,
    vsync: this,
  );

  final UtilityRepository _utilityRepo = UtilityRepository(Get.find());
  final TopupRepository _topupRepository = TopupRepository(Get.find());

  TextEditingController? nominalSalesController;
  TextEditingController? nominalAdminController;
  final formSalesKey = GlobalKey<FormState>();
  final formAdminKey = GlobalKey<FormState>();

  final bankProvider = <WispayBankModel>[].obs;
  final selectedBank = const WispayBankModel().obs;
  final _activeTab = 0.obs;
  var nominalSales = ''.obs;
  var nominalAdmin = ''.obs;
  var isSubmitting = false.obs;

  final nominalValidator = MultiValidator([
    RequiredValidator(errorText: 'Nominal wajib diisi'),
    MinLengthValidator(4, errorText: 'Minimal Top Up Rp10.000'),
  ]);

  @override
  void onInit() {
    super.onInit();
    _getListBank();
    tabController.addListener(_tabListener);
    nominalSalesController = TextEditingController()..addListener(onChangeNominalSales);
    nominalAdminController = TextEditingController()..addListener(onChangeNominalAdmin);
  }

  @override
  void onClose() {
    super.onClose();
    nominalSalesController?.removeListener(onChangeNominalSales);
    nominalAdminController?.removeListener(onChangeNominalAdmin);
  }

  void _tabListener() {
    if (!tabController.indexIsChanging) {
      _activeTab.value = tabController.index;
      FocusManager.instance.primaryFocus?.unfocus();
    }
  }

  void onSelect(idx) {
    selectedBank(bankProvider[idx]);
    Get.back();
  }

  void onChangeNominalSales() {
    nominalSales.value = nominalSalesController?.text ?? '';
  }

  void onChangeNominalAdmin() {
    nominalAdmin.value = nominalAdminController?.text ?? '';
  }

  void confirmTopUpAdmin() async {
    FocusManager.instance.primaryFocus?.unfocus();
    final _amount = nominalAdmin.value.replaceAll('.', '');
    final _data = RequestBodyModel(amount: _amount, wispayBankId: selectedBank.value.id, kind: OUTLET_TO_ADMIN);
    _postData(data: _data).then((value) async {
      if (value?.id != null) {
        final _response = await _detailTopupController.getDetail(value!.id!);
        if (_response) {
          homeController.getTopupRequests();
          Get.offNamedUntil(Routes.HOW_TO_PAY_TOPUP, (route) => route.isFirst);
        }
      }
    });
  }

  void confirmTopUpSales() async {
    FocusManager.instance.primaryFocus?.unfocus();
    final _amount = nominalSales.value.replaceAll('.', '');
    final _data = RequestBodyModel(amount: _amount, kind: OUTLET_TO_SELLER);
    WispayBottomSheet.showBottomSheet(
      TopUpSalesConfirmModal(
        nominalValue: _amount,
        salesName: catController.profile.value.upline?.name ?? '',
        salesPhone: decrypt(catController.profile.value.upline?.phone),
        onConfirm: () => _postData(data: _data).then((value) {
          if (value != null) Get.offNamedUntil(Routes.HISTORY_TOPUP, (route) => route.isFirst);
        }),
      ),
    );
  }

  Future<DetailRequestTopupModel?> _postData({required RequestBodyModel data}) async {
    isSubmitting.value = true;

    WispayDialog.showLoading();
    final _res = await _topupRepository.createTopupRequest(data: data);
    if (_res.success == true) {
      WispayDialog.closeDialog();
      isSubmitting.value = false;
      return _res.data!;
    } else {
      isSubmitting.value = false;
      WispaySnackbar.showError(_res.message!, onClose: () => WispayDialog.closeDialog());
      return null;
    }
  }

  void _getListBank() async {
    final _res = await _utilityRepo.getWispayBanks(URL_WISPAY_BANK_TOUPUP);
    if (_res.success == true) {
      bankProvider.value = _res.data!;
    }
  }
}
