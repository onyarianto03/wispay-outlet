import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class SalesCard extends StatelessWidget {
  const SalesCard({
    Key? key,
    required this.name,
    required this.phoneNumber,
    required this.image,
  }) : super(key: key);

  final String name;
  final String phoneNumber;
  final String image;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      decoration: BoxDecoration(
        boxShadow: [buildShadow(spreadRadius: 1)],
        borderRadius: BorderRadius.circular(10),
        color: WispayColors.cWhite,
        image: DecorationImage(
          image: AssetImage(Assets.iconsMiscWave1.path),
          alignment: Alignment.centerRight,
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CustomImage(source: image, width: 32, height: 32, borderRadius: 0),
          const SizedBox(width: Spacing.defaultSpacing),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                title: name,
                textType: TextType.Bold,
                size: FontSize.medium,
              ),
              CustomText(
                title: phoneNumber,
              ),
            ],
          )
        ],
      ),
    );
  }
}
