// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/number_helper.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/rounded_icon_button.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import 'package:wispay_outlet/generated/locales.g.dart';

class SaldoHeader extends GetView<CategoriesController> {
  const SaldoHeader({
    Key? key,
    required this.saldo,
  }) : super(key: key);

  final String saldo;

  @override
  Widget build(BuildContext context) {
    return BaseCard(
      margin: EdgeInsets.zero,
      decorationImage: Assets.iconsMiscWave3,
      spreadRadius: 0,
      blurRadius: 20,
      child: _buildWidget(context),
    );
  }

  Widget _buildWidget(BuildContext context) {
    return Column(
      children: [
        _buildMenu(context),
        const Divider(thickness: 1, height: Spacing.defaultSpacing * 2),
        GestureDetector(
          onTap: () => Get.toNamed(Routes.HISTORY_TOPUP),
          child: Container(
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                CustomText(title: 'Riwayat Permintaan Top-Up', color: WispayColors.cBlack333),
                Icon(Icons.chevron_right, color: WispayColors.cBlack333)
              ],
            ),
          ),
        )
      ],
    );
  }

  Row _buildMenu(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: InkWell(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const CustomText(
                  size: FontSize.small,
                  title: 'Sisa Saldo',
                ),
                const SizedBox(height: Spacing.xSmall),
                CustomText(
                  title: NumberHelper.formatMoney(saldo),
                  textType: TextType.SemiBold,
                  color: WispayColors.cPrimary,
                  size: FontSize.medium,
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: Row(
            children: [
              RoundedIconButton(
                icon: Assets.iconsButtonQr.path,
                title: 'Kode QR',
                onPressed: () {
                  WispayDialog.showQR(
                    context,
                    NumberHelper.formatPhoneRemove62(decrypt(controller.profile.value.phone)),
                    controller.profile.value.name,
                  );
                },
              ),
              const Spacer(),
              RoundedIconButton(
                icon: Assets.iconsButtonPaperPlane.path,
                title: LocaleKeys.transfer.tr,
                onPressed: () => Get.toNamed(Routes.TRANSFER),
              ),
              const Spacer(),
              RoundedIconButton(
                icon: Assets.iconsButtonTopup.path,
                title: LocaleKeys.topup.tr,
                onPressed: () => Get.toNamed(Routes.REQUEST_TOPUP),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
