// Flutter imports:
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// Package imports:
import 'package:get/get_utils/src/extensions/widget_extensions.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/number_helper.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/bottomsheet_title.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';

class TopUpSalesConfirmModal extends StatelessWidget {
  const TopUpSalesConfirmModal({
    Key? key,
    required this.nominalValue,
    required this.onConfirm,
    required this.salesName,
    required this.salesPhone,
  }) : super(key: key);

  final String nominalValue;
  final void Function() onConfirm;
  final String salesName;
  final String salesPhone;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        left: Spacing.defaultSpacing,
        right: Spacing.defaultSpacing,
        bottom: Spacing.defaultSpacing,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const BottomSheetTitle(title: 'Top-Up Saldo Saya'),
          const SizedBox(height: Spacing.small),
          TrxDetailItem(
            title: 'Nama Agen',
            value: salesName,
            isBoldValue: true,
          ),
          TrxDetailItem(
            title: 'Nomor HP/Kode Agen',
            value: salesPhone,
            isBoldValue: true,
          ),
          TrxDetailItem(
            title: 'Nominal',
            value: NumberHelper.formatMoney(nominalValue.replaceAll('.', '')),
            isBoldValue: true,
          ),
          const Divider(thickness: 1).marginOnly(bottom: Spacing.small),
          CustomButton(title: 'Kirim Permintaan Top Up', onPress: onConfirm),
        ],
      ),
    );
  }
}
