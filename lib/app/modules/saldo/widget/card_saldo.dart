// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/date_helper.dart';
import 'package:wispay_outlet/app/core/helpers/number_helper.dart';
import 'package:wispay_outlet/app/core/helpers/transaction_kind_helper.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/saldo/saldo_item.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/generated/locales.g.dart';

class CardSaldo extends GetView {
  const CardSaldo({
    Key? key,
    required this.saldoItem,
  }) : super(key: key);

  final SaldoItem saldoItem;

  @override
  Widget build(BuildContext context) {
    final TransactionKind tk = TransactionKind();
    bool isIn = saldoItem.inOUT == 'IN';
    String plusMin = isIn ? '+' : '-';

    return BaseCard(
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      margin: EdgeInsets.zero,
      spreadRadius: 0,
      blurRadius: 20,
      child: Column(
        children: [
          Row(
            children: [
              CustomText(
                title: tk.humanizeTransactionKind(saldoItem.kind),
                color: WispayColors.cBlack333,
              ),
              const Spacer(),
              CustomText(
                title: DateHelper.formatDate(saldoItem.createdAt, format: 'dd MMM'),
                color: WispayColors.cGreyBC,
                size: FontSize.small,
              ),
            ],
          ),
          SizedBox(
            height: Spacing.xSmall.w,
          ),
          Row(
            children: [
              CustomText(
                title: plusMin + NumberHelper.formatMoney(saldoItem.amount ?? ''),
                color: isIn ? WispayColors.cGreen : WispayColors.cRed,
                textType: TextType.SemiBold,
              ),
              const Spacer(),
              CustomText(
                title: LocaleKeys.saldo_e_wallet.tr,
                color: WispayColors.cBlack666,
                size: FontSize.small,
              ),
            ],
          )
        ],
      ),
    );
  }
}
