// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/topup/detail_request_topup_model.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/saldo/controllers/detail_topup_request_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class CardHistoryTopUp extends GetView {
  CardHistoryTopUp({
    Key? key,
    required this.item,
  }) : super(key: key);

  final DetailRequestTopupModel item;
  final DetailTopupRequestController _controller = Get.find<DetailTopupRequestController>();

  @override
  Widget build(BuildContext context) {
    return BaseCard(
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      margin: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
      spreadRadius: 0,
      blurRadius: 20,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CustomText(
                title: item.code!,
                color: WispayColors.cBlack,
                size: FontSize.small,
              ),
              const Spacer(),
              CustomText(
                title: DateHelper.formatDate(item.createdAt, format: 'dd MMM'),
                color: WispayColors.cGreyBC,
                size: FontSize.small,
              ),
            ],
          ),
          SizedBox(
            height: Spacing.xSmall.w,
          ),
          Row(
            children: [
              const CustomText(
                title: 'Top-up Saldo',
                textType: TextType.Bold,
              ),
              const Spacer(),
              CustomText(
                title: item.status! == PENDING && item.kind != OUTLET_TO_SELLER
                    ? TransactionStatusHelper.statusColorAndText(WAITING_PAYMENT)['text']
                    : TransactionStatusHelper.statusColorAndText(item.status!)['text'],
                size: FontSize.small,
                color: item.status! == PENDING && item.kind != OUTLET_TO_SELLER
                    ? TransactionStatusHelper.statusColorAndText(WAITING_PAYMENT)['color']
                    : TransactionStatusHelper.statusColorAndText(item.status!)['color'],
                textType: TextType.Bold,
              ),
            ],
          ),
          const SizedBox(height: Spacing.xSmall),
          CustomText(title: NumberHelper.formatMoney(item.amount!)),
          item.status! == PENDING && item.kind != OUTLET_TO_SELLER
              ? InfoWidget(
                  text: 'Bayar sebelum ${DateHelper.formatDate(item.expiredAt, format: 'dd MMM yyyy, HH:mm WIB')}',
                  margin: const EdgeInsets.only(top: Spacing.small),
                  rightWidget: InkWell(
                    onTap: () {
                      WispayDialog.showLoading();
                      _controller.getDetail(item.id!).then((value) {
                        WispayDialog.closeDialog();
                        Get.toNamed(Routes.HOW_TO_PAY_TOPUP);
                      });
                    },
                    child: const CustomText(
                      title: 'Cara Bayar',
                      size: FontSize.small,
                      color: WispayColors.cPrimary,
                      textType: TextType.Bold,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                )
              : const SizedBox(height: 0)
        ],
      ),
    );
  }
}
