// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';

// Project imports:
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/modules/saldo/controllers/request_topup_controller.dart';

class BankProviderTopUpDropdown extends GetView<RequestTopupController> {
  const BankProviderTopUpDropdown({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Dropdown(
        label: 'Pilih Bank',
        value: controller.selectedBank.value.bankName ?? "",
        withImage: true,
        imageUrl: controller.selectedBank.value.logo?.url ?? "",
        onTap: () {
          WispayBottomSheet.scrollable(
              title: 'Pilih Bank Tujuan',
              withSearch: false,
              withTitle: true,
              itemBuilder: (_, scrollController) {
                return ListView.separated(
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  reverse: false,
                  itemBuilder: ((context, index) => ListTile(
                        dense: true,
                        onTap: () => controller.onSelect(index),
                        leading: CustomImage(
                          source: controller.bankProvider[index].logo?.url ?? '',
                          width: 32,
                          fit: BoxFit.contain,
                          height: 32,
                          borderRadius: 4,
                        ),
                        title: CustomText(
                          title: controller.bankProvider[index].bankName ?? '',
                        ),
                        trailing: Visibility(
                          child: const Icon(
                            Icons.check_circle,
                            color: WispayColors.cGreen2,
                            size: 20,
                          ),
                          visible: controller.selectedBank.value == controller.bankProvider[index],
                        ),
                      )),
                  separatorBuilder: (_, __) => const Divider(),
                  itemCount: controller.bankProvider.length,
                );
              });
        },
      ),
    );
  }
}
