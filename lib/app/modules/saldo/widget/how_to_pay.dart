import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/countdown/countdown.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class HowToPay extends StatelessWidget {
  const HowToPay({
    Key? key,
    required this.va,
    required this.amount,
    required this.expiredAt,
    required this.accountNumber,
    required this.bankLogo,
    required this.adminFee,
  }) : super(key: key);

  final String va;
  final String amount;
  final String expiredAt;
  final String accountNumber;
  final String bankLogo;
  final String adminFee;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const CustomText(
              title: 'Batas Pembayaran',
            ),
            CountDownTimer(
              secondsRemaining: DateTime.now().difference(DateTime.parse(expiredAt)).inSeconds.toInt().abs(),
              whenTimeExpires: () => {},
              countDownFormatter: _countdownFormatter,
              countDownTimerStyle: const TextStyle(
                color: WispayColors.cBlack,
                fontWeight: FontWeight.w600,
              ),
            )
          ],
        ),
        const SizedBox(height: Spacing.small),
        const Divider(thickness: 1),
        const SizedBox(height: Spacing.small),
        const CustomText(title: 'Nomor Virtual Account'),
        const SizedBox(height: Spacing.xSmall),
        Row(
          children: [
            CustomImage(source: bankLogo, borderRadius: 0, width: 32, height: 32, fit: BoxFit.contain),
            const SizedBox(width: Spacing.defaultSpacing),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    title: accountNumber,
                    textType: TextType.SemiBold,
                    size: FontSize.medium,
                    color: WispayColors.cBlack,
                  ),
                  const SizedBox(height: Spacing.xSmall),
                  CustomText(
                    title: va,
                    size: FontSize.small,
                  ),
                ],
              ),
            ),
            const SizedBox(width: Spacing.defaultSpacing),
            GestureDetector(
              onTap: () => _copyToClipboard(context, accountNumber, title: 'Nomor Virtual Account'),
              child: const CustomText(
                title: 'Salin Nomor',
                color: WispayColors.cPrimary,
                textType: TextType.SemiBold,
              ),
            ),
          ],
        ),
        const SizedBox(height: Spacing.small),
        const Divider(thickness: 1),
        const SizedBox(height: Spacing.small),
        const CustomText(title: 'Jumlah Transfer'),
        const SizedBox(height: Spacing.xSmall),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              title: NumberHelper.formatMoney(amount),
              textType: TextType.SemiBold,
              color: WispayColors.cBlack,
              size: FontSize.medium,
            ),
            GestureDetector(
              onTap: () => _copyToClipboard(context, amount, title: 'Jumlah transfer'),
              child: const CustomText(
                title: 'Salin Jumlah',
                color: WispayColors.cPrimary,
                textType: TextType.SemiBold,
              ),
            ),
          ],
        ),
        const SizedBox(height: Spacing.small),
        Row(
          children: [
            Lottie.asset(Assets.lottieInfo, width: 24, height: 24),
            const SizedBox(width: Spacing.xSmall),
            Expanded(
                child: Text.rich(
              TextSpan(
                text: 'Total bayar sudah termasuk biaya layanan ',
                children: [
                  TextSpan(
                    text: NumberHelper.formatMoney(adminFee),
                    style: const TextStyle(fontWeight: FontWeight.w600),
                  ),
                ],
              ),
            )),
          ],
        )
      ],
    );
  }

  _countdownFormatter(int seconds) {
    final hours = (seconds / 3600).truncate();
    seconds = (seconds % 3600).truncate();
    final minutes = (seconds / 60).truncate() % 60;
    final secondsStr = (seconds % 60);

    if (hours == 0) {
      return '$minutes menit $secondsStr detik';
    }

    return '$hours jam $minutes menit $secondsStr detik';
  }

  void _copyToClipboard(BuildContext context, String text, {String title = 'Nomor Virtual Account'}) {
    Clipboard.setData(ClipboardData(text: text)).then((_) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(title + ' berhasil disalin'),
        backgroundColor: WispayColors.cSecondary,
        duration: const Duration(seconds: 2),
      ));
    });
  }
}
