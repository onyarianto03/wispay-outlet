// Package imports:
import 'package:get/get.dart';
import 'package:supercharged/supercharged.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/product_category_helper.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/data/models/product_category/product_category_item/product_category_item_model.dart';
import 'package:wispay_outlet/app/data/models/product_category/product_category_model.dart';
import 'package:wispay_outlet/app/data/models/profile/profile_model.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';
import 'package:wispay_outlet/app/data/repositories/product_repository.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class CategoriesController extends GetxController {
  final ProductRepository _productRepository = ProductRepository(Get.find());
  final AccountRepository _accountRepository = AccountRepository(Get.find());
  final productCategoryItems = <ProductCategoryItemModel>[].obs;
  final homeCategories = <ProductCategoryItemModel>[].obs;
  final filterProductCategories = <ProductCategoryItemModel>[].obs;
  final groupedCategories = <String, List<ProductCategoryItemModel>>{}.obs;

  final isLoading = false.obs;
  final isError = false.obs;
  final isSuccess = false.obs;
  final profile = ProfileModel().obs;
  int? dataPlanId;

  @override
  void onInit() async {
    ever<bool>(
      isSuccess,
      (v) {
        if (v) {
          _generateHomeCategory();
          generateFilterCategories();
        }
      },
    );

    isLoading.value = true;
    final _response = await _productRepository.getProductCategories();
    final _account = await _accountRepository.getProfile();
    if (_response.success == true && _account.success == true) {
      profile.value = _account.data!;
      _assignRouteName(_response.data!, _account.data?.isTselPartner);
      dataPlanId = _response.data!.data!.firstWhere((element) => element.code == ProductCode.DATA_PLAN).id;
      isLoading.value = false;
      isSuccess.value = true;
    } else {
      isLoading.value = false;
      isSuccess.value = false;
    }
    super.onInit();
  }

  _assignRouteName(ProductCategoryModel categories, isTselPartner) {
    categories.data?.forEach((element) {
      if (element.code != ProductCode.DATA_PLAN) {
        productCategoryItems.add(
          ProductCategoryItemModel(
            id: element.id,
            name: ProductCategoryHelper.productCodeToProductName(element.code!),
            code: element.code,
            image: element.image,
            imagePath: element.image?.thumbnail?.url,
            routeName: ProductCategoryHelper.getProductNavigation(element.code),
            isActive: element.isActive,
            groupKey: element.groupKey,
            position: element.position,
          ),
        );
      }
    });

    if (isTselPartner != true) {
      productCategoryItems.removeWhere((element) => element.code == ProductCode.SELL_OUT);
    }
  }

  void _generateHomeCategory() {
    for (int i = 0; i < 7; i++) {
      homeCategories.addIf(productCategoryItems.isNotEmpty, productCategoryItems[i]);
    }

    homeCategories.add(ProductCategoryItemModel(
      id: 999,
      name: 'Lainnya',
      code: 'Others',
      imagePath: Assets.iconsLainnya.path,
      routeName: ProductCategoryHelper.getProductNavigation('others'),
    ));

    _groupCategoryByGroupKey();
  }

  void generateFilterCategories() {
    filterProductCategories.clear();
    filterProductCategories.add(ProductCategoryItemModel(
      id: null,
      name: 'Semua Kategori',
      code: null,
    ));

    for (var i = 0; i < productCategoryItems.length; i++) {
      filterProductCategories.add(productCategoryItems[i]);
    }
  }

  void _groupCategoryByGroupKey() {
    final Map<String, List<ProductCategoryItemModel>> newGroup = productCategoryItems
        .filter((element) => element.code != ProductCode.DATA_PLAN)
        .groupBy((element) => element.groupKey!);

    groupedCategories.value = newGroup;
  }

  void searchFilterCategories(String value) {
    filterProductCategories.clear();

    if (value.isNotEmpty) {
      final List<ProductCategoryItemModel> filtered =
          productCategoryItems.where((element) => element.name!.toLowerCase().contains(value.toLowerCase())).toList();

      for (var i = 0; i < filtered.length; i++) {
        filterProductCategories.add(filtered[i]);
      }
    } else {
      for (var i = 0; i < productCategoryItems.length; i++) {
        filterProductCategories.add(productCategoryItems[i]);
      }
    }
  }

  Future<bool> getProfile() async {
    final _res = await _accountRepository.getProfile();
    if (_res.success == true) {
      profile.value = _res.data as ProfileModel;
      return true;
    }

    return false;
  }
}
