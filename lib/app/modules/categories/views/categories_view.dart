// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/product_category_item.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import '../controllers/categories_controller.dart';

class CategoriesView extends GetView<CategoriesController> {
  const CategoriesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 3,
        shadowColor: WispayColors.cGrey90.withOpacity(0.1),
        title: const Text('Kategori Produk'),
        centerTitle: true,
        leading: IconButton(
          onPressed: () => Get.back(),
          icon: const Icon(Icons.chevron_left, size: 35),
        ),
      ),
      body: SingleChildScrollView(
        // padding: const EdgeInsets.all(Spacing.defaultSpacing),
        child: Obx(() {
          final gorupCat = controller.groupedCategories;
          List<Widget> children = [];
          for (var i = 0; i < gorupCat.keys.length; i++) {
            children.add(
              CustomText(
                title: ProductCategoryHelper.groupKeyToName(gorupCat.keys.elementAt(i)) ?? "",
                size: FontSize.large,
                textType: TextType.Bold,
                color: WispayColors.cBlack333,
                margin: const EdgeInsets.all(Spacing.defaultSpacing),
              ),
            );

            children.add(
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                child: StaggeredGridView.countBuilder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  crossAxisCount: 4,
                  itemCount: gorupCat[gorupCat.keys.elementAt(i)]?.length,
                  mainAxisSpacing: 16,
                  crossAxisSpacing: 16,
                  itemBuilder: (_, idx) {
                    final item = gorupCat[gorupCat.keys.elementAt(i)]![idx];
                    return ProductCategoryItem(
                      onTap: () => Get.toNamed(item.routeName!,
                          arguments:
                              item.code == ProductCode.PHONE_CREDIT ? [item.id, controller.dataPlanId] : item.id),
                      image: item.image?.thumbnail?.url ?? Assets.iconsDefaultIcon.path,
                      title: item.name!,
                      isLoading: false,
                    );
                  },
                  staggeredTileBuilder: (_) => const StaggeredTile.fit(1),
                ),
              ),
            );

            if (i != gorupCat.keys.length - 1) {
              children.add(Divider(thickness: 8, color: WispayColors.cGreyC.withOpacity(0.05)));
            }
          }
          return Column(crossAxisAlignment: CrossAxisAlignment.start, children: children);
        }),
      ),
    );
  }
}
