import 'package:get/get.dart';

import '../controllers/omni_controller.dart';

class OmniBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OmniController>(
      () => OmniController(),
    );
  }
}
