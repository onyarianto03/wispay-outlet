import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

import '../controllers/omni_controller.dart';

class OmniView extends GetView<OmniController> {
  const OmniView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Combo Sakti'),
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          child: Obx(
            () => controller.isLoading.value
                ? Center(
                    child: Lottie.asset(Assets.lottieLoading, width: 100),
                  )
                : Form(
                    key: controller.formKey,
                    onChanged: controller.onchangeForm,
                    child: Column(
                      children: [
                        CustomTextInput(
                          value: controller.number.value,
                          textEditingController: controller.tNumber,
                          label: 'Nomor Hp pelanggan',
                          hintText: 'nomor hp pelanggan',
                          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                          keyboardType: TextInputType.number,
                          validator: MultiValidator([
                            RequiredValidator(errorText: 'Nomor Hp tidak boleh kosong'),
                            IndonesiaPhoneNumber(errorText: 'Nomor Hp tidak valid'),
                          ]),
                          margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
                        ),
                        CustomTextInput(
                          value: controller.code.value,
                          textEditingController: controller.tCode,
                          label: 'Kode bayar',
                          hintText: 'kode bayar',
                          withCustomSuffixIcon: true,
                          suffixIcon: InkWell(
                            onTap: () => controller.onScanQR(),
                            radius: 20,
                            child: Padding(
                              padding: const EdgeInsets.all(Spacing.medium),
                              child: Assets.iconsQrInput.image(width: 20, height: 20),
                            ),
                          ),
                          validator: RequiredValidator(errorText: 'Kode bayar tidak boleh kosong'),
                          margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
                        ),
                        CustomButton(
                          title: 'Cek Tagihan',
                          isEnabled: controller.isEnabled.value,
                          onPress: () => controller.onCheckBill(),
                        )
                      ],
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}
