import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/services/location_service.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/app/views/qr_scanner_view.dart';

import '../widgets/status_card.dart';

class OmniController extends GetxController {
  TextEditingController tCode = TextEditingController();
  TextEditingController tNumber = TextEditingController();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final ProductController _productController = Get.find();
  final LocationService _locationService = LocationService();
  final CategoriesController _home = Get.find();

  GlobalKey<FormState> formKey = GlobalKey<FormState>(debugLabel: 'form omni key');
  GlobalKey<FormFieldState> phoneKey = GlobalKey<FormFieldState>();
  GlobalKey<FormFieldState> codeKey = GlobalKey<FormFieldState>();

  final categoryId = Get.arguments;
  final code = ''.obs;
  final number = ''.obs;
  final isEnabled = false.obs;
  final isLoading = true.obs;
  final currentLocation = Rxn<Position>();
  String? token = "";

  @override
  void onInit() async {
    super.onInit();
    tCode.addListener(onchangeCode);
    tNumber.addListener(onchangeNumber);
    currentLocation.value = await _locationService.getCurrentLocation();
    _getInitialData();
  }

  void onScanQR() {
    Get.to(() => const QrScannerView())!.then((value) {
      final barcode = value as Barcode;
      tCode.text = barcode.code!;
    });
  }

  void onchangeCode() => code.value = tCode.text;
  void onchangeNumber() => number.value = tNumber.text;

  void onchangeForm() {
    bool isValid = formKey.currentState!.validate();
    if (isValid) {
      isEnabled.value = true;
    } else {
      isEnabled.value = false;
    }
  }

  void onCheckBill() async {
    FocusManager.instance.primaryFocus?.unfocus();
    final isValid = formKey.currentState!.validate();
    final commissionSetting = _home.profile.value.comissionSetting;
    double comission = double.parse(commissionSetting?.paketData ?? commissionSetting?.all ?? '0');

    if (isValid) {
      FocusManager.instance.primaryFocus?.unfocus();
      final _response = await _order();
      if (_response != null) {
        token = _response.meta?.token;
        WispayBottomSheet.showConfirm(
          onConfirm: () => onPay(),
          title: 'Combo Sakti',
          totalPrice: NumberHelper.formatMoney(_response.total.toString()),
          withPromo: false,
          children: [
            comission > 0
                ? InfoWidget(
                    text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                      comission.toString(),
                    )}',
                    margin: const EdgeInsets.only(top: 5, bottom: 5),
                  )
                : const SizedBox(height: 0),
            TrxDetailItem(
              title: 'Nama Produk',
              value: _response.detailPayload?.productName,
              multiLine: true,
            ),
            TrxDetailItem(
              title: 'Deskripsi',
              value: _response.detailPayload?.allowanceDesc,
              multiLine: true,
            ),
            TrxDetailItem(
              title: 'Masa Aktif',
              value:
                  'Sampai ${DateHelper.formatDate(_response.detailPayload?.productLengthDateId, format: 'dd MMM yyyy')}',
              multiLine: true,
            ),
            const Divider(),
            const SizedBox(height: Spacing.small),
            TrxDetailItem(title: 'Nomor Hp Pelanggan', value: number.value),
            TrxDetailItem(title: 'Tagihan', value: NumberHelper.formatMoney(_response.price)),
            TrxDetailItem(title: 'Biaya Admin', value: NumberHelper.formatMoney(_response.adminFee)),
          ],
        );
      }
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': decrypt(_home.profile.value.phone)},
    );
  }

  void _next(String pin) async {
    final _suplier = _home.profile.value.isTselPartner == true ? 'smtel' : 'digipos';
    final body = {
      "pin": pin,
      "token": token,
    };
    final _res = await _transactionRepository.confirm(
      supplier: _suplier,
      productName: 'tsel-omni-channel',
      body: RequestBodyModel.fromJson(body),
    );

    if (_res.success == true) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(data: _res.data),
            transactionId: _res.data?.id,
            productCode: ProductCode.TSEL_OMNI,
            status: _res.data?.status ?? TransactionStatusHelper.PENDING,
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void _getInitialData() async {
    isLoading.value = true;
    final productBrands = await _productController.getProductBrands(categoryId);
    final productSubcategories = await _productController.getSubCategories(productBrands?.first.id);
    await _productController.getProducts(productSubcategories?.first.id);
    isLoading.value = false;
  }

  Future<ProductModel?> _order() async {
    final _suplier = _home.profile.value.isTselPartner == true ? 'smtel' : 'digipos';
    final body = {
      "product_id": _productController.products.first.id,
      "payment_code": encrypt(code.value),
      "phone": encrypt(NumberHelper.formatPhoneToIndonesian(number.value)),
      "latitude": currentLocation.value?.latitude.toString(),
      "longitude": currentLocation.value?.longitude.toString(),
    };

    final _res = await _transactionRepository.order(
      supplier: _suplier,
      productName: 'tsel-omni-channel',
      body: RequestBodyModel.fromJson(body),
    );

    return _res.data;
  }
}
