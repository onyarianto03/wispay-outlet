// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import '../../../../../generated/assets.gen.dart';
import '../controllers/pascabayar_controller.dart';

class PascabayarView extends GetView<PascabayarController> {
  const PascabayarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pascabayar'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => controller.isLoading.value
            ? Center(
                child: Lottie.asset(Assets.lottieLoading, width: 100),
              )
            : Padding(
                padding: const EdgeInsets.all(Spacing.defaultSpacing),
                child: Form(
                  autovalidateMode: AutovalidateMode.disabled,
                  key: controller.formKey,
                  child: Column(
                    children: [
                      CustomTextInput(
                        value: controller.phoneNumber.value,
                        hintText: 'masukkan nomor hp',
                        showContact: true,
                        isGSM: true,
                        onContactPicked: (value) => controller.onPickedContact(value),
                        textEditingController: controller.tController,
                        onClear: controller.tController.clear,
                        validator: controller.phoneNumberValidator,
                        keyboardType: TextInputType.phone,
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      CustomButton(
                        title: 'Cek Tagihan',
                        onPress: controller.onCheck,
                        isEnabled: !controller.isSubmitting.value,
                        isLoading: controller.isSubmitting.value,
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
