// Package imports:
import 'package:get/get.dart';

// Project imports:
import '../controllers/pascabayar_controller.dart';

class PascabayarBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PascabayarController>(
      () => PascabayarController(),
    );
  }
}
