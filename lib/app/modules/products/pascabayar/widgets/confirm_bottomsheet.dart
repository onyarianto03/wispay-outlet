// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/pascabayar/controllers/pascabayar_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class ConfirmBottomsheet extends GetView<PascabayarController> {
  const ConfirmBottomsheet({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const InfoWidget(text: 'Tagihan ke pembeli sudah termasuk keuntungan Rp 2.000'),
        const SizedBox(height: Spacing.defaultSpacing),
        const TrxDetailItem(title: 'Nama Produk', value: 'Telkomsel Pascabayar'),
        const TrxDetailItem(title: 'Nomor Hp', value: '081234567890'),
        const TrxDetailItem(title: 'Tagihan', value: 'Rp 100.000'),
        const SizedBox(height: Spacing.small),
        _buildVoucher(),
        const Divider(thickness: 1, height: 32),
        Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  CustomText(title: 'Total tagihan'),
                  CustomText(
                    title: 'Rp 100.000',
                    textType: TextType.Bold,
                    color: WispayColors.cPrimary,
                    size: FontSize.medium,
                  ),
                ],
              ),
            ),
            Expanded(
              child: CustomButton(title: 'Lanjut Bayar', onPress: controller.onPay),
            ),
          ],
        ),
        const SizedBox(height: Spacing.defaultSpacing),
      ],
    );
  }

  Widget _buildVoucher() {
    return Container(
      padding: const EdgeInsets.all(Spacing.small),
      decoration: BoxDecoration(
        color: Colors.white,
        image: const DecorationImage(
          image: Assets.iconsMiscWave1,
          alignment: Alignment.centerRight,
        ),
        borderRadius: BorderRadius.circular(8),
        boxShadow: [buildShadow(spreadRadius: 4, blurRadius: 4)],
      ),
      child: Row(
        children: [
          Assets.iconsKupon.image(width: 24),
          const CustomText(
            title: 'Tambah promo/voucher',
            textType: TextType.SemiBold,
            margin: EdgeInsets.only(left: Spacing.small),
            color: WispayColors.cBlack666,
          ),
          const Spacer(),
          const Icon(
            Icons.chevron_right,
            size: 35,
            color: WispayColors.cBlack666,
          )
        ],
      ),
    );
  }
}
