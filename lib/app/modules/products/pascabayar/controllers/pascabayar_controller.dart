// Flutter imports:
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import '../widgets/status_card.dart';

class PascabayarController extends GetxController {
  TextEditingController tController = TextEditingController();
  final ProductController _productController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final CategoriesController _homeController = Get.find();
  final formKey = GlobalKey<FormState>();

  final phoneNumber = ''.obs;
  var isLastPage = false;
  var isLoading = false.obs;
  final categoryId = Get.arguments;
  var isSubmitting = false.obs;
  String? token = "";

  final phoneNumberValidator = MultiValidator([
    RequiredValidator(errorText: 'harus diisi'),
    IndonesiaPhoneNumber(errorText: 'nomor hp tidak valid'),
  ]);

  @override
  void onInit() {
    super.onInit();
    getInitialData();
    tController.addListener(onChange);
  }

  void getInitialData() async {
    isLoading.value = true;
    await _productController.getProductBrands(categoryId);
    // final productSubcategories = await _productController.getSubCategories(productBrands?.first.id);
    // await _productController.getProducts(productSubcategories?.first.id);

    isLoading.value = false;
  }

  @override
  void onClose() {}

  void onChange() => phoneNumber.value = tController.text;
  void onPickedContact(String? phone) {
    if (phone != null) {
      tController.text = phone;
    }
  }

  void _next(String pin) async {
    final body = {
      "pin": pin,
      "token": token,
    };
    final confirmResponse = await _transactionRepository.confirm(
      supplier: _productController.products.first.productSupplier!.code!.toLowerCase(),
      productName: 'pulsa-postpaid',
      body: RequestBodyModel.fromJson(body),
    );
    if (confirmResponse.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(
              confirmResponse: confirmResponse.data!,
            ),
            transactionId: confirmResponse.data?.id,
            productCode: ProductCode.PASCABAYAR,
            status: confirmResponse.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': decrypt(_homeController.profile.value.phone)},
    );
  }

  onCheck() async {
    final isValid = formKey.currentState!.validate();
    FocusManager.instance.primaryFocus?.unfocus();

    isSubmitting.value = true;
    final findBrand = _productController.productBrand.firstWhere(
      (brand) =>
          brand.name?.toLowerCase() ==
          GSMHelper.getOperatorName(
            NumberHelper.formatPhoneRemove62(phoneNumber.value),
          ),
    );
    final productSubcategories = await _productController.getSubCategories(findBrand.id);
    await _productController.getProducts(productSubcategories?.first.id);

    if (isValid) {
      final body = {
        "product_id": _productController.products.first.id,
        "phone": encrypt(NumberHelper.formatPhoneToIndonesian(phoneNumber.value)),
      };
      final response = await _transactionRepository.order(
        supplier: _productController.products.first.productSupplier!.code!.toLowerCase(),
        productName: 'pulsa-postpaid',
        body: RequestBodyModel.fromJson(body),
      );
      token = response.data?.meta?.token;
      isSubmitting.value = false;

      if (response.success != false) {
        final commissionSetting = _homeController.profile.value.comissionSetting;
        double comission = double.parse(commissionSetting?.pulsaPascabayar ?? commissionSetting?.all ?? '0');
        WispayBottomSheet.showConfirm(
          onConfirm: () => onPay(),
          withPromo: false,
          title: "Pascabayar",
          totalPrice: NumberHelper.formatMoney(
            response.data?.total ?? '0.0',
          ),
          children: [
            comission > 0
                ? InfoWidget(
                    text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                      comission.toString(),
                    )}',
                    margin: const EdgeInsets.only(top: 5),
                  )
                : const SizedBox(height: 0),
            const SizedBox(height: Spacing.defaultSpacing),
            TrxDetailItem(title: 'Nama Produk', value: response.data?.productName),
            TrxDetailItem(
                title: 'Nomor Hp', value: NumberHelper.formatPhoneRemove62(decrypt(response.data?.customerPhone))),
            TrxDetailItem(
                title: 'Tagihan',
                value: NumberHelper.formatMoney(
                  response.data?.price ?? '0.0',
                )),
            TrxDetailItem(
              title: 'Biaya Admin',
              value: NumberHelper.formatMoney(
                response.data?.adminFee ?? '0.0',
              ),
              marginBottom: 0,
            ),
          ],
        );
      }
    }
  }
}
