import 'package:auto_size_text/auto_size_text.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/send_money/controllers/payment_instructions_controller.dart';
import 'package:wispay_outlet/app/modules/products/send_money/widgets/how_to_pay.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class PaymentInstructionsView extends GetView<PaymentInstructionsController> {
  const PaymentInstructionsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cara Pembayaran'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () => Get.back(),
        ),
      ),
      body: SafeArea(
        child: Obx(
          () => controller.isLoading.value
              ? Center(child: Lottie.asset(Assets.lottieLoading, width: 100))
              : SingleChildScrollView(
                  padding: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
                  child: Column(
                    children: [
                      BaseCard(
                        child: HowToPay(data: controller.transaction.value),
                      ),
                      BaseCard(
                        margin: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                        padding: const EdgeInsets.only(
                          top: Spacing.defaultSpacing,
                          left: Spacing.defaultSpacing,
                          right: Spacing.defaultSpacing,
                          bottom: Spacing.medium,
                        ),
                        child: ExpandablePanel(
                          collapsed: const SizedBox(),
                          theme: const ExpandableThemeData(
                            iconPadding: EdgeInsets.zero,
                            useInkWell: true,
                          ),
                          header: const CustomText(
                            title: 'Panduan Pembayaran',
                            textType: TextType.Bold,
                            size: FontSize.medium,
                            color: WispayColors.cBlack,
                          ),
                          expanded: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: Spacing.small),
                              _note(
                                number: '1. ',
                                content: const TextSpan(
                                  text: 'Lakukan pembayaran melalui ATM/Mobile Banking/Internet Banking',
                                ),
                              ),
                              _note(
                                number: '2. ',
                                content: const TextSpan(
                                  text: 'Isi ',
                                  children: [
                                    TextSpan(
                                      text: 'Nomor Virtual Account ',
                                      style: TextStyle(fontWeight: FontWeight.w600),
                                    ),
                                    TextSpan(
                                      text: 'sesuai dengan yang tertera di panduan cara bayar. Pastikan nomor sesuai.',
                                    ),
                                  ],
                                ),
                              ),
                              _note(
                                number: '3. ',
                                content: const TextSpan(text: 'Ikuti instruksi untuk menyelesaikan transaksi'),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
        ),
      ),
    );
  }

  _note({required String number, required TextSpan content}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText(title: number),
        Expanded(
          child: AutoSizeText.rich(
            content,
            style: const TextStyle(fontSize: 14, color: WispayColors.cBlack666),
          ),
        )
      ],
    ).marginOnly(bottom: Spacing.xSmall);
  }
}
