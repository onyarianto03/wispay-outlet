import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/form_note.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

import '../controllers/send_money_controller.dart';

class SendMoneyView extends GetView<SendMoneyController> {
  const SendMoneyView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text('Kirim Tunai'),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.chevron_left,
              size: 35,
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: SafeArea(
          child: Obx(
            () => controller.isLoading.value
                ? Center(
                    child: Lottie.asset(Assets.lottieLoading, width: 100),
                  )
                : Container(
                    color: WispayColors.cWhite,
                    padding: const EdgeInsets.all(Spacing.defaultSpacing),
                    child: Form(
                      autovalidateMode: AutovalidateMode.disabled,
                      key: controller.formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _dropdownField(
                            items: controller.bankList,
                            label: 'Pilih Bank Tujuan',
                            title: 'Bank Tujuan',
                            onSelect: (value) => controller.onSelectBank(value),
                            key: controller.dropdownKey,
                            selectedItem: controller.selectedBank.value,
                            onSearch: controller.onSearch,
                            onClose: () {
                              controller.search.value = '';
                              controller.getInitialData();
                              controller.bankPage = 1;
                            },
                            onReachedEnd: controller.onLoadMore,
                          ),
                          const SizedBox(height: Spacing.defaultSpacing),
                          CustomTextInput(
                            hintText: 'Nomor Rekening Tujuan',
                            value: controller.rekening.value,
                            keyboardType: TextInputType.number,
                            validator: controller.rekeningValidator,
                            textEditingController: controller.rekeningController,
                            isEnabled: !controller.isSubmitting.value,
                            onClear: controller.rekeningController?.clear,
                            margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
                          ),
                          WispayOutlinedButton(
                            title: 'Periksa Rekening',
                            onPress: controller.checkRekening,
                            width: double.infinity,
                            isEnabled: !controller.isSubmitting.value &&
                                controller.rekening.value != '' &&
                                controller.selectedBank.value.name != null,
                            isLoading: controller.isSubmitting.value,
                            height: 40,
                          ),
                          const SizedBox(height: Spacing.defaultSpacing),
                          controller.showReceipent.value
                              ? Container(
                                  padding: const EdgeInsets.all(Spacing.defaultSpacing),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: WispayColors.cWhite2,
                                  ),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      const Icon(
                                        Icons.check_circle,
                                        color: WispayColors.cGreen,
                                      ),
                                      const SizedBox(width: Spacing.small),
                                      Expanded(
                                        child: CustomText(
                                          title:
                                              '${controller.selectedBank.value.name} - ${decrypt(controller.inquiryData.value.accountName)}',
                                          margin: const EdgeInsets.only(top: Spacing.xxSmall),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : const FormNote(
                                  children: [
                                    CustomText(
                                      title:
                                          'Pengiriman tunai di atas pukul 18.30 WIB akan diproses pada hari berikutnya.',
                                    ),
                                    CustomText(title: 'Maksiman nominal transfer sebesar Rp5.000.000,-/transaksi.'),
                                    CustomText(title: 'Dapatkan 100 poin untuk setiap transaksi transfer uang.')
                                  ],
                                ),
                          const Spacer(),
                          controller.showReceipent.value
                              ? CustomButton(
                                  title: 'Lanjut',
                                  onPress: controller.onNext,
                                )
                              : Align(
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: [
                                      const CustomText(
                                        title: 'Didukung oleh',
                                        size: FontSize.xxSmall,
                                        color: WispayColors.cBlack,
                                        textType: TextType.SemiBold,
                                      ),
                                      CustomImage(
                                        source: Assets.imagesXfers.path,
                                        height: 23,
                                        width: 55,
                                      )
                                    ],
                                  ),
                                ),
                        ],
                      ),
                    ),
                  ),
          ),
        ),
      ),
    );
  }

  Widget _dropdownField({
    title,
    ValueChanged<String>? onSearch,
    ValueChanged<BaseModel>? onSelect,
    bool enabled = true,
    required List<BaseModel> items,
    GlobalKey<FormFieldState<BaseModel>>? key,
    BaseModel? selectedItem,
    String? label,
    void Function()? onClose,
    void Function()? onReachedEnd,
  }) {
    return FormField(
      validator: (BaseModel? value) {
        if (value?.id == null) {
          return 'Silahkan pilih $title';
        }
        return null;
      },
      enabled: enabled,
      key: key,
      onSaved: (BaseModel? newValue) => onSelect?.call(newValue!),
      builder: (FormFieldState<BaseModel> field) => Dropdown(
        errorText: field.errorText,
        value: field.value?.name ?? "",
        label: label,
        enabled: enabled,
        onTap: () => WispayBottomSheet.scrollableList<BaseModel>(
          onSearch: onSearch,
          title: '$title',
          items: items,
          selectedItem: selectedItem!,
          onClose: onClose,
          onReachedEnd: onReachedEnd,
          onSelect: (idx) {
            FocusManager.instance.primaryFocus?.unfocus();
            field.didChange(items[idx]);
            key?.currentState?.save();
            Get.back();
          },
        ),
      ),
    );
  }
}
