import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/send_money/controllers/payment_method_controller.dart';

class ConfirmBankView extends GetView {
  const ConfirmBankView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PaymentMethodController controller = Get.find();
    final arguments = Get.arguments;
    final order = arguments['order'];
    final selectedBank = arguments['selectedBank'];
    final wispayBank = arguments['wispayBank'];

    return Scaffold(
      appBar: AppBar(
        title: const Text('Pembayaran'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: BaseCard(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const CustomText(title: 'Metode Bayar', textType: TextType.SemiBold),
                      const SizedBox(height: Spacing.small),
                      Row(
                        children: [
                          wispayBank.logo?.url != null
                              ? Image.network(wispayBank.logo?.url, width: 32, height: 32)
                              : const SizedBox(),
                          const SizedBox(width: Spacing.medium),
                          CustomText(
                            title: 'Transfer ${wispayBank.bankName}',
                            textType: TextType.Medium,
                            color: WispayColors.cBlack,
                            size: FontSize.medium,
                          ),
                        ],
                      ),
                      const Divider(thickness: 1).marginSymmetric(vertical: Spacing.small),
                      TrxDetailItem(title: 'Nominal Transfer', value: NumberHelper.formatMoney(order.price)),
                      TrxDetailItem(title: 'Biaya Admin', value: NumberHelper.formatMoney(order.adminFee)),
                      TrxDetailItem(title: 'Total Transfer', value: NumberHelper.formatMoney(order.total)),
                      const Divider(thickness: 1).marginSymmetric(vertical: Spacing.small),
                      const CustomText(title: 'Rekening Penerima', textType: TextType.SemiBold),
                      const SizedBox(height: Spacing.small),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          selectedBank.image?.url != null
                              ? Image.network(selectedBank.image?.url, width: 32, height: 32)
                              : const SizedBox(),
                          SizedBox(width: selectedBank.image?.url != null ? Spacing.medium : 0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText(
                                title: '${selectedBank.name} - ${decrypt(order.detailPayload.name)}',
                                textType: TextType.Medium,
                                color: WispayColors.cBlack,
                                size: FontSize.medium,
                              ),
                              const SizedBox(height: Spacing.xSmall),
                              CustomText(title: decrypt(order.customerId), textType: TextType.Medium),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            CustomButton(
              title: 'Lanjut Bayar',
              onPress: () => controller.confirmBank(order),
            ).marginAll(Spacing.defaultSpacing)
          ],
        ),
      ),
    );
  }
}
