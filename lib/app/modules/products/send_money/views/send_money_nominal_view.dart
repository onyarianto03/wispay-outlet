import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/utils/input_price_formatter.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/send_money/controllers/send_money_nominal_controller.dart';

class SendMoneyNominalView extends GetView<SendMoneyNominalController> {
  const SendMoneyNominalView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text('Atur Nominal'),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.chevron_left,
              size: 35,
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: SafeArea(
          child: Container(
            color: WispayColors.cWhite,
            padding: const EdgeInsets.all(Spacing.defaultSpacing),
            child: Obx(
              () => Form(
                autovalidateMode: AutovalidateMode.disabled,
                key: controller.formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const CustomText(
                      title: 'Rekening Tujuan',
                      textType: TextType.SemiBold,
                    ),
                    const SizedBox(height: Spacing.small),
                    _rekeningTujuan(),
                    const SizedBox(height: Spacing.defaultSpacing),
                    CustomTextInput(
                      label: 'Nominal Transfer',
                      hintText: ' ',
                      value: controller.nominal.value,
                      keyboardType: TextInputType.number,
                      validator: controller.nominalValidator,
                      textEditingController: controller.nominalController,
                      onClear: controller.nominalController?.clear,
                      inputFormatters: [InputPriceFormatter()],
                    ),
                    const SizedBox(height: Spacing.defaultSpacing),
                    CustomTextInput(
                      label: 'Catatan',
                      hintText: '(opsional)',
                      value: controller.note.value,
                      keyboardType: TextInputType.multiline,
                      textEditingController: controller.noteController,
                      maxLines: 3,
                    ),
                    const SizedBox(height: Spacing.defaultSpacing),
                    const Spacer(),
                    CustomButton(
                      title: 'Kirim Uang',
                      onPress: controller.sendMoney,
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  BaseCard _rekeningTujuan() {
    final arguments = controller.arguments;
    final inquiry = arguments['inquiry'];
    final selectedBank = arguments['selectedBank'];

    return BaseCard(
      margin: const EdgeInsets.all(0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          selectedBank.image?.url != null
              ? Image.network(selectedBank.image?.url, width: 32, height: 32)
              : const SizedBox(),
          SizedBox(width: selectedBank.image?.url != null ? Spacing.defaultSpacing : 0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                title: '${selectedBank.name} - ${decrypt(inquiry.accountName)}',
                textType: TextType.SemiBold,
                size: FontSize.medium,
                color: WispayColors.cBlack,
              ),
              const SizedBox(height: Spacing.xSmall),
              CustomText(
                title: decrypt(inquiry.accountNo),
              )
            ],
          )
        ],
      ),
    );
  }
}
