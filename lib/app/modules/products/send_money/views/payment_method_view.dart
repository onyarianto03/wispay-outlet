import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/card_menu_widget.dart';
import 'package:wispay_outlet/app/modules/products/send_money/controllers/payment_method_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class PaymentMethodView extends GetView<PaymentMethodController> {
  const PaymentMethodView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Metode Bayar'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: Obx(
          () => controller.isLoading.value
              ? Center(child: Lottie.asset(Assets.lottieLoading, width: 100))
              : Container(
                  padding: const EdgeInsets.all(Spacing.defaultSpacing),
                  child: ListView.separated(
                    separatorBuilder: (ctx, idx) => const SizedBox(
                      height: Spacing.defaultSpacing,
                    ),
                    itemCount: controller.wispayBanks.length,
                    itemBuilder: (ctx, idx) {
                      final item = controller.wispayBanks[idx];
                      final walletBalance = controller.homeController.profile.value.outletWallet!.total;
                      final isWispayWallet = item.code == 'Wispay';
                      final isSaldoNotEnough =
                          double.parse(walletBalance) < double.parse(controller.arguments['nominal']);
                      final walletBalance_ = NumberHelper.formatMoney(walletBalance);

                      return CardMenuWidget(
                        menu: [
                          CardMenu(
                            title: item.bankName!,
                            icon: '',
                            onPress: () => controller.onSelectBank(item),
                            subTitleColor: isSaldoNotEnough ? WispayColors.cRed2 : WispayColors.cPrimary,
                            subTitle: isWispayWallet
                                ? isSaldoNotEnough
                                    ? 'Saldo tidak mencukupi $walletBalance_'
                                    : walletBalance_
                                : null,
                          ),
                        ],
                      );
                    },
                  ),
                ),
        ),
      ),
    );
  }
}
