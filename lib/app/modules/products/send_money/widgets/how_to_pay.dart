import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/global_widgets/countdown/countdown.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/send_money/controllers/payment_instructions_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class HowToPay extends GetView<PaymentInstructionsController> {
  const HowToPay({
    Key? key,
    required this.data,
  }) : super(key: key);

  final TransactionDataModel data;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const CustomText(
              title: 'Batas Pembayaran',
            ),
            CountDownTimer(
              secondsRemaining: DateTime.parse(data.remittance!.expiredAt!)
                  .difference(
                    DateTime.now(),
                  )
                  .inSeconds,
              whenTimeExpires: () => {},
              countDownFormatter: _countdownFormatter,
              countDownTimerStyle: const TextStyle(
                color: WispayColors.cBlack,
                fontWeight: FontWeight.w600,
              ),
            )
          ],
        ),
        const SizedBox(height: Spacing.small),
        const Divider(thickness: 1),
        const SizedBox(height: Spacing.small),
        const CustomText(title: 'Nomor Virtual Account'),
        const SizedBox(height: Spacing.xSmall),
        Row(
          children: [
            data.remittance?.wispayBank?.logo?.url != null
                ? Image.network(data.remittance!.wispayBank!.logo!.url!, width: 32, height: 32)
                : const SizedBox(),
            SizedBox(width: data.remittance?.wispayBank?.logo?.url != null ? Spacing.defaultSpacing : 0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    title: data.remittance!.wispayBank!.number!,
                    textType: TextType.SemiBold,
                    size: FontSize.medium,
                    color: WispayColors.cBlack,
                  ),
                  const SizedBox(height: Spacing.xSmall),
                  CustomText(
                    title: data.remittance!.wispayBank!.name!,
                    size: FontSize.small,
                  ),
                ],
              ),
            ),
            const SizedBox(width: Spacing.defaultSpacing),
            GestureDetector(
              onTap: () => _copyToClipboard(context, data.remittance!.wispayBank!.number!),
              child: const CustomText(
                title: 'Salin Nomor',
                color: WispayColors.cPrimary,
                textType: TextType.SemiBold,
              ),
            ),
          ],
        ),
        const SizedBox(height: Spacing.small),
        const Divider(thickness: 1),
        const SizedBox(height: Spacing.small),
        const CustomText(title: 'Jumlah Transfer'),
        const SizedBox(height: Spacing.xSmall),
        ExpandablePanel(
          collapsed: const SizedBox(),
          controller: controller.nominalController,
          header: Row(
            children: [
              CustomText(
                title: NumberHelper.formatMoney(data.total),
                textType: TextType.SemiBold,
                color: WispayColors.cBlack,
                size: FontSize.medium,
              ),
              const SizedBox(width: Spacing.defaultSpacing),
              RotationTransition(
                turns: Tween(begin: 0.0, end: 1.0).animate(controller.animationController),
                child: const Icon(Icons.keyboard_arrow_up, color: WispayColors.cBlack666),
              ),
              const Spacer(),
              GestureDetector(
                onTap: () => _copyToClipboard(context, data.total, title: 'Jumlah transfer'),
                child: const CustomText(
                  title: 'Salin Jumlah',
                  color: WispayColors.cPrimary,
                  textType: TextType.SemiBold,
                ),
              ),
            ],
          ),
          expanded: Column(
            children: [
              const SizedBox(height: Spacing.small),
              Row(children: [
                const CustomText(title: 'Nominal Transfer'),
                const Spacer(),
                CustomText(
                  title: NumberHelper.formatMoney(data.subtotal),
                  textType: TextType.SemiBold,
                  color: WispayColors.cBlack,
                ),
              ]),
              const SizedBox(height: Spacing.xSmall),
              Row(children: [
                const CustomText(title: 'Biaya Admin'),
                const Spacer(),
                CustomText(
                  title: NumberHelper.formatMoney(data.adminFee),
                  textType: TextType.SemiBold,
                  color: WispayColors.cBlack,
                ),
              ]),
            ],
          ),
          theme: const ExpandableThemeData(
            hasIcon: false,
            useInkWell: true,
          ),
        ),
        const SizedBox(height: Spacing.small),
        Row(
          children: [
            Lottie.asset(Assets.lottieInfo, width: 24, height: 24),
            const SizedBox(width: Spacing.xSmall),
            Expanded(
                child: Text.rich(
              TextSpan(
                text: 'Total bayar sudah termasuk biaya layanan ',
                children: [
                  TextSpan(
                    text: NumberHelper.formatMoney('1000'),
                    style: const TextStyle(fontWeight: FontWeight.w600),
                  ),
                ],
              ),
            )),
          ],
        )
      ],
    );
  }

  _countdownFormatter(int seconds) {
    final hours = (seconds / 3600).truncate();
    seconds = (seconds % 3600).truncate();
    final minutes = (seconds / 60).truncate() % 60;
    final secondsStr = (seconds % 60);

    if (hours == 0) {
      return '$minutes menit $secondsStr detik';
    }

    return '$hours jam $minutes menit $secondsStr detik';
  }

  void _copyToClipboard(BuildContext context, String text, {String title = 'Nomor Virtual Account'}) {
    Clipboard.setData(ClipboardData(text: text)).then((_) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(title + ' berhasil disalin'),
        backgroundColor: WispayColors.cSecondary,
        duration: const Duration(seconds: 2),
      ));
    });
  }
}
