import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/models/wispay_bank_model/wispay_bank_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/data/repositories/utility_repository.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/products/send_money/views/payment_instructions_view.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/products/send_money/widgets/status_card.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class PaymentMethodController extends GetxController {
  final UtilityRepository _utilityRepository = UtilityRepository(Get.find());
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final CategoriesController homeController = Get.find();

  final Map<String, dynamic> arguments = Get.arguments;
  final wispayBanks = <WispayBankModel>[].obs;
  final isLoading = false.obs;
  String token = '';

  @override
  void onInit() {
    super.onInit();
    getWispayBanks();
  }

  @override
  void onClose() {}

  void getWispayBanks() async {
    isLoading.value = true;
    final _response = await _utilityRepository.getWispayBanks(URL_WISPAY_BANK_REMITTANCE);
    if (_response.success != false) {
      wispayBanks.value = _response.data!;
    }
    isLoading.value = false;
  }

  void onSelectBank(WispayBankModel bank) async {
    final isSaldoNotEnough =
        double.parse(arguments['nominal']) > double.parse(homeController.profile.value.outletWallet!.total);

    if (bank.code == 'Wispay') {
      !isSaldoNotEnough ? showModal(bank) : null;
    } else {
      final inquiry = arguments['inquiry'];
      final _body = {
        'product_id': arguments['product'].id,
        'phone': homeController.profile.value.phone,
        'customer_id': inquiry.accountNo,
        'nominal': arguments['nominal'],
        'wispay_bank_id': bank.id,
        'note': arguments['note'],
      };

      final _order = await _transactionRepository.order(
        supplier: 'wispay',
        productName: 'remittance',
        body: RequestBodyModel.fromJson(_body),
      );

      if (_order.success!) {
        final _arguments = {
          'order': _order.data,
          'selectedBank': arguments['selectedBank'],
          'wispayBank': bank,
        };
        Get.toNamed(Routes.SEND_MONEY_CONFIRM_BANK, arguments: _arguments);
      }
    }
  }

  void confirmBank(ProductModel data) async {
    final body = {
      "token": data.meta!.token!,
    };

    final _confirm = await _transactionRepository.confirm(
      supplier: 'wispay',
      productName: 'remittance',
      body: RequestBodyModel.fromJson(body),
      withPaymentDialog: false,
      withLoadingDialog: true,
    );

    if (_confirm.success!) {
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => const PaymentInstructionsView(),
          settings: RouteSettings(arguments: _confirm.data),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void _next(String pin) async {
    final body = {
      "pin": pin,
      "token": token,
    };

    final _confirm = await _transactionRepository.confirm(
      supplier: 'wispay',
      productName: 'remittance',
      body: RequestBodyModel.fromJson(body),
    );

    if (_confirm.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(
              data: _confirm.data!,
              bankName: arguments['selectedBank'].name,
            ),
            transactionId: _confirm.data?.id,
            productCode: ProductCode.REMITTANCE,
            status: _confirm.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': homeController.profile.value.phone},
    );
  }

  void showModal(WispayBankModel bank) async {
    final inquiry = arguments['inquiry'];
    final _body = {
      'product_id': arguments['product'].id,
      'phone': homeController.profile.value.phone,
      'customer_id': inquiry.accountNo,
      'nominal': arguments['nominal'],
      'wispay_bank_id': bank.id,
      'note': arguments['note'],
    };

    final _order = await _transactionRepository.order(
      supplier: 'wispay',
      productName: 'remittance',
      body: RequestBodyModel.fromJson(_body),
    );

    if (_order.success!) {
      token = _order.data!.meta!.token!;
      final commissionSetting = homeController.profile.value.comissionSetting;
      double comission = double.parse(commissionSetting?.kirimUang ?? commissionSetting?.all ?? '0');

      WispayBottomSheet.showConfirm(
        title: 'Kirim Uang',
        onConfirm: () => onPay(),
        totalTitle: 'Total',
        totalPrice: NumberHelper.formatMoney(_order.data!.total!),
        withPromo: false,
        children: [
          const SizedBox(height: Spacing.small),
          comission > 0
              ? InfoWidget(
                  text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                    comission.toString(),
                  )}',
                  margin: const EdgeInsets.only(top: 5),
                )
              : const SizedBox(height: 0),
          const SizedBox(height: Spacing.defaultSpacing),
          TrxDetailItem(
            title: 'Rekening Tujuan',
            value: '${arguments['selectedBank'].name} - ${decrypt(_order.data!.customerId!)}',
          ),
          TrxDetailItem(title: 'Pemilik Rekening', value: decrypt(_order.data!.detailPayload!.name!)),
          TrxDetailItem(title: 'Nominal Transfer', value: NumberHelper.formatMoney(arguments['nominal'])),
          TrxDetailItem(title: 'Biaya Admin', value: NumberHelper.formatMoney(_order.data!.adminFee!)),
        ],
      );
    }
  }
}
