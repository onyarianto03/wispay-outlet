import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/remittance_inquiry_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class SendMoneyController extends GetxController {
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final ProductController _productController = Get.find();
  TextEditingController? rekeningController;

  final int categoryId = Get.arguments;
  final bankList = <BaseModel>[].obs;

  final formKey = GlobalKey<FormState>();
  final dropdownKey = GlobalKey<FormFieldState<BaseModel>>();

  final selectedBank = const BaseModel().obs;
  final product = ProductModel().obs;
  final rekening = ''.obs;
  final search = ''.obs;
  final isLoading = false.obs;
  final isLoadMore = false.obs;
  final isSubmitting = false.obs;
  final showReceipent = false.obs;
  final inquiryData = RemittanceInquiryModel().obs;

  int bankPage = 1;
  bool _isLastPage = false;

  final rekeningValidator = MultiValidator([
    RequiredValidator(errorText: 'Nomor Rekening Tujuan wajib diisi'),
    MaxLengthValidator(15, errorText: 'Maksimal 15 digit'),
  ]);

  @override
  void onInit() {
    super.onInit();
    rekeningController = TextEditingController()..addListener(onChangeRekening);
    isLoading.value = true;
    getInitialData();
  }

  @override
  void onClose() {
    rekeningController?.removeListener(onChangeRekening);
  }

  void getInitialData() async {
    final _banks = await getProductBrands(page: 1);
    if (_banks != null) bankList.value = _banks;
    isLoading.value = false;
  }

  Future<List<ProductBrandModel>?> getProductBrands({required int page, String? query}) async {
    final _filter = {
      'page': page,
      'q': query,
    };
    final _banks = await _productController.getProductBrands(categoryId, filter: FilterModel.fromJson(_filter));
    return _banks;
  }

  void onSearch(String query) async {
    search.value = query;
    getProductSearch(query);
  }

  void getProductSearch(String query) async {
    final _banks = await getProductBrands(page: 1, query: query);
    if (_banks != null) bankList.value = _banks;
  }

  void onLoadMore() async {
    if (_isLastPage) return;
    bankPage++;
    isLoadMore.value = true;
    final _banks = await getProductBrands(page: bankPage, query: search.value);
    if (_banks != null) {
      _banks.isEmpty ? _isLastPage = true : bankList.addAll(_banks);
    }
    isLoadMore.value = false;
  }

  void onSelectBank(BaseModel item) async {
    selectedBank.value = item;
    showReceipent.value = false;
  }

  void onChangeRekening() {
    rekening.value = rekeningController?.text ?? '';
    showReceipent.value = false;
  }

  void checkRekening() async {
    FocusManager.instance.primaryFocus?.unfocus();
    final isValid = formKey.currentState!.validate();
    if (isValid) {
      final _body = {
        'accountNo': encrypt(rekening.value),
        'bankShortCode': selectedBank.value.code,
      };
      isSubmitting.value = true;

      final _subcategory = await _productController.getSubCategories(selectedBank.value.id);
      if (_subcategory != null) {
        final _products = await _productController.getProducts(_subcategory.first.id);
        if (_products != null) {
          product.value = _products.first;
          final _inquiry = await _transactionRepository.inquiry(
            supplier: 'wispay',
            productName: 'remittance',
            body: RequestBodyModel.fromJson(_body),
          );
          if (_inquiry.success != false) {
            inquiryData.value = _inquiry.data!;
            showReceipent.value = true;
          }
        }
      }
      isSubmitting.value = false;
    }
  }

  void onNext() {
    final arguments = {
      'selectedBank': selectedBank.value,
      'inquiry': inquiryData.value,
      'product': product.value,
    };
    Get.toNamed(Routes.SEND_MONEY_NOMINAL, arguments: arguments);
  }
}
