import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';

class PaymentInstructionsController extends GetxController with SingleGetTickerProviderMixin {
  final TransactionDataModel? arguments = Get.arguments;
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  ExpandableController nominalController = ExpandableController();
  late AnimationController animationController = AnimationController(
    vsync: this,
    duration: const Duration(milliseconds: 300),
    upperBound: 0.5,
  );

  final transaction = const TransactionDataModel().obs;
  final isLoading = true.obs;

  @override
  void onInit() {
    super.onInit();
    nominalController.addListener(onChangeNominal);
    animationController.forward(from: 0.0);
    getDetail();
  }

  @override
  void onClose() {
    super.onClose();
    nominalController.removeListener(onChangeNominal);
  }

  void getDetail() async {
    if (arguments?.id != null) {
      isLoading.value = true;
      final _detail = await _transactionRepository.getDetail(id: arguments?.id);
      isLoading.value = false;
      if (_detail.success!) {
        transaction.value = _detail.data!;
      }
    }
  }

  void onChangeNominal() {
    nominalController.value ? animationController.reverse(from: 0.5) : animationController.forward(from: 0.0);
  }
}
