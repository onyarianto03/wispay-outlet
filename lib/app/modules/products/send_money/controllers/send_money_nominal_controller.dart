import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class SendMoneyNominalController extends GetxController {
  TextEditingController? nominalController;
  TextEditingController? noteController;
  final formKey = GlobalKey<FormState>();
  final Map<String, dynamic> arguments = Get.arguments;

  var nominal = ''.obs;
  var note = ''.obs;
  var isSubmitting = false.obs;

  final nominalValidator = MultiValidator([
    RequiredValidator(errorText: 'Nominal wajib diisi'),
    MinLengthValidator(6, errorText: 'Nominal minimal Rp10.000'),
  ]);

  @override
  void onInit() {
    super.onInit();
    nominalController = TextEditingController()..addListener(onChangeNominal);
    noteController = TextEditingController()..addListener(onChangeNote);
  }

  @override
  void onClose() {
    nominalController?.removeListener(onChangeNominal);
    noteController?.removeListener(onChangeNote);
  }

  void onChangeNominal() => nominal.value = nominalController?.text ?? '';
  void onChangeNote() => nominal.value = nominalController?.text ?? '';

  void sendMoney() {
    final bool isValid = formKey.currentState!.validate();
    FocusManager.instance.primaryFocus!.unfocus();

    if (isValid) {
      final _arguments = {
        'nominal': nominal.value.replaceAll('.', ''),
        'note': note.value,
      };
      arguments.addAll(_arguments);

      Get.toNamed(Routes.SEND_MONEY_PAYMENT_METHOD, arguments: arguments);
    }
  }

  void clear() => nominal.value = '';
}
