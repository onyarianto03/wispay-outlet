import 'package:get/get.dart';
import 'package:wispay_outlet/app/modules/products/send_money/controllers/payment_instructions_controller.dart';

import 'package:wispay_outlet/app/modules/products/send_money/controllers/payment_method_controller.dart';
import 'package:wispay_outlet/app/modules/products/send_money/controllers/send_money_nominal_controller.dart';

import '../controllers/send_money_controller.dart';

class SendMoneyBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PaymentInstructionsController>(
      () => PaymentInstructionsController(),
    );
    Get.lazyPut<PaymentMethodController>(
      () => PaymentMethodController(),
    );
    Get.lazyPut<SendMoneyNominalController>(
      () => SendMoneyNominalController(),
    );
    Get.lazyPut<SendMoneyController>(
      () => SendMoneyController(),
    );
  }
}
