import 'package:get/get.dart';

import '../controllers/sell_out_controller.dart';

class SellOutBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SellOutController>(
      () => SellOutController(),
    );
  }
}
