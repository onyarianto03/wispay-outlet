import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/core/values/strings.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/products/sell_out/views/sell_out_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/app/views/qr_scanner_view.dart';

class SellOutController extends GetxController with SingleGetTickerProviderMixin {
  final methodList = ['ONLINE', 'OFFLINE'];

  late TabController tController;
  final TransactionRepository _repo = TransactionRepository(Get.find());

  GlobalKey<FormState> firstKey = GlobalKey<FormState>();
  GlobalKey<FormState> secondKey = GlobalKey<FormState>();

  TextEditingController tImeiFirst = TextEditingController();
  TextEditingController tImeiSecond = TextEditingController();
  TextEditingController tCode = TextEditingController();
  final CategoriesController _categoriesController = Get.find();

  final _selectedIndex = 0.obs;

  final imeiFirst = ''.obs;
  final imeiSecond = ''.obs;
  final code = ''.obs;
  final isEnableFirst = true.obs;
  final isEnableSecond = false.obs;
  final kind = SELLOUT_KIND_HANDPHONE.obs;
  final selectedMethod = ''.obs;

  @override
  void onInit() {
    super.onInit();
    tController = TabController(length: 2, vsync: this);
    tController.addListener(_tabListener);

    tImeiFirst.addListener(onchangeImeiFirst);
    tImeiSecond.addListener(onchangeImeiSecond);
    tCode.addListener(onchangeCode);
  }

  @override
  void onClose() {}

  void onchangeForm() {
    if (_selectedIndex.value == 0) {
      kind.value = SELLOUT_KIND_HANDPHONE;
      bool isValid = firstKey.currentState!.validate();
      if (isValid) {
        isEnableFirst.value = true;
      } else {
        isEnableFirst.value = false;
      }
    } else {
      kind.value = SELLOUT_KIND_MODEM;
      bool isValid = secondKey.currentState!.validate();
      if (isValid) {
        isEnableSecond.value = true;
      } else {
        isEnableSecond.value = false;
      }
    }
  }

  void _tabListener() {
    selectedMethod.value = '';
    if (!tController.indexIsChanging) {
      _selectedIndex.value = tController.index;
    }
  }

  void onchangeImeiFirst() => imeiFirst.value = tImeiFirst.text;
  void onchangeImeiSecond() => imeiSecond.value = tImeiSecond.text;
  void onchangeCode() => code.value = tCode.text;

  void onScanQR(TextEditingController conn) {
    Get.to(() => const QrScannerView())!.then((value) {
      final barcode = value as Barcode;
      conn.text = barcode.code!;
    });
  }

  void onSubmit() {
    if (_selectedIndex.value == 0) {
      bool isValid = firstKey.currentState!.validate();
      if (isValid) {
        onPay();
      }
    } else {
      bool isValid = secondKey.currentState!.validate();
      if (isValid) {
        onPay();
      }
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': _categoriesController.profile.value.phone},
    );
  }

  void _next(String pin) async {
    Map<String, dynamic> data = {
      'pin': pin,
      'kind': kind.value,
      'imei': _selectedIndex.value == 0 ? encrypt(imeiFirst.value) : encrypt(imeiSecond.value),
      'sim_card': _selectedIndex.value == 0 ? encrypt(code.value) : null,
      'status_channel': selectedMethod.value,
    };

    final _response = await _repo.createSellOut(data: RequestBodyModel.fromJson(data));

    if (_response.success == true) {
      Get.off(() => SellOutStatusView(sellOutStatus: _response.data?.status == 'FAILED' ? false : true));
    }
  }

  void onSelectMethod() {
    WispayBottomSheet.show(
      title: 'Metode Belanja',
      children: [
        ListView.separated(
          shrinkWrap: true,
          itemCount: methodList.length,
          separatorBuilder: (context, index) => const Divider(color: Colors.grey),
          itemBuilder: (_, idx) => ListTile(
            dense: true,
            visualDensity: const VisualDensity(horizontal: -2, vertical: -2),
            contentPadding: EdgeInsets.zero,
            onTap: () {
              selectedMethod.value = methodList[idx];
              Get.back();
            },
            title: CustomText(
              title: GetUtils.capitalizeFirst(methodList[idx]) ?? "",
              textType: TextType.SemiBold,
            ),
            trailing: Visibility(
              child: const Icon(
                Icons.check_circle,
                color: WispayColors.cGreen2,
                size: 20,
              ),
              visible: selectedMethod.value == methodList[idx],
            ),
          ),
        ),
        const SizedBox(height: Spacing.defaultSpacing),
      ],
    );
  }
}
