import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/products/sell_out/controllers/sell_out_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';

class HPForm extends GetView<SellOutController> {
  const HPForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      child: Obx(
        () => Form(
          key: controller.firstKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: Column(
            children: [
              CustomTextInput(
                value: controller.code.value,
                textEditingController: controller.tImeiFirst,
                label: 'Nomor IMEI',
                hintText: 'nomor IMEI',
                autovalidateMode: AutovalidateMode.onUserInteraction,
                withCustomSuffixIcon: true,
                suffixIcon: InkWell(
                  onTap: () => controller.onScanQR(controller.tImeiFirst),
                  radius: 20,
                  child: Padding(
                    padding: const EdgeInsets.all(Spacing.medium),
                    child: Assets.iconsQrInput.image(width: 20, height: 20),
                  ),
                ),
                validator: RequiredValidator(errorText: 'nomor IMEI tidak boleh kosong'),
                margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
              ),
              CustomTextInput(
                value: controller.code.value,
                textEditingController: controller.tCode,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                label: 'Kode Kartu Perdana',
                hintText: 'kode kartu perdana',
                withCustomSuffixIcon: true,
                suffixIcon: InkWell(
                  onTap: () => controller.onScanQR(controller.tCode),
                  radius: 20,
                  child: Padding(
                    padding: const EdgeInsets.all(Spacing.medium),
                    child: Assets.iconsQrInput.image(width: 20, height: 20),
                  ),
                ),
                validator: RequiredValidator(errorText: 'Kode Kartu Perdana tidak boleh kosong'),
                margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
              ),
              Dropdown(
                value: GetUtils.capitalizeFirst(controller.selectedMethod.value) ?? "",
                onTap: () => controller.onSelectMethod(),
                label: 'Pilih Metode Pembayaran',
                topLabel: 'Metode Pembayaran',
              ),
              const SizedBox(height: Spacing.defaultSpacing),
              CustomButton(
                title: 'Simpan Penjualan',
                onPress: () => controller.onSubmit(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
