import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class SellOutStatusView extends StatelessWidget {
  const SellOutStatusView({
    Key? key,
    required this.sellOutStatus,
  }) : super(key: key);

  final bool sellOutStatus;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text(' '),
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Spacer(),
              AutoSizeText.rich(
                TextSpan(
                  text: 'Pencatatan ',
                  children: [
                    TextSpan(
                      text: sellOutStatus ? 'Berhasil!' : 'Gagal',
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                style: const TextStyle(fontSize: FontSize.medium),
              ),
              const SizedBox(height: Spacing.defaultSpacing),
              if (sellOutStatus)
                Assets.iconsTrxStatusSuccess.image(width: 100)
              else
                Assets.iconsTrxStatusFailed.image(width: 100),
              const SizedBox(height: Spacing.defaultSpacing),
              if (sellOutStatus)
                const CustomText(
                  title: 'Pencatatan paket bundling Telkomsel berhasil dilakukan.',
                  textAlign: TextAlign.center,
                )
              else
                const CustomText(
                  title: 'Nomor IMEI atau kode kartu tidak valid.\nSilakan coba lagi.',
                  textAlign: TextAlign.center,
                ),
              const Spacer(),
              CustomButton(
                title: sellOutStatus ? 'Oke' : 'Coba lagi',
                onPress: () {
                  if (!sellOutStatus) {
                    Get.offNamed(Routes.SELL_OUT);
                  } else {
                    Get.offNamed(Routes.HOME);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
