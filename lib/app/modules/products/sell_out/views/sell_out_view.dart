import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/global_widgets/top_tab_bar.dart';
import 'package:wispay_outlet/app/modules/products/sell_out/widgets/hp_form.dart';
import 'package:wispay_outlet/app/modules/products/sell_out/widgets/modem_form.dart';

import '../controllers/sell_out_controller.dart';

class SellOutView extends GetView<SellOutController> {
  const SellOutView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sell Out'),
        leading: IconButton(
          icon: const Icon(Icons.chevron_left, size: 35),
          onPressed: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: DefaultTabController(
          length: 2,
          child: Column(
            children: [
              TopTapBar(
                controller: controller.tController,
                tabs: const [
                  Tab(text: 'Hp & Tablet'),
                  Tab(text: 'Modem'),
                ],
              ),
              Expanded(
                child: TabBarView(
                  controller: controller.tController,
                  children: const [
                    HPForm(),
                    ModemForm(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
