import 'package:get/get.dart';

import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/asuransi_gadget_form_controller.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/confirm_controller.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/detail_premi_controller.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/select_gadget_controller.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/select_premi_controller.dart';

import '../controllers/asuransi_gadget_controller.dart';

class AsuransiGadgetBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ConfirmController>(
      () => ConfirmController(),
      fenix: true,
    );
    Get.lazyPut<DetailPremiController>(
      () => DetailPremiController(),
      fenix: true,
    );
    Get.lazyPut<SelectPremiController>(
      () => SelectPremiController(),
      fenix: true,
    );
    Get.lazyPut<AsuransiGadgetFormController>(
      () => AsuransiGadgetFormController(),
      fenix: true,
    );
    Get.lazyPut<SelectGadgetController>(
      () => SelectGadgetController(),
    );
    Get.lazyPut<AsuransiGadgetController>(
      () => AsuransiGadgetController(),
      fenix: true,
    );
  }
}
