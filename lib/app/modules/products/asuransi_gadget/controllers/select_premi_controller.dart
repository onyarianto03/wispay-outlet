import 'package:get/get.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/insurance_repository.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/select_gadget_controller.dart';

class SelectPremiController extends GetxController {
  final data = <ProductModel>[].obs;
  final SelectGadgetController _selectGadgetController = Get.find();
  final InsuranceRepository _repo = InsuranceRepository(Get.find());

  @override
  void onInit() {
    super.onInit();
    _getPremiList();
  }

  @override
  void onClose() {}

  void _getPremiList() async {
    const _suplier = 'we-plus';
    const _productName = 'insurance-gadget';
    final RequestBodyModel _body = RequestBodyModel(
      insuranceDeviceCategoryId: _selectGadgetController.selectedTypeGadget.value.id,
      gadgetBrand: _selectGadgetController.selectedMerkGadget.value,
      gadgetName: _selectGadgetController.selectedModelGadget.value.gadgetName,
    );
    final _response = await _repo.inquiry(supplier: _suplier, productName: _productName, body: _body);

    if (_response.success == true) {
      data.assignAll(_response.data!);
    }
  }
}
