import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

const BENEFIT_IDX = 0;
const CLAIM_IDX = 1;

class AsuransiGadgetController extends GetxController with SingleGetTickerProviderMixin {
  late PageController pageController = PageController();

  final selectedIndex = BENEFIT_IDX.obs;
  final containerHeight = 270.0.obs;
  final buttonText = 'Cek Premi'.obs;
  final buttonAction = VoidCallback.obs;

  @override
  void onInit() {
    ever<int>(selectedIndex, (val) {
      if (val == BENEFIT_IDX) {
        buttonText.value = 'Cek Premi';
        containerHeight.value = 270;
      } else if (val == CLAIM_IDX) {
        buttonText.value = 'Ajukan Klaim';
        containerHeight.value = 500;
      }
    });
    super.onInit();
  }

  @override
  void onClose() {}

  void onChangePage(idx) {
    selectedIndex.value = idx;
    pageController.animateToPage(
      idx,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeOut,
    );
  }

  void onButtonPress() {
    if (selectedIndex.value == BENEFIT_IDX) {
      Get.toNamed(Routes.ASURANSI_GADGET_SELECT_WIDGET);
    } else {}
  }

  List<Widget> buildMenuItem(List<CardMenu> items) {
    return items
        .map(
          (e) => Column(
            children: [
              Row(
                children: [
                  Image.asset(
                    e.icon,
                    width: 40,
                    height: 40,
                  ),
                  const SizedBox(width: Spacing.defaultSpacing),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomText(
                          title: e.title,
                          textType: TextType.SemiBold,
                          color: WispayColors.cBlack,
                        ),
                        CustomText(
                          title: e.subTitle!,
                          textType: TextType.Regular,
                          color: WispayColors.cGrey82,
                          size: FontSize.small,
                          textStyle: const TextStyle(height: 1.4),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(height: Spacing.defaultSpacing)
            ],
          ),
        )
        .toList();
  }
}
