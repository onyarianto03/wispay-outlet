import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wispay_outlet/app/core/helpers/date_helper.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/repositories/insurance_repository.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:dio/dio.dart' as dio;

final List<Map<String, dynamic>> _steps = [
  {"title": "Data Pelanggan", "subTitle": "Next : Alamat Pelanggan", "progress": 0.25, "centerText": "1/4"},
  {"title": "Alamat Pengguna", "subTitle": "Next : Nomor IMEI", "progress": 0.50, "centerText": "2/4"},
  {"title": "Nomor IMEI", "subTitle": "Next : Pilih Premi", "progress": 0.75, "centerText": "3/4"},
  {"title": "Foto Gadget", "subTitle": "Next : Konfirmasi", "progress": 1.0, "centerText": "4/4"},
];

enum IMAGE_TYPE { FRONT, BACK }

class AsuransiGadgetFormController extends GetxController {
  final PageController pageController = PageController();
  final InsuranceRepository _repo = InsuranceRepository(Get.find());
  final ImagePicker _picker = ImagePicker();

  final nextButtonEnabled = false.obs;
  final nextButtonEnable = [false, false, false, false].obs;
  final ProductModel premiData = Get.arguments;
  final formSteps = _steps;

  final cCompleteName = TextEditingController();
  final cKtp = TextEditingController();
  final cEmail = TextEditingController();
  final cPhoneNumber = TextEditingController();
  final cAddress = TextEditingController();
  final cSeri = TextEditingController();
  final cImei = TextEditingController();

  final formKeys = [GlobalKey<FormState>(), GlobalKey<FormState>(), GlobalKey<FormState>(), GlobalKey<FormState>()];
  final provinceFieldKey = GlobalKey<FormFieldState<BaseModel>>();
  final cityFieldKey = GlobalKey<FormFieldState<BaseModel>>();
  final dateFieldKey = GlobalKey<FormFieldState<String>>();
  final genderKey = GlobalKey<FormFieldState<BaseModel>>();

  final provinceList = <BaseModel>[].obs;
  final cityList = <BaseModel>[].obs;
  final _proviceList = <BaseModel>[];
  final _cityList = <BaseModel>[];
  final completeName = ''.obs;
  final ktp = ''.obs;
  final email = ''.obs;
  final phoneNumber = ''.obs;
  final address = ''.obs;
  final seri = ''.obs;
  final imei = ''.obs;
  final formattedDate = ''.obs;
  final selectedProvince = const BaseModel().obs;
  final selectedCity = const BaseModel().obs;
  final isEnabledCity = false.obs;

  final current = 0.obs;
  final imageFile = File('').obs;
  final backImageFile = File('').obs;
  final gender = const BaseModel().obs;

  String date = '';
  final frontImageId = Rxn<int>();
  final backImageId = Rxn<int>();
  final genderList = [
    const BaseModel(id: 1, name: 'Laki - laki', code: 'm'),
    const BaseModel(id: 2, name: 'Perempuan', code: 'f'),
  ];

  @override
  void onInit() {
    ever<int>(current, (val) {
      pageController.jumpToPage(val);
    });
    cCompleteName.addListener(onchangeCompleteName);
    cEmail.addListener(onchangeEmail);
    cPhoneNumber.addListener(onchangePhoneNumber);
    cAddress.addListener(onchangeAddress);
    cSeri.addListener(onchangeSeri);
    cImei.addListener(onchangeImei);
    cKtp.addListener(onchangeKtp);

    _getProvinceList();

    super.onInit();
  }

  @override
  void onClose() {
    cCompleteName.removeListener(onchangeCompleteName);
    cEmail.removeListener(onchangeEmail);
    cPhoneNumber.removeListener(onchangePhoneNumber);
  }

  void onBack() {
    if (current.value > 0) {
      current.value = current.value - 1;
    } else {
      Get.back();
    }
  }

  Future<String?> captureImmage(IMAGE_TYPE type) async {
    final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    dio.FormData? formData;
    if (photo != null) {
      if (type == IMAGE_TYPE.FRONT) {
        formData = dio.FormData.fromMap({
          'insurance_image[kind]': 'GADGET_FRONT',
          'insurance_image[attachment]': await dio.MultipartFile.fromFile(
            photo.path,
            contentType: MediaType('image', 'png'),
          )
        });
        imageFile.value = File(photo.path);
      } else {
        backImageFile.value = File(photo.path);
        formData = dio.FormData.fromMap({
          'insurance_image[kind]': 'GADGET_BACK',
          'insurance_image[attachment]': await dio.MultipartFile.fromFile(
            photo.path,
            contentType: MediaType('image', 'png'),
          )
        });
      }
      final url = await _uploadImage(formData);
      return url;
    }

    return null;
  }

  /// private methods
  void _getProvinceList() async {
    final _response = await _repo.getProvinces();
    if (_response.success == true) {
      provinceList.assignAll(_response.data!);
      _proviceList.assignAll(_response.data!);
    }
  }

  void _getCityList(int provinceId) async {
    final _response = await _repo.getCities(provinceId);
    if (_response.success == true) {
      cityList.assignAll(_response.data!);
      _cityList.assignAll(_response.data!);
    }
  }

  Future<String> _uploadImage(dio.FormData formData) async {
    final _response = await _repo.uploadImage(formData);
    if (_response.success == true) {
      if (_response.data?.kind == 'GADGET_FRONT') {
        frontImageId.value = _response.data?.id;
      } else {
        backImageId.value = _response.data?.id;
      }

      return _response.data?.attachment?.url ?? '';
    }

    return '';
  }

  /// end private methods

  onSlectProvince(BaseModel item) {
    isEnabledCity.value = true;
    selectedProvince.value = item;
    selectedCity.value = const BaseModel();
    provinceList.assignAll(_proviceList);
    _getCityList(item.id ?? 0);
    cityFieldKey.currentState?.reset();
  }

  onSelectCity(BaseModel item) {
    selectedCity.value = item;
    cityList.assignAll(_cityList);
  }

  onchangeform(int index) {
    if (formKeys[index].currentState!.validate()) {
      nextButtonEnable[index] = true;
    } else {
      nextButtonEnable[index] = false;
    }
  }

  onSearchProvince(String text) {
    if (text.isEmpty) {
      provinceList.assignAll(_proviceList);
    } else {
      final _list = _proviceList.where((item) => item.name!.toLowerCase().contains(text.toLowerCase())).toList();
      provinceList.assignAll(_list);
    }
  }

  onSearchCity(String text) {
    if (text.isEmpty) {
      cityList.assignAll(_cityList);
    } else {
      final _list = _cityList.where((item) => item.name!.toLowerCase().contains(text.toLowerCase())).toList();
      cityList.assignAll(_list);
    }
  }

  onchangeCompleteName() => completeName.value = cCompleteName.text;
  onchangeEmail() => email.value = cEmail.text;
  onchangePhoneNumber() => phoneNumber.value = cPhoneNumber.text;
  onchangeAddress() => address.value = cAddress.text;
  onchangeSeri() => seri.value = cSeri.text;
  onchangeImei() => imei.value = cImei.text;
  onchangeKtp() => ktp.value = cKtp.text;

  void handleButtonNext() {
    bool isValid = formKeys[current.value].currentState!.validate();
    if (isValid && current.value < 3) {
      current.value += 1;
    } else if (isValid && current.value == 3) {
      Get.toNamed(Routes.ASURANSI_GADGET_CONFIRM);
    }
  }

  void selectDate(FormFieldState<String> field) async {
    final _date = await showDatePicker(
      context: Get.context!,
      initialDate: DateTime.now(),
      firstDate: DateTime(1950),
      lastDate: DateTime(2050),
      currentDate: DateTime.now(),
      helpText: 'Pilih tanggal',
      cancelText: 'Batal',
      fieldHintText: 'Tanggal Lahir',
      fieldLabelText: 'Tanggal Lahir',
    );

    if (_date != null) {
      final _f = DateHelper.formatDate(_date.toString(), format: 'dd - MM - yyyy');
      date = DateHelper.formatDate(_date.toString(), format: 'yyyy-MM-dd');
      formattedDate.value = _f;
      field.didChange(_f);
    }
  }
}
