import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/insurance_model/insurance_gadget_model.dart';
import 'package:wispay_outlet/app/data/repositories/insurance_repository.dart';

class SelectGadgetController extends GetxController {
  final InsuranceRepository _repo = InsuranceRepository(Get.find());
  final TextEditingController cPrice = TextEditingController();

  final gadgetType = <BaseModel>[].obs;
  final merkGadget = <String>[].obs;
  final modelGadget = <InsuranceGadgetModel>[].obs;

  final selectedTypeGadget = const BaseModel().obs;
  final selectedMerkGadget = ''.obs;
  final selectedModelGadget = InsuranceGadgetModel().obs;
  final isButtonEnabled = false.obs;
  final isEnableMerkGadgetField = false.obs;
  final isEnableModelGadgetField = false.obs;
  final priceValue = '0.0'.obs;
  final isLoading = false.obs;
  final loadedPrice = false.obs;
  final _merkGadgetList = <String>[];
  final _modelGadgetList = <InsuranceGadgetModel>[];

  @override
  void onInit() {
    super.onInit();
    cPrice.addListener(() {
      priceValue.value = cPrice.text;
    });
    _getGadgetType();
  }

  void _getGadgetType() async {
    isLoading.value = true;
    final response = await _repo.getDeviceCategory();
    gadgetType.clear();
    if (response.success != false) {
      gadgetType.assignAll(response.data!);
    }
    isLoading.value = false;
  }

  void _getMerkGadget() async {
    final response = await _repo.getBrands(selectedTypeGadget.value.code!);
    merkGadget.clear();
    if (response.success != false) {
      merkGadget.assignAll(response.data!);
      _merkGadgetList.assignAll(response.data!);
    }
  }

  void _getModelGadget() async {
    final response = await _repo.getDevices(selectedTypeGadget.value.code!, selectedMerkGadget.value);
    modelGadget.clear();
    if (response.success != false) {
      modelGadget.assignAll(response.data!);
      _modelGadgetList.assignAll(response.data!);
    }
  }

  void onSearchMerkGadget(String str) {
    if (str.isEmpty) {
      merkGadget.clear();
      merkGadget.assignAll(_merkGadgetList);
    } else {
      final _l = _merkGadgetList.where((item) => item.toLowerCase().contains(str.toLowerCase())).toList();
      merkGadget.assignAll(_l);
    }
  }

  void onSearchModelGadget(String str) {
    if (str.isEmpty) {
      modelGadget.assignAll(_modelGadgetList);
    } else {
      final _list =
          _modelGadgetList.where((item) => item.gadgetName!.toLowerCase().contains(str.toLowerCase())).toList();
      modelGadget.assignAll(_list);
    }
  }

  void onSelectedTypeGadget(idx) {
    selectedTypeGadget.value = gadgetType[idx];
    selectedMerkGadget.value = '';
    selectedModelGadget.value = InsuranceGadgetModel();
    isEnableModelGadgetField.value = false;
    _getMerkGadget();
    isEnableMerkGadgetField.value = true;
    isButtonEnabled.value = false;
    loadedPrice.value = false;

    Get.back();
  }

  void onSelectMerkGadget(idx) {
    selectedMerkGadget.value = merkGadget[idx];
    merkGadget.assignAll(_merkGadgetList);
    selectedModelGadget.value = InsuranceGadgetModel();
    _getModelGadget();
    isEnableModelGadgetField.value = true;
    isButtonEnabled.value = false;
    loadedPrice.value = false;

    Get.back();
  }

  void onSelectModelGadget(idx) {
    selectedModelGadget.value = modelGadget[idx];
    modelGadget.assignAll(_modelGadgetList);
    isButtonEnabled.value = true;
    FocusManager.instance.primaryFocus?.unfocus();
    priceValue.value = double.parse(selectedModelGadget.value.gadgetPrice.toString()).toString();
    loadedPrice.value = true;
    Get.back();
  }
}
