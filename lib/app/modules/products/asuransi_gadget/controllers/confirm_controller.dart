import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/helpers/transaction_status_helper.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

import 'asuransi_gadget_form_controller.dart';
import '../controllers/select_gadget_controller.dart';
import '../widgets/status_card.dart';

class ConfirmController extends GetxController {
  SelectGadgetController selectGadgetController = Get.find();
  final CategoriesController _profile = Get.find();
  AsuransiGadgetFormController formController = Get.find();
  final TransactionRepository _repo = TransactionRepository(Get.find());

  final isExpanded = false.obs;
  final expandableChildren = [].obs;
  @override
  void onInit() {
    ever<bool>(isExpanded, (val) {
      if (val) {
        expandableChildren.value = [
          TrxDetailItem(title: 'Tipe Gadget', value: selectGadgetController.selectedTypeGadget.value.name!),
          TrxDetailItem(title: 'Nomor Seri', value: formController.seri.value),
          TrxDetailItem(title: 'Nomor Seri', value: formController.imei.value),
        ];
      } else {
        expandableChildren.value = [];
      }
    });
    super.onInit();
  }

  @override
  void onClose() {}

  handleExpand() {
    isExpanded.value = !isExpanded.value;
  }

  void _next(String pin) async {
    final payload = {
      "pin": pin,
      "name": formController.completeName.value,
      "email": encrypt(formController.email.value),
      "phone": encrypt(formController.phoneNumber.value),
      "address": formController.address.value,
      "gender": formController.gender.value.code,
      "date_of_birth": formController.date,
      "identity_number": formController.ktp.value,
      "province": formController.selectedProvince.value.name,
      "city": formController.selectedCity.value.name,
      "product_id": formController.premiData.id,
      "gadget_brand": selectGadgetController.selectedModelGadget.value.gadgetBrand,
      "gadget_name": selectGadgetController.selectedModelGadget.value.gadgetName,
      "gadget_imei": encrypt(formController.imei.value),
      "gadget_serial_number": encrypt(formController.seri.value),
      "insurance_device_category_id": selectGadgetController.selectedTypeGadget.value.id,
      "insurance_image_front_id": formController.frontImageId.value.toString(),
      "insurance_image_back_id": formController.backImageId.value.toString(),
    };
    const _suplier = 'we-plus';
    const _productName = 'insurance-gadget';
    final response = await _repo.confirm(
      supplier: _suplier,
      productName: _productName,
      body: RequestBodyModel.fromJson(payload),
    );

    if (response.success == true) {
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(data: response.data),
            transactionId: response.data?.id,
            productCode: ProductCode.INSURANCE,
            status: response.data?.status ?? TransactionStatusHelper.PENDING,
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': decrypt(_profile.profile.value.phone)},
    );
  }
}
