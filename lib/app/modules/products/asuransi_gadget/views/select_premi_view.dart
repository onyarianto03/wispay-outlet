import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/select_premi_controller.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/widgets/card_premi.dart';

class SelectPremiView extends GetView<SelectPremiController> {
  const SelectPremiView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pilihan Premi'),
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      body: Obx(
        () => ListView.separated(
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          itemCount: controller.data.length,
          separatorBuilder: (context, index) => const SizedBox(height: Spacing.defaultSpacing),
          itemBuilder: (_, idx) => CardPremi(data: controller.data[idx]),
        ),
      ),
    );
  }
}
