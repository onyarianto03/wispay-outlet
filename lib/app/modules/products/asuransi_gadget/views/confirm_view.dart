import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/confirm_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class ConfirmView extends GetView<ConfirmController> {
  const ConfirmView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Konfirmasi Pembelian'),
        leading: IconButton(
          icon: const Icon(Icons.chevron_left, size: 35),
          onPressed: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          ListView(
            padding: const EdgeInsets.all(Spacing.defaultSpacing),
            children: [
              Container(
                decoration: BoxDecoration(
                  color: WispayColors.cWhite,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [buildShadow(blurRadius: 10, spreadRadius: 0)],
                  image: const DecorationImage(
                    image: Assets.iconsMiscWave2,
                    alignment: Alignment.bottomLeft,
                  ),
                ),
                padding: const EdgeInsets.all(Spacing.defaultSpacing),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const CustomText(
                      title: 'Tipe Asuransi',
                      color: WispayColors.cBlack333,
                      textType: TextType.SemiBold,
                      margin: EdgeInsets.only(bottom: 1),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomText(
                          title: controller.formController.premiData.productBrand?.name ?? "",
                          color: WispayColors.cBlack333,
                        ),
                        CustomText(
                          title:
                              NumberHelper.formatMoney(decrypt(controller.formController.premiData.insurance?.premi)),
                          color: WispayColors.cBlack333,
                          textType: TextType.Bold,
                        ),
                      ],
                    ),
                    const Divider(thickness: 1, height: 32),
                    const CustomText(
                      title: 'Info Pembeli',
                      color: WispayColors.cBlack333,
                      textType: TextType.SemiBold,
                      margin: EdgeInsets.only(bottom: Spacing.small),
                    ),
                    TrxDetailItem(title: 'Nama', value: controller.formController.completeName.value),
                    TrxDetailItem(title: 'Email', value: controller.formController.email.value),
                    TrxDetailItem(title: 'Nomor Hp', value: controller.formController.phoneNumber.value),
                    const CustomText(title: 'Alamat', margin: EdgeInsets.only(bottom: Spacing.defaultSpacing)),
                    CustomText(
                      title: controller.formController.address.value,
                      color: WispayColors.cBlack333,
                      maxLines: 3,
                    ),
                    const SizedBox(height: Spacing.large)
                  ],
                ),
              ),
              const SizedBox(height: Spacing.defaultSpacing),
              Container(
                padding: const EdgeInsets.all(Spacing.defaultSpacing),
                decoration: BoxDecoration(
                  color: WispayColors.cWhite,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [buildShadow(blurRadius: 10, spreadRadius: 0)],
                ),
                child: ExpandablePanel(
                  header: const CustomText(
                    title: 'Info Gadget',
                    color: WispayColors.cBlack333,
                    textType: TextType.SemiBold,
                  ),
                  collapsed: const SizedBox(),
                  expanded: Column(
                    children: [
                      const SizedBox(height: Spacing.small),
                      TrxDetailItem(
                        title: 'Tipe Gadget',
                        value: controller.selectGadgetController.selectedTypeGadget.value.name!,
                      ),
                      TrxDetailItem(
                        title: 'Nomor Seri',
                        value: controller.formController.seri.value,
                      ),
                      TrxDetailItem(
                        title: 'Nomor IMEI',
                        value: controller.formController.imei.value,
                      ),
                    ],
                  ),
                  theme: const ExpandableThemeData(
                    iconPadding: EdgeInsets.zero,
                    useInkWell: true,
                  ),
                ),
              ),
            ],
          ),
          Positioned(
              child: CustomButton(
                title: 'Bayar',
                onPress: () => controller.onPay(),
              ),
              bottom: Spacing.defaultSpacing,
              left: Spacing.defaultSpacing,
              right: Spacing.defaultSpacing),
        ],
      ),
    );
  }
}
