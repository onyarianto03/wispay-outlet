import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/detail_premi_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class DetailPremiView extends GetView<DetailPremiController> {
  const DetailPremiView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail Asuransi'),
        leading: IconButton(
          icon: const Icon(Icons.chevron_left, size: 35),
          onPressed: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      backgroundColor: WispayColors.bgColor,
      body: Stack(
        fit: StackFit.expand,
        children: [
          SingleChildScrollView(
            padding: const EdgeInsets.all(Spacing.defaultSpacing),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      clipBehavior: Clip.hardEdge,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Spacing.small),
                        boxShadow: [buildShadow(blurRadius: 20)],
                      ),
                      child: Image.network(
                        controller.premiData.image?.url ?? '',
                        width: 40,
                      ),
                    ),
                    const SizedBox(width: Spacing.defaultSpacing),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomText(
                          title: controller.premiData.name ?? '',
                          textType: TextType.SemiBold,
                          size: FontSize.medium,
                          color: WispayColors.cBlack333,
                        ),
                        CustomText(
                          title: controller.premiData.productBrand?.name ?? '',
                          size: FontSize.small,
                          color: WispayColors.cGrey4f,
                        ),
                      ],
                    )
                  ],
                ),
                const SizedBox(height: Spacing.defaultSpacing),
                HtmlWidget(
                  controller.premiData.description ?? '',
                  textStyle: const TextStyle(
                    fontSize: FontSize.defaultSize,
                    color: WispayColors.cGrey4f,
                  ),
                ),
                const SizedBox(height: 120)
              ],
            ),
          ),
          Positioned(
            bottom: Spacing.defaultSpacing,
            left: Spacing.defaultSpacing,
            right: Spacing.defaultSpacing,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing, vertical: Spacing.small),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [buildShadow(blurRadius: 30, spreadRadius: 0)],
              ),
              child: Row(
                children: [
                  Expanded(
                    flex: 5,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const CustomText(
                          title: 'Harga Premi',
                          color: WispayColors.cBlack333,
                          size: FontSize.small,
                        ),
                        CustomText(
                          title: NumberHelper.formatMoney(decrypt(controller.premiData.insurance?.premi)),
                          size: FontSize.defaultSize,
                          color: WispayColors.cPrimary,
                          textType: TextType.SemiBold,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: CustomButton(
                      title: 'Pilih',
                      onPress: () => Get.toNamed(Routes.ASURANSI_GADGET_FORM, arguments: controller.premiData),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
