import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/widgets/address_form.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/widgets/device_form.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/widgets/imei_form.dart';

import '../widgets/user_data_form.dart';
import '../controllers/asuransi_gadget_form_controller.dart';
import '../widgets/form_title.dart';

class AsuransiGadgetFormView extends GetView<AsuransiGadgetFormController> {
  const AsuransiGadgetFormView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (controller.current.value > 0) {
          controller.current.value -= 1;
          return false;
        }
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(controller.premiData.name ?? ""),
          leading: IconButton(icon: const Icon(Icons.chevron_left, size: 35), onPressed: controller.onBack),
          centerTitle: true,
        ),
        body: GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(Spacing.defaultSpacing),
              child: Stack(
                fit: StackFit.expand,
                children: [
                  Column(
                    children: [
                      Obx(
                        () => FormTitle(
                          title: controller.formSteps[controller.current.value]['title'],
                          subTitle: controller.formSteps[controller.current.value]['subTitle'],
                          progress: controller.formSteps[controller.current.value]['progress'],
                          centerText: controller.formSteps[controller.current.value]['centerText'],
                        ),
                      ),
                      Expanded(
                        child: PageView(
                          physics: const NeverScrollableScrollPhysics(),
                          controller: controller.pageController,
                          children: const [
                            UserDataForm(),
                            AddressForm(),
                            ImeiForm(),
                            DeviceForm(),
                          ],
                        ),
                      )
                    ],
                  ),
                  Positioned(
                    child: Visibility(
                      child: CustomButton(
                        title: "Lanjutkan",
                        onPress: () {
                          FocusScope.of(context).unfocus();
                          controller.handleButtonNext();
                        },
                      ),
                      visible: MediaQuery.of(context).viewInsets.bottom == 0,
                    ),
                    bottom: 0,
                    left: 0,
                    right: 0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
