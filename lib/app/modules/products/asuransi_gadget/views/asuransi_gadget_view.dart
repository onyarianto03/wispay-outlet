import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/widgets/claim_view.dart';

import '../widgets/benefit_view.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import '../controllers/asuransi_gadget_controller.dart';
import '../widgets/button_tab.dart';

class AsuransiGadgetView extends GetView<AsuransiGadgetController> {
  const AsuransiGadgetView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Asuransi Gadget'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Obx(
            () => SingleChildScrollView(
              physics: controller.selectedIndex.value == 0
                  ? const NeverScrollableScrollPhysics()
                  : const BouncingScrollPhysics(),
              child: Container(
                padding: const EdgeInsets.all(Spacing.defaultSpacing),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Assets.imagesGadget.image(),
                    const SizedBox(height: Spacing.defaultSpacing),
                    Obx(
                      () => Row(
                        children: [
                          ButtonTab(
                            label: 'Manfaat',
                            isActive: controller.selectedIndex.value == 0,
                            onTap: () => controller.onChangePage(0),
                          ),
                          const SizedBox(width: Spacing.defaultSpacing),
                          ButtonTab(
                            label: 'Klaim',
                            isActive: controller.selectedIndex.value == 1,
                            onTap: () => controller.onChangePage(1),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 490,
                      child: PageView(
                        onPageChanged: controller.onChangePage,
                        controller: controller.pageController,
                        children: const [
                          BenefitView(),
                          ClaimView(),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            child: Container(
              padding: const EdgeInsets.only(
                left: Spacing.defaultSpacing,
                right: Spacing.defaultSpacing,
                bottom: Spacing.defaultSpacing,
              ),
              color: Colors.white,
              child: Obx(
                () => CustomButton(
                  title: controller.buttonText.value,
                  onPress: () => controller.onButtonPress(),
                ),
              ),
            ),
            bottom: 0,
            right: 0,
            left: 0,
          )
        ],
      ),
    );
  }
}
