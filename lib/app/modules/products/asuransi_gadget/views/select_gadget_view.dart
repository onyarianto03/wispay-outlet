import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/insurance_model/insurance_gadget_model.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/select_gadget_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class SelectGadgetView extends GetView<SelectGadgetController> {
  const SelectGadgetView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Asuransi Gadget'),
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(Spacing.defaultSpacing),
        child: Obx(
          () => Column(
            children: [
              _dropdown(),
              const SizedBox(height: Spacing.defaultSpacing),
              _dropdownScroll2(
                title: 'Merk Gadget',
                onSearch: controller.onSearchMerkGadget,
                enabled: controller.isEnableMerkGadgetField.value,
                items: controller.merkGadget,
                onSelect: controller.onSelectMerkGadget,
                selectedItem: controller.selectedMerkGadget.value,
              ),
              const SizedBox(height: Spacing.defaultSpacing),
              _dropdownScroll3(
                title: 'Jenis Gadget',
                onSearch: controller.onSearchModelGadget,
                items: controller.modelGadget,
                enabled: controller.isEnableModelGadgetField.value,
                selectedItem: controller.selectedModelGadget.value,
              ),
              const SizedBox(height: Spacing.defaultSpacing),
              BaseCard(
                blurRadius: 0,
                spreadRadius: 0,
                color: WispayColors.cBorderE,
                margin: EdgeInsets.zero,
                child: CustomText(
                  title: controller.loadedPrice.value
                      ? NumberHelper.formatMoney(controller.priceValue.value)
                      : 'Harga Pasaran',
                  size: FontSize.medium,
                  textType: TextType.SemiBold,
                  color: WispayColors.cBlack666,
                ),
              ),
              const SizedBox(height: Spacing.defaultSpacing),
              CustomButton(
                title: 'Cek Premi',
                isEnabled: controller.isButtonEnabled.value,
                onPress: () => Get.toNamed(Routes.ASURANSI_GADGET_PREMI),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Dropdown _dropdown() {
    return Dropdown(
      value: controller.selectedTypeGadget.value.name ?? "",
      label: 'Pilih Tipe Gadget',
      onTap: () => WispayBottomSheet.show(
        title: 'Tipe Gadget',
        children: [
          ListView.separated(
            shrinkWrap: true,
            itemCount: controller.gadgetType.length,
            separatorBuilder: (context, index) => const Divider(thickness: 1),
            itemBuilder: (context, index) => ListTile(
              dense: true,
              contentPadding: EdgeInsets.zero,
              onTap: () => controller.onSelectedTypeGadget(index),
              visualDensity: const VisualDensity(vertical: -2),
              trailing: Visibility(
                child: const Icon(
                  Icons.check_circle,
                  color: WispayColors.cGreen2,
                  size: 20,
                ),
                visible: controller.gadgetType[index].id == controller.selectedTypeGadget.value.id,
              ),
              title: CustomText(
                title: controller.gadgetType[index].name ?? "",
                color: WispayColors.cBlack333,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _dropdownScroll2({
    title,
    ValueChanged<int>? onSelect,
    ValueChanged<String>? onSearch,
    bool enabled = true,
    required List<String> items,
    required String selectedItem,
  }) {
    return Dropdown(
      value: selectedItem,
      label: 'Pilih $title',
      enabled: enabled,
      onTap: () => WispayBottomSheet.scrollableList<String>(
        onSearch: onSearch,
        title: '$title',
        items: items,
        onSelect: onSelect,
        selectedItem: selectedItem,
      ),
    );
  }

  Widget _dropdownScroll3({
    title,
    ValueChanged<String>? onSearch,
    bool enabled = true,
    required List<InsuranceGadgetModel> items,
    required InsuranceGadgetModel selectedItem,
  }) {
    return Dropdown(
      value: selectedItem.gadgetName ?? "",
      label: 'Pilih $title',
      enabled: enabled,
      onTap: () => WispayBottomSheet.scrollable(
        onSearch: onSearch,
        title: title,
        onclose: () => FocusManager.instance.primaryFocus?.unfocus(),
        itemBuilder: ((context, scrollController) => ListView.separated(
              controller: scrollController,
              shrinkWrap: true,
              itemCount: items.length,
              separatorBuilder: (context, index) => const Divider(thickness: 1),
              itemBuilder: (context, index) => ListTile(
                dense: true,
                contentPadding: EdgeInsets.zero,
                onTap: () => controller.onSelectModelGadget(index),
                visualDensity: const VisualDensity(vertical: -2),
                trailing: Visibility(
                  visible: selectedItem.gadgetName == items[index].gadgetName,
                  child: const Icon(
                    Icons.check_circle,
                    color: WispayColors.cGreen2,
                    size: 20,
                  ),
                ),
                title: CustomText(
                  title: items[index].gadgetName ?? "",
                  color: WispayColors.cBlack333,
                ),
              ),
            )),
      ),
    );
  }
}
