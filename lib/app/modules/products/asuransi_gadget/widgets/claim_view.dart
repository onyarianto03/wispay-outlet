import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

import '../controllers/asuransi_gadget_controller.dart';

class ClaimView extends GetView<AsuransiGadgetController> {
  const ClaimView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final listItems = controller.buildMenuItem(kListClaimGadget);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const CustomText(
          title: 'Perlindungan Gadget Mudah & Aman',
          textType: TextType.Bold,
          size: FontSize.medium,
          margin: EdgeInsets.symmetric(vertical: Spacing.defaultSpacing),
          color: WispayColors.cBlack333,
        ),
        ...listItems,
        const SizedBox(height: Spacing.large),
        const CustomText(
          title: 'Layanan Klaim',
          size: FontSize.medium,
          textType: TextType.Bold,
          color: WispayColors.cBlack333,
        ),
        const SizedBox(height: Spacing.defaultSpacing),
        const AutoSizeText.rich(
          TextSpan(
            text: 'Pusat bantuan: ',
            style: TextStyle(fontSize: FontSize.defaultSize),
            children: [
              TextSpan(
                text: '(nomor cs WE+)',
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
            ],
          ),
        )
      ],
    );
  }
}
