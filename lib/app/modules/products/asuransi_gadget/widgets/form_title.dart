import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

class FormTitle extends StatelessWidget {
  const FormTitle({
    Key? key,
    required this.title,
    required this.subTitle,
    required this.progress,
    required this.centerText,
  }) : super(key: key);

  final String centerText;
  final String title;
  final String subTitle;
  final double progress;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircularPercentIndicator(
          radius: 50,
          percent: progress,
          lineWidth: 4,
          animateFromLastPercent: true,
          animation: true,
          progressColor: WispayColors.cGreen2,
          center: CustomText(
            title: centerText,
            color: WispayColors.cGrey4f,
            size: FontSize.medium,
            textType: TextType.SemiBold,
          ),
        ),
        const SizedBox(width: Spacing.defaultSpacing),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: title,
              size: FontSize.large,
              color: WispayColors.cBlack333,
              textType: TextType.Bold,
            ),
            CustomText(
              title: subTitle,
              size: FontSize.small,
            ),
          ],
        )
      ],
    );
  }
}
