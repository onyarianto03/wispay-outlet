import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/asuransi_gadget_form_controller.dart';

class AddressForm extends GetView<AsuransiGadgetFormController> {
  const AddressForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Form(
        key: controller.formKeys[1],
        child: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.only(top: Spacing.large),
          children: [
            CustomTextInput(
              value: controller.address.value,
              label: 'Alamat',
              hintText: 'alamat pengguna',
              hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
              autovalidateMode: AutovalidateMode.onUserInteraction,
              maxLines: 4,
              textEditingController: controller.cAddress,
              validator: RequiredValidator(errorText: 'alamat tidak boleh kosong'),
            ),
            const SizedBox(height: Spacing.defaultSpacing),
            _dropdownField(
              title: 'Provinsi',
              items: controller.provinceList,
              onSelect: (value) => controller.onSlectProvince(value),
              key: controller.provinceFieldKey,
              selectedItem: controller.selectedProvince.value,
              onSearch: controller.onSearchProvince,
            ),
            const SizedBox(height: Spacing.defaultSpacing),
            _dropdownField(
              title: "Kota",
              items: controller.cityList,
              enabled: controller.isEnabledCity.value,
              onSelect: (value) => controller.onSelectCity(value),
              key: controller.cityFieldKey,
              selectedItem: controller.selectedCity.value,
              onSearch: controller.onSearchCity,
            ),
            const SizedBox(height: 80),
          ],
        ),
      ),
    );
  }

  Widget _dropdownField({
    title,
    ValueChanged<String>? onSearch,
    ValueChanged<BaseModel>? onSelect,
    bool enabled = true,
    required List<BaseModel> items,
    GlobalKey<FormFieldState<BaseModel>>? key,
    BaseModel? selectedItem,
  }) {
    return FormField(
      validator: (BaseModel? value) {
        if (value?.id == null && selectedItem?.id == null) {
          return 'Silahkan pilih $title';
        }
        return null;
      },
      enabled: enabled,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      key: key,
      onSaved: (BaseModel? newValue) => onSelect?.call(newValue!),
      builder: (FormFieldState<BaseModel> field) => Dropdown(
        errorText: field.errorText,
        value: selectedItem?.name ?? "",
        topLabel: title,
        label: 'Pilih $title',
        enabled: enabled,
        onTap: () => WispayBottomSheet.scrollableList<BaseModel>(
          onSearch: onSearch,
          title: '$title',
          items: items,
          selectedItem: selectedItem!,
          onSelect: (idx) {
            FocusManager.instance.primaryFocus?.unfocus();
            field.didChange(items[idx]);
            key?.currentState?.save();
            Get.back();
          },
        ),
      ),
    );
  }
}
