import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/asuransi_gadget_form_controller.dart';

class ImeiForm extends GetView<AsuransiGadgetFormController> {
  const ImeiForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Form(
        key: controller.formKeys[2],
        child: ListView(
          padding: const EdgeInsets.only(top: Spacing.large),
          shrinkWrap: true,
          children: [
            const InfoWidget(
              withWidget: true,
              textWidget: AutoSizeText.rich(
                TextSpan(
                  text: 'Anda bisa mengecek ',
                  style: TextStyle(fontSize: FontSize.small, color: WispayColors.cBlack333),
                  children: [
                    TextSpan(
                      text: 'nomor IMEI ',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    TextSpan(
                      text: 'dengan menekan USSD ',
                    ),
                    TextSpan(
                      text: '*#06# ',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    TextSpan(
                      text: 'di aplikasi dialler ponsel.',
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: Spacing.large),
            CustomTextInput(
              value: controller.seri.value,
              hintText: 'Nomor Seri',
              hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
              textEditingController: controller.cSeri,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: RequiredValidator(errorText: 'Nomor Seri tidak boleh kosong'),
            ),
            const SizedBox(height: Spacing.defaultSpacing),
            CustomTextInput(
              value: controller.imei.value,
              hintText: 'Nomor IMEI',
              hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
              textEditingController: controller.cImei,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: RequiredValidator(errorText: 'Nomor IMEI tidak boleh kosong'),
            ),
          ],
        ),
      ),
    );
  }
}
