import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/global_widgets/radio_button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/asuransi_gadget_form_controller.dart';

class GenderField extends GetWidget<AsuransiGadgetFormController> {
  const GenderField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormField(
      validator: (BaseModel? value) {
        if (value?.id == null && controller.gender.value.id == null) {
          return 'Jenis kelamin harus dipilih';
        }
        return null;
      },
      autovalidateMode: AutovalidateMode.onUserInteraction,
      key: controller.genderKey,
      builder: (FormFieldState<BaseModel> field) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const CustomText(
            title: 'Jenis Kelamin',
            margin: EdgeInsets.only(bottom: Spacing.xSmall),
            textType: TextType.SemiBold,
            size: FontSize.small,
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: controller.genderList
                .map(
                  (val) => Expanded(
                    child: RadioButton(
                      margin: val.id == 1 ? const EdgeInsets.only(right: Spacing.defaultSpacing) : EdgeInsets.zero,
                      groupValue: controller.gender.value,
                      value: val,
                      isError: field.errorText != null,
                      isActive: controller.gender.value == val,
                      title: val.name ?? "",
                      onChanged: (value) {
                        controller.gender.value = value!;
                        field.didChange(value);
                        field.save();
                      },
                    ),
                  ),
                )
                .toList(),
          ),
          Visibility(
            visible: field.errorText != null,
            child: CustomText(
              margin: const EdgeInsets.only(
                top: Spacing.small,
                left: Spacing.small,
              ),
              title: field.errorText ?? "",
              size: FontSize.small,
              color: WispayColors.cRed2,
            ),
          ),
        ],
      ),
    );
  }
}
