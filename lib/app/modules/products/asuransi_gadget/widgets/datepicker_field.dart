import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/asuransi_gadget_form_controller.dart';

class DatePickerField extends GetWidget<AsuransiGadgetFormController> {
  const DatePickerField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (String? value) {
        if (value?.isEmpty == true && controller.formattedDate.value.isEmpty) {
          return 'Tanggal Lahir Harus diisi';
        }
        return null;
      },
      initialValue: '',
      key: controller.dateFieldKey,
      builder: (FormFieldState<String> field) => GestureDetector(
        onTap: () {
          FocusScope.of(Get.context!).unfocus();
          controller.selectDate(field);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const CustomText(
              title: 'Tanggal Lahir',
              margin: EdgeInsets.only(bottom: Spacing.xSmall),
              textType: TextType.SemiBold,
              size: FontSize.small,
            ),
            Container(
              width: double.maxFinite,
              padding: const EdgeInsets.symmetric(horizontal: Spacing.small, vertical: Spacing.medium),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                  color: field.errorText != null ? WispayColors.cRed2 : WispayColors.cBlackBBB,
                  width: field.errorText != null ? 1.8 : 1,
                ),
              ),
              child: CustomText(
                title: controller.formattedDate.value.isEmpty ? 'dd - mm - yyyy' : controller.formattedDate.value,
              ),
            ),
            Visibility(
              visible: field.errorText != null,
              child: CustomText(
                margin: const EdgeInsets.only(
                  top: Spacing.small,
                  left: Spacing.small,
                ),
                title: field.errorText ?? '',
                size: FontSize.small,
                color: WispayColors.cRed2,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
