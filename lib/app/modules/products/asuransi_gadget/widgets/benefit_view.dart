import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

import '../controllers/asuransi_gadget_controller.dart';

class BenefitView extends GetView<AsuransiGadgetController> {
  const BenefitView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final listItems = controller.buildMenuItem(kListBenefitGadget);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const CustomText(
          title: 'Perlindungan Gadget Mudah & Aman',
          textType: TextType.Bold,
          size: FontSize.medium,
          margin: EdgeInsets.symmetric(vertical: Spacing.defaultSpacing),
          color: WispayColors.cBlack333,
        ),
        ...listItems,
      ],
    );
  }
}
