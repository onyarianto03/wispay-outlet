import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/asuransi_gadget_form_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class DeviceForm extends GetView<AsuransiGadgetFormController> {
  const DeviceForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: controller.formKeys[3],
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: Spacing.large),
          const InfoWidget(text: 'Unggah foto gadget Anda tampak depan dan belakang'),
          const SizedBox(height: Spacing.large),
          _image(
            title: 'Foto Gadget (Depan)',
            type: IMAGE_TYPE.FRONT,
          ),
          const Divider(thickness: 1, height: 32),
          _image(
            title: 'Foto Gadget (Belakang)',
            type: IMAGE_TYPE.BACK,
          ),
        ],
      ),
    );
  }

  Widget _image({
    required IMAGE_TYPE type,
    required String title,
  }) {
    return FormField(
      // initialValue: null,
      validator: (String? value) {
        if (value != null && value.isEmpty) {
          return 'Harap unggah $title';
        }

        return null;
      },
      autovalidateMode: AutovalidateMode.onUserInteraction,
      builder: (FormFieldState<String> field) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(
            title: title,
            size: FontSize.small,
            textType: TextType.SemiBold,
            margin: const EdgeInsets.only(bottom: Spacing.xSmall),
          ),
          InkWell(
            onTap: () async {
              String? url = await controller.captureImmage(type);
              field.didChange(url);
            },
            child: Container(
              constraints: const BoxConstraints(
                maxWidth: 100,
                maxHeight: 100,
              ),
              clipBehavior: Clip.hardEdge,
              decoration: BoxDecoration(
                color: WispayColors.cWhite,
                borderRadius: BorderRadius.circular(Spacing.small),
              ),
              child: field.value != null && field.value?.isNotEmpty == true
                  ? AspectRatio(aspectRatio: 1 / 1, child: CustomImage(source: field.value!, fit: BoxFit.fill))
                  : Center(child: Assets.iconsButtonTopup.image(width: 32, height: 32)),
            ),
          ),
          Visibility(
            child: CustomText(
              title: field.errorText ?? "",
              margin: const EdgeInsets.only(top: Spacing.small),
              color: Colors.red,
              size: FontSize.small,
            ),
            visible: field.hasError,
          )
        ],
      ),
    );
  }
}
