import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class CardPremi extends StatelessWidget {
  const CardPremi({
    Key? key,
    required this.data,
  }) : super(key: key);

  final ProductModel data;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Get.toNamed(Routes.ASURANSI_GADGET_DETAIL_PREMI, arguments: data),
      child: Container(
        padding: const EdgeInsets.all(Spacing.defaultSpacing),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: WispayColors.cWhite,
          boxShadow: [buildShadow(blurRadius: 40)],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  clipBehavior: Clip.hardEdge,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(Spacing.small),
                    boxShadow: [buildShadow(blurRadius: 20)],
                  ),
                  child: Image.network(
                    data.image?.url ?? '',
                    width: 40,
                  ),
                ),
                const SizedBox(
                  width: Spacing.defaultSpacing,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      title: data.name ?? "",
                      size: FontSize.medium,
                      textType: TextType.SemiBold,
                      color: WispayColors.cBlack333,
                    ),
                    CustomText(
                      title: data.productBrand?.name ?? "",
                      color: WispayColors.cGrey4f,
                    )
                  ],
                )
              ],
            ),
            const SizedBox(height: Spacing.defaultSpacing),
            Stack(
              fit: StackFit.loose,
              children: [
                Container(
                  clipBehavior: Clip.hardEdge,
                  decoration: const BoxDecoration(color: WispayColors.cWhite),
                  constraints: const BoxConstraints(maxHeight: 150),
                  child: ListView(
                    physics: const NeverScrollableScrollPhysics(),
                    children: [
                      HtmlWidget(
                        data.description ?? "",
                        textStyle: const TextStyle(
                          fontSize: FontSize.defaultSize,
                          color: WispayColors.cBlack333,
                        ),
                      )
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: [Colors.white70, Colors.white30],
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                      ),
                    ),
                    height: 30,
                  ),
                ),
              ],
            ),
            const CustomText(
              title: 'Selengkapnya',
              decoration: TextDecoration.underline,
              color: WispayColors.cPrimary,
              textType: TextType.SemiBold,
              margin: EdgeInsets.only(top: Spacing.xxSmall),
            ),
            const Divider(thickness: 1, height: 32),
            Row(
              children: [
                Expanded(
                  flex: 4,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const CustomText(
                        title: 'Harga Premi',
                        color: WispayColors.cBlack333,
                        size: FontSize.small,
                      ),
                      CustomText(
                        title: NumberHelper.formatMoney(decrypt(data.insurance?.premi)),
                        color: WispayColors.cPrimary,
                        textType: TextType.SemiBold,
                        margin: const EdgeInsets.only(top: 4),
                      )
                    ],
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: CustomButton(
                    title: 'Pilih',
                    onPress: () => Get.toNamed(Routes.ASURANSI_GADGET_FORM, arguments: data),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
