// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class StatusCard extends StatelessWidget {
  const StatusCard({
    Key? key,
    this.data,
  }) : super(key: key);

  final TransactionDataModel? data;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      decoration: BoxDecoration(
        boxShadow: const [shadow],
        color: Colors.white,
        image: DecorationImage(
          image: AssetImage(Assets.iconsMiscWave2.path),
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.circular(10.r),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TrxDetailItem(title: 'Nomor Transaksi', value: data?.code),
          TrxDetailItem(
            title: 'Tanggal',
            value: DateHelper.formatDate(data?.createdAt, format: 'dd MMM yyyy HH:mm:ss'),
          ),
          TrxDetailItem(title: 'Nama Produk', value: data?.detailPayload?.partnerName),
          TrxDetailItem(
            title: 'Tanggal Mulai',
            value: DateHelper.formatDate(data?.detailPayload?.startAt, format: 'dd MMM yyyy HH:mm:ss'),
          ),
          TrxDetailItem(
            title: 'Tanggal Berakhir',
            value: DateHelper.formatDate(data?.detailPayload?.finishAt, format: 'dd MMM yyyy HH:mm:ss'),
          ),
          TrxDetailItem(
            title: 'Total Tagihan',
            isBoldTitle: true,
            value: NumberHelper.formatMoney(data?.total),
            isBoldValue: true,
          ),
        ],
      ),
    );
  }
}
