import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/controllers/asuransi_gadget_form_controller.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/widgets/datepicker_field.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/widgets/gender_field.dart';

class UserDataForm extends GetView<AsuransiGadgetFormController> {
  const UserDataForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Form(
        autovalidateMode: AutovalidateMode.disabled,
        key: controller.formKeys[0],
        child: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.only(top: Spacing.large),
          physics: const ScrollPhysics(),
          primary: true,
          children: [
            CustomTextInput(
              value: controller.completeName.value,
              label: 'Nama Lengkap',
              hintText: 'Cth : Agus Mulyadi',
              helperText: 'nama lengkap sesuai KTP',
              hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
              textInputAction: TextInputAction.next,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              textEditingController: controller.cCompleteName,
              validator: RequiredValidator(errorText: 'Nama Lengkap Harus diisi'),
            ),
            const SizedBox(height: Spacing.defaultSpacing),
            CustomTextInput(
              value: controller.ktp.value,
              label: 'Nomor KTP',
              hintText: '0000 0000 0000 0000',
              keyboardType: TextInputType.number,
              hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
              textInputAction: TextInputAction.next,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              textEditingController: controller.cKtp,
              validator: MultiValidator([
                RequiredValidator(errorText: 'Nomor KTP Harus diisi'),
                MinLengthValidator(16, errorText: 'Nomor KTP Harus 16 digit'),
                MaxLengthValidator(16, errorText: 'Nomor KTP Harus 16 digit'),
              ]),
            ),
            const SizedBox(height: Spacing.defaultSpacing),
            const DatePickerField(),
            const SizedBox(height: Spacing.defaultSpacing),
            CustomTextInput(
              value: controller.email.value,
              label: 'Alamat Email',
              hintText: 'Cth : agusmulyadi@gmail.com',
              autovalidateMode: AutovalidateMode.onUserInteraction,
              textEditingController: controller.cEmail,
              validator: MultiValidator([
                RequiredValidator(errorText: 'Email Harus diisi'),
                EmailValidator(errorText: 'Email tidak valid'),
              ]),
              hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
              textInputAction: TextInputAction.next,
            ),
            const SizedBox(height: Spacing.defaultSpacing),
            CustomTextInput(
              value: controller.phoneNumber.value,
              label: 'Nomor Hp',
              hintText: 'Cth : 081234567890',
              autovalidateMode: AutovalidateMode.onUserInteraction,
              textEditingController: controller.cPhoneNumber,
              validator: MultiValidator([
                RequiredValidator(errorText: 'Nomor Hp Harus diisi'),
                IndonesiaPhoneNumber(errorText: 'Nomor Hp tidak valid'),
              ]),
              hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
              textInputAction: TextInputAction.done,
            ),
            const SizedBox(height: Spacing.defaultSpacing),
            const GenderField(),
            const SizedBox(height: 70),
          ],
        ),
      ),
    );
  }
}
