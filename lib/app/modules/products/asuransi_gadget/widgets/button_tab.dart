import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

class ButtonTab extends StatelessWidget {
  const ButtonTab({
    Key? key,
    required this.label,
    required this.isActive,
    this.onTap,
  }) : super(key: key);

  final String label;
  final bool isActive;
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(
            title: label,
            color: isActive ? WispayColors.cBlack333 : WispayColors.cGrey82,
            textType: TextType.SemiBold,
          ),
          isActive
              ? AnimatedContainer(
                  duration: const Duration(milliseconds: 1000),
                  height: 2,
                  color: WispayColors.cPrimary,
                  width: 20,
                )
              : const SizedBox()
        ],
      ),
    );
  }
}
