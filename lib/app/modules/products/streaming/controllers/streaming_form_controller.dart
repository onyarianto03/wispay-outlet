import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

import '../widgets/status_card.dart';

class StreamingFormController extends GetxController {
  TextEditingController phoneController = TextEditingController();
  final BaseModel brandItem = Get.arguments;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final ProductController productController = Get.find();
  final ProductBrandModel pBrand = Get.arguments;
  final CategoriesController _homeController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final ScrollController sController = ScrollController();

  final isEnableCard = false.obs;
  var isLoading = false.obs;
  var isLastPage = false;
  int page = 1;
  final products = <ProductModel>[].obs;
  var selectedItem = ProductModel();

  final phone = ''.obs;

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
    getInitialData();
    sController.addListener(_scrollListener);
    phoneController.addListener(() => phone.value = phoneController.text);
  }

  void getInitialData() async {
    isLoading.value = true;
    await productController.getSubCategories(pBrand.id);
    getProducts();
  }

  void _scrollListener() async {
    if (sController.position.pixels == sController.position.maxScrollExtent) {
      if (isLastPage == true) return;
      page++;
      getProducts(isLoadmore: true);
    }
  }

  void getProducts({bool? isLoadmore}) async {
    final _filter = {
      'page': page,
      'limit': 14,
    };

    final newProducts = await productController.getProducts(productController.productSubCategory.first.id,
        filter: FilterModel.fromJson(_filter));

    if (newProducts != null) {
      if (isLoadmore == true && newProducts.isEmpty) {
        isLastPage = true;
      } else if (isLoadmore == true) {
        products.addAll(newProducts);
      } else {
        products.value = newProducts;
      }
    }
    isLoading.value = false;
  }

  void onPickedContact(String? phone) {
    if (phone != null) {
      phoneController.text = phone;
    }
  }

  @override
  void onClose() {}

  void onchangeForm() {
    bool isValid = formKey.currentState!.validate();
    if (isValid) {
      formKey.currentState!.save();
      isEnableCard.value = true;
    } else {
      isEnableCard.value = false;
    }
  }

  void _next(String pin) async {
    final body = {
      "pin": pin,
      "product_id": selectedItem.id,
      "customer_id": encrypt(NumberHelper.formatPhoneToIndonesian(phone.value)),
    };
    final confirmResponse = await _transactionRepository.confirm(
      supplier: selectedItem.productSupplier?.code?.toLowerCase(),
      productName: 'streaming',
      body: RequestBodyModel.fromJson(body),
    );

    if (confirmResponse.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(
              confirmResponse: confirmResponse.data!,
            ),
            transactionId: confirmResponse.data?.id,
            productCode: ProductCode.STREAMING,
            status: confirmResponse.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': _homeController.profile.value.phone},
    );
  }

  void showConfirm(ProductModel item) {
    FocusManager.instance.primaryFocus?.unfocus();
    final commissionSetting = _homeController.profile.value.comissionSetting;
    double comission = double.parse(commissionSetting?.pdam ?? commissionSetting?.all ?? '0');
    WispayBottomSheet.showConfirm(
      title: 'Streaming',
      withPromo: false,
      totalPrice: NumberHelper.formatMoney(item.total.toString()),
      onConfirm: () => onPay(),
      children: [
        comission > 0
            ? InfoWidget(
                text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                  comission.toString(),
                )}',
                margin: const EdgeInsets.only(top: 5),
              )
            : const SizedBox(height: 0),
        const SizedBox(height: Spacing.defaultSpacing),
        TrxDetailItem(title: 'Nama Produk', value: item.name),
        TrxDetailItem(title: 'Nomor Hp', value: phone.value),
        TrxDetailItem(title: 'Harga', value: NumberHelper.formatMoney(item.total.toString())),
      ],
    );
    selectedItem = item;
  }

  void showHowToRedeem() {
    FocusManager.instance.primaryFocus?.unfocus();
    WispayBottomSheet.show(
      title: 'Cara Redeem',
      children: [
        const SizedBox(height: Spacing.defaultSpacing),
        Row(
          children: [
            const CustomText(title: '1.  '),
            AutoSizeText.rich(
              TextSpan(
                text: 'Masuk ke ${pBrand.name}',
                style: const TextStyle(color: WispayColors.cBlack333, fontSize: FontSize.defaultSize, height: 1.5),
              ),
            )
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            CustomText(title: '2.  '),
            Expanded(
              child: CustomText(
                title: 'Masukkan Kode Voucher yang Anda dapatkan dari Wispay',
                color: WispayColors.cBlack333,
                maxLines: 2,
                size: FontSize.defaultSize,
                textStyle: TextStyle(height: 1.5),
              ),
            )
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            CustomText(title: '3.  '),
            Expanded(
              child: AutoSizeText.rich(
                TextSpan(
                  text: 'Klik tombol ',
                  children: [
                    TextSpan(text: 'MASUKKAN KODE ', style: TextStyle(fontWeight: FontWeight.w600)),
                    TextSpan(text: 'di halaman redeem Spotify'),
                  ],
                ),
                style: TextStyle(color: WispayColors.cBlack333, fontSize: FontSize.defaultSize, height: 1.5),
              ),
            )
          ],
        ),
        const SizedBox(height: Spacing.defaultSpacing),
      ],
    );
  }
}
