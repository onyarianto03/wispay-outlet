import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';

class StreamingController extends GetxController {
  final ProductController _productController = Get.find();
  final categoryId = Get.arguments;
  var isLoading = false.obs;
  final pBrand = <ProductBrandModel>[].obs;

  @override
  void onInit() {
    super.onInit();
    getInitialData();
  }

  void getInitialData() async {
    isLoading.value = true;
    final _filter = {
      'limit': 30,
    };
    final brand = await _productController.getProductBrands(categoryId, filter: FilterModel.fromJson(_filter));
    if (brand != null) {
      pBrand.value = brand;
    }
    isLoading.value = false;
  }

  @override
  void onClose() {}
}
