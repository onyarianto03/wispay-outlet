import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/products/streaming/views/streaming_form_view.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

import '../controllers/streaming_controller.dart';

class StreamingView extends GetView<StreamingController> {
  const StreamingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Streaming'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => controller.isLoading.value
            ? Center(
                child: Lottie.asset(Assets.lottieLoading, width: 100),
              )
            : SafeArea(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: Spacing.defaultSpacing),
                    const CustomText(
                      title: 'Voucher yang Tersedia',
                      size: FontSize.medium,
                      color: WispayColors.cBlack333,
                      textType: TextType.SemiBold,
                      margin: EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                    ),
                    Expanded(
                      child: StaggeredGridView.countBuilder(
                        padding: const EdgeInsets.all(Spacing.defaultSpacing),
                        shrinkWrap: true,
                        crossAxisCount: 2,
                        itemCount: controller.pBrand.length,
                        crossAxisSpacing: Spacing.defaultSpacing,
                        mainAxisSpacing: Spacing.defaultSpacing,
                        staggeredTileBuilder: (index) => const StaggeredTile.fit(1),
                        itemBuilder: (BuildContext context, int index) => InkWell(
                          onTap: () => Get.to(() => const StreamingFormView(), arguments: controller.pBrand[index]),
                          child: Column(
                            children: [
                              BaseCard(
                                blurRadius: 20,
                                spreadRadius: 0,
                                margin: EdgeInsets.zero,
                                child: CustomImage(
                                  source: controller.pBrand[index].image?.url ?? "",
                                  width: 90,
                                  height: 35,
                                ),
                              ),
                              const SizedBox(height: Spacing.small),
                              CustomText(
                                title: controller.pBrand[index].name ?? "-",
                                textType: TextType.SemiBold,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
