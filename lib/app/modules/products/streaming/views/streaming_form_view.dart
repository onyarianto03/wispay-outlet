import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:form_field_validator/form_field_validator.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/empty_product.dart';
import 'package:wispay_outlet/app/global_widgets/form_note.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/products/streaming/controllers/streaming_form_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class StreamingFormView extends GetView<StreamingFormController> {
  const StreamingFormView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(controller.brandItem.name ?? 'Streaming'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => controller.isLoading.value
            ? Center(
                child: Lottie.asset(Assets.lottieLoading, width: 100),
              )
            : SafeArea(
                child: Padding(
                  padding: const EdgeInsets.all(Spacing.defaultSpacing),
                  child: Form(
                    onChanged: () => controller.onchangeForm(),
                    key: controller.formKey,
                    child: controller.products.isEmpty
                        ? const EmptyProduct()
                        : Column(
                            children: [
                              CustomTextInput(
                                keyboardType: TextInputType.phone,
                                value: controller.phone.value,
                                showContact: true,
                                onContactPicked: (value) => controller.onPickedContact(value),
                                hintText: 'No HP Pelanggan',
                                validator: MultiValidator([
                                  RequiredValidator(errorText: 'No HP Pelanggan tidak boleh kosong'),
                                  IndonesiaPhoneNumber(errorText: 'No HP Pelanggan tidak valid'),
                                ]),
                                textEditingController: controller.phoneController,
                                margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
                              ),
                              Expanded(
                                child: ListView(
                                  controller: controller.sController,
                                  shrinkWrap: true,
                                  children: [
                                    _note(),
                                    StaggeredGridView.countBuilder(
                                      physics: const NeverScrollableScrollPhysics(),
                                      padding: const EdgeInsets.symmetric(vertical: Spacing.defaultSpacing),
                                      shrinkWrap: true,
                                      itemCount: controller.products.length,
                                      crossAxisCount: 2,
                                      mainAxisSpacing: Spacing.defaultSpacing,
                                      crossAxisSpacing: Spacing.defaultSpacing,
                                      itemBuilder: (_, idx) => Obx(() => InkWell(
                                            onTap: controller.isEnableCard.value
                                                ? () => controller.showConfirm(controller.products[idx])
                                                : null,
                                            child: BaseCard(
                                              isEnabled: controller.isEnableCard.value,
                                              blurRadius: 20,
                                              spreadRadius: 0,
                                              margin: EdgeInsets.zero,
                                              child: Column(
                                                children: [
                                                  CustomText(
                                                    title: controller.products[idx].name ?? "",
                                                    textType: TextType.SemiBold,
                                                    textAlign: TextAlign.center,
                                                    margin: const EdgeInsets.only(bottom: Spacing.xSmall),
                                                  ),
                                                  CustomText(
                                                    title:
                                                        NumberHelper.formatMoney(controller.products[idx].defaultPrice),
                                                    textType: TextType.Regular,
                                                    color: WispayColors.cPrimary,
                                                    size: FontSize.small,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          )),
                                      staggeredTileBuilder: (_) => const StaggeredTile.fit(1),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                  ),
                ),
              ),
      ),
    );
  }

  Widget _note() {
    return FormNote(
      children: [
        const AutoSizeText.rich(
          TextSpan(
            text: 'Format kode yang akan diberikan : \n',
            children: [
              TextSpan(
                text: 'Kode Voucher || Serial Number =\nABCDEFG||123456',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                ),
              )
            ],
          ),
          style: TextStyle(
            color: WispayColors.cBlack333,
            height: 1.5,
          ),
        ),
        GestureDetector(
          onTap: () => controller.showHowToRedeem(),
          child: const AutoSizeText.rich(
            TextSpan(
              text: 'Masukkan Kode Voucher ke halaman redeem. \nCara redeem ',
              children: [
                TextSpan(
                  text: 'lihat di sini.',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: WispayColors.cPrimary,
                    decoration: TextDecoration.underline,
                  ),
                )
              ],
            ),
            style: TextStyle(
              color: WispayColors.cBlack333,
              height: 1.5,
            ),
          ),
        )
      ],
    );
  }
}
