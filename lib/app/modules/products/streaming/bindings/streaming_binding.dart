import 'package:get/get.dart';

import 'package:wispay_outlet/app/modules/products/streaming/controllers/streaming_form_controller.dart';

import '../controllers/streaming_controller.dart';

class StreamingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<StreamingFormController>(
      () => StreamingFormController(),
      fenix: true,
    );
    Get.lazyPut<StreamingController>(
      () => StreamingController(),
    );
  }
}
