import 'package:get/get.dart';

import 'package:wispay_outlet/app/modules/products/telkom/controllers/telkom_indihome_controller.dart';
import 'package:wispay_outlet/app/modules/products/telkom/controllers/telkom_telpon_controller.dart';

import '../controllers/telkom_controller.dart';

class TelkomBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TelkomIndihomeController>(
      () => TelkomIndihomeController(),
    );
    Get.lazyPut<TelkomTelponController>(
      () => TelkomTelponController(),
    );
    Get.lazyPut<TelkomController>(
      () => TelkomController(),
    );
  }
}
