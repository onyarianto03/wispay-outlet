import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';

class TelkomController extends GetxController {
  final ProductController productController = Get.find();
  final categoryId = Get.arguments;
  final isLoading = false.obs;
  @override
  void onInit() {
    super.onInit();
    getInitialData();
  }

  void getInitialData() async {
    isLoading.value = true;
    await productController.getProductBrands(categoryId);
    isLoading.value = false;
  }

  @override
  void onClose() {}
}
