import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/products/telkom/widgets/status_card.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class TelkomTelponController extends GetxController {
  TextEditingController phoneController = TextEditingController();
  final ProductController _productController = Get.find();
  final CategoriesController _homeController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final phone = ''.obs;
  final formKey = GlobalKey<FormState>();
  var isLoading = false.obs;
  final isSubmitting = false.obs;
  final brandId = Get.arguments;
  final _products = <ProductModel>[].obs;
  String? token = "";

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
    getInitialData();
    phoneController.addListener(() => phone.value = phoneController.text);
  }

  void getInitialData() async {
    isLoading.value = true;
    final productSubcategories = await _productController.getSubCategories(brandId);
    final products = await _productController.getProducts(productSubcategories?.first.id);
    if (products != null) {
      _products.value = products;
    }
    isLoading.value = false;
  }

  void onPickedContact(String? phone) {
    if (phone != null) {
      phoneController.text = phone;
    }
  }

  @override
  void onClose() {}

  void showConfirm() async {
    final isValid = formKey.currentState!.validate();

    if (isValid) {
      FocusManager.instance.primaryFocus?.unfocus();
      isSubmitting.value = true;
      final body = {
        "product_id": _products.first.id,
        "phone": encrypt(NumberHelper.formatPhoneToIndonesian(phone.value)),
      };
      final response = await _transactionRepository.order(
        supplier: _products.first.productSupplier!.code!.toLowerCase(),
        productName: 'pulsa-postpaid',
        body: RequestBodyModel.fromJson(body),
      );
      token = response.data?.meta?.token;
      isSubmitting.value = false;
      if (response.success != false) {
        final commissionSetting = _homeController.profile.value.comissionSetting;
        double comission = double.parse(commissionSetting?.telkom ?? commissionSetting?.all ?? '0.0');
        WispayBottomSheet.showConfirm(
          onConfirm: () => onPay(),
          withPromo: false,
          title: "Tagihan Telkom",
          totalPrice: NumberHelper.formatMoney(
            response.data?.total.toString() ?? '0.0',
          ),
          children: [
            comission > 0
                ? InfoWidget(
                    text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                      comission.toString(),
                    )}',
                    margin: const EdgeInsets.only(top: 5),
                  )
                : const SizedBox(height: 0),
            const SizedBox(height: Spacing.defaultSpacing),
            TrxDetailItem(title: 'Nama Produk', value: response.data?.productName),
            TrxDetailItem(title: 'Nama Pelanggan', value: decrypt(response.data?.detailPayload?.name)),
            TrxDetailItem(
                title: 'Nomor Hp', value: NumberHelper.formatPhoneRemove62(decrypt(response.data?.customerPhone))),
            TrxDetailItem(
                title: 'Tagihan',
                value: NumberHelper.formatMoney(
                  response.data?.price ?? '0.0',
                )),
            TrxDetailItem(
              title: 'Biaya Admin',
              value: NumberHelper.formatMoney(
                response.data?.adminFee.toString() ?? '0.0',
              ),
              marginBottom: 0,
            ),
          ],
        );
      }
    }
  }

  void _next(String pin) async {
    final body = {
      "pin": pin,
      "token": token,
    };
    final confirmResponse = await _transactionRepository.confirm(
      supplier: _products.first.productSupplier!.code!.toLowerCase(),
      productName: 'pulsa-postpaid',
      body: RequestBodyModel.fromJson(body),
    );

    if (confirmResponse.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(
              confirmResponse: confirmResponse.data!,
            ),
            transactionId: confirmResponse.data?.id,
            productCode: ProductCode.TELKOM_TAGIHAN_TELPON,
            productBrandCode: confirmResponse.data?.product?.productBrand?.code,
            status: confirmResponse.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': _homeController.profile.value.phone},
    );
  }
}
