import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/helpers/string_helper.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class StatusCard extends StatelessWidget {
  const StatusCard({
    Key? key,
    required this.confirmResponse,
  }) : super(key: key);

  final TransactionDataModel confirmResponse;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      decoration: BoxDecoration(
        boxShadow: const [shadow],
        color: Colors.white,
        image: DecorationImage(
          image: AssetImage(Assets.iconsMiscWave2.path),
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.circular(10.r),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TrxDetailItem(
            title: 'Nomor Transaksi',
            value: confirmResponse.code ?? '-',
            isBoldValue: true,
          ),
          TrxDetailItem(
            title: 'Tanggal',
            value: DateHelper.formatDate(confirmResponse.createdAt, format: 'dd MMM yyyy HH:mm:ss'),
            isBoldValue: true,
          ),
          TrxDetailItem(
            title: 'ID Pelanggan',
            value: decrypt(confirmResponse.customerId),
            isBoldValue: true,
          ),
          TrxDetailItem(
            title: 'Nama',
            value: StringHelper.convertArrayToString(decrypt(confirmResponse.detailPayload?.name)),
            isBoldValue: true,
          ),
          TrxDetailItem(
            title: 'Total Tagihan',
            isBoldTitle: true,
            value: NumberHelper.formatMoney(confirmResponse.total),
            isBoldValue: true,
          ),
        ],
      ),
    );
  }
}
