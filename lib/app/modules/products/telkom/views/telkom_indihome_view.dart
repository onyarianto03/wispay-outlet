import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/telkom/controllers/telkom_indihome_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class TelkomIndihomeView extends GetView<TelkomIndihomeController> {
  const TelkomIndihomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tagihan Telkom'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => controller.isLoading.value
            ? Center(
                child: Lottie.asset(Assets.lottieLoading, width: 100),
              )
            : Padding(
                padding: const EdgeInsets.all(Spacing.defaultSpacing),
                child: Form(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  key: controller.formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomTextInput(
                        value: controller.customerId.value,
                        validator: MultiValidator(
                          [
                            RequiredValidator(errorText: 'ID Pelanggan/Nomor Meter tidak boleh kosong'),
                          ],
                        ),
                        hintText: 'ID Pelanggan/Nomor Meter',
                        textEditingController: controller.customerIdController,
                        hintStyle: const TextStyle(
                          color: WispayColors.cBlackBBB,
                        ),
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      CustomButton(
                          title: 'Cek Tagihan',
                          onPress: controller.showConfirm,
                          isEnabled: !controller.isSubmitting.value,
                          isLoading: controller.isSubmitting.value),
                      const SizedBox(height: Spacing.defaultSpacing),
                      const CustomText(
                        title: 'Keterangan:',
                        color: WispayColors.cBlack,
                        textType: TextType.Bold,
                        size: FontSize.medium,
                        margin: EdgeInsets.only(bottom: Spacing.small),
                      ),
                      Container(
                        padding: const EdgeInsets.all(Spacing.defaultSpacing),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8.r)),
                          color: WispayColors.cPrimary.withOpacity(0.2),
                        ),
                        child: Column(
                          children: [
                            _note(
                              number: '1. ',
                              content: const TextSpan(
                                text: 'Tagihan telepon rumah/Indihome tidak tersedia pada jam ',
                                children: [
                                  TextSpan(
                                    text: '23.00 - 01.00 WIB ',
                                    style: TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                  TextSpan(text: 'sesuai ketentuan Telkom.'),
                                ],
                              ),
                            ),
                            _note(
                              number: '2. ',
                              content: const TextSpan(
                                text: 'Waktu proses tagihan membutuhkan waktu maksimal 1x24 jam.',
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Widget _note({required String number, required TextSpan content}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText(
          title: number,
          color: WispayColors.cBlack,
        ),
        Expanded(
          child: AutoSizeText.rich(
            content,
            style: const TextStyle(fontSize: 14, color: WispayColors.cBlack),
          ),
        )
      ],
    ).marginOnly(bottom: Spacing.xSmall);
  }
}
