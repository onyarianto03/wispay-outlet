import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

import '../controllers/telkom_controller.dart';

class TelkomView extends GetView<TelkomController> {
  const TelkomView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Telkom'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => controller.isLoading.value
            ? Center(
                child: Lottie.asset(Assets.lottieLoading, width: 100),
              )
            : SafeArea(
                child: Container(
                  color: WispayColors.cWhite,
                  padding: const EdgeInsets.all(Spacing.defaultSpacing),
                  child: ListView.separated(
                      separatorBuilder: (context, index) => const SizedBox(
                            height: 0,
                          ),
                      itemCount: controller.productController.productBrand.length,
                      itemBuilder: (context, index) {
                        final item = controller.productController.productBrand[index];
                        return Column(children: [
                          _item(
                              title: item.name,
                              image: item.image?.thumbnail?.url,
                              onPress: () => Get.toNamed(
                                    ProductCategoryHelper.getProductNavigation(item.code)!,
                                    arguments: item.id,
                                  )),
                          const Divider(thickness: 1)
                        ]);
                      }),
                ),
              ),
      ),
    );
  }

  _item({String? title, String? image, void Function()? onPress}) {
    return InkWell(
      onTap: onPress,
      child: Row(
        children: [
          CustomImage(source: image ?? '', width: 36, height: 36),
          const SizedBox(width: Spacing.medium),
          CustomText(
            title: title ?? '',
            textType: TextType.SemiBold,
            color: WispayColors.cBlack,
          ),
        ],
      ),
    ).paddingSymmetric(vertical: Spacing.medium);
  }
}
