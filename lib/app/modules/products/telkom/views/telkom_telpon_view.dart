import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/modules/products/telkom/controllers/telkom_telpon_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class TelkomTelponView extends GetView<TelkomTelponController> {
  const TelkomTelponView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tagihan Telpon'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(Spacing.defaultSpacing),
        child: Obx(
          () => controller.isLoading.value
              ? Center(
                  child: Lottie.asset(Assets.lottieLoading, width: 100),
                )
              : Form(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  key: controller.formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomTextInput(
                        value: controller.phone.value,
                        isGSM: true,
                        showContact: true,
                        onContactPicked: (value) => controller.onPickedContact(value),
                        validator: MultiValidator(
                          [
                            RequiredValidator(errorText: 'Nomor HP tidak boleh kosong'),
                            IndonesiaPhoneNumber(errorText: 'Nomor telepon tidak valid'),
                          ],
                        ),
                        hintText: 'Nomor HP',
                        textEditingController: controller.phoneController,
                        hintStyle: const TextStyle(
                          color: WispayColors.cBlackBBB,
                        ),
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      CustomButton(
                        title: 'Cek Tagihan',
                        onPress: controller.showConfirm,
                        isEnabled: controller.phone.value != '' && !controller.isSubmitting.value,
                        isLoading: controller.isSubmitting.value,
                      ),
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
