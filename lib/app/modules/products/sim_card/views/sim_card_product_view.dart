import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/card/card_product.dart';
import 'package:wispay_outlet/app/global_widgets/empty_widget.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

import '../controllers/sim_card_product_controller.dart';

class SimCardProductView extends GetView<SimCardProductController> {
  const SimCardProductView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(controller.brand.name ?? ""),
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      body: Obx(
        () => controller.isLoading.value
            ? Center(child: Lottie.asset(Assets.lottieLoading, width: 100))
            : controller.products.isNotEmpty
                ? NotificationListener(
                    onNotification: (notification) {
                      if (notification is ScrollEndNotification) {
                        if (notification.metrics.pixels == notification.metrics.maxScrollExtent) {
                          controller.onLoadMore();
                          return true;
                        }
                      }
                      return true;
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: Spacing.defaultSpacing),
                        const CustomText(
                          title: 'Pilih tipe kartu',
                          color: WispayColors.cBlack333,
                          textType: TextType.SemiBold,
                          size: FontSize.medium,
                          margin: EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                        ),
                        const SizedBox(height: Spacing.small),
                        Expanded(
                          child: RefreshIndicator(
                            onRefresh: controller.getInitialData,
                            child: ListView.separated(
                              shrinkWrap: true,
                              primary: false,
                              physics: const BouncingScrollPhysics(),
                              padding: const EdgeInsets.symmetric(
                                horizontal: Spacing.defaultSpacing,
                                vertical: Spacing.medium,
                              ),
                              itemBuilder: (_, idx) {
                                final product = controller.products[idx];
                                return CardProduct(
                                  name: product.name,
                                  image: product.image?.url,
                                  onTap: () => controller.onSelect(idx),
                                  point: product.productPrice?.point,
                                  price: product.productPrice?.price,
                                );
                              },
                              separatorBuilder: (_, __) => const SizedBox(
                                height: Spacing.defaultSpacing,
                              ),
                              itemCount: controller.products.length,
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                : Center(
                    child: EmptyWidget(
                      title: 'Produk tidak tersedia',
                      subTitle: '',
                      image: Assets.iconsEmptyList.path,
                    ),
                  ),
      ),
    );
  }
}
