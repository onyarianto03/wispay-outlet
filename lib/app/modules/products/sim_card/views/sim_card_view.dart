import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

import '../views/sim_card_product_view.dart';
import '../controllers/sim_card_controller.dart';

class SimCardView extends GetView<SimCardController> {
  const SimCardView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Kartu Perdana'),
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      body: Obx(() => controller.isLoading.value
          ? Center(child: Lottie.asset(Assets.lottieLoading, width: 100))
          : Padding(
              padding: const EdgeInsets.all(Spacing.defaultSpacing),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const CustomText(
                    title: 'Pilih operator',
                    color: WispayColors.cBlack333,
                    textType: TextType.SemiBold,
                    size: FontSize.medium,
                  ),
                  Expanded(
                    child: StaggeredGridView.countBuilder(
                      padding: const EdgeInsets.symmetric(vertical: Spacing.defaultSpacing),
                      crossAxisCount: 2,
                      itemCount: controller.productBrands.length,
                      itemBuilder: (BuildContext context, int index) => GestureDetector(
                        onTap: () =>
                            Get.to(() => const SimCardProductView(), arguments: controller.productBrands[index]),
                        child: Column(
                          children: [
                            BaseCard(
                              blurRadius: 20,
                              spreadRadius: 0,
                              margin: EdgeInsets.zero,
                              padding: const EdgeInsets.all(Spacing.medium),
                              child: CustomImage(
                                source: controller.productBrands[index].image?.url ?? "",
                                height: 90,
                              ),
                            ),
                            CustomText(title: controller.productBrands[index].name ?? "", textType: TextType.SemiBold),
                          ],
                        ),
                      ),
                      staggeredTileBuilder: (_) => const StaggeredTile.fit(1),
                      crossAxisSpacing: Spacing.defaultSpacing,
                      mainAxisSpacing: Spacing.defaultSpacing,
                    ),
                  )
                ],
              ),
            )),
    );
  }
}
