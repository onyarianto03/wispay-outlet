import 'package:get/get.dart';

import '../controllers/sim_card_product_controller.dart';
import '../controllers/summary_sim_card_controller.dart';

import '../controllers/sim_card_controller.dart';

class SimCardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SummarySimCardController>(
      () => SummarySimCardController(),
      fenix: true,
    );
    Get.lazyPut<SimCardProductController>(
      () => SimCardProductController(),
      fenix: true,
    );
    Get.lazyPut<SimCardController>(
      () => SimCardController(),
    );
  }
}
