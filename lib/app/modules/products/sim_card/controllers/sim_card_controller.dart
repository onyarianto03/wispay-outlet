import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';

class SimCardController extends GetxController {
  final ProductController _productController = Get.find();

  final productBrands = <ProductBrandModel>[].obs;
  final isLoading = false.obs;
  final int categoryId = Get.arguments;

  @override
  void onInit() {
    super.onInit();
    getProductBrand();
  }

  @override
  void onClose() {}

  void getProductBrand() async {
    isLoading.value = true;
    final _subCategories = await _productController.getProductBrands(categoryId);
    if (_subCategories != null) {
      productBrands.value = _subCategories;
    }
    isLoading.value = false;
  }
}
