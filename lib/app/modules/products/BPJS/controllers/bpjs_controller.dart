import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/helpers/string_helper.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/products/BPJS/widgets/status_card.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class BpjsController extends GetxController {
  List<BaseModel> monthList = List.generate(
    12,
    (index) => BaseModel(
      id: index + 1,
      name: Jiffy().add(months: index).format('MMMM yyyy'),
    ),
  );

  final ProductController _productController = Get.find();
  final CategoriesController _homeController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final categoryId = Get.arguments;

  TextEditingController? phoneController;
  TextEditingController? customerIdController;
  final formKey = GlobalKey<FormState>();
  final monthFieldKey = GlobalKey<FormFieldState<BaseModel>>();

  var selectedMonth = const BaseModel().obs;
  var customerId = ''.obs;
  var isSubmitting = false.obs;
  var isLoading = false.obs;
  String? token = "";

  final customerIdValidator = MultiValidator([
    RequiredValidator(errorText: 'ID Pelanggan/Nomor Meter wajib diisi'),
    // MinLengthValidator(4, errorText: 'Minimal 4 Digit'),
  ]);

  final count = 0.obs;
  @override
  void onInit() async {
    super.onInit();
    getInitialData();
    customerIdController = TextEditingController()..addListener(onChangeCustomerID);
  }

  void getInitialData() async {
    isLoading.value = true;
    final productBrands = await _productController.getProductBrands(categoryId);
    final productSubcategories = await _productController.getSubCategories(productBrands?.first.id);
    await _productController.getProducts(productSubcategories?.first.id);
    isLoading.value = false;
  }

  @override
  void onClose() {
    super.onClose();
    customerIdController?.removeListener(onChangeCustomerID);
  }

  void onSelectMonth(BaseModel item) {
    selectedMonth.value = item;
  }

  void onChangeCustomerID() {
    customerId.value = customerIdController?.text ?? '';
  }

  onCheck() async {
    final isValid = formKey.currentState!.validate();

    if (isValid) {
      FocusManager.instance.primaryFocus?.unfocus();
      isSubmitting.value = true;
      final body = {
        "product_id": _productController.products.first.id,
        "customer_id": encrypt(customerId.value),
        "period": selectedMonth.value.id,
      };
      final response = await _transactionRepository.order(
        supplier: _productController.products.first.productSupplier!.code!.toLowerCase(),
        productName: 'bpjs',
        body: RequestBodyModel.fromJson(body),
      );
      token = response.data?.meta?.token;
      isSubmitting.value = false;

      if (response.success != false) {
        final commissionSetting = _homeController.profile.value.comissionSetting;
        double comission = double.parse(commissionSetting?.bpjs ?? commissionSetting?.all ?? '0');
        WispayBottomSheet.showConfirm(
          onConfirm: () => onPay(),
          withPromo: false,
          title: "Bayar BPJS",
          totalPrice: NumberHelper.formatMoney(
            response.data?.total ?? '0.0',
          ),
          children: [
            comission > 0
                ? InfoWidget(
                    text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                      comission.toString(),
                    )}',
                    margin: const EdgeInsets.only(top: 5),
                  )
                : const SizedBox(height: 0),
            const SizedBox(height: Spacing.defaultSpacing),
            TrxDetailItem(title: 'No. BPJS', value: decrypt(response.data?.customerId)),
            TrxDetailItem(title: 'Nama', value: StringHelper.convertArrayToString(decrypt(response.data?.name))),
            TrxDetailItem(title: 'Jumlah Peserta', value: response.data?.detailPayload?.numberParticipant),
            TrxDetailItem(title: 'Jumlah Bulan', value: response.data?.detailPayload?.period),
            TrxDetailItem(
                title: 'Tagihan',
                value: NumberHelper.formatMoney(
                  response.data?.price ?? '0.0',
                )),
            TrxDetailItem(
              title: 'Biaya Admin',
              value: NumberHelper.formatMoney(
                response.data?.adminFee ?? '0.0',
              ),
              marginBottom: 0,
            ),
          ],
        );
      }
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': decrypt(_homeController.profile.value.phone)},
    );
  }

  void _next(String pin) async {
    final body = {
      "pin": pin,
      "token": token,
    };
    final confirmResponse = await _transactionRepository.confirm(
      supplier: _productController.products.first.productSupplier?.code?.toLowerCase(),
      productName: 'bpjs',
      body: RequestBodyModel.fromJson(body),
    );
    if (confirmResponse.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(
              confirmResponse: confirmResponse.data!,
            ),
            transactionId: confirmResponse.data?.id,
            productCode: ProductCode.BPJS,
            status: confirmResponse.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }
}
