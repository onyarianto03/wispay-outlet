import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

import '../controllers/bpjs_controller.dart';

class BpjsView extends GetView<BpjsController> {
  const BpjsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            title: const Text('BPJS'),
            centerTitle: true,
            leading: IconButton(
              icon: const Icon(
                Icons.chevron_left,
                size: 35,
              ),
              onPressed: () => Get.back(),
            ),
          ),
          body: Obx(
            () => controller.isLoading.value
                ? Center(
                    child: Lottie.asset(Assets.lottieLoading, width: 100),
                  )
                : SafeArea(
                    child: Container(
                      padding: const EdgeInsets.all(Spacing.defaultSpacing),
                      child: Form(
                        autovalidateMode: AutovalidateMode.disabled,
                        key: controller.formKey,
                        child: Column(
                          children: [
                            CustomTextInput(
                              hintText: 'Nomor Peserta BPJS',
                              value: controller.customerId.value,
                              keyboardType: TextInputType.number,
                              validator: controller.customerIdValidator,
                              textEditingController: controller.customerIdController,
                              isEnabled: !controller.isSubmitting.value,
                              onClear: controller.customerIdController?.clear,
                              margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
                            ),
                            _dropdownField(
                              title: 'Bulan',
                              label: 'Bayar sampai',
                              items: controller.monthList,
                              onSelect: (value) => controller.onSelectMonth(value),
                              key: controller.monthFieldKey,
                              selectedItem: controller.selectedMonth.value,
                            ),
                            const SizedBox(height: Spacing.defaultSpacing),
                            CustomButton(
                              title: 'Cek Tagihan',
                              onPress: controller.onCheck,
                              isEnabled: controller.customerId.value != '' && !controller.isSubmitting.value,
                              isLoading: controller.isSubmitting.value,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
          )),
    );
  }

  Widget _dropdownField({
    title,
    ValueChanged<String>? onSearch,
    ValueChanged<BaseModel>? onSelect,
    bool enabled = true,
    required List<BaseModel> items,
    GlobalKey<FormFieldState<BaseModel>>? key,
    BaseModel? selectedItem,
    String? label,
  }) {
    return FormField(
      validator: (BaseModel? value) {
        if (value?.id == null) {
          return 'Silahkan pilih $title';
        }
        return null;
      },
      enabled: enabled,
      key: key,
      onSaved: (BaseModel? newValue) => onSelect?.call(newValue!),
      builder: (FormFieldState<BaseModel> field) => Dropdown(
        errorText: field.errorText,
        value: field.value?.name ?? "",
        label: label,
        enabled: enabled,
        onTap: () => WispayBottomSheet.scrollableList<BaseModel>(
          onSearch: onSearch,
          title: '$title',
          items: items,
          withSearch: false,
          selectedItem: selectedItem!,
          onSelect: (idx) {
            FocusManager.instance.primaryFocus?.unfocus();
            field.didChange(items[idx]);
            key?.currentState?.save();
            Get.back();
          },
        ),
      ),
    );
  }
}
