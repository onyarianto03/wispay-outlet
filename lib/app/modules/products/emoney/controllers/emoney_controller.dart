// Package imports:

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

import '../widgets/status_card.dart';

class EmoneyController extends GetxController {
  final ProductController _productController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final CategoriesController _homeController = Get.find();

  final listCard = <BaseModel>[].obs;
  final listDenom = <BaseModel>[].obs;
  final TextEditingController cNumber = TextEditingController();
  final cardNumber = ''.obs;

  final selectedCard = const BaseModel().obs;
  final selectedDenom = const BaseModel().obs;

  final key = GlobalKey<FormState>();

  final categoryId = Get.arguments;

  final isLoading = false.obs;
  @override
  void onInit() {
    cNumber.addListener(onchangeNumber);
    getInitialData();
    super.onInit();
  }

  @override
  void onClose() {}

  void getInitialData() async {
    isLoading.value = true;
    final _brands = await _productController.getProductBrands(categoryId);
    isLoading.value = false;
    if (_brands != null) {
      listCard.value = _brands;
    }
  }

  void onSelectCard(BaseModel model) async {
    selectedCard(model);
    Get.back();
    final _subcategories = await _productController.getSubCategories(model.id);
    if (_subcategories != null) {
      final _denoms = await _productController.getProducts(_subcategories[0].id);
      listDenom.value = _denoms!;
    }
  }

  void onSelectDenom(BaseModel model) {
    selectedDenom(model);
    Get.back();
  }

  void onchangeNumber() => cardNumber.value = cNumber.text;

  void _next(String pin) async {
    final _body = {
      "pin": pin,
      "product_id": selectedDenom.value.id,
      "customer_id": encrypt(cardNumber.value),
    };

    WispayDialog.showPaymentDialog();
    final _confirm = await _transactionRepository.confirm(
      supplier: selectedDenom.value.productSupplier?.code?.toLowerCase() ?? '',
      productName: 'emoney',
      body: RequestBodyModel.fromJson(_body),
    );
    if (_confirm.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(data: _confirm.data!, cardNumber: cardNumber.value),
            transactionId: _confirm.data?.id,
            productCode: ProductCode.EMONEY,
            status: _confirm.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    } else {
      WispayDialog.closeDialog();
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': decrypt(_homeController.profile.value.phone)},
    );
  }

  void onSubmit() {
    bool isValid = key.currentState!.validate();
    FocusManager.instance.primaryFocus?.unfocus();

    if (isValid) {
      WispayBottomSheet.showConfirm(
        onConfirm: () => onPay(),
        title: "Uang Elektronik",
        totalPrice: NumberHelper.formatMoney(selectedDenom.value.total?.toString()),
        withPromo: false,
        children: [
          InfoWidget(
            text:
                'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(_homeController.profile.value.comissionSetting?.dompetElektronik ?? _homeController.profile.value.comissionSetting?.all)}',
          ),
          const SizedBox(height: Spacing.defaultSpacing),
          TrxDetailItem(
            title: 'Nama Produk',
            value: selectedDenom.value.name ?? '-',
          ),
          TrxDetailItem(title: 'Nomor Kartu', value: cardNumber.value),
          TrxDetailItem(title: 'Harga', value: NumberHelper.formatMoney(selectedDenom.value.defaultPrice?.toString())),
        ],
      );
    }
  }
}
