// Flutter imports:
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/form_note.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

// Project imports:
import '../controllers/emoney_controller.dart';

class EmoneyView extends GetView<EmoneyController> {
  const EmoneyView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Uang Elektronik'),
          leading: IconButton(
            icon: const Icon(
              Icons.chevron_left,
              size: 35,
            ),
            onPressed: () {
              Get.back();
            },
          ),
          centerTitle: true,
        ),
        body: SafeArea(
          child: Obx(
            () => controller.isLoading.value
                ? Center(
                    child: Lottie.asset(Assets.lottieLoading, width: 100),
                  )
                : SingleChildScrollView(
                    padding: const EdgeInsets.all(Spacing.defaultSpacing),
                    child: Form(
                      key: controller.key,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _dropdownField(
                            title: 'Tipe Kartu',
                            selectedItem: controller.selectedCard.value,
                            items: controller.listCard,
                            withImage: true,
                            onSelect: (idx) {
                              controller.onSelectCard(controller.listCard[idx]);
                            },
                          ),
                          const SizedBox(
                            height: Spacing.defaultSpacing,
                          ),
                          CustomTextInput(
                            value: controller.cardNumber.value,
                            hintText: 'Nomor Kartu',
                            textEditingController: controller.cNumber,
                            fillColor: WispayColors.cWhite,
                            filled: true,
                            hintStyle: const TextStyle(color: WispayColors.cDisabledColor),
                            maxLength: 16,
                            validator: MultiValidator([
                              RequiredValidator(
                                errorText: 'Nomor Kartu harus diisi',
                              ),
                              MinLengthValidator(16, errorText: 'Minimal 16 karakter'),
                            ]),
                          ),
                          const SizedBox(height: Spacing.defaultSpacing),
                          _dropdownField(
                            title: 'Nominal Saldo',
                            selectedItem: controller.selectedDenom.value,
                            items: controller.listDenom,
                            withImage: false,
                            onSelect: (idx) {
                              controller.onSelectDenom(controller.listDenom[idx]);
                            },
                          ),
                          const SizedBox(height: Spacing.defaultSpacing),
                          CustomButton(
                            title: 'Top-up',
                            onPress: () => controller.onSubmit(),
                          ),
                          const SizedBox(height: Spacing.xMedium),
                          const FormNote(
                            children: [
                              CustomText(
                                title:
                                    'Setelah top-up kartu, mohon perbarui saldo melalui hp dengan fitur NFC atau kunjungi gerai ATM terdekat.',
                                color: WispayColors.cBlack333,
                              ),
                              CustomText(
                                title: 'Saldo maksimal di kartu adalah Rp2.000.000.',
                                color: WispayColors.cBlack333,
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
          ),
        ),
      ),
    );
  }

  Widget _dropdownField({
    required String title,
    required bool withImage,
    required List<BaseModel> items,
    required BaseModel selectedItem,
    required ValueChanged<int> onSelect,
  }) {
    return FormField(
      validator: (value) => value == null ? 'Pilih $title' : null,
      builder: (FormFieldState<BaseModel> field) => Dropdown(
        errorText: field.errorText,
        value: selectedItem.name ?? "",
        imageUrl: selectedItem.image?.url ?? "",
        withImage: withImage,
        label: 'Pilih $title',
        imageHeight: 25,
        imageWidth: 25,
        onTap: () => Future.delayed(
          const Duration(milliseconds: 100),
          () => WispayBottomSheet.scrollableList<BaseModel>(
            imageHeight: 25,
            imageWidth: 50,
            withSearch: false,
            title: title,
            withImage: withImage,
            items: items,
            selectedItem: selectedItem,
            onSelect: (idx) {
              field.didChange(items[idx]);
              onSelect.call(idx);
            },
          ),
        ),
      ),
    );
  }
}
