import 'package:flutter/cupertino.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';

class SkeletonKingWidget extends StatelessWidget {
  const SkeletonKingWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: WispayColors.cSecondary,
      highlightColor: WispayColors.cSecondary.withOpacity(0.3),
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: Spacing.xMedium, horizontal: Spacing.defaultSpacing),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: WispayColors.cSecondary.withOpacity(0.08),
        ),
        child: Column(
          children: [
            Stack(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    height: 14,
                    width: 216,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: WispayColors.cSecondary.withOpacity(0.08),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: Spacing.medium - 2),
            Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    height: 14,
                    width: 87,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: WispayColors.cSecondary.withOpacity(0.08),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: Spacing.medium - 2),
            Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    height: 14,
                    width: 87,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: WispayColors.cSecondary.withOpacity(0.08),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
