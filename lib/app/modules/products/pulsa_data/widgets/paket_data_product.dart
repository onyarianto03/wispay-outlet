import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class PaketDataProduct extends StatelessWidget {
  const PaketDataProduct({
    Key? key,
    required this.name,
    required this.price,
    this.description,
    this.poin,
    this.onTap,
  }) : super(key: key);

  final String name;
  final String? description;
  final String price;
  final String? poin;
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: BaseCard(
        spreadRadius: 0,
        blurRadius: 20,
        margin: EdgeInsets.zero,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: name,
              textType: TextType.Bold,
              margin: const EdgeInsets.only(bottom: Spacing.xSmall),
            ),
            CustomText(
              title: description ?? "",
              size: FontSize.small,
              textStyle: const TextStyle(height: 1.5),
              margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomText(
                  title: price,
                  size: FontSize.small,
                  color: WispayColors.cPrimary,
                ),
                Row(
                  children: [
                    Assets.iconsCoin.image(width: 16),
                    const SizedBox(width: Spacing.xSmall),
                    CustomText(
                      title: (poin ?? '0') + ' Poin',
                      size: FontSize.xSmall,
                    ),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
