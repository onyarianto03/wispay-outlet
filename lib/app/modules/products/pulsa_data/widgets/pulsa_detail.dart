// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_logo.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class PulsaDetail extends StatelessWidget {
  const PulsaDetail({
    Key? key,
    required this.payload,
    required this.status,
    this.hidebutton = false,
  }) : super(key: key);

  final TransactionDataModel payload;
  final String status;
  final bool hidebutton;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(Assets.iconsMiscWave2.path),
          alignment: Alignment.bottomLeft,
        ),
        borderRadius: BorderRadius.circular(Spacing.defaultSpacing),
        color: Colors.white,
        boxShadow: [buildShadow()],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          const InfoWidget(
            text: 'Struk ini adalah bukti pembayaran yang sah.',
            margin: EdgeInsets.only(bottom: Spacing.large),
            maxLines: 2,
          ),
          TrxDetailLogo(
            productName: payload.productName ?? '-',
            productImage: payload.product?.productBrand?.image?.thumbnail?.url,
          ),
          const SizedBox(height: Spacing.large),
          TrxDetailItem(
            title: 'Status',
            value: TransactionStatusHelper.statusColorAndText(status)['text'],
            valueColor: TransactionStatusHelper.statusColorAndText(status)['color'],
          ),
          TrxDetailItem(
            title: 'Nomor Transaksi',
            value: payload.code ?? '-',
          ),
          TrxDetailItem(
            title: 'Tanggal',
            value: DateHelper.formatDate(payload.createdAt, format: 'dd MMM yyyy HH:mm'),
          ),
          TrxDetailItem(
            title: 'ID Pelanggan',
            value: decrypt(payload.customerId),
          ),
          TrxDetailItem(
            title: 'Nomor SN',
            value: payload.status == "SUCCESS" ? decrypt(payload.serial) : '-',
            isBoldValue: true,
            marginBottom: 0,
          ),
          const Divider(height: 32),
          TrxDetailItem(
            title: 'Total Tagihan',
            value:
                NumberHelper.formatMoney((double.parse(payload.total) + double.parse(payload.commission)).toString()),
            isBoldTitle: true,
          ),
        ],
      ),
    );
  }
}
