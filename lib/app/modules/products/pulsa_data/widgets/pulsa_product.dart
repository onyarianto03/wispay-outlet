import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class PulsaProduct extends StatelessWidget {
  const PulsaProduct({
    Key? key,
    required this.name,
    required this.price,
    this.poin,
    this.isPurchaseable = true,
    this.onTap,
    this.isEnabled = false,
  }) : super(key: key);

  final GestureTapCallback? onTap;
  final String name;
  final String price;
  final String? poin;
  final bool isEnabled;
  final bool isPurchaseable;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: !isEnabled || !isPurchaseable ? null : onTap,
      child: BaseCard(
        margin: EdgeInsets.zero,
        padding: const EdgeInsets.symmetric(horizontal: 10),
        blurRadius: 20,
        isEnabled: isEnabled || isPurchaseable,
        spreadRadius: 0,
        height: 100,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomText(
              title: name,
              textType: TextType.Bold,
              textAlign: TextAlign.center,
              margin: const EdgeInsets.only(bottom: Spacing.small),
              maxLines: 2,
            ),
            CustomText(
              title: !isPurchaseable ? 'Dalam Kendala' : price,
              color: !isPurchaseable ? WispayColors.cRed : WispayColors.cPrimary,
              size: FontSize.small,
              margin: const EdgeInsets.only(bottom: Spacing.xSmall),
            ),
            if (poin != null)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Assets.iconsCoin.image(width: 16),
                  const SizedBox(width: Spacing.xSmall),
                  CustomText(
                    title: (poin ?? '0') + ' Poin',
                    size: FontSize.xSmall,
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }
}
