import 'package:get/get.dart';

import 'package:wispay_outlet/app/modules/products/pulsa_data/controllers/paket_data_products_controller.dart';

import '../controllers/pulsa_data_controller.dart';

class PulsaDataBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PaketDataProductsController>(
      () => PaketDataProductsController(),
      fenix: true,
    );
    Get.lazyPut<PulsaDataController>(
      () => PulsaDataController(),
    );
  }
}
