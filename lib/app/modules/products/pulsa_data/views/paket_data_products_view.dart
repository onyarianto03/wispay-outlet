import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/number_helper.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/modules/products/pulsa_data/controllers/paket_data_products_controller.dart';
import 'package:wispay_outlet/app/modules/products/pulsa_data/widgets/paket_data_product.dart';
import 'package:wispay_outlet/app/modules/products/pulsa_data/widgets/skeleton_army.dart';

class PaketDataProductsView extends GetView<PaketDataProductsController> {
  const PaketDataProductsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(controller.screenTitle),
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () => Get.back(),
        ),
        centerTitle: true,
      ),
      body: Obx(
        () => ListView.separated(
          controller: controller.sController,
          itemCount: controller.isLoading.value ? 2 : controller.products.length,
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          itemBuilder: (context, index) {
            if (controller.isLoading.value) {
              return const SkeletonArmyWidget();
            }
            final item = controller.products[index];
            return PaketDataProduct(
              name: item.name ?? '',
              price: NumberHelper.formatMoney(item.productPrice?.total.toString() ?? item.total.toString()),
              description: item.description ?? '',
              poin: NumberHelper.formatMoneyWithoutSymbol(item.productPrice?.point.toString() ?? item.point.toString()),
              onTap: () => controller.showModalConfirm(item),
            );
          },
          separatorBuilder: (_, __) => const SizedBox(height: Spacing.defaultSpacing),
        ),
      ),
    );
  }
}
