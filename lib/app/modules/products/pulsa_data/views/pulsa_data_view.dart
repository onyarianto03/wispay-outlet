import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:form_field_validator/form_field_validator.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/core/utils/input_price_formatter.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/top_tab_bar.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/products/pulsa_data/views/paket_data_products_view.dart';
import 'package:wispay_outlet/app/modules/products/pulsa_data/widgets/pulsa_product.dart';
import 'package:wispay_outlet/app/modules/products/pulsa_data/widgets/skeleton_king.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

import '../controllers/pulsa_data_controller.dart';

class PulsaDataView extends GetView<PulsaDataController> {
  const PulsaDataView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pulsa & Paket data'),
        leading: IconButton(
          icon: const Icon(Icons.chevron_left, size: 35),
          onPressed: () => Get.back(),
        ),
        centerTitle: true,
      ),
      body: Obx(
        () => controller.isLoading.value
            ? Center(
                child: Lottie.asset(Assets.lottieLoading, width: 100),
              )
            : Column(
                children: [
                  const SizedBox(height: Spacing.defaultSpacing),
                  CustomTextInput(
                    value: controller.phone.value,
                    isGSM: true,
                    showContact: true,
                    onContactPicked: (value) => controller.onPickedContact(value),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: MultiValidator(
                      [
                        RequiredValidator(errorText: 'Nomor HP tidak boleh kosong'),
                        IndonesiaPhoneNumber(errorText: 'Nomor HP tidak valid'),
                      ],
                    ),
                    hintText: 'Masukkan Nomor HP',
                    textEditingController: controller.cPhone,
                    margin: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                    hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
                  ),
                  Obx(() {
                    if (!controller.showProducts.value) {
                      return _placeholderImage();
                    } else {
                      return Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Column(
                            children: [
                              DefaultTabController(
                                length: 2,
                                child: TopTapBar(
                                  controller: controller.tabController,
                                  tabs: const [Tab(text: 'Pulsa'), Tab(text: 'Paket Data')],
                                ),
                              ),
                              Expanded(
                                child: TabBarView(
                                  controller: controller.tabController,
                                  children: [_pulsaView(), _paketDataView()],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }
                  })
                ],
              ),
      ),
    );
  }

  Widget _placeholderImage() {
    return Column(
      children: [
        const SizedBox(height: Spacing.large),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Assets.iconsGsmAxis.image(width: 44),
            Assets.iconsGsmIndosat.image(width: 44),
            Assets.iconsGsmTelkomsel.image(width: 44),
          ],
        ),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Assets.iconsGsmSmartfren.image(width: 44),
            Assets.iconsGsm3.image(width: 44),
            Assets.iconsGsmXl.image(width: 44),
          ],
        )
      ],
    );
  }

  Widget _pulsaView() {
    return Obx(
      () => ListView(
        controller: controller.sController,
        shrinkWrap: true,
        children: [
          Visibility(
            child: Container(
              padding: const EdgeInsets.all(Spacing.defaultSpacing),
              decoration: const BoxDecoration(
                color: Color(0xFFF7F9FA),
                image: DecorationImage(
                  image: Assets.iconsMiscWave2,
                  alignment: Alignment.bottomLeft,
                ),
              ),
              child: Column(
                children: [
                  const CustomText(
                    title:
                        "Beli pulsa sesuai kebutuhan mulai 1.000 - 1.000.000. nominal kelipatan 1.000 (harga sesuai area nomor tujuan)",
                  ),
                  const SizedBox(height: Spacing.defaultSpacing),
                  CustomTextInput(
                    value: controller.pulsa.value,
                    hintText: 'masukkan nominal pulsa',
                    textEditingController: controller.cPulsa,
                    fillColor: Colors.white,
                    filled: true,
                    keyboardType: TextInputType.number,
                    inputFormatters: [InputPriceFormatter()],
                    margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
                  ),
                  CustomButton(
                    title: 'Top-Up Pulsa',
                    onPress: controller.showConfirmPulsaDigipos,
                  )
                ],
              ),
            ),
            visible: controller.provider.value == TELKOMSEL,
          ),
          Visibility(
            child: const CustomText(
              title: "Atau pilih denom regular (harga pasti)",
              margin: EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
              textType: TextType.SemiBold,
              color: WispayColors.cBlack333,
            ),
            visible: controller.provider.value == TELKOMSEL && controller.p.isNotEmpty,
          ),
          const SizedBox(height: Spacing.defaultSpacing),
          StaggeredGridView.countBuilder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
            crossAxisCount: 2,
            itemCount: controller.isLoadingProduct.value ? 2 : controller.listPulsa.length,
            crossAxisSpacing: Spacing.defaultSpacing,
            mainAxisSpacing: Spacing.defaultSpacing,
            staggeredTileBuilder: (int index) => const StaggeredTile.fit(1),
            itemBuilder: (context, index) {
              if (controller.isLoadingProduct.value) {
                return const SkeletonKingWidget();
              }
              final item = controller.listPulsa[index];
              return PulsaProduct(
                isEnabled: controller.isEnabledCard.value,
                isPurchaseable: item.isPurchaseable ?? true,
                name: item.name ?? "",
                price: NumberHelper.formatMoney(item.productPrice?.total.toString() ?? item.total.toString()),
                poin:
                    NumberHelper.formatMoneyWithoutSymbol(item.productPrice?.point.toString() ?? item.point.toString()),
                onTap: () => controller.showConfirmPulsaNational(item),
              );
            },
          )
        ],
      ),
    );
  }

  Widget _paketDataView() {
    return StaggeredGridView.countBuilder(
        controller: controller.xController,
        padding: const EdgeInsets.symmetric(vertical: Spacing.defaultSpacing, horizontal: Spacing.defaultSpacing),
        crossAxisCount: 2,
        itemCount: controller.isLoadingProduct.value ? 2 : controller.listPaketData.length,
        crossAxisSpacing: Spacing.defaultSpacing,
        mainAxisSpacing: Spacing.defaultSpacing,
        staggeredTileBuilder: (int index) => const StaggeredTile.fit(1),
        itemBuilder: (context, index) {
          if (controller.isLoadingProduct.value) {
            return const SkeletonKingWidget();
          }
          return InkWell(
            onTap: () {
              FocusManager.instance.primaryFocus?.unfocus();
              Get.to(() => const PaketDataProductsView(), arguments: {
                "title": "Paket Data",
                "subcategoryId": controller.listPaketData[index].id,
                "phone": controller.phone.value
              });
            },
            child: BaseCard(
              height: 70,
              padding: const EdgeInsets.symmetric(horizontal: Spacing.small, vertical: Spacing.defaultSpacing),
              margin: EdgeInsets.zero,
              blurRadius: 20,
              spreadRadius: 0,
              child: Center(
                child: CustomText(
                  textAlign: TextAlign.center,
                  maxLines: 3,
                  title: controller.listPaketData[index].name ?? "",
                  textType: TextType.Bold,
                  margin: const EdgeInsets.only(bottom: Spacing.xSmall),
                ),
              ),
            ),
          );
        });
  }
}
