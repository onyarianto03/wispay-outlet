import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

import '../widgets/status_card.dart';

class PaketDataProductsController extends GetxController {
  String screenTitle = Get.arguments['title'];
  final ScrollController sController = ScrollController();
  final ProductController _productController = Get.find();
  final CategoriesController _homeController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());

  final subcategoryId = Get.arguments['subcategoryId'];
  final phone = Get.arguments['phone'];
  final products = <ProductModel>[].obs;
  var selectedItem = ProductModel();

  int page = 1;
  var isLastPage = false;
  final isLoading = false.obs;

  @override
  void onInit() {
    super.onInit();
    getProducts();
    sController.addListener(_scrollListener);
  }

  void getProducts({bool isLoadmore = false}) async {
    if (products.isEmpty) {
      isLoading.value = true;
    }
    final _filter = {
      'page': page,
      'limit': 14,
    };
    final newProducts = await _productController.getProducts(subcategoryId, filter: FilterModel.fromJson(_filter));

    if (newProducts != null) {
      if (isLoadmore == true && newProducts.isEmpty) {
        isLastPage = true;
      } else if (isLoadmore == true) {
        products.addAll(newProducts);
      } else {
        products.value = newProducts;
      }
    }
    isLoading.value = false;
  }

  void _scrollListener() async {
    if (sController.position.pixels == sController.position.maxScrollExtent) {
      if (isLastPage == true) return;
      page++;
      getProducts(isLoadmore: true);
    }
  }

  void _next(String pin) async {
    final body = {
      "pin": pin,
      "product_id": selectedItem.id,
      "phone": encrypt(NumberHelper.formatPhoneToIndonesian(phone)),
    };
    final confirmResponse = await _transactionRepository.confirm(
      supplier: selectedItem.productSupplier?.code?.toLowerCase(),
      productName: 'data-plan',
      body: RequestBodyModel.fromJson(body),
    );

    if (confirmResponse.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(
              confirmResponse: confirmResponse.data!,
            ),
            transactionId: confirmResponse.data?.id,
            productCode: ProductCode.DATA_PLAN,
            status: confirmResponse.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': _homeController.profile.value.phone},
    );
  }

  void showModalConfirm(ProductModel item) {
    selectedItem = item;
    final commissionSetting = _homeController.profile.value.comissionSetting;
    double comission = double.parse(commissionSetting?.pulsa ?? commissionSetting?.all ?? '0');
    WispayBottomSheet.showConfirm(
      title: 'Paket Data',
      withPromo: false,
      onConfirm: () => onPay(),
      totalPrice: NumberHelper.formatMoney(item.productPrice?.total.toString() ?? item.total.toString()),
      children: [
        const SizedBox(height: Spacing.small),
        comission > 0
            ? InfoWidget(
                text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                  comission.toString(),
                )}',
                margin: const EdgeInsets.only(top: 5),
              )
            : const SizedBox(height: 0),
        const SizedBox(height: Spacing.defaultSpacing),
        TrxDetailItem(title: 'Nama Produk', value: item.name),
        TrxDetailItem(title: 'Nomor Hp', value: phone),
        TrxDetailItem(
            title: 'Harga',
            value: NumberHelper.formatMoney(item.productPrice?.total.toString() ?? item.total.toString()))
      ],
    );
  }
}
