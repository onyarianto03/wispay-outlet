import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

import '../widgets/status_card.dart';

class PulsaDataController extends GetxController with SingleGetTickerProviderMixin {
  TextEditingController cPhone = TextEditingController();
  TextEditingController cPulsa = TextEditingController();
  final ScrollController sController = ScrollController();
  final ScrollController xController = ScrollController();
  final ProductController _productController = Get.find();
  final CategoriesController _homeController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  late TabController tabController;

  final p = <ProductModel>[].obs;
  final phone = ''.obs;
  final provider = ''.obs;
  final pulsa = ''.obs;
  final isEnabledCard = false.obs;
  final showProducts = false.obs;
  final isLoading = false.obs;
  final isLoadingProduct = false.obs;
  final categoryId = Get.arguments;
  var _isOrderDigipos = false;
  String? token = "";

  int pagePulsa = 1;
  var isLastPagePulsa = false;
  int pagePaketData = 1;
  var isLastPagePaketData = false;

  final listPulsa = <ProductModel>[].obs;
  final listPaketData = <BaseModel>[].obs;
  //brands
  final pBrandPulsa = <ProductBrandModel>[].obs;
  final pBrandPaketData = <ProductBrandModel>[].obs;

  var selectedItem = ProductModel();

  @override
  void onInit() {
    cPhone.addListener(onchangephone);
    cPulsa.addListener(onchangePulsa);
    tabController = TabController(length: 2, vsync: this);
    ever<String>(phone, (val) {
      if (val.length > 8) {
        provider.value = GSMHelper.getOperatorName(val);
        isEnabledCard.value = true;
      } else {
        isEnabledCard.value = false;
        showProducts.value = false;
        provider.value = '';
      }
    });
    getInitialData();
    debounce(phone, (_) async {
      if (phone.value.length > 8) {
        showProducts.value = true;
        getPulsa();
        getPaketData();
      }
    }, time: const Duration(milliseconds: 500));

    sController.addListener(_scrollListener);
    xController.addListener(_xscrollListener);
    super.onInit();
  }

  void getInitialData() async {
    isLoading.value = true;
    final brandPulsa = await _productController.getProductBrands(categoryId[0]);
    final brandPaketData = await _productController.getProductBrands(categoryId[1]);

    if (brandPulsa != null) {
      pBrandPulsa.value = brandPulsa;
    }
    if (brandPaketData != null) {
      pBrandPaketData.value = brandPaketData;
    }
    isLoading.value = false;
  }

  void _scrollListener() async {
    if (sController.position.pixels == sController.position.maxScrollExtent) {
      if (isLastPagePulsa == true) return;
      pagePulsa++;
      getPulsa(isLoadmore: true);
    }
  }

  void _xscrollListener() async {
    if (sController.position.pixels == sController.position.maxScrollExtent) {
      if (isLastPagePaketData == true) return;
      pagePaketData++;
      getPaketData(isLoadmore: true);
    }
  }

  void getPulsa({bool? isLoadmore}) async {
    if (listPulsa.isEmpty) {
      isLoadingProduct.value = true;
    }
    final _filter = {
      'page': pagePulsa,
      'limit': 14,
      "is_digipos": false,
    };
    final gsm = GSMHelper.getOperatorName(
      NumberHelper.formatPhoneRemove62(phone.value),
    );
    if (gsm.isNotEmpty) {
      final findBrand = pBrandPulsa.firstWhere(
        (brand) => brand.name?.toLowerCase() == gsm,
      );
      final pSubcategory = await _productController.getSubCategories(findBrand.id);
      final newProducts =
          await _productController.getProducts(pSubcategory?.first.id, filter: FilterModel.fromJson(_filter));
      if (newProducts != null) {
        if (isLoadmore == true && newProducts.isEmpty) {
          isLastPagePulsa = true;
        } else if (isLoadmore == true) {
          listPulsa.addAll(newProducts);
        } else {
          listPulsa.value = newProducts;
        }
      }
    }
    isLoadingProduct.value = false;
  }

  void getPaketData({bool? isLoadmore}) async {
    if (listPaketData.isEmpty) {
      isLoadingProduct.value = true;
    }
    final _filter = {
      'page': pagePaketData,
      'limit': 14,
    };
    final gsm = GSMHelper.getOperatorName(
      NumberHelper.formatPhoneRemove62(phone.value),
    );
    if (gsm.isNotEmpty) {
      final findBrand = pBrandPaketData.firstWhere(
        (brand) => brand.name?.toLowerCase() == gsm,
      );
      final newSubCategory =
          await _productController.getSubCategories(findBrand.id, filter: FilterModel.fromJson(_filter));
      if (newSubCategory != null) {
        if (isLoadmore == true && newSubCategory.isEmpty) {
          isLastPagePaketData = true;
        } else if (isLoadmore == true) {
          listPaketData.addAll(newSubCategory);
        } else {
          listPaketData.value = newSubCategory;
        }
      }
    }
    isLoadingProduct.value = false;
  }

  @override
  void onClose() {}

  onchangephone() {
    phone.value = cPhone.text;
  }

  void onPickedContact(String? phone) {
    if (phone != null) {
      cPhone.text = phone;
    }
  }

  onchangePulsa() => pulsa.value = cPulsa.text;

  void _next(String pin) async {
    dynamic body;
    if (_isOrderDigipos) {
      body = {
        "pin": pin,
        "token": token,
      };
    } else {
      body = {
        "pin": pin,
        "product_id": selectedItem.id,
        "phone": encrypt(NumberHelper.formatPhoneToIndonesian(phone.value)),
      };
    }
    final confirmResponse = await _transactionRepository.confirm(
      supplier: _isOrderDigipos ? 'digipos' : selectedItem.productSupplier?.code?.toLowerCase(),
      productName: _isOrderDigipos ? 'pulsa-digipos' : 'pulsa-national',
      body: RequestBodyModel.fromJson(body),
    );

    if (confirmResponse.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(
              confirmResponse: confirmResponse.data!,
            ),
            transactionId: confirmResponse.data?.id,
            productCode: ProductCode.PHONE_CREDIT,
            status: confirmResponse.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': _homeController.profile.value.phone},
    );
  }

  void showConfirmPulsaNational(ProductModel item) {
    selectedItem = item;
    _isOrderDigipos = false;
    final commissionSetting = _homeController.profile.value.comissionSetting;
    double comission = double.parse(commissionSetting?.pulsa ?? commissionSetting?.all ?? '0');
    FocusManager.instance.primaryFocus?.unfocus();
    WispayBottomSheet.showConfirm(
      title: 'Top-Up Pulsa',
      withPromo: false,
      onConfirm: () => onPay(),
      totalPrice: NumberHelper.formatMoney(item.productPrice?.total.toString() ?? item.total.toString()),
      children: [
        const SizedBox(height: Spacing.small),
        comission > 0
            ? InfoWidget(
                text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                  comission.toString(),
                )}',
                margin: const EdgeInsets.only(top: 5),
              )
            : const SizedBox(height: 0),
        const SizedBox(height: Spacing.defaultSpacing),
        TrxDetailItem(title: 'Nama Produk', value: item.name),
        TrxDetailItem(title: 'Nomor Hp', value: phone.value),
        TrxDetailItem(
            title: 'Harga',
            value: NumberHelper.formatMoney(item.productPrice?.total.toString() ?? item.total.toString())),
      ],
    );
  }

  void showConfirmPulsaDigipos() async {
    _isOrderDigipos = true;
    final commissionSetting = _homeController.profile.value.comissionSetting;
    double comission = double.parse(commissionSetting?.pulsa ?? commissionSetting?.all ?? '0');
    FocusManager.instance.primaryFocus?.unfocus();
    final body = {
      "phone": encrypt(NumberHelper.formatPhoneToIndonesian(phone.value)),
      "nominal": pulsa.value.replaceAll(".", ""),
    };
    final response = await _transactionRepository.order(
      supplier: 'digipos',
      productName: 'pulsa-digipos',
      body: RequestBodyModel.fromJson(body),
    );
    token = response.data?.meta?.token;

    if (response.success == true) {
      WispayBottomSheet.showConfirm(
        title: 'Top-Up Pulsa',
        withPromo: false,
        onConfirm: () => onPay(),
        totalPrice:
            NumberHelper.formatMoney(response.data?.productPrice?.total.toString() ?? response.data?.total.toString()),
        children: [
          const SizedBox(height: Spacing.small),
          comission > 0
              ? InfoWidget(
                  text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                    comission.toString(),
                  )}',
                  margin: const EdgeInsets.only(top: 5),
                )
              : const SizedBox(height: 0),
          const SizedBox(height: Spacing.defaultSpacing),
          TrxDetailItem(title: 'Nama Produk', value: response.data?.productName),
          TrxDetailItem(
              title: 'Nomor Hp', value: NumberHelper.formatPhoneRemove62(decrypt(response.data?.customerPhone))),
          TrxDetailItem(
              title: 'Harga',
              value: NumberHelper.formatMoney(
                  response.data?.productPrice?.total.toString() ?? response.data?.total.toString())),
        ],
      );
    }
  }
}
