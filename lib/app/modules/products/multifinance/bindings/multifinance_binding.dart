// Package imports:
import 'package:get/get.dart';

// Project imports:
import '../controllers/multifinance_controller.dart';

class MultifinanceBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MultifinanceController>(
      () => MultifinanceController(),
    );
  }
}
