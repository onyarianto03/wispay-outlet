// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/product_repository.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import '../widgets/status_card.dart';

class MultifinanceController extends GetxController {
  final ProductRepository _productRepository = ProductRepository(Get.find());
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final TextEditingController customerIdController = TextEditingController();
  final CategoriesController _homeController = Get.find();
  final categoryId = Get.arguments;

  final serviceProvider = <ProductModel>[].obs;
  final isLoading = false.obs;
  final isSubmitting = false.obs;
  final selectedService = ProductModel().obs;
  final customerId = ''.obs;
  String token = '';

  final _key = GlobalKey<FormState>();
  get formKey => _key;

  @override
  void onInit() {
    super.onInit();
    customerIdController.addListener(() {
      customerId.value = customerIdController.text;
    });
    getInitialData();
  }

  void getInitialData() async {
    isLoading.value = true;
    final _filter = {
      'product_category_id': categoryId,
    };
    final _services = await _productRepository.getProducts(filter: FilterModel.fromJson(_filter));
    if (_services.success != false) {
      serviceProvider.value = _services.data ?? [];
    }
    isLoading.value = false;
  }

  void onSelect(ProductModel service) async {
    selectedService.value = service;
    Get.back();
  }

  void _next(String pin) async {
    final _body = {
      "pin": pin,
      "token": token,
    };
    WispayDialog.showPaymentDialog();
    final _confirm = await _transactionRepository.confirm(
      supplier: selectedService.value.productSupplier?.code,
      productName: 'multi-finance',
      body: RequestBodyModel.fromJson(_body),
    );
    if (_confirm.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(data: _confirm.data!),
            transactionId: _confirm.data?.id,
            productCode: ProductCode.MULTIFINANCE,
            status: _confirm.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    } else {
      WispayDialog.closeDialog();
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': decrypt(_homeController.profile.value.phone)},
    );
  }

  void onCheck() async {
    FocusManager.instance.primaryFocus?.unfocus();
    bool isValid = _key.currentState!.validate();

    if (isValid) {
      isSubmitting.value = true;
      final _body = {
        'customer_id': encrypt(customerId.value),
        'product_id': selectedService.value.id,
      };

      final _order = await _transactionRepository.order(
        supplier: selectedService.value.productSupplier?.code ?? '',
        productName: 'multi-finance',
        body: RequestBodyModel.fromJson(_body),
      );

      isSubmitting.value = false;

      if (_order.success != false) {
        token = _order.data!.meta!.token!;
        WispayBottomSheet.showConfirm(
          onConfirm: () => onPay(),
          title: "Info Tagihan",
          totalTitle: 'Total Tagihan',
          totalPrice: NumberHelper.formatMoney(_order.data?.total),
          withPromo: false,
          children: [
            InfoWidget(
              text:
                  'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(_homeController.profile.value.comissionSetting?.multiFinance ?? _homeController.profile.value.comissionSetting?.all)}',
            ),
            const SizedBox(height: Spacing.defaultSpacing),
            TrxDetailItem(title: 'Penyedia layanan', value: selectedService.value.name),
            TrxDetailItem(title: 'Nomor pelanggan', value: customerId.value),
            TrxDetailItem(title: 'Nama pelanggan', value: decrypt(_order.data?.name)),
            TrxDetailItem(title: 'Jumlah tagihan', value: NumberHelper.formatMoney(_order.data?.price)),
            TrxDetailItem(title: 'Biaya admin', value: NumberHelper.formatMoney(_order.data?.adminFee)),
          ],
        );
      }
    }
  }
}
