// Flutter imports:
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import '../controllers/multifinance_controller.dart';

class MultifinanceView extends GetView<MultifinanceController> {
  const MultifinanceView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Multi Finance'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () => Get.back(),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(Spacing.defaultSpacing),
        child: Obx(
          () => controller.isLoading.value
              ? Center(
                  child: Lottie.asset(Assets.lottieLoading, width: 100),
                )
              : Form(
                  autovalidateMode: AutovalidateMode.disabled,
                  key: controller.formKey,
                  child: Column(
                    children: [
                      _dropdownField(
                        title: 'Penyedia Layanan',
                        selectedItem: controller.selectedService.value,
                        items: controller.serviceProvider,
                        withImage: true,
                        onSelect: (idx) {
                          controller.onSelect(controller.serviceProvider[idx]);
                        },
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      CustomTextInput(
                        value: controller.customerId.value,
                        textEditingController: controller.customerIdController,
                        hintText: 'Nomor Pelanggan',
                        validator: MultiValidator([
                          RequiredValidator(errorText: 'Nomor Pelanggan tidak boleh kosong'),
                        ]),
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      CustomButton(
                        title: 'Cek Tagihan',
                        onPress: () => controller.onCheck(),
                        isLoading: controller.isSubmitting.value,
                        isEnabled: !controller.isSubmitting.value,
                      )
                    ],
                  ),
                ),
        ),
      ),
    );
  }

  Widget _dropdownField({
    required String title,
    required bool withImage,
    required List<ProductModel> items,
    required ProductModel selectedItem,
    required ValueChanged<int> onSelect,
  }) {
    return FormField(
      validator: (value) => value == null ? 'Pilih $title' : null,
      builder: (FormFieldState<BaseModel> field) => Dropdown(
        errorText: field.errorText,
        value: selectedItem.name ?? "",
        imageUrl: selectedItem.image?.url ?? "",
        withImage: withImage,
        label: 'Pilih $title',
        imageHeight: 25,
        imageWidth: 25,
        onTap: () => Future.delayed(
          const Duration(milliseconds: 100),
          () => WispayBottomSheet.scrollableList<BaseModel>(
            imageHeight: 25,
            imageWidth: 50,
            withSearch: false,
            title: title,
            withImage: withImage,
            items: items,
            selectedItem: selectedItem,
            onSelect: (idx) {
              field.didChange(items[idx]);
              onSelect.call(idx);
            },
          ),
        ),
      ),
    );
  }
}
