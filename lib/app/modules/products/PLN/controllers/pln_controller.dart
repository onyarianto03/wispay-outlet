import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/products/PLN/widgets/status_card.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class PlnController extends GetxController with SingleGetTickerProviderMixin {
  late TabController tabController = TabController(
    length: 2,
    vsync: this,
  );
  TextEditingController? customerIdController;
  final _formKeyToken = GlobalKey<FormState>();
  final _formKeyTagihan = GlobalKey<FormState>();

  final ProductController _productController = Get.find();
  final CategoriesController _homeController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());

  final categoryId = Get.arguments;

  final _activeTab = 0.obs;
  var phone = ''.obs;
  var customerId = ''.obs;
  var isSubmitting = false.obs;
  final isLoading = false.obs;
  final showToken = false.obs;
  final token = <ProductModel>[].obs;
  final subCategories = <BaseModel>[].obs;

  get formKeyToken => _formKeyToken;
  get formKeyTagihan => _formKeyTagihan;

  final customerIdValidator = MultiValidator([
    RequiredValidator(errorText: 'ID Pelanggan/Nomor Meter wajib diisi'),
    MinLengthValidator(10, errorText: 'Minimal 10 Digit'),
  ]);

  @override
  void onInit() {
    super.onInit();

    getInitialData();
    tabController.addListener(_tabListener);
    customerIdController = TextEditingController()..addListener(onChangeCustomerID);
  }

  @override
  void onClose() {
    super.onClose();
    customerIdController?.removeListener(onChangeCustomerID);
  }

  void _tabListener() {
    if (!tabController.indexIsChanging) {
      _activeTab.value = tabController.index;
      customerIdController?.clear();
      showToken.value = false;
      FocusManager.instance.primaryFocus?.unfocus();
    }
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  void onChangeCustomerID() {
    customerId.value = customerIdController?.text ?? '';
    showToken.value = false;
  }

  void getInitialData() async {
    final productBrands = await _productController.getProductBrands(categoryId);
    final productSubcategories = await _productController.getSubCategories(productBrands?.first.id);
    if (productSubcategories != null) subCategories.value = productSubcategories;
  }

  void getProductToken() async {
    showToken.value = false;
    isLoading.value = true;
    final _dataProductToken = subCategories.where((obj) => obj.name == 'Pra Bayar').toList()[0];
    final _products = await _productController.getProducts(_dataProductToken.id);
    showToken.value = true;
    isLoading.value = false;
    if (_products != null) token.value = _products;
  }

  void getTagihan() async {
    showToken.value = false;
    isLoading.value = true;
    final _dataProductToken = subCategories.where((obj) => obj.name == 'Pasca Bayar').toList()[0];
    final _products = await _productController.getProducts(_dataProductToken.id);
    showToken.value = true;
    if (_products != null) {
      showBottomSheet(_products[0], 'pln-postpaid');
    } else {
      isLoading.value = false;
    }
  }

  onCheck(bool? isToken) async {
    FocusManager.instance.primaryFocus?.unfocus();

    if (isToken!) {
      final isValidToken = _formKeyToken.currentState!.validate();

      if (isValidToken) {
        getProductToken();
      }
    } else {
      final isValidTagihan = _formKeyTagihan.currentState!.validate();

      if (isValidTagihan) {
        getTagihan();
      }
    }
  }

  void showBottomSheet(ProductModel data, String productName) async {
    final _body = {
      "product_id": data.id,
      "customer_id": encrypt(customerId.value),
    };
    final supplier = data.productSupplier?.code?.toLowerCase() ?? '';
    final _transactionOrder = await _transactionRepository.order(
      supplier: supplier,
      productName: productName,
      body: RequestBodyModel.fromJson(_body),
    );
    isLoading.value = false;
    if (_transactionOrder.success != false) {
      WispayBottomSheet.showConfirm(
        onConfirm: () => onPay(
          supplier: supplier,
          productName: productName,
          token: _transactionOrder.data?.meta?.token ?? '',
        ),
        title: "Bayar Tagihan",
        totalPrice: NumberHelper.formatMoney(_transactionOrder.data?.total),
        withPromo: false,
        point: NumberHelper.formatMoneyWithoutSymbol(data.defaultPoint),
        children: [
          InfoWidget(
            text:
                'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(_homeController.profile.value.comissionSetting?.pln ?? _homeController.profile.value.comissionSetting?.all)}',
          ),
          const SizedBox(height: Spacing.defaultSpacing),
          TrxDetailItem(title: 'ID Pelanggan/Nomor Meter', value: customerId.value),
          TrxDetailItem(title: 'Nama', value: decrypt(_transactionOrder.data?.detailPayload?.name)),
          productName == 'pln-prepaid'
              ? TrxDetailItem(title: 'Tarik/Daya', value: _transactionOrder.data?.detailPayload?.power ?? '')
              : TrxDetailItem(title: 'Bulan', value: _transactionOrder.data?.detailPayload?.period ?? ''),
          TrxDetailItem(title: 'Harga', value: NumberHelper.formatMoney(_transactionOrder.data?.price)),
          TrxDetailItem(title: 'Biaya Admin', value: NumberHelper.formatMoney(_transactionOrder.data?.adminFee)),
        ],
      );
    }
  }

  void _next({
    required String pin,
    required String supplier,
    required String productName,
    required String token,
  }) async {
    final _body = {
      "pin": pin,
      "token": token,
    };
    final _confirm = await _transactionRepository.confirm(
      supplier: supplier,
      productName: productName,
      body: RequestBodyModel.fromJson(_body),
    );
    if (_confirm.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(data: _confirm.data!),
            transactionId: _confirm.data?.id,
            productCode: ProductCode.PLN,
            status: _confirm.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void onPay({
    required String supplier,
    required String productName,
    required String token,
  }) {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {
        'next': (String pin) => _next(
              pin: pin,
              supplier: supplier,
              productName: productName,
              token: token,
            )
      },
    );
  }
}
