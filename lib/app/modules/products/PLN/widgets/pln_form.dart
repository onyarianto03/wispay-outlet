import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/PLN/controllers/pln_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PLNForm extends StatelessWidget {
  const PLNForm({
    Key? key,
    required this.refKey,
    required this.controller,
    required this.note,
    this.nominalToken = const SizedBox(),
    required this.buttonTitle,
    this.isToken = false,
  }) : super(key: key);

  final PlnController controller;
  final GlobalKey<FormState> refKey;
  final Widget note;
  final Widget nominalToken;
  final String buttonTitle;
  final bool isToken;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Obx(
        () => Form(
          autovalidateMode: AutovalidateMode.disabled,
          key: refKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                color: WispayColors.cWhite,
                child: Padding(
                  padding: const EdgeInsets.all(Spacing.defaultSpacing),
                  child: Column(
                    children: <Widget>[
                      CustomTextInput(
                        hintText: 'Masukkan ID Pelanggan/Nomor Meter',
                        value: controller.customerId.value,
                        keyboardType: TextInputType.number,
                        validator: controller.customerIdValidator,
                        textEditingController: controller.customerIdController,
                        isEnabled: !controller.isSubmitting.value,
                        onClear: controller.customerIdController?.clear,
                        margin: const EdgeInsets.only(bottom: Spacing.medium),
                      ),
                      CustomButton(
                        title: buttonTitle,
                        onPress: () => controller.onCheck(isToken),
                        isLoading: controller.isLoading.value,
                        isEnabled: controller.customerId.value != '' && !controller.isLoading.value,
                      ),
                      const SizedBox(
                        height: Spacing.defaultSpacing,
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: Spacing.medium,
                    ),
                    const CustomText(
                      title: 'Keterangan:',
                      color: WispayColors.cBlack,
                      textType: TextType.Bold,
                      margin: EdgeInsets.only(bottom: Spacing.small),
                    ),
                    Container(
                      padding: const EdgeInsets.all(Spacing.defaultSpacing),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8.r)),
                        color: WispayColors.cPrimary.withOpacity(0.2),
                      ),
                      child: note,
                    ),
                  ],
                ),
              ),
              nominalToken,
            ],
          ),
        ),
      ),
    );
  }
}
