// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/global_widgets/top_tab_bar.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/PLN/widgets/pln_form.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:auto_size_text/auto_size_text.dart';

// Project imports:
import '../controllers/pln_controller.dart';

class PlnView extends GetView<PlnController> {
  const PlnView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('PLN'),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(Icons.chevron_left, size: 35),
            onPressed: () => Get.back(),
          ),
        ),
        body: SafeArea(
          child: DefaultTabController(
            length: 2,
            child: Column(
              children: [
                Container(
                  color: WispayColors.cWhite,
                  child: TopTapBar(
                    controller: controller.tabController,
                    tabs: const [
                      Tab(text: 'Beli Token'),
                      Tab(text: 'Bayar Tagihan'),
                    ],
                  ),
                ),
                Expanded(
                  child: TabBarView(
                    controller: controller.tabController,
                    children: [
                      // Beli Token
                      Obx(
                        () => PLNForm(
                          controller: controller,
                          refKey: controller.formKeyToken,
                          buttonTitle: 'Tampilkan Nominal Token',
                          isToken: true,
                          note: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              _note(
                                number: '1. ',
                                content: const TextSpan(
                                  text: 'Pembelian Token tidak dapat dilakukan pada jam ',
                                  children: [
                                    TextSpan(
                                      text: '23.00 - 01.00 WIB ',
                                      style: TextStyle(fontWeight: FontWeight.w600),
                                    ),
                                    TextSpan(text: 'Sesuai ketentuan PLN.'),
                                  ],
                                ),
                              ),
                              _note(
                                number: '2. ',
                                content: const TextSpan(
                                  text: 'Harap cek ',
                                  children: [
                                    TextSpan(
                                      text: 'limit kWh ',
                                      style: TextStyle(fontWeight: FontWeight.w600),
                                    ),
                                    TextSpan(text: 'sebelum membeli token.'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          nominalToken:
                              controller.showToken.value ? _buildItem(context, controller.token) : const SizedBox(),
                        ),
                      ),

                      // Bayar Tagihan
                      PLNForm(
                        controller: controller,
                        refKey: controller.formKeyTagihan,
                        buttonTitle: 'Cek Tagihan',
                        note: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            _note(
                              number: '1. ',
                              content: const TextSpan(
                                text: 'Bayar tagihan tidak dapat dilakukan pada jam ',
                                children: [
                                  TextSpan(
                                    text: '23.00 - 01.00 WIB ',
                                    style: TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                  TextSpan(text: 'Sesuai ketentuan PLN.'),
                                ],
                              ),
                            ),
                            _note(
                              number: '2. ',
                              content: const TextSpan(
                                text: 'Batas pembayaran tagihan paling lambat ',
                                children: [
                                  TextSpan(
                                    text: 'tanggal 20 ',
                                    style: TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                  TextSpan(text: 'setiap bulan.'),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _note({required String number, required TextSpan content}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText(
          title: number,
          color: WispayColors.cBlack,
        ),
        Expanded(
          child: AutoSizeText.rich(
            content,
            style: const TextStyle(fontSize: 14, color: WispayColors.cBlack),
          ),
        )
      ],
    ).marginOnly(bottom: Spacing.xSmall);
  }

  Widget _buildItem(BuildContext context, List<ProductModel> token) {
    return Padding(
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      child: StaggeredGridView.countBuilder(
        crossAxisCount: 2,
        physics: const NeverScrollableScrollPhysics(),
        padding: const EdgeInsets.symmetric(vertical: Spacing.small),
        shrinkWrap: true,
        mainAxisSpacing: Spacing.medium,
        crossAxisSpacing: Spacing.medium,
        controller: ScrollController(keepScrollOffset: false),
        itemCount: token.length,
        itemBuilder: (_, i) {
          return InkWell(
            onTap: () {
              controller.showBottomSheet(token[i], 'pln-prepaid');
            },
            child: Container(
              padding: const EdgeInsets.all(Spacing.medium),
              // height: 60,
              decoration: BoxDecoration(
                color: WispayColors.cWhite,
                borderRadius: BorderRadius.circular(8.r),
                boxShadow: const [shadow],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomText(
                    title: token[i].name ?? '',
                    color: WispayColors.cBlack,
                    textType: TextType.Bold,
                    textAlign: TextAlign.center,
                  ),
                  CustomText(
                    title: NumberHelper.formatMoney(token[i].total.toString()),
                    color: WispayColors.cPrimary,
                  ),
                ],
              ),
            ),
          );
        },
        staggeredTileBuilder: (_) => const StaggeredTile.extent(1, 78),
      ),
    );
  }
}
