import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/services/storage_service.dart';
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/profile/profile_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/models/utility/prediction_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/app/views/map_view.dart';

import '../widgets/status_card.dart';

class SummaryModemController extends GetxController {
  final Map<String, dynamic> arguments = Get.arguments;
  final CategoriesController homeController = Get.find();
  TextEditingController phoneController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final StorageService _storage = Get.find();

  final phoneNumberValidator = MultiValidator([
    RequiredValidator(errorText: 'Nomor HP harus diisi'),
    IndonesiaPhoneNumber(errorText: 'Nomor HP tidak valid'),
  ]);

  final phone = ''.obs;
  get formKey => _formKey;
  final isLoading = false.obs;
  final deliveryAddress = Rxn<LocationDataModel>();
  String token = '';

  @override
  onInit() {
    super.onInit();
    phoneController.addListener(onChangePhone);
    phoneController.text = NumberHelper.formatPhoneRemove62(decrypt(homeController.profile.value.phone));
    getDeliveryAddress();
  }

  @override
  void onClose() {
    phoneController.removeListener(onChangePhone);
  }

  void getDeliveryAddress() {
    final _address = _storage.getDeliveryAddress();
    final ProfileModel profile = homeController.profile.value;

    if (_address != null) {
      deliveryAddress.value = _address;
    } else {
      deliveryAddress.value = LocationDataModel.fromJson({
        'address': decrypt(profile.outletAddress),
        'latitude': decrypt(profile.outletLatitude),
        'longitude': decrypt(profile.outletLongitude),
      });
    }
  }

  void onPickedContact(String? phone) {
    if (phone != null) {
      phoneController.text = phone;
    }
  }

  void onChangePhone() {
    phone.value = phoneController.text;
  }

  void onChangeAddress() async {
    final _data = await Get.to<LocationDataModel>(() => const MapView());

    if (_data != null) {
      _storage.saveDeliveryAddress(_data);
      getDeliveryAddress();
    }
  }

  void _next(String pin) async {
    final ProductModel data = arguments['product'];

    final Map<String, dynamic> _body = {
      'token': token,
      'pin': pin,
    };

    final _confirm = await _transactionRepository.confirm(
      supplier: data.productSupplier?.code ?? '',
      productName: 'modem',
      body: RequestBodyModel.fromJson(_body),
    );

    if (_confirm.data != null) {
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(data: _confirm.data!),
            transactionId: _confirm.data?.id,
            productCode: ProductCode.SIM_CARD,
            status: _confirm.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': decrypt(homeController.profile.value.phone)},
    );
  }

  void onConfirm() async {
    final isValid = _formKey.currentState!.validate();

    if (isValid) {
      final ProductModel data = arguments['product'];
      final int qty = arguments['qty'];
      final ProfileModel profile = homeController.profile.value;
      final _address = deliveryAddress.value;

      final Map<String, dynamic> _body = {
        'product_id': data.id,
        'quantity': qty,
        'phone': encrypt(NumberHelper.formatPhoneToIndonesian(phone.value)),
        'address': decrypt(profile.outletAddress) != _address?.address ? _address?.address : null,
        'latitude': decrypt(profile.outletLatitude) != _address?.latitude ? _address?.latitude : null,
        'longitude': decrypt(profile.outletLongitude) != _address?.longitude ? _address?.longitude : null,
      };

      isLoading.value = true;

      final _order = await _transactionRepository.order(
        supplier: data.productSupplier?.code ?? '',
        productName: 'modem',
        body: RequestBodyModel.fromJson(_body),
      );

      isLoading.value = false;

      if (_order.success!) {
        token = _order.data?.meta?.token ?? '';
        final commissionSetting = profile.comissionSetting;
        double comission = double.parse(commissionSetting?.modem ?? commissionSetting?.all ?? '0.0');

        WispayBottomSheet.showConfirm(
          title: 'Ringkasan Pembelian',
          totalTitle: 'Harga Jual',
          totalPrice: NumberHelper.formatMoney(_order.data?.total),
          onConfirm: () => onPay(),
          withPromo: false,
          point: NumberHelper.formatMoneyWithoutSymbol(data.productPrice?.point),
          children: [
            comission > 0
                ? InfoWidget(
                    text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                      comission.toString(),
                    )}',
                    margin: const EdgeInsets.only(top: 5),
                  )
                : const SizedBox(height: 0),
            const SizedBox(height: Spacing.defaultSpacing),
            TrxDetailItem(title: 'Nama Produk', value: _order.data?.productName ?? ''),
            TrxDetailItem(
              title: 'Nomor HP',
              value: NumberHelper.formatPhoneRemove62(decrypt(_order.data?.customerPhone)),
            ),
            TrxDetailItem(title: 'Harga', value: NumberHelper.formatMoney(_order.data?.price)),
            TrxDetailItem(title: 'Biaya Admin', value: NumberHelper.formatMoney(_order.data?.adminFee ?? '0')),
          ],
        );
      }
    }
  }
}
