import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/controllable_input.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';

import '../views/summary_modem_view.dart';

class ModemProductController extends GetxController {
  final ProductController _productController = Get.find();

  final products = <ProductModel>[].obs;
  final ProductBrandModel brand = Get.arguments;
  final isLoading = false.obs;
  final subCategoryId = 0.obs;
  int page = 1;
  bool isEndPage = false;
  bool isLoadMore = false;

  @override
  void onInit() {
    super.onInit();
    isLoading.value = true;
    getInitialData();
  }

  @override
  void onClose() {}

  void onLoadMore() async {
    if (isEndPage || isLoadMore) return;
    page++;
    isLoadMore = true;
    final _products = await _productController.getProducts(
      subCategoryId.value,
      filter: FilterModel(
        page: page,
        isItem: true,
      ),
    );
    if (_products != null) {
      if (_products.isNotEmpty) {
        products.addAll(_products);
      } else {
        isEndPage = true;
      }
    }
  }

  onSelect(idx) {
    final qty = 1.obs;

    WispayBottomSheet.scrollable(
      withSearch: false,
      withTitle: false,
      initialChildSize: 0.65,
      minChildSize: 0.65,
      itemBuilder: (_, sc) => Stack(
        fit: StackFit.expand,
        children: [
          ListView(
            controller: sc,
            children: [
              CustomImage(
                source: products[idx].image?.url ?? "",
                width: double.infinity,
                height: 150,
              ),
              CustomText(
                title: products[idx].name ?? "",
                size: FontSize.medium,
                textType: TextType.SemiBold,
                margin: const EdgeInsets.symmetric(vertical: Spacing.small),
              ),
              const Divider(thickness: 1),
              const SizedBox(height: Spacing.defaultSpacing),
              HtmlWidget(products[idx].description ?? ""),
              const Divider(thickness: 1),
              const SizedBox(height: 120),
            ],
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              color: WispayColors.cWhite,
              child: Obx(
                () => Column(
                  children: [
                    const SizedBox(height: Spacing.defaultSpacing - 3),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const CustomText(title: 'Total'),
                              CustomText(
                                title: NumberHelper.formatMoney(
                                  (double.parse(products[idx].productPrice?.price) * qty.value).toString(),
                                ),
                                textType: TextType.SemiBold,
                                color: WispayColors.cPrimary,
                                size: FontSize.large,
                              ),
                            ],
                          ),
                        ),
                        ControllableInput(
                          value: qty.value.toString(),
                          onChangeValue: (val) => qty.value = val,
                        )
                      ],
                    ),
                    const SizedBox(height: Spacing.defaultSpacing),
                    CustomButton(
                      title: 'Beli',
                      onPress: () {
                        final _arguments = {
                          'product': products[idx],
                          'qty': qty.value,
                        };
                        Get.back();
                        Get.to(() => const SummaryModemView(), arguments: _arguments);
                      },
                    ),
                    const SizedBox(height: Spacing.defaultSpacing),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> getInitialData() async {
    page = 1;
    final _subcategories = await _productController.getSubCategories(brand.id);
    if (_subcategories != null) {
      subCategoryId.value = _subcategories.first.id!;
      final _products = await _productController.getProducts(
        _subcategories.first.id,
        filter: const FilterModel(isItem: true),
      );
      if (_products != null) {
        if (_products.isNotEmpty) {
          products.value = _products;
        } else {
          isEndPage = true;
        }
      }
      isLoading(false);
    }
  }
}
