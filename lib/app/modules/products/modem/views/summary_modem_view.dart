import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/profile/profile_model.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';

import '../controllers/summary_modem_controller.dart';

class SummaryModemView extends GetView<SummaryModemController> {
  const SummaryModemView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ProductModel data = controller.arguments['product'];
    final int qty = controller.arguments['qty'];
    final ProfileModel profile = controller.homeController.profile.value;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Ringkasan Pembelian'),
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () => Get.back(),
        ),
        centerTitle: true,
      ),
      body: Obx(
        () => Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          key: controller.formKey,
          child: Container(
            color: Colors.white,
            padding: const EdgeInsets.symmetric(vertical: Spacing.defaultSpacing),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const CustomText(
                        title: 'Pembelian',
                        size: FontSize.small,
                        textType: TextType.SemiBold,
                      ),
                      const SizedBox(height: Spacing.small),
                      Row(
                        children: [
                          Expanded(
                            child: CustomText(
                              title: data.name ?? "",
                              size: FontSize.medium,
                              color: WispayColors.cBlack333,
                            ),
                          ),
                          CustomText(
                            title: " x$qty",
                            size: FontSize.medium,
                            color: WispayColors.cPrimary,
                          ),
                        ],
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      CustomTextInput(
                        value: controller.phone.value,
                        textEditingController: controller.phoneController,
                        validator: controller.phoneNumberValidator,
                        onClear: () => controller.phone.value = "",
                        showContact: true,
                        onContactPicked: (value) => controller.onPickedContact(value),
                        hintText: 'Nomor HP',
                        hintStyle: const TextStyle(color: WispayColors.cDisabledColor),
                        label: 'No. HP Pembeli',
                        keyboardType: TextInputType.phone,
                        isEnabled: !controller.isLoading.value,
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      const CustomText(
                        title: 'Alamat Pengiriman',
                        size: FontSize.small,
                        textType: TextType.SemiBold,
                        margin: EdgeInsets.only(bottom: Spacing.small),
                      ),
                      Container(
                        padding: const EdgeInsets.all(Spacing.defaultSpacing),
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Spacing.small),
                          border: Border.all(
                            color: WispayColors.cDisabledColor,
                            width: profile.outletAddress != null ? 1 : 0,
                          ),
                          color: profile.outletAddress == null ? WispayColors.cRed2.withOpacity(0.1) : null,
                        ),
                        child: profile.outletAddress != null
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      CustomText(
                                        title: profile.name ?? '',
                                        color: WispayColors.cBlack333,
                                        textType: TextType.SemiBold,
                                      ),
                                      const Spacer(),
                                      InkWell(
                                        onTap: controller.onChangeAddress,
                                        child: const CustomText(
                                          title: 'Ganti Alamat',
                                          textType: TextType.SemiBold,
                                          color: WispayColors.cPrimary,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: Spacing.small),
                                  CustomText(
                                    title: controller.deliveryAddress.value?.address ?? '',
                                    color: WispayColors.cBlack333,
                                    size: FontSize.medium,
                                  ),
                                ],
                              )
                            : const AutoSizeText.rich(
                                TextSpan(
                                  text: 'Maaf, Anda belum mengatur alamat pengiriman modem. ',
                                  children: [
                                    TextSpan(
                                      text: 'Atur sekarang',
                                      style: TextStyle(
                                        color: WispayColors.cRed,
                                        fontWeight: FontWeight.w600,
                                        decoration: TextDecoration.underline,
                                      ),
                                    ),
                                  ],
                                  style: TextStyle(fontSize: 14, color: WispayColors.cBlack333),
                                ),
                              ),
                      ),
                    ],
                  ),
                ),
                const Spacer(),
                const Divider(thickness: 1),
                const SizedBox(height: Spacing.medium),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const CustomText(title: 'Total'),
                            CustomText(
                              title:
                                  NumberHelper.formatMoney((double.parse(data.productPrice?.price) * qty).toString()),
                              color: WispayColors.cPrimary,
                              size: FontSize.large,
                              textType: TextType.Bold,
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: CustomButton(
                          title: 'Lanjut Bayar',
                          onPress: controller.onConfirm,
                          isEnabled: controller.phone.value.isNotEmpty &&
                              !controller.isLoading.value &&
                              profile.outletAddress != null,
                          isLoading: controller.isLoading.value,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
