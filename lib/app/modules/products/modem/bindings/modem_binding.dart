import 'package:get/get.dart';

import 'package:wispay_outlet/app/modules/products/modem/controllers/modem_product_controller.dart';
import 'package:wispay_outlet/app/modules/products/modem/controllers/summary_modem_controller.dart';

import '../controllers/modem_controller.dart';

class ModemBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SummaryModemController>(
      () => SummaryModemController(),
      fenix: true,
    );
    Get.lazyPut<ModemProductController>(
      () => ModemProductController(),
      fenix: true,
    );
    Get.lazyPut<ModemController>(
      () => ModemController(),
    );
  }
}
