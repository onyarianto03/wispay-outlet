// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';

class PhysicalVoucerController extends GetxController {
  final ProductController _productController = Get.find();

  final items = <ProductBrandModel>[].obs;
  final isLoading = false.obs;
  final int categoryId = Get.arguments;

  @override
  void onInit() {
    super.onInit();
    getProductBrand();
  }

  void getProductBrand() async {
    isLoading.value = true;
    final _subCategories = await _productController.getProductBrands(categoryId);
    if (_subCategories != null) {
      items.value = _subCategories;
    }
    isLoading.value = false;
  }

  @override
  void onClose() {}
}
