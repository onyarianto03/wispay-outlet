// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';

import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import '../widgets/status_card.dart';

class PhysicalVoucherFormController extends GetxController {
  TextEditingController tController = TextEditingController();
  final ProductController _productController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final CategoriesController homeController = Get.find();
  final ProductBrandModel brand = Get.arguments;

  final formKey = GlobalKey<FormState>();

  final phoneNumberValidator = MultiValidator([
    RequiredValidator(errorText: 'Nomor HP harus diisi'),
    IndonesiaPhoneNumber(errorText: 'Nomor HP tidak valid'),
  ]);

  final isValid = false.obs;
  final phoneNumber = ''.obs;
  final isLoading = false.obs;
  final products = <ProductModel>[].obs;
  final subCategoryId = 0.obs;
  bool isLoadMore = false;
  bool isEndPage = false;
  int page = 1;

  @override
  void onInit() {
    tController.addListener(onchange);

    super.onInit();
    isLoading(true);
    getInitialData();
  }

  @override
  void onClose() {
    tController.removeListener(onchange);
    super.onClose();
  }

  void onLoadMore() async {
    if (isEndPage || isLoadMore) return;
    page++;
    isLoadMore = true;
    final _products = await _productController.getProducts(
      subCategoryId.value,
      filter: FilterModel(isPhysicalVoucher: true, page: page),
    );
    if (_products != null) {
      if (_products.isNotEmpty) {
        products.addAll(_products);
      } else {
        isEndPage = true;
      }
    }
  }

  void onchange() {
    isValid.value = formKey.currentState!.validate();
    phoneNumber.value = tController.text;
  }

  void onPickedContact(String? phone) {
    if (phone != null) {
      tController.text = phone;
    }
  }

  void _next(String pin, ProductModel data) async {
    final _body = {
      'product_id': data.id,
      'pin': pin,
      'phone': encrypt(NumberHelper.formatPhoneToIndonesian(phoneNumber.value)),
    };

    final _confirm = await _transactionRepository.confirm(
      supplier: data.productSupplier?.code,
      productName: 'physical-voucher',
      body: RequestBodyModel.fromJson(_body),
    );

    if (_confirm.success!) {
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(data: _confirm.data!),
            transactionId: _confirm.data!.id!,
            productCode: ProductCode.PHYSICAL_VOUCHER,
            status: _confirm.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void onPay(ProductModel data) {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': (pin) => _next(pin, data), 'phone': homeController.profile.value.phone},
    );
  }

  void showModalConfirm(ProductModel data) {
    FocusManager.instance.primaryFocus?.unfocus();
    bool isValid = formKey.currentState!.validate();

    if (isValid) {
      final commissionSetting = homeController.profile.value.comissionSetting;
      double comission = double.parse(commissionSetting?.kirimUang ?? commissionSetting?.all ?? '0');

      WispayBottomSheet.showConfirm(
        title: 'Kirim Uang',
        onConfirm: () => onPay(data),
        totalTitle: 'Total',
        totalPrice: NumberHelper.formatMoney(data.total.toString()),
        point: NumberHelper.formatMoneyWithoutSymbol(data.productPrice?.point),
        withPromo: false,
        children: [
          const SizedBox(height: Spacing.small),
          comission > 0
              ? InfoWidget(
                  text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                    comission.toString(),
                  )}',
                  margin: const EdgeInsets.only(top: 5),
                )
              : const SizedBox(height: 0),
          const SizedBox(height: Spacing.defaultSpacing),
          TrxDetailItem(title: 'Nama Produk', value: data.name ?? ''),
          TrxDetailItem(title: 'Nomor Hp', value: phoneNumber.value),
          TrxDetailItem(title: 'Harga', value: NumberHelper.formatMoney(data.productPrice?.price)),
        ],
      );
    }
  }

  Future<void> getInitialData() async {
    page = 1;
    final _subcategories = await _productController.getSubCategories(brand.id);
    if (_subcategories != null) {
      subCategoryId.value = _subcategories.first.id!;
      final _products = await _productController.getProducts(
        _subcategories.first.id,
        filter: const FilterModel(isPhysicalVoucher: true),
      );
      if (_products != null) {
        if (_products.isNotEmpty) {
          products.value = _products;
        } else {
          isEndPage = true;
        }
      }
      isLoading(false);
    }
  }
}
