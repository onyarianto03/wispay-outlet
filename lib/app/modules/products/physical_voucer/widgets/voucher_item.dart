// Flutter imports:
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';

// Project imports:
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/physical_voucer/controllers/physical_voucher_form_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class VoucherItem extends GetView<PhysicalVoucherFormController> {
  const VoucherItem({
    Key? key,
    required this.data,
  }) : super(key: key);

  final ProductModel data;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => controller.showModalConfirm(data),
      child: Container(
        padding: const EdgeInsets.all(Spacing.defaultSpacing),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: data.name ?? '',
              textType: TextType.SemiBold,
              margin: const EdgeInsets.only(bottom: Spacing.xSmall),
            ),
            CustomText(
              title: data.description?.trim() ?? '-',
              size: FontSize.small,
            ),
            const SizedBox(height: Spacing.medium),
            Row(
              children: [
                CustomText(
                  title: NumberHelper.formatMoney(data.productPrice?.price),
                  color: WispayColors.cPrimary,
                  textType: TextType.SemiBold,
                  size: FontSize.small,
                ),
                const Spacer(),
                Assets.iconsCoin.image(width: 15, height: 15),
                CustomText(
                  title: '${NumberHelper.formatMoneyWithoutSymbol(data.productPrice?.point)} Poin',
                  size: FontSize.small,
                  textType: TextType.SemiBold,
                  margin: const EdgeInsets.only(left: Spacing.xSmall),
                )
              ],
            )
          ],
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [buildShadow(spreadRadius: 5)],
        ),
      ),
    );
  }
}
