// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import '../controllers/physical_voucer_controller.dart';

class PhysicalVoucerView extends GetView<PhysicalVoucerController> {
  const PhysicalVoucerView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Voucher Fisik'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: Obx(
          () => controller.isLoading.value
              ? Center(child: Lottie.asset(Assets.lottieLoading, width: 100))
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: Spacing.defaultSpacing),
                    const CustomText(
                      title: 'Pilih operator',
                      size: FontSize.medium,
                      color: WispayColors.cBlack333,
                      textType: TextType.SemiBold,
                      margin: EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                    ),
                    Expanded(
                      child: StaggeredGridView.countBuilder(
                        padding: const EdgeInsets.all(Spacing.defaultSpacing),
                        shrinkWrap: true,
                        crossAxisCount: 2,
                        itemCount: controller.items.length,
                        crossAxisSpacing: Spacing.defaultSpacing,
                        mainAxisSpacing: Spacing.defaultSpacing,
                        staggeredTileBuilder: (index) => const StaggeredTile.fit(1),
                        itemBuilder: (BuildContext context, int index) => InkWell(
                          onTap: () => Get.toNamed(Routes.PHYSICAL_VOUCER_FORM, arguments: controller.items[index]),
                          child: Column(
                            children: [
                              BaseCard(
                                blurRadius: 20,
                                spreadRadius: 0,
                                margin: EdgeInsets.zero,
                                padding: const EdgeInsets.all(Spacing.medium),
                                child: CustomImage(
                                  source: controller.items[index].image?.url ?? "",
                                  height: 90,
                                ),
                              ),
                              const SizedBox(height: Spacing.small),
                              CustomText(
                                title: controller.items[index].name ?? "-",
                                textType: TextType.SemiBold,
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
        ),
      ),
    );
  }
}
