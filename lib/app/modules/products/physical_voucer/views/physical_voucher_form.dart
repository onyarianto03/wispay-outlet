// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/empty_widget.dart';
import 'package:wispay_outlet/app/modules/products/physical_voucer/widgets/voucher_item.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import '../controllers/physical_voucher_form_controller.dart';

class ProductList extends GetView<PhysicalVoucherFormController> {
  const ProductList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(controller.brand.name ?? 'Voucher Fisik'),
        centerTitle: true,
        elevation: 5,
        shadowColor: Colors.black.withOpacity(0.12),
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () => Get.back(),
        ),
      ),
      backgroundColor: WispayColors.cWhite2,
      body: SafeArea(
        child: Obx(
          () => controller.isLoading.value
              ? Center(child: Lottie.asset(Assets.lottieLoading, width: 100))
              : Form(
                  key: controller.formKey,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  child: controller.products.isNotEmpty
                      ? Column(
                          children: [
                            Container(
                              color: Colors.white,
                              padding: const EdgeInsets.all(Spacing.defaultSpacing),
                              child: CustomTextInput(
                                value: controller.phoneNumber.value,
                                hintText: 'Nomor HP Pelanggan',
                                showContact: true,
                                onContactPicked: (value) => controller.onPickedContact(value),
                                textEditingController: controller.tController,
                                onClear: controller.tController.clear,
                                validator: controller.phoneNumberValidator,
                                keyboardType: TextInputType.phone,
                              ),
                            ),
                            Expanded(
                              child: RefreshIndicator(
                                onRefresh: controller.getInitialData,
                                child: NotificationListener(
                                  onNotification: (notification) {
                                    if (notification is ScrollEndNotification) {
                                      if (notification.metrics.pixels == notification.metrics.maxScrollExtent) {
                                        controller.onLoadMore();
                                        return true;
                                      }
                                    }
                                    return true;
                                  },
                                  child: ListView.separated(
                                    padding: const EdgeInsets.all(Spacing.defaultSpacing),
                                    shrinkWrap: true,
                                    physics: const BouncingScrollPhysics(),
                                    itemBuilder: (_, idx) => VoucherItem(data: controller.products[idx]),
                                    separatorBuilder: (_, __) => const SizedBox(height: Spacing.defaultSpacing),
                                    itemCount: controller.products.length,
                                  ),
                                ),
                              ),
                            )
                          ],
                        )
                      : Center(
                          child: EmptyWidget(
                            title: 'Produk tidak tersedia',
                            subTitle: '',
                            image: Assets.iconsEmptyList.path,
                          ),
                        ),
                ),
        ),
      ),
    );
  }
}
