// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/modules/products/physical_voucer/controllers/physical_voucher_form_controller.dart';
import '../controllers/physical_voucer_controller.dart';

class PhysicalVoucerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PhysicalVoucerController>(
      () => PhysicalVoucerController(),
    );
    Get.lazyPut<PhysicalVoucherFormController>(
      () => PhysicalVoucherFormController(),
    );
  }
}
