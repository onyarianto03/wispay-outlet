import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/products/pdam/widgets/status_card.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class PdamController extends GetxController {
  TextEditingController? phoneController;
  TextEditingController? customerIdController;
  final formKey = GlobalKey<FormState>();

  final ProductController _productController = Get.find();
  final CategoriesController _homeController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final categoryId = Get.arguments;

  var selectedArea = ProductModel().obs;
  var customerId = ''.obs;
  var isSubmitting = false.obs;
  var isLoading = false.obs;

  int? subCategoryId;
  int _areaPage = 1;
  bool _areaIsLastPage = false;
  final _areaKeywoard = ''.obs;
  String? token = "";
  final products = <ProductModel>[].obs;

  final customerIdValidator = MultiValidator([
    RequiredValidator(errorText: 'ID Pelanggan/Nomor Meter wajib diisi'),
    // MinLengthValidator(4, errorText: 'Minimal 4 Digit'),
  ]);

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
    getInitialData();
    customerIdController = TextEditingController()..addListener(onChangeCustomerID);

    debounce<String>(_areaKeywoard, (keyword) {
      getProducts();
    }, time: const Duration(milliseconds: 500));
  }

  void getInitialData() async {
    isLoading.value = true;
    final productBrands = await _productController.getProductBrands(categoryId);
    final productSubcategories = await _productController.getSubCategories(productBrands?.first.id);
    subCategoryId = productSubcategories?.first.id;
    getProducts();
    isLoading.value = false;
  }

  void getProducts({bool? isLoadmore}) async {
    final _filter = {
      'page': _areaPage,
      'limit': 14,
      'q': _areaKeywoard.value,
    };

    final newProducts = await _productController.getProducts(subCategoryId, filter: FilterModel.fromJson(_filter));

    if (newProducts != null) {
      if (isLoadmore == true && newProducts.isEmpty) {
        _areaIsLastPage = true;
      } else if (isLoadmore == true) {
        products.addAll(newProducts);
      } else {
        products.value = newProducts;
      }
    }
  }

  @override
  void onClose() {
    super.onClose();
    customerIdController?.removeListener(onChangeCustomerID);
  }

  void onSelectArea(idx) {
    selectedArea(products[idx]);
  }

  void onChangeCustomerID() {
    customerId.value = customerIdController?.text ?? '';
  }

  onCheck() async {
    final isValid = formKey.currentState!.validate();

    if (isValid && selectedArea.value.name != '') {
      isSubmitting.value = true;
      final body = {
        "product_id": selectedArea.value.id,
        "customer_id": encrypt(customerId.value),
      };
      final response = await _transactionRepository.order(
        supplier: selectedArea.value.productSupplier!.code!.toLowerCase(),
        productName: 'pdam',
        body: RequestBodyModel.fromJson(body),
      );
      token = response.data?.meta?.token;
      isSubmitting.value = false;
      if (response.success != false) {
        final commissionSetting = _homeController.profile.value.comissionSetting;
        double comission = double.parse(commissionSetting?.pdam ?? commissionSetting?.all ?? '0');
        WispayBottomSheet.showConfirm(
          onConfirm: () => onPay(),
          withPromo: false,
          title: "Bayar PDAM",
          totalPrice: NumberHelper.formatMoney(
            response.data?.total ?? '0.0',
          ),
          children: [
            comission > 0
                ? InfoWidget(
                    text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                      comission.toString(),
                    )}',
                    margin: const EdgeInsets.only(top: 5),
                  )
                : const SizedBox(height: 0),
            const SizedBox(height: Spacing.defaultSpacing),
            TrxDetailItem(title: 'Area', value: response.data?.productName),
            TrxDetailItem(title: 'ID pelanggan', value: decrypt(response.data?.customerId)),
            TrxDetailItem(title: 'Nama', value: decrypt(response.data?.detailPayload?.name)),
            TrxDetailItem(
                title: 'Tagihan',
                value: NumberHelper.formatMoney(
                  response.data?.price ?? '0.0',
                )),
            TrxDetailItem(
              title: 'Biaya Admin',
              value: NumberHelper.formatMoney(
                response.data?.adminFee ?? '0.0',
              ),
              marginBottom: 0,
            ),
          ],
        );
      }
    }
  }

  Future<void> onSearchRegency(value) async {
    products.clear();
    _areaPage = 1;
    _areaIsLastPage = false;
    _areaKeywoard.value = value;
  }

  void loadMoreArea() async {
    if (_areaIsLastPage == true) return;
    _areaPage++;
    getProducts(isLoadmore: true);
  }

  void _next(String pin) async {
    final body = {
      "pin": pin,
      "token": token,
    };
    final confirmResponse = await _transactionRepository.confirm(
      supplier: selectedArea.value.productSupplier?.code?.toLowerCase(),
      productName: 'pdam',
      body: RequestBodyModel.fromJson(body),
    );
    if (confirmResponse.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(
              confirmResponse: confirmResponse.data!,
            ),
            transactionId: confirmResponse.data?.id,
            productCode: ProductCode.PDAM,
            status: confirmResponse.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': _homeController.profile.value.phone},
    );
  }
}
