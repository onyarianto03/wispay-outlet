import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/pdam/controllers/pdam_controller.dart';

class AreaListDropdown extends StatelessWidget {
  AreaListDropdown({
    Key? key,
    required this.scrollController,
    required this.onSelect,
  }) : super(key: key);

  final PdamController _controller = Get.find<PdamController>();
  final ScrollController scrollController;
  final ValueChanged<BaseModel> onSelect;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 40,
          child: TextField(
            onChanged: (terms) => _controller.onSearchRegency(terms),
            style: const TextStyle(fontSize: FontSize.medium),
            decoration: InputDecoration(
              prefixIcon: const Icon(Icons.search),
              isDense: true,
              hintText: 'Pilih Area',
              hintStyle: const TextStyle(
                color: WispayColors.cGrey99,
                fontSize: FontSize.small,
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
              ),
            ),
          ),
        ),
        const SizedBox(height: Spacing.small),
        Expanded(
          child: NotificationListener(
            onNotification: (notification) {
              if (notification is ScrollEndNotification) {
                if (notification.metrics.pixels == notification.metrics.maxScrollExtent) {
                  _controller.loadMoreArea();
                  return true;
                }
              }
              return true;
            },
            child: Obx(
              () => ListView.separated(
                shrinkWrap: true,
                controller: scrollController,
                itemCount: _controller.products.length,
                physics: const BouncingScrollPhysics(),
                separatorBuilder: (context, index) => const Divider(thickness: 1),
                itemBuilder: (context, index) => ListTile(
                  dense: true,
                  contentPadding: EdgeInsets.zero,
                  onTap: () {
                    Get.back();
                    onSelect.call(_controller.products[index]);
                    _controller.onSelectArea(index);
                  },
                  visualDensity: const VisualDensity(vertical: -2),
                  trailing: Visibility(
                    child: const Icon(
                      Icons.check_circle,
                      color: WispayColors.cGreen2,
                      size: 20,
                    ),
                    visible: _controller.products[index].id == _controller.selectedArea.value.id,
                  ),
                  title: CustomText(
                    title: _controller.products[index].name ?? "",
                    color: WispayColors.cBlack333,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
