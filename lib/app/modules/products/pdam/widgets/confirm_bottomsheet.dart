// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/bottomsheet_title.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/pdam/controllers/pdam_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class ConfirmBottomsheet extends GetView<PdamController> {
  const ConfirmBottomsheet({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: Spacing.defaultSpacing,
        right: Spacing.defaultSpacing,
        bottom: Spacing.defaultSpacing,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const BottomSheetTitle(title: 'Info Tagihan'),
          const InfoWidget(text: 'Tagihan ke pembeli sudah termasuk keuntungan Rp 2.000'),
          const SizedBox(height: Spacing.defaultSpacing),
          TrxDetailItem(title: 'Area', value: controller.selectedArea.value.name ?? '-'),
          TrxDetailItem(title: 'ID Pelanggan', value: controller.customerId.value),
          const TrxDetailItem(title: 'Nama pelanggan', value: 'Agus Mulyadi'),
          const TrxDetailItem(title: 'Bulan/Tahun', value: 'Des/2021'),
          const TrxDetailItem(title: 'Pemakaian', value: '20m³'),
          const TrxDetailItem(title: 'Tagihan', value: 'Rp70.500'),
          const TrxDetailItem(title: 'Biaya admin', value: 'Rp2.500'),
          const SizedBox(height: Spacing.small),
          _buildVoucher(),
          const Divider(thickness: 1, height: 32),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    CustomText(title: 'Total tagihan'),
                    CustomText(
                      title: 'Rp72.500',
                      textType: TextType.Bold,
                      color: WispayColors.cPrimary,
                      size: FontSize.medium,
                    ),
                  ],
                ),
              ),
              Expanded(child: CustomButton(title: 'Lanjut Bayar', onPress: controller.onPay)),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildVoucher() {
    return Container(
      padding: const EdgeInsets.all(Spacing.small),
      decoration: BoxDecoration(
        color: Colors.white,
        image: const DecorationImage(
          image: Assets.iconsMiscWave1,
          alignment: Alignment.centerRight,
        ),
        borderRadius: BorderRadius.circular(8),
        boxShadow: [buildShadow(spreadRadius: 4, blurRadius: 4)],
      ),
      child: Row(
        children: [
          Assets.iconsKupon.image(width: 24),
          const CustomText(
            title: 'Tambah promo/voucher',
            textType: TextType.SemiBold,
            margin: EdgeInsets.only(left: Spacing.small),
            color: WispayColors.cBlack666,
          ),
          const Spacer(),
          const Icon(
            Icons.chevron_right,
            size: 35,
            color: WispayColors.cBlack666,
          )
        ],
      ),
    );
  }
}
