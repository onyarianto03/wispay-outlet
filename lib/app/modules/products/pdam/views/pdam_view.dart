import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/products/pdam/widgets/area_list_dropdown.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

import '../controllers/pdam_controller.dart';

class PdamView extends GetView<PdamController> {
  const PdamView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text('PDAM'),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.chevron_left,
              size: 35,
            ),
            onPressed: () => Get.back(),
          ),
        ),
        body: Obx(
          () => controller.isLoading.value
              ? Center(
                  child: Lottie.asset(Assets.lottieLoading, width: 100),
                )
              : SafeArea(
                  child: Container(
                    padding: const EdgeInsets.all(Spacing.defaultSpacing),
                    child: Form(
                      autovalidateMode: AutovalidateMode.disabled,
                      key: controller.formKey,
                      child: Column(
                        children: [
                          _dropdown(
                            label: 'Pilih area',
                            placeholder: 'Pilih area',
                            selectedItem: controller.selectedArea.value,
                          ),
                          const SizedBox(height: Spacing.defaultSpacing),
                          CustomTextInput(
                            hintText: 'Masukkan ID Pelanggan',
                            value: controller.customerId.value,
                            keyboardType: TextInputType.number,
                            validator: controller.customerIdValidator,
                            textEditingController: controller.customerIdController,
                            isEnabled: !controller.isSubmitting.value,
                            onClear: controller.customerIdController?.clear,
                            margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
                          ),
                          const Spacer(),
                          CustomButton(
                            title: 'Cek Tagihan',
                            onPress: controller.onCheck,
                            isEnabled: controller.customerId.value != '' &&
                                controller.selectedArea.value.name != null &&
                                !controller.isSubmitting.value,
                            isLoading: controller.isSubmitting.value,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
        ),
      ),
    );
  }

  Widget _dropdown({
    String label = '',
    String? placeholder,
    required BaseModel selectedItem,
  }) {
    return FormField(
      validator: (BaseModel? value) {
        if (value?.id == null) {
          return 'Silahkan pilih $label';
        }
        return null;
      },
      builder: (FormFieldState<BaseModel> state) {
        return Dropdown(
          errorText: state.errorText,
          topLabel: label,
          label: placeholder,
          value: selectedItem.name ?? "",
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
            WispayBottomSheet.scrollable(
              title: 'Pilih $placeholder',
              withSearch: false,
              itemBuilder: (_, sc) => AreaListDropdown(
                scrollController: sc,
                onSelect: (value) => state.didChange(value),
              ),
            );
          },
        );
      },
    );
  }
}
