import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class EWalletController extends GetxController {
  final ProductController _productController = Get.find();

  final data = <BaseModel>[].obs;
  final categoryId = Get.arguments;

  final isLoading = false.obs;
  @override
  void onInit() {
    super.onInit();
    getInitialData();
  }

  void getInitialData() {
    isLoading.value = true;
    _productController.getProductBrands(categoryId).then((value) {
      isLoading.value = false;
      if (value != null) {
        data.value = value;
      }
    });
  }

  void onSelect(BaseModel item) {
    String _route = item.name == 'LinkAja' ? Routes.E_WALLET_LINKAJA : Routes.E_WALLET_FORM;

    Get.toNamed(_route, arguments: item);
  }
}
