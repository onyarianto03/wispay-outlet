import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/products/e-wallet/widgets/status_card.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class EwalletLinkajaController extends GetxController {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController phoneController;
  late TextEditingController nominalController;
  final ProductController _productController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final CategoriesController _homeController = Get.find();

  final BaseModel brands = Get.arguments;

  final phone = ''.obs;
  final nominal = ''.obs;
  final isLoading = false.obs;
  final isSubmitting = false.obs;
  final product = ProductModel().obs;
  String token = '';

  final phoneValidator = MultiValidator([
    RequiredValidator(errorText: 'Nomor HP Tujuan wajib diisi'),
    IndonesiaPhoneNumber(errorText: 'Nomor HP tidak valid'),
  ]);

  final nominalValidator = MultiValidator([
    RequiredValidator(errorText: 'Nominal wajib diisi'),
    MinLengthValidator(6, errorText: 'Minimal Rp10.000'),
  ]);

  get getFormKey => _formKey;

  @override
  void onInit() {
    super.onInit();
    phoneController = TextEditingController()..addListener(onChangePhone);
    nominalController = TextEditingController()..addListener(onChangeNominal);
    getInitialData();
  }

  @override
  void onClose() {
    super.onClose();
    phoneController.removeListener(onChangePhone);
    nominalController.removeListener(onChangeNominal);
  }

  void onChangePhone() {
    phone.value = phoneController.text;
  }

  void onChangeNominal() {
    nominal.value = nominalController.text;
  }

  void getInitialData() async {
    isLoading.value = true;
    final _subcategories = await _productController.getSubCategories(brands.id);
    isLoading.value = false;
    if (_subcategories != null) {
      final _products = await _productController.getProducts(_subcategories.first.id);
      if (_products != null) {
        product.value = _products.first;
      }
    }
  }

  void _next(String pin) async {
    final _body = {
      "pin": pin,
      "token": token,
    };
    WispayDialog.showPaymentDialog();
    final _confirm = await _transactionRepository.confirm(
      supplier: product.value.productSupplier?.code,
      productName: 'link-aja',
      body: RequestBodyModel.fromJson(_body),
    );
    if (_confirm.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(data: _confirm.data!),
            transactionId: _confirm.data?.id,
            productCode: ProductCode.EWALLET,
            status: _confirm.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    } else {
      WispayDialog.closeDialog();
    }
  }

  onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'isPayment': true, 'phone': decrypt(_homeController.profile.value.phone)},
    );
  }

  submit() async {
    final isValid = _formKey.currentState!.validate();
    FocusManager.instance.primaryFocus?.unfocus();

    if (isValid) {
      final _body = {
        'phone': encrypt(NumberHelper.formatPhoneToIndonesian(phone.value)),
        'nominal': nominal.value,
        'product_id': product.value.id,
      };
      isSubmitting.value = true;
      final _order = await _transactionRepository.order(
        supplier: product.value.productSupplier?.code ?? '',
        productName: 'link-aja',
        body: RequestBodyModel.fromJson(_body),
      );
      if (_order.success != false) {
        token = _order.data!.meta!.token!;
        WispayBottomSheet.showConfirm(
          onConfirm: () => onPay(),
          title: "Dompet Elektronik",
          totalPrice: NumberHelper.formatMoney(_order.data?.total),
          withPromo: false,
          children: [
            InfoWidget(
              text:
                  'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(_homeController.profile.value.comissionSetting?.dompetElektronik ?? _homeController.profile.value.comissionSetting?.all)}',
            ),
            const SizedBox(height: Spacing.defaultSpacing),
            TrxDetailItem(
              title: 'Nama Produk',
              value: _order.data!.name!,
            ),
            TrxDetailItem(title: 'Nomor HP', value: decrypt(_order.data?.customerId)),
            TrxDetailItem(title: 'Harga', value: NumberHelper.formatMoney(_order.data?.price)),
            TrxDetailItem(title: 'Biaya Admin', value: NumberHelper.formatMoney(_order.data?.adminFee)),
          ],
        );
      }
      isSubmitting.value = false;
    }
  }

  void onPickedContact(String? phone) {
    if (phone != null) {
      phoneController.text = phone;
    }
  }
}
