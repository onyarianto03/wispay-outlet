import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/products/e-wallet/widgets/status_card.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class EwalletFormController extends GetxController {
  final _formKey = GlobalKey<FormState>();
  final BaseModel argument = Get.arguments;
  final productItems = <ProductModel>[].obs;
  late TextEditingController phoneController;
  final ProductController _productController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final CategoriesController _homeController = Get.find();

  final BaseModel brands = Get.arguments;

  var phone = ''.obs;
  var isEnabled = false.obs;
  var isSubmitting = false.obs;
  final isLoading = false.obs;
  String token = '';

  final phoneValidator = MultiValidator([
    RequiredValidator(errorText: 'Nomor HP Tujuan wajib diisi'),
    IndonesiaPhoneNumber(errorText: 'Nomor HP tidak valid'),
  ]);

  get getFormKey => _formKey;

  @override
  void onInit() {
    super.onInit();
    phoneController = TextEditingController()..addListener(onChangePhone);
    getInitialData();
  }

  @override
  void onClose() {
    super.onClose();
    phoneController.removeListener(onChangePhone);
  }

  void onChangePhone() {
    phone.value = phoneController.text;
  }

  void onFormChange() {
    bool isValid = getFormKey.currentState!.validate();
    isEnabled.value = isValid;
  }

  void getInitialData() async {
    isLoading.value = true;
    final _subcategories = await _productController.getSubCategories(brands.id);
    isLoading.value = false;
    if (_subcategories != null) {
      final _products = await _productController.getProducts(_subcategories.first.id);
      if (_products != null) {
        productItems.value = _products;
      }
    }
  }

  void _next({String? pin, String? supplier, int? productId}) async {
    final _body = {
      "pin": pin,
      "product_id": productId,
      "phone": encrypt(NumberHelper.formatPhoneToIndonesian(phone.value))
    };
    WispayDialog.showPaymentDialog();
    final _confirm = await _transactionRepository.confirm(
      supplier: supplier,
      productName: 'ewallet',
      body: RequestBodyModel.fromJson(_body),
    );
    if (_confirm.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(data: _confirm.data!),
            transactionId: _confirm.data?.id,
            productCode: ProductCode.EWALLET,
            status: _confirm.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    } else {
      WispayDialog.closeDialog();
    }
  }

  onPay({String? supplier, int? productId}) {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {
        'next': (pin) => _next(pin: pin, supplier: supplier, productId: productId),
        'isPayment': true,
        'phone': decrypt(_homeController.profile.value.phone),
      },
    );
  }

  submit(ProductModel item) async {
    FocusManager.instance.primaryFocus?.unfocus();
    WispayBottomSheet.showConfirm(
      onConfirm: () => onPay(supplier: item.productSupplier!.code!, productId: item.id),
      title: "Dompet Elektronik",
      totalPrice: NumberHelper.formatMoney(item.total),
      withPromo: false,
      children: [
        InfoWidget(
          text:
              'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(_homeController.profile.value.comissionSetting?.dompetElektronik ?? _homeController.profile.value.comissionSetting?.all)}',
        ),
        const SizedBox(height: Spacing.defaultSpacing),
        TrxDetailItem(
          title: 'Nama Produk',
          value: item.name ?? '',
        ),
        TrxDetailItem(title: 'Nomor HP', value: phone.value),
        TrxDetailItem(title: 'Harga', value: NumberHelper.formatMoney(item.defaultPrice)),
        TrxDetailItem(title: 'Biaya Admin', value: NumberHelper.formatMoney(item.defaultAdminFee ?? '0')),
      ],
    );
  }

  void onPickedContact(String? phone) {
    if (phone != null) {
      phoneController.text = phone;
    }
  }
}
