import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/utils/input_price_formatter.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/e-wallet/controllers/ewallet_linkaja_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class EwalletLinkajaView extends GetView<EwalletLinkajaController> {
  const EwalletLinkajaView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('LinkAja'),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.chevron_left,
              size: 35,
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: SafeArea(
          child: Obx(
            () => controller.isLoading.value
                ? Center(child: Lottie.asset(Assets.lottieLoading, width: 100))
                : SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(vertical: Spacing.defaultSpacing),
                    child: Form(
                      key: controller.getFormKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomTextInput(
                            value: controller.phone.value,
                            hintText: 'Masukkan Nomor HP',
                            textEditingController: controller.phoneController,
                            hintStyle: const TextStyle(color: WispayColors.cDisabledColor),
                            validator: controller.phoneValidator,
                            showContact: true,
                            onContactPicked: (value) => controller.onPickedContact(value),
                            onClear: () => controller.phone.value = '',
                            margin: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                            fillColor: WispayColors.cWhite,
                            keyboardType: TextInputType.phone,
                            filled: true,
                          ),
                          const SizedBox(height: Spacing.defaultSpacing),
                          BaseCard(
                            decorationImage: AssetImage(Assets.iconsMiscWave2.path),
                            margin: const EdgeInsets.all(0),
                            borderRadius: 0,
                            withShadow: false,
                            child: Column(
                              children: [
                                const CustomText(
                                  title:
                                      'Beli saldo LinkAja sesuai kebutuhan mulai dari Rp10.000, dengan nominal kelipatan 1.000.',
                                ),
                                const SizedBox(height: Spacing.defaultSpacing),
                                CustomTextInput(
                                  value: controller.nominal.value,
                                  hintText: 'Masukkan Nominal Saldo',
                                  textEditingController: controller.nominalController,
                                  hintStyle: const TextStyle(color: WispayColors.cDisabledColor),
                                  validator: controller.nominalValidator,
                                  keyboardType: TextInputType.number,
                                  onClear: () => controller.nominal.value = '',
                                  inputFormatters: [InputPriceFormatter()],
                                  fillColor: WispayColors.cWhite,
                                  filled: true,
                                ),
                                const SizedBox(height: Spacing.defaultSpacing),
                                CustomButton(
                                  title: 'Top-Up Saldo',
                                  isEnabled: controller.phone.value.length > 8 &&
                                      controller.nominal.value.length > 5 &&
                                      !controller.isSubmitting.value,
                                  isLoading: controller.isSubmitting.value,
                                  onPress: controller.submit,
                                )
                              ],
                            ),
                          ),
                          const SizedBox(height: Spacing.defaultSpacing),
                          const CustomText(
                            title: 'Keterangan:',
                            color: WispayColors.cBlack,
                            textType: TextType.Bold,
                            size: FontSize.medium,
                            margin: EdgeInsets.only(
                              bottom: Spacing.small,
                              left: Spacing.defaultSpacing,
                              right: Spacing.defaultSpacing,
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.all(Spacing.defaultSpacing),
                            margin: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(8.r)),
                              color: WispayColors.cPrimary.withOpacity(0.2),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                CustomText(
                                  title: 'Pastikan akun tujuan LinkAja tidak melewati limit berikut:',
                                  color: WispayColors.cBlack,
                                  margin: EdgeInsets.only(bottom: Spacing.small),
                                ),
                                CustomText(
                                  title: '1. Rp20 juta/bulan untuk akun full service',
                                  color: WispayColors.cBlack,
                                  margin: EdgeInsets.only(bottom: Spacing.xSmall),
                                ),
                                CustomText(
                                  title: '2. Rp10 juta/bulan untuk akun regular',
                                  color: WispayColors.cBlack,
                                  margin: EdgeInsets.only(bottom: Spacing.xSmall),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}
