import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/e-wallet/controllers/e_wallet_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class EWalletView extends GetView<EWalletController> {
  const EWalletView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dompet Elektronik'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: Obx(
          () => controller.isLoading.value
              ? Center(
                  child: Lottie.asset(Assets.lottieLoading, width: 100),
                )
              : Container(
                  color: WispayColors.cWhite,
                  child: ListView.separated(
                    padding: const EdgeInsets.all(Spacing.defaultSpacing),
                    itemCount: controller.data.length,
                    separatorBuilder: (context, index) => const Divider(thickness: 1),
                    itemBuilder: (_, idx) {
                      final item = controller.data[idx];

                      return InkWell(
                        onTap: () => controller.onSelect(item),
                        child: Row(
                          children: [
                            Assets.iconsDefaultIcon.image(width: 36, height: 36),
                            const SizedBox(width: Spacing.defaultSpacing),
                            CustomText(
                              title: item.name!,
                              textType: TextType.SemiBold,
                              color: WispayColors.cBlack,
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
        ),
      ),
    );
  }
}
