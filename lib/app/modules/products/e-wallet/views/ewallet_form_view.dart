import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/products/e-wallet/controllers/ewallet_form_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class EwalletFormView extends GetView<EwalletFormController> {
  const EwalletFormView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(controller.argument.name ?? 'Dompet Elektronik'),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.chevron_left,
              size: 35,
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: SafeArea(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
            child: Obx(
              () => controller.isLoading.value
                  ? Center(child: Lottie.asset(Assets.lottieLoading, width: 100))
                  : Form(
                      onChanged: controller.onFormChange,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      key: controller.getFormKey,
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: Spacing.defaultSpacing),
                            CustomTextInput(
                              value: controller.phone.value,
                              hintText: 'Masukkan Nomor HP',
                              textEditingController: controller.phoneController,
                              hintStyle: const TextStyle(color: WispayColors.cDisabledColor),
                              validator: controller.phoneValidator,
                              showContact: true,
                              onContactPicked: (value) => controller.onPickedContact(value),
                              onClear: () => controller.phone.value = '',
                              fillColor: WispayColors.cWhite,
                              keyboardType: TextInputType.phone,
                              filled: true,
                            ),
                            const SizedBox(height: Spacing.defaultSpacing),
                            controller.argument.name == 'ShopeePay' ? _notes() : const SizedBox(),
                            StaggeredGridView.countBuilder(
                              physics: const NeverScrollableScrollPhysics(),
                              padding: const EdgeInsets.symmetric(vertical: Spacing.defaultSpacing),
                              shrinkWrap: true,
                              itemCount: controller.productItems.length,
                              crossAxisCount: 2,
                              mainAxisSpacing: Spacing.defaultSpacing,
                              crossAxisSpacing: Spacing.defaultSpacing,
                              itemBuilder: (_, idx) {
                                final item = controller.productItems[idx];

                                return InkWell(
                                  onTap: controller.isEnabled.value ? () => controller.submit(item) : null,
                                  child: BaseCard(
                                    isEnabled: controller.isEnabled.value,
                                    spreadRadius: 0,
                                    margin: EdgeInsets.zero,
                                    child: Column(
                                      children: [
                                        CustomText(
                                          title: item.name ?? "",
                                          textType: TextType.SemiBold,
                                          textAlign: TextAlign.center,
                                          margin: const EdgeInsets.only(bottom: Spacing.xSmall),
                                        ),
                                        CustomText(
                                          title: NumberHelper.formatMoney(item.defaultPrice),
                                          textType: TextType.Regular,
                                          color: WispayColors.cPrimary,
                                          size: FontSize.small,
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                              staggeredTileBuilder: (_) => const StaggeredTile.fit(1),
                            ),
                          ],
                        ),
                      ),
                    ),
            ),
          ),
        ),
      ),
    );
  }

  Column _notes() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const CustomText(
          title: 'Keterangan:',
          color: WispayColors.cBlack,
          textType: TextType.Bold,
          size: FontSize.medium,
          margin: EdgeInsets.only(
            bottom: Spacing.small,
          ),
        ),
        Container(
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.r)),
            color: WispayColors.cPrimary.withOpacity(0.2),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              CustomText(
                title: 'Total saldo yang masuk ke akun customer akan dipotong sebesar Rp500,- oleh pihak ShopeePay.',
                color: WispayColors.cBlack,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
