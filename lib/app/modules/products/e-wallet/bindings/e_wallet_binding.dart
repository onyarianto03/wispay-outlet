import 'package:get/get.dart';
import 'package:wispay_outlet/app/modules/products/e-wallet/controllers/ewallet_form_controller.dart';

import 'package:wispay_outlet/app/modules/products/e-wallet/controllers/ewallet_linkaja_controller.dart';

import '../controllers/e_wallet_controller.dart';

class EWalletBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EwalletFormController>(
      () => EwalletFormController(),
    );
    Get.lazyPut<EwalletLinkajaController>(
      () => EwalletLinkajaController(),
    );
    Get.lazyPut<EWalletController>(
      () => EWalletController(),
    );
  }
}
