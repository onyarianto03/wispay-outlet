import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/values/values.dart';

class FirstNote extends StatelessWidget {
  const FirstNote({
    Key? key,
    required this.value,
  }) : super(key: key);

  final String value;

  @override
  Widget build(BuildContext context) {
    return AutoSizeText.rich(
      TextSpan(
        text: 'UserID pelanggan di Game:\n',
        style: const TextStyle(color: WispayColors.cBlack333, fontSize: FontSize.defaultSize),
        children: [
          TextSpan(
            text: value,
            style: const TextStyle(fontWeight: FontWeight.w600),
          )
        ],
      ),
    );
  }
}
