import 'package:get/get.dart';

import 'package:wispay_outlet/app/modules/products/voucher_game/controllers/voucher_game_form_controller.dart';

import '../controllers/voucher_game_controller.dart';

// Package imports:

// Project imports:

class VoucherGameBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VoucherGameFormController>(
      () => VoucherGameFormController(),
      fenix: true,
    );
    Get.lazyPut<VoucherGameController>(
      () => VoucherGameController(),
    );
  }
}
