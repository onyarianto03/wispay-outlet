// Package imports:
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';

class VoucherGameController extends GetxController {
  final ProductController _productController = Get.find();
  final ScrollController sController = ScrollController();
  final categoryId = Get.arguments;
  final count = 0.obs;
  var isLoading = false.obs;
  bool isLastPage = false;
  int page = 1;
  final brands = <ProductBrandModel>[].obs;

  @override
  void onInit() {
    super.onInit();
    getInitialData();
    sController.addListener(_scrollListener);
  }

  void getInitialData() async {
    isLoading.value = true;
    getBrands();
  }

  void getBrands({bool? isLoadmore}) async {
    final _filter = {
      'page': page,
      'limit': 14,
    };

    final newBrands = await _productController.getProductBrands(categoryId, filter: FilterModel.fromJson(_filter));

    if (newBrands != null) {
      if (isLoadmore == true && newBrands.isEmpty) {
        isLastPage = true;
      } else if (isLoadmore == true) {
        brands.addAll(newBrands);
      } else {
        brands.value = newBrands;
      }
    }
    isLoading.value = false;
  }

  void _scrollListener() async {
    if (sController.position.pixels == sController.position.maxScrollExtent) {
      if (isLastPage == true) return;
      page++;
      getBrands(isLoadmore: true);
    }
  }

  @override
  void onClose() {}
}
