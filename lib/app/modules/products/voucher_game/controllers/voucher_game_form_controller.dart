import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/product_supplier.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_status_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

import '../widgets/status_card.dart';

class VoucherGameFormController extends GetxController {
  final ProductBrandModel pBrand = Get.arguments;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final cPhone = TextEditingController();
  final cUserId = TextEditingController();
  final cColumn3rdPlaceholder = TextEditingController();
  final ProductController productController = Get.find();
  final CategoriesController _homeController = Get.find();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final ScrollController sController = ScrollController();

  final isEnableCard = false.obs;
  final column3rdPlaceholder = ''.obs;
  final phone = ''.obs;
  final userId = ''.obs;
  String? token = "";
  var isLoading = false.obs;
  var isSubmitting = false.obs;
  var selectedItem = ProductModel();
  var isLastPage = false;
  int page = 1;
  final products = <ProductModel>[].obs;

  final GlobalKey<FormFieldState> vPhone = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> vUserId = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> vColumn3rdPlaceholder = GlobalKey<FormFieldState>();

  @override
  void onInit() {
    super.onInit();
    getInitialData();
    cPhone.addListener(onchangePhone);
    cUserId.addListener(onchangeUserId);
    cColumn3rdPlaceholder.addListener(onchangeColumn3rdPlaceholder);

    sController.addListener(_scrollListener);
  }

  void onchangeForm() {
    final validateIstopUp = pBrand.isTopup! ? phone.value.isNotEmpty : true;
    final validateIsPhoneOnly = pBrand.isPhoneOnly == false ? userId.value.isNotEmpty : true;
    final validateIsColumn3rd = pBrand.column3rdPlaceholder!.isNotEmpty ? column3rdPlaceholder.value.isNotEmpty : true;
    if (validateIstopUp && validateIsPhoneOnly && validateIsColumn3rd) {
      isEnableCard.value = true;
    } else {
      isEnableCard.value = false;
    }
  }

  void _scrollListener() async {
    if (sController.position.pixels == sController.position.maxScrollExtent) {
      if (isLastPage == true) return;
      page++;
      getProducts(isLoadmore: true);
    }
  }

  void getInitialData() async {
    isLoading.value = true;
    await productController.getSubCategories(pBrand.id);
    getProducts();
    isLoading.value = false;
  }

  void getProducts({bool? isLoadmore}) async {
    final _filter = {
      'page': page,
      'limit': 14,
    };

    final newProducts = await productController.getProducts(productController.productSubCategory.first.id,
        filter: FilterModel.fromJson(_filter));

    if (newProducts != null) {
      if (isLoadmore == true && newProducts.isEmpty) {
        isLastPage = true;
      } else if (isLoadmore == true) {
        products.addAll(newProducts);
      } else {
        products.value = newProducts;
      }
    }
  }

  void onPickedContact(String? phone) {
    if (phone != null) {
      cPhone.text = phone;
    }
  }

  @override
  void onClose() {
    super.onClose();
    cColumn3rdPlaceholder.removeListener(onchangeColumn3rdPlaceholder);
    cPhone.removeListener(onchangePhone);
    cUserId.removeListener(onchangeUserId);
  }

  void onchangePhone() => phone.value = cPhone.text;
  void onchangeUserId() => userId.value = cUserId.text;
  void onchangeColumn3rdPlaceholder() => column3rdPlaceholder.value = cColumn3rdPlaceholder.text;

  void _next(String pin) async {
    final Map<String, dynamic>? body;
    var isAyoconnect = selectedItem.productSupplier?.code?.toLowerCase() == ProductSupplier.AYOCONNECT;
    if (!isAyoconnect && pBrand.isTopup == true) {
      var isSerialCustomerId = pBrand.column3rdPlaceholder != '' ? '$userId-$column3rdPlaceholder' : userId.value;
      body = {
        "pin": pin,
        "product_id": selectedItem.id,
        "phone": encrypt(NumberHelper.formatPhoneToIndonesian(phone.value)),
        "customer_id": encrypt(isSerialCustomerId),
      };
    } else {
      body = {
        "pin": pin,
        "token": token,
      };
    }
    final confirmResponse = await _transactionRepository.confirm(
      supplier: selectedItem.productSupplier?.code?.toLowerCase(),
      productName: 'voucher-game',
      body: RequestBodyModel.fromJson(body),
    );

    if (confirmResponse.data != null) {
      Get.back();
      Navigator.pushAndRemoveUntil(
        Get.context!,
        MaterialPageRoute(
          builder: (context) => TransactionStatusView(
            child: StatusCard(
              confirmResponse: confirmResponse.data!,
            ),
            transactionId: confirmResponse.data?.id,
            productCode: ProductCode.VOUCHER_GAME,
            status: confirmResponse.data?.status ?? '',
          ),
        ),
        ModalRoute.withName(Routes.HOME),
      );
    }
  }

  void onPay() {
    Get.back();
    Get.toNamed(
      Routes.PAYMENT,
      arguments: {'next': _next, 'phone': _homeController.profile.value.phone},
    );
  }

  void onCheck(ProductModel item) async {
    final isValid = formKey.currentState!.validate();
    selectedItem = item;

    if (isValid) {
      isSubmitting.value = true;
      final isSerialCustomerId = pBrand.column3rdPlaceholder != '' ? '$userId-$column3rdPlaceholder' : userId.value;
      final body = {
        "product_id": item.id,
        "customer_id": encrypt(isSerialCustomerId),
      };
      BaseResponse? response;

      final isAyoconnect = item.productSupplier?.code?.toLowerCase() == ProductSupplier.AYOCONNECT;
      if (pBrand.isTopup == false) {
        response = await _transactionRepository.order(
          supplier: item.productSupplier?.code?.toLowerCase() ?? '',
          productName: isAyoconnect ? 'voucher-game' : 'voucher-game-zoneid',
          body: RequestBodyModel.fromJson(body),
        );
        token = response.data?.meta?.token;
      }

      isSubmitting.value = false;
      final commissionSetting = _homeController.profile.value.comissionSetting;
      double comission = double.parse(commissionSetting?.voucherGame ?? commissionSetting?.all ?? '0');

      if (response?.success == true || token == "") {
        WispayBottomSheet.showConfirm(
          onConfirm: () => onPay(),
          withPromo: false,
          title: "Voucher Game",
          totalPrice: NumberHelper.formatMoney(
            pBrand.isTopup == true ? item.total.toString() : response?.data?.total ?? '0.0',
          ),
          children: [
            comission > 0
                ? InfoWidget(
                    text: 'Tagihan ke pembeli sudah termasuk keuntungan ${NumberHelper.formatMoney(
                      comission.toString(),
                    )}',
                    margin: const EdgeInsets.only(top: 5),
                  )
                : const SizedBox(height: 0),
            const SizedBox(height: Spacing.defaultSpacing),
            TrxDetailItem(title: 'Nama Produk', value: item.name),
            pBrand.isPhoneOnly != true
                ? TrxDetailItem(
                    title: 'UserId', value: pBrand.isTopup == true ? isSerialCustomerId : response?.data.customerId)
                : const SizedBox(height: 0),
            TrxDetailItem(
                title: 'Nomor Hp', value: pBrand.isTopup == true ? phone.value : decrypt(response?.data.customerPhone)),
            TrxDetailItem(
                title: 'Harga',
                value: NumberHelper.formatMoney(
                  item.total.toString(),
                )),
          ],
        );
      }
    }
  }
}
