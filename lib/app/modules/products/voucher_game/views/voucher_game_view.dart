// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/products/voucher_game/views/voucher_game_form_view.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

// Project imports:
import '../controllers/voucher_game_controller.dart';

class VoucherGameView extends GetView<VoucherGameController> {
  const VoucherGameView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Voucher Game'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => controller.isLoading.value
            ? Center(
                child: Lottie.asset(Assets.lottieLoading, width: 100),
              )
            : SafeArea(
                child: ListView(
                  controller: controller.sController,
                  children: [
                    const CustomText(
                      title: 'Voucher yang Tersedia',
                      margin: EdgeInsets.all(Spacing.defaultSpacing),
                    ),
                    StaggeredGridView.countBuilder(
                      padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      crossAxisCount: 2,
                      itemCount: controller.brands.length,
                      crossAxisSpacing: Spacing.defaultSpacing,
                      mainAxisSpacing: Spacing.defaultSpacing,
                      staggeredTileBuilder: (index) => const StaggeredTile.fit(1),
                      itemBuilder: (context, index) {
                        final item = controller.brands[index];
                        return InkWell(
                          onTap: () => Get.to(() => const VoucherGameFormView(), arguments: item),
                          child: SizedBox(
                            height: Get.height * 0.14,
                            child: Column(
                              children: [
                                BaseCard(
                                  padding: EdgeInsets.zero,
                                  blurRadius: 20,
                                  spreadRadius: 0,
                                  margin: EdgeInsets.zero,
                                  child: AspectRatio(
                                    aspectRatio: 2.5 / 1,
                                    child: CustomImage(source: item.image?.thumbnail?.url ?? ""),
                                  ),
                                ),
                                const SizedBox(height: Spacing.small),
                                CustomText(
                                  title: item.name ?? "-",
                                  textType: TextType.SemiBold,
                                  textAlign: TextAlign.center,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
