import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:form_field_validator/form_field_validator.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/form_note.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/products/voucher_game/controllers/voucher_game_form_controller.dart';
import 'package:wispay_outlet/app/modules/products/voucher_game/widgets/first_note.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class VoucherGameFormView extends GetView<VoucherGameFormController> {
  const VoucherGameFormView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(controller.pBrand.name ?? ""),
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      body: Obx(
        () => controller.isLoading.value
            ? Center(
                child: Lottie.asset(Assets.lottieLoading, width: 100),
              )
            : SafeArea(
                child: Column(
                  children: [
                    const SizedBox(height: Spacing.defaultSpacing),
                    _buildForm(),
                    Expanded(
                      child: ListView(
                        shrinkWrap: true,
                        controller: controller.sController,
                        padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                        children: [
                          FormNote(children: _buildNote()),
                          StaggeredGridView.countBuilder(
                            physics: const NeverScrollableScrollPhysics(),
                            padding: const EdgeInsets.symmetric(vertical: Spacing.defaultSpacing),
                            shrinkWrap: true,
                            itemCount: controller.products.length,
                            crossAxisCount: 2,
                            mainAxisSpacing: Spacing.defaultSpacing,
                            crossAxisSpacing: Spacing.defaultSpacing,
                            itemBuilder: (_, idx) {
                              final item = controller.products[idx];

                              return Obx(
                                () => InkWell(
                                  onTap: controller.isEnableCard.value && item.isPurchaseable == true
                                      ? () => controller.onCheck(item)
                                      : null,
                                  child: BaseCard(
                                    isEnabled: controller.isEnableCard.value,
                                    blurRadius: 20,
                                    spreadRadius: 0,
                                    margin: EdgeInsets.zero,
                                    child: Column(
                                      children: [
                                        CustomText(
                                          title: item.name ?? "",
                                          textType: TextType.SemiBold,
                                          textAlign: TextAlign.center,
                                          maxLines: 1,
                                          margin: const EdgeInsets.only(bottom: Spacing.xSmall),
                                        ),
                                        CustomText(
                                          title: item.isPurchaseable == true
                                              ? NumberHelper.formatMoney('${item.productPrice?.total ?? item.total}')
                                              : 'Dalam Kendala',
                                          textType: TextType.Regular,
                                          color:
                                              item.isPurchaseable == true ? WispayColors.cPrimary : WispayColors.cRed,
                                          size: FontSize.small,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                            staggeredTileBuilder: (_) => const StaggeredTile.fit(1),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  Padding _buildForm() {
    ProductBrandModel _brand = controller.pBrand;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
      child: Form(
        onChanged: controller.onchangeForm,
        key: controller.formKey,
        autovalidateMode: AutovalidateMode.disabled,
        child: Column(
          children: [
            _brand.isTopup == true
                ? Obx(() => CustomTextInput(
                      value: controller.phone.value,
                      hintText: 'No Hp Pelanggan',
                      showContact: true,
                      textEditingController: controller.cPhone,
                      onContactPicked: (value) => controller.onPickedContact(value),
                      key: controller.vPhone,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      margin: const EdgeInsets.only(bottom: Spacing.small),
                      validator: RequiredValidator(errorText: 'No Hp Pelanggan tidak boleh kosong'),
                    ))
                : const SizedBox(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _brand.isPhoneOnly == false
                    ? Obx(
                        () => Expanded(
                          child: CustomTextInput(
                            helperText: " ",
                            value: controller.userId.value,
                            hintText: 'User ID',
                            key: controller.vUserId,
                            autovalidateMode: AutovalidateMode.onUserInteraction,
                            textEditingController: controller.cUserId,
                            validator: RequiredValidator(errorText: 'User ID tidak boleh kosong'),
                          ),
                        ),
                      )
                    : const SizedBox(),
                // Visibility(
                //   child: Container(
                //     margin: const EdgeInsets.symmetric(horizontal: Spacing.xSmall),
                //     width: 7,
                //     height: 3,
                //     color: Colors.red,
                //   ),
                //   visible: _brand.column3rdPlaceholder.isNotEmpty,
                // ),
                SizedBox(width: _brand.column3rdPlaceholder!.isEmpty ? 0 : Spacing.defaultSpacing),
                _brand.column3rdPlaceholder!.isNotEmpty
                    ? Obx(
                        () => Expanded(
                          child: CustomTextInput(
                            helperText: " ",
                            value: controller.column3rdPlaceholder.value,
                            hintText: _brand.column3rdPlaceholder!,
                            key: controller.vColumn3rdPlaceholder,
                            autovalidateMode: AutovalidateMode.onUserInteraction,
                            textEditingController: controller.cColumn3rdPlaceholder,
                            validator: RequiredValidator(
                              errorText: '${_brand.column3rdPlaceholder} tidak boleh kosong',
                            ),
                          ),
                        ),
                      )
                    : const SizedBox(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _buildNote() {
    final _list = <Widget>[];
    final ProductBrandModel brand = controller.pBrand;
    Widget _first = const FirstNote(value: 'UserID = 2580 - 28985');
    Widget _second = CustomText(
      title: 'Masukkan kode token ke dalam akun game ${brand.name}, yang didapat setelah transaksi berhasil.',
      color: WispayColors.cBlack333,
    );
    if (brand.isWithSerial == true) {
      _second = CustomText(
        title: 'Item akan langsung ditambahkan pada akun ${brand.name} anda.',
        color: WispayColors.cBlack333,
      );
    }
    if (brand.column3rdPlaceholder!.isEmpty) {
      _first = const FirstNote(value: 'UserID = 258028988');
    }
    if (brand.isPhoneOnly == false) {
      _list.assignAll([_first, _second]);
      return _list;
    }

    _list.assign(_second);
    return _list;
  }
}
