// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/date_helper.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/empty_widget.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/referral/controllers/referral_controller.dart';
import 'package:wispay_outlet/app/modules/referral/widgets/referral_list_item_widget.dart';

class ReferralListView extends GetView<ReferralController> {
  const ReferralListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Daftar Undanganmu'),
        centerTitle: true,
        leading: IconButton(
          onPressed: () => Get.back(),
          icon: const Icon(Icons.chevron_left, size: 35),
        ),
      ),
      body: Obx(
        () => Stack(
          children: [
            controller.listComission.isEmpty
                ? Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.all(Spacing.defaultSpacing),
                      child: CustomButton(
                        onPress: () => Get.back(),
                        title: 'kembali',
                      ),
                    ),
                  )
                : const SizedBox(),
            if (controller.listComission.isEmpty)
              const Padding(
                padding: EdgeInsets.all(Spacing.defaultSpacing),
                child: Center(
                  child: EmptyWidget(
                    title: 'Daftar undangan masih kosong',
                    subTitle: 'Ajak pelanggan untuk daftar dengan kode undanganmu dan dapatkan hadiahnya!',
                  ),
                ),
              )
            else
              SingleChildScrollView(
                padding: const EdgeInsets.all(Spacing.defaultSpacing),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      title: 'Nama Undangan (${controller.listComission.length})',
                      size: FontSize.medium,
                      textType: TextType.SemiBold,
                      color: WispayColors.cBlack333,
                    ),
                    const SizedBox(height: Spacing.defaultSpacing),
                    ListView.separated(
                      itemCount: controller.listComission.length,
                      separatorBuilder: (context, index) => const SizedBox(height: Spacing.small),
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        final data = controller.listComission[index];
                        return ReferralListItemWidget(
                          title: decrypt(data.consumer?.name),
                          dateJoined: DateHelper.formatDate(data.createdAt, format: 'dd MMM yyyy'),
                          isActive: data.consumer?.isActive ?? true,
                        );
                      },
                    )
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
