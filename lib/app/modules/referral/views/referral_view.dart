// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/referral/views/referral_list_view.dart';
import 'package:wispay_outlet/app/modules/referral/widgets/list_referral_menu.dart';
import 'package:wispay_outlet/app/modules/referral/widgets/referral_desc.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import '../controllers/referral_controller.dart';

class ReferralView extends GetView<ReferralController> {
  const ReferralView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Kode Undangan'),
        centerTitle: true,
        leading: IconButton(
          onPressed: () => Get.back(),
          icon: const Icon(Icons.chevron_left, size: 35),
        ),
      ),
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: [
            ListView(
              children: [
                Assets.imagesReferralHeader.image(),
                Padding(
                  padding: const EdgeInsets.all(Spacing.defaultSpacing),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListReferralMenu(onTap: () => Get.to(() => const ReferralListView())),
                      const CustomText(
                        title: 'Komisi Tambahan dari Wismart',
                        margin: EdgeInsets.only(top: Spacing.defaultSpacing),
                        size: FontSize.medium,
                        textType: TextType.Bold,
                        color: WispayColors.cBlack333,
                      ),
                      const CustomText(
                        title:
                            'Yuk, ajak teman dan keluarga belanja di Wismart! Masukkan kode undangan saat daftar dan dapatkan hadiahnya.',
                        maxLines: 2,
                        size: FontSize.small,
                        margin: EdgeInsets.only(bottom: Spacing.defaultSpacing),
                      ),
                      ReferralDesc(
                        icon: Assets.iconsReferralCoinReferral.path,
                        title: 'Komisi Tambahan dari Wismart',
                        subTitle: 'Komisi 5% dari tiap transaksi Wismart oleh pelanggan undanganmu (selama 1 bulan).',
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      ReferralDesc(
                        icon: Assets.iconsReferralWalletReferral.path,
                        title: 'Saldo Untuk Pelanggan Baru',
                        subTitle: 'Gratis saldo 10rb untuk pelanggan baru yang daftar dengan kode undanganmu.',
                      )
                    ],
                  ),
                ),
                const SizedBox(height: Spacing.large * 5),
              ],
            ),
            Positioned(
              bottom: 0,
              right: 0,
              left: 0,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: const BorderRadius.vertical(top: Radius.circular(16)),
                  boxShadow: [buildShadow(spreadRadius: 3, offset: const Offset(0, -1), blurRadius: 3)],
                ),
                padding: const EdgeInsets.all(Spacing.defaultSpacing),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const CustomText(title: 'Kode saya:'),
                        Obx(
                          () => CustomText(
                            title: controller.hController.profile.value.referenceCode ?? "-",
                            color: WispayColors.cPrimary,
                            textType: TextType.Bold,
                            size: FontSize.medium,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: Spacing.defaultSpacing),
                    CustomButton(
                      title: 'Bagikan Kode',
                      onPress: () => controller.onGenerateLink(),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
