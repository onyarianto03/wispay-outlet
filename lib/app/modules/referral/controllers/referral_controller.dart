// Flutter imports:

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/string_helper.dart';
import 'package:wispay_outlet/app/core/services/firebase_dynamic_list_service.dart';
import 'package:wispay_outlet/app/data/models/referral_model/referral_model.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:share_plus/share_plus.dart';

class ReferralController extends GetxController {
  final CategoriesController hController = Get.find();
  final AccountRepository _repo = AccountRepository(Get.find());

  final listComission = <ReferralModel>[].obs;

  @override
  void onInit() {
    _getListReferral();
    super.onInit();
  }

  @override
  void onClose() {}

  void onGenerateLink() async {
    String? code = hController.profile.value.referenceCode;

    final _link = await FirebaseDynamicLinkService.generateLink(short: true, code: code);

    Share.share(_generateMessage(code, _link));
  }

  String _generateMessage(code, link) {
    const enter = '\n';
    final linkCode = StringHelper.toBold(code);
    final _message =
        'Yuk, unduh aplikasi ${StringHelper.toBold('Wispay')}, dan gunakan kode $linkCode untuk mendapatkan lebih banyak keuntungannya.$enter${enter}Klik link ini untuk unduh aplikasi: $enter$link';
    return _message;
  }

  void _getListReferral() async {
    final _response = await _repo.referralComission();
    if (_response.data != null) {
      listComission.addAll(_response.data!);
    }
  }
}
