// Package imports:
import 'package:get/get.dart';

// Project imports:
import '../controllers/referral_controller.dart';

class ReferralBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ReferralController>(
      () => ReferralController(),
    );
  }
}
