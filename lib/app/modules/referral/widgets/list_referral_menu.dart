// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:badges/badges.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/referral/controllers/referral_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class ListReferralMenu extends GetWidget<ReferralController> {
  const ListReferralMenu({Key? key, this.onTap}) : super(key: key);

  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: Spacing.medium,
          horizontal: Spacing.defaultSpacing,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(Spacing.small),
          border: Border.all(color: WispayColors.cBorderE, width: 1),
          image: const DecorationImage(
            image: Assets.iconsMiscWave1,
            alignment: Alignment.centerRight,
            isAntiAlias: true,
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const CustomText(
              title: 'Daftar Undanganmu',
              color: WispayColors.cBlack333,
              textType: TextType.SemiBold,
            ),
            const Spacer(),
            Obx(() => Visibility(
                  visible: controller.listComission.isNotEmpty,
                  child: Badge(
                    badgeColor: WispayColors.cRed2,
                    elevation: 0,
                    badgeContent: CustomText(
                      margin: const EdgeInsets.all(3),
                      title: controller.listComission.length.toString(),
                      color: Colors.white,
                      size: FontSize.small,
                      textType: TextType.SemiBold,
                    ),
                  ),
                )),
            const Icon(Icons.chevron_right, size: 25)
          ],
        ),
      ),
    );
  }
}
