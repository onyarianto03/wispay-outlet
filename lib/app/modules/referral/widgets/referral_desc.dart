// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';

class ReferralDesc extends StatelessWidget {
  const ReferralDesc({
    Key? key,
    required this.icon,
    required this.subTitle,
    required this.title,
  }) : super(key: key);

  final String icon;
  final String title;
  final String subTitle;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image.asset(
          icon,
          width: 40,
          height: 40,
        ),
        const SizedBox(width: Spacing.defaultSpacing),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                title: title,
                color: WispayColors.cBlack,
                textType: TextType.SemiBold,
              ),
              CustomText(
                title: subTitle,
                maxLines: 2,
                size: FontSize.small,
              )
            ],
          ),
        )
      ],
    );
  }
}
