// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';

class ReferralListItemWidget extends StatelessWidget {
  const ReferralListItemWidget({
    Key? key,
    required this.dateJoined,
    required this.title,
    required this.isActive,
  }) : super(key: key);

  final String title;
  final String dateJoined;
  final bool isActive;

  @override
  Widget build(BuildContext context) {
    return Container(
      foregroundDecoration: BoxDecoration(
        color: !isActive ? WispayColors.cBlackBBB.withOpacity(0.2) : Colors.transparent,
        borderRadius: BorderRadius.circular(Spacing.small),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Spacing.small),
        color: WispayColors.cWhite,
        border: Border.all(color: WispayColors.cBorderE, width: 1),
      ),
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: CustomText(
              title: title,
              maxLines: 1,
              minFontSize: FontSize.defaultSize,
              overflow: TextOverflow.ellipsis,
              color: WispayColors.cBlack333,
              textType: TextType.SemiBold,
            ),
          ),
          Expanded(
            child: CustomText(
              title: 'Bergabung $dateJoined',
              size: FontSize.small,
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ),
    );
  }
}
