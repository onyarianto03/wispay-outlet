// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/card_menu.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/card/card_menu_widget.dart';
import 'package:wispay_outlet/app/modules/account/widgets/card_poin_widget.dart';
import 'package:wispay_outlet/app/modules/account/widgets/profile_widget.dart';
import '../controllers/account_controller.dart';

class AccountView extends GetView<AccountController> {
  const AccountView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Akun'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: controller.onRefresh,
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(Spacing.defaultSpacing),
            child: Column(
              children: [
                // const AccountInfoWidget(),
                // const SizedBox(height: Spacing.large + 2),
                const ProfileWidget(),
                const SizedBox(height: Spacing.large + 1),
                const CardPoinWidget(),
                const SizedBox(height: Spacing.defaultSpacing),
                CardMenuWidget(menu: kWispayLaterMenu),
                const SizedBox(height: Spacing.defaultSpacing),
                CardMenuWidget(menu: kOutletMenu),
                const SizedBox(height: Spacing.defaultSpacing),
                CardMenuWidget(menu: kSettingMenu),
                const SizedBox(height: Spacing.defaultSpacing),
                CardMenuWidget(menu: kReferralMenu),
                const SizedBox(height: Spacing.defaultSpacing),
                CardMenuWidget(menu: kPinMenu, onPress: () => controller.changePin()),
                const SizedBox(height: Spacing.defaultSpacing),
                CardMenuWidget(menu: kHelpMenu),
                const SizedBox(height: Spacing.defaultSpacing),
                CardMenuWidget(
                  menu: kLogoutMenu,
                  onPress: () => controller.onLogout(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
