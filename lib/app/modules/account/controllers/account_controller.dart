// Package imports:

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/services/storage_service.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';
import 'package:wispay_outlet/app/modules/auth/pin_input/views/update_pin_view.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class AccountController extends GetxController {
  final _authRepository = AccountRepository(Get.find());
  TextEditingController pinController = TextEditingController();
  TextEditingController pinController2 = TextEditingController();
  late PageController pageController = PageController(initialPage: 0);
  final StorageService _storage = Get.find();
  CategoriesController homeController = Get.find();

  final current = 0.obs;
  final title = 'PIN Keamanan Baru'.obs;
  final subtitle = 'Atur PIN keamanan yang baru'.obs;

  @override
  void onInit() {
    ever<int>(current, (val) {
      pageController.jumpToPage(val);
    });
    super.onInit();
  }

  @override
  void onClose() {
    pinController.dispose();
    pinController2.dispose();
    super.onClose();
  }

  void changePin() {
    Get.toNamed(
      Routes.PIN_INPUT,
      arguments: {
        'next': (pin) => _next(pin),
        'subtitle': 'Masukkan PIN saat ini',
      },
    );
  }

  void _next(String pin) async {
    WispayDialog.showLoading();
    BaseResponse res = await _authRepository.validatePIN(pin: pin);
    if (res.data != null) {
      WispayDialog.closeDialog();
      Get.to(
        () => UpdatePinView(
          phone: decrypt(homeController.profile.value.phone),
          otpType: OTPType.UPDATE_PIN,
        ),
      );
    } else {
      WispaySnackbar.showError(res.message!, onClose: () => WispayDialog.closeDialog());
    }
  }

  Future<void> onRefresh() {
    return homeController.getProfile();
  }

  void onBack() {
    if (current.value > 0) {
      current.value = current.value - 1;
      pinController2.clear();
    } else {
      Get.back();
      pinController.clear();
    }
  }

  onLogout() async {
    _storage.removeToken();
    WispayDialog.showConfirmDialog(
      title: 'Apakah anda yakin ingin keluar?',
      onConfirm: () {
        Get.offAllNamed(Routes.LOGIN);
      },
      onCancel: () => Get.back(),
    );
  }
}
