// Flutter imports:
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';

// Project imports:
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class CardPoinWidget extends GetWidget<CategoriesController> {
  const CardPoinWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.toNamed(Routes.MY_POINT),
      child: BaseCard(
        margin: EdgeInsets.zero,
        padding: const EdgeInsets.all(Spacing.defaultSpacing),
        decorationImage: AssetImage(Assets.iconsMiscWave1.path),
        child: Row(
          children: [
            Assets.iconsCoin.image(width: 24, height: 24),
            const SizedBox(
              width: Spacing.defaultSpacing,
            ),
            const CustomText(
              title: 'Poin Saya',
              size: FontSize.medium,
              color: WispayColors.cBlack333,
              textType: TextType.SemiBold,
            ),
            const Spacer(),
            Obx(() => CustomText(
                  title: NumberHelper.formatMoneyWithoutSymbol(controller.profile.value.outletPoint?.total) + ' poin',
                  size: FontSize.medium,
                  textType: TextType.SemiBold,
                  textStyle: const TextStyle(letterSpacing: 0.6),
                )),
            const SizedBox(width: Spacing.defaultSpacing),
            const Icon(
              Icons.arrow_forward_ios_rounded,
              size: 16,
            )
          ],
        ),
      ),
    );
  }
}
