// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class ProfileWidget extends GetView<CategoriesController> {
  const ProfileWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Get.toNamed(Routes.PROFILE_ACCOUNT),
      child: SizedBox(
        height: 42,
        child: Row(
          children: [
            Obx(
              () => CustomImage(
                source: controller.profile.value.avatar?.url ?? "",
                defaultImage: Assets.iconsProfilePlaceholder.image(),
                borderRadius: 42 / 2,
                width: 42,
                height: 42,
              ),
            ),
            const SizedBox(width: Spacing.defaultSpacing),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        Obx(
                          () => CustomText(
                            title: controller.profile.value.name ?? "",
                            size: FontSize.xLarge,
                            textType: TextType.Bold,
                            color: WispayColors.cBlack333,
                          ),
                        ),
                        const SizedBox(width: Spacing.small),
                        const Icon(
                          Icons.chevron_right,
                          size: 20,
                          color: WispayColors.cBlack,
                        )
                      ],
                    ),
                  ),
                  const SizedBox(height: Spacing.xSmall),
                  Obx(() => CustomText(title: decrypt(controller.profile.value.phone))),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
