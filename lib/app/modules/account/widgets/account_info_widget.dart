// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class AccountInfoWidget extends GetView<CategoriesController> {
  const AccountInfoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isVerified = controller.profile.value.status == 'VERIFIED';

    return Container(
      width: double.infinity,
      padding: isVerified
          ? const EdgeInsets.all(Spacing.defaultSpacing)
          : const EdgeInsets.symmetric(
              horizontal: Spacing.defaultSpacing,
              vertical: Spacing.small,
            ),
      decoration: BoxDecoration(
        color: WispayColors.cSecondary.withAlpha(51),
        borderRadius: BorderRadius.circular(8.r),
      ),
      child: isVerified ? _buildIsVerified() : _buildNotVerified(),
    );
  }

  Row _buildIsVerified() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 36,
          width: 36,
          padding: const EdgeInsets.symmetric(horizontal: Spacing.xSmall),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6.r),
            gradient: const LinearGradient(
              colors: [Color(0XFF0A4DF4), Color(0XFF8FAEFA)],
              begin: AlignmentDirectional.centerStart,
              end: AlignmentDirectional.centerEnd,
            ),
          ),
          child: Center(
            child: CustomText(
              title: "90%",
              maxLines: 1,
              color: WispayColors.cWhite,
              size: FontSize.defaultSize,
              textType: TextType.SemiBold,
              textStyle: TextStyle(letterSpacing: 0.6.sp),
            ),
          ),
        ),
        const SizedBox(width: Spacing.medium),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CustomText(
                title: 'Yuk, isi Data Outlet untuk melengkapi akunmu.',
                maxLines: 1,
                color: WispayColors.cBlack333,
                size: FontSize.small,
              ),
              const SizedBox(height: Spacing.medium),
              ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(6.r)),
                child: LinearProgressIndicator(
                  value: 0.9,
                  backgroundColor: WispayColors.cWhite,
                  color: WispayColors.cPrimary,
                  minHeight: 6.h,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildNotVerified() {
    return InkWell(
      onTap: () => Get.toNamed(Routes.ACCOUNT_VERIFY),
      child: AutoSizeText.rich(
        TextSpan(
          text: 'Mohon lakukan verifikasi akun terlebih dahulu untuk dapat melanjutkan bertransaksi. ',
          style: TextStyle(color: WispayColors.cBlack333, fontSize: FontSize.small, height: 1.3.sp),
          children: [
            TextSpan(
              text: 'Verifikasi ulang',
              style: TextStyle(
                decoration: TextDecoration.underline,
                color: WispayColors.cPrimary,
                fontWeight: FontWeight.w600,
                letterSpacing: 0.3.sp,
                fontSize: FontSize.small,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
