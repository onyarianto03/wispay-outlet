// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/modules/data_outlet/widgets/data_outlet_form.dart';
import '../controllers/data_outlet_controller.dart';

class DataOutletView extends GetView<DataOutletController> {
  const DataOutletView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        controller.getBack();
        return false;
      },
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Data Outlet'),
            leading: IconButton(
              icon: const Icon(Icons.chevron_left, size: 35),
              onPressed: controller.getBack,
            ),
            centerTitle: true,
          ),
          body: SafeArea(
            child: Container(
              color: WispayColors.cWhite,
              height: double.infinity,
              child: SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.all(Spacing.defaultSpacing),
                  child: const DataOutletForm(),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
