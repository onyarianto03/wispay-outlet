// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/data_outlet/controllers/data_outlet_controller.dart';

class DataOutletForm extends GetView<DataOutletController> {
  const DataOutletForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTextInput(
          label: 'Nama Outlet',
          hintText: 'Nama Outlet',
          value: controller.outletName.value,
          textEditingController: controller.nameController,
          contentPadding: const EdgeInsets.symmetric(
            vertical: Spacing.medium,
            horizontal: Spacing.defaultSpacing,
          ),
        ),
        const CustomText(
          title: 'Alamat Outlet',
          textAlign: TextAlign.left,
          margin: EdgeInsets.symmetric(vertical: Spacing.medium),
          size: FontSize.defaultSize,
          color: WispayColors.cBlack,
          textType: TextType.SemiBold,
        ),
        Obx(() => _dropdown(
              label: 'Provinsi',
              value: controller.province.value,
              placeholder: 'Pilih Provinsi',
              data: controller.provinces,
              onRequest: () => controller.getProvinces(),
              onReachedEnd: () => controller.loadMoreProvinces(),
              onSelect: (idx) => controller.onSelectProvince(idx),
            )),
        Obx(() => _dropdown(
              label: 'Kota/Kabupaten',
              value: controller.regency.value,
              placeholder: 'Pilih Kota/Kabupaten',
              data: controller.regencies,
              enabled: controller.province.value.name != null,
              onReachedEnd: () => controller.loadMoreRegency(),
              onSelect: (idx) => controller.onSelectRegency(idx),
            )),
        Obx(() => _dropdown(
              label: 'Kecamatan',
              value: controller.district.value,
              placeholder: 'Pilih Kecamatan',
              data: controller.districts,
              onReachedEnd: () => controller.loadMoreDistricts(),
              onSelect: (idx) => controller.onSelectDistrict(idx),
              enabled: controller.regency.value.name != null,
            )),
        const CustomText(
          title: 'Alamat Lengkap',
          textAlign: TextAlign.left,
          margin: EdgeInsets.only(bottom: Spacing.small),
          size: FontSize.small,
          textType: TextType.SemiBold,
        ),
        GestureDetector(
          onTap: () => controller.onPickLocation(),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(Spacing.defaultSpacing.r),
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(color: WispayColors.cGreyBC),
                borderRadius: BorderRadius.circular(Spacing.defaultSpacing.r),
              ),
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(Spacing.defaultSpacing.r),
                      ),
                    ),
                    height: 150,
                    child: Obx(
                      () => GoogleMap(
                        mapType: MapType.normal,
                        zoomControlsEnabled: false,
                        zoomGesturesEnabled: false,
                        rotateGesturesEnabled: false,
                        scrollGesturesEnabled: false,
                        onMapCreated: (_c) => controller.mapController.complete(_c),
                        initialCameraPosition: CameraPosition(
                          target: LatLng(
                            double.parse(
                                decrypt(controller.categoriesController.profile.value.outletLatitude ?? '0.0')),
                            double.parse(
                                decrypt(controller.categoriesController.profile.value.outletLongitude ?? '0.0')),
                          ),
                          zoom: 18,
                        ),
                        markers: <Marker>{controller.marker.value},
                      ),
                    ),
                  ),
                  Container(
                    color: WispayColors.cWhite,
                    padding: EdgeInsets.symmetric(vertical: Spacing.medium.w),
                    child: const CustomText(
                      title: 'Pilih Titik Lokasi',
                      color: WispayColors.cPrimary,
                      size: FontSize.medium,
                      textType: TextType.SemiBold,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Obx(() => CustomText(
              title: controller.outletLocation.value.address ?? "",
              textAlign: TextAlign.left,
              margin: const EdgeInsets.only(
                bottom: Spacing.large,
                top: Spacing.small,
              ),
              size: FontSize.defaultSize,
              textType: TextType.SemiBold,
              color: WispayColors.cBlack,
            )),
        CustomButton(
          title: 'Simpan Perubahan',
          onPress: () => controller.onSubmit(),
        ),
      ],
    );
  }

  Column _dropdown({
    String label = '',
    String? placeholder,
    required BaseModel value,
    required List<BaseModel> data,
    VoidCallback? onRequest,
    VoidCallback? onReachedEnd,
    required void Function(int) onSelect,
    bool enabled = true,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText(
          title: label,
          textAlign: TextAlign.left,
          margin: const EdgeInsets.only(bottom: Spacing.small),
          size: FontSize.small,
          textType: TextType.SemiBold,
        ),
        Dropdown(
          enabled: enabled,
          label: placeholder,
          value: value.name ?? "",
          onTap: () {
            WispayBottomSheet.scrollableList<BaseModel>(
              title: placeholder ?? "",
              items: data,
              selectedItem: value,
              onReachedEnd: onReachedEnd,
              initialChildSize: 0.9,
              onSelect: onSelect,
            );
          },
        ).marginOnly(bottom: Spacing.small),
      ],
    );
  }
}
