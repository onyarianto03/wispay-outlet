// Package imports:
import 'package:get/get.dart';

// Project imports:
import '../controllers/data_outlet_controller.dart';

class DataOutletBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(DataOutletController());
  }
}
