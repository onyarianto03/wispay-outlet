// Dart imports:
import 'dart:async';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/models/utility/prediction_model.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';
import 'package:wispay_outlet/app/data/repositories/utility_repository.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/app/views/map_view.dart';

class DataOutletController extends GetxController {
  final UtilityRepository _utilityRepository = UtilityRepository(Get.find());
  final AccountRepository _accountRepository = AccountRepository(Get.find());

  final CategoriesController categoriesController = Get.find();
  final _formKey = GlobalKey<FormState>();
  final TextEditingController nameController = TextEditingController();
  late final Completer<GoogleMapController> mapController = Completer();

  final CameraPosition cameraPosition = const CameraPosition(target: LatLng(0, 0), zoom: 15);
  final marker = const Marker(markerId: MarkerId('marker'), position: LatLng(0, 0)).obs;
  final kCurrentPosition = const LatLng(-6.1753924, 106.8271528).obs;

  var outletName = ''.obs;
  var isSubmitting = false.obs;

  final province = const BaseModel().obs;
  final regency = const BaseModel().obs;
  final district = const BaseModel().obs;
  final provinces = <BaseModel>[].obs;
  final regencies = <BaseModel>[].obs;
  final districts = <BaseModel>[].obs;
  int page = 1;
  bool _islastPage = false;
  int _regencyPage = 1;
  bool _regencyIsLastPage = false;
  int _districtPage = 1;
  bool _districtIsLastPage = false;
  bool? forceSetAddress = Get.arguments?['forceSetAddress'];
  final outletLocation = const LocationDataModel().obs;

  final outletNameValidator = MultiValidator([
    RequiredValidator(errorText: 'Nama Outlet wajib diisi'),
  ]);

  get getFormKey => _formKey;

  @override
  void onInit() {
    super.onInit();
    _requestPermission();
    final profile = categoriesController.profile.value;

    Map<String, dynamic> _p = {
      'id': profile.district?.regency?.province?.id,
      'name': profile.district?.regency?.province?.name,
    };

    Map<String, dynamic> _r = {
      'id': profile.district?.regency?.id,
      'name': profile.district?.regency?.name,
    };

    Map<String, dynamic> _d = {
      'id': profile.district?.id,
      'name': profile.district?.name,
    };

    province.value = BaseModel.fromJson(_p);
    regency.value = BaseModel.fromJson(_r);
    district.value = BaseModel.fromJson(_d);

    outletName.value = decrypt(profile.outletName);
    outletLocation.value = LocationDataModel(
      longitude: decrypt(profile.outletLatitude),
      latitude: decrypt(profile.outletLongitude),
      address: decrypt(profile.outletAddress),
    );
    marker.value = Marker(
      markerId: const MarkerId('marker'),
      position: LatLng(
        double.parse(decrypt(profile.outletLatitude)),
        double.parse(decrypt(profile.outletLongitude)),
      ),
    );
    nameController.text = decrypt(profile.outletName);
    nameController.addListener(onChangeOutletName);

    getProvinces();
  }

  @override
  void onClose() {
    super.onClose();
    nameController.removeListener(onChangeOutletName);
  }

  void onChangeOutletName() {
    outletName.value = nameController.text;
  }

  Future<void> goToMarker(CameraPosition target) async {
    final GoogleMapController controller = await mapController.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(target));
  }

  void _requestPermission() async {
    var _status = await Permission.location.status;
    if (_status.isDenied || _status.isLimited || _status.isRestricted) {
      await Permission.location.request();
    }
  }

  void onPickLocation() async {
    final LocationDataModel? _data = await Get.to<LocationDataModel>(() => const MapView());

    final GoogleMapController _map = await mapController.future;

    if (_data != null) {
      LatLng _latLng = LatLng(double.parse(_data.latitude!), double.parse(_data.longitude!));
      outletLocation.value = _data;
      await _map.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: _latLng,
            zoom: 15,
          ),
        ),
      );

      marker.value = Marker(
        markerId: const MarkerId('marker'),
        position: _latLng,
      );
    }
  }

  void getProvinces() async {
    final _response = await _utilityRepository.getProvinces(filter: FilterModel(page: page));

    if (_response.data != null) {
      provinces.value = _response.data!;
    }
  }

  void getRegency() async {
    final _response = await _utilityRepository.getRegencyByProvinceId(
      proviceId: province.value.id ?? 0,
    );

    if (_response.data != null) {
      regencies.value = _response.data!;
    }
  }

  void getDistrict() async {
    final _response = await _utilityRepository.getDistrictsByRegency(
      id: regency.value.id ?? 0,
    );

    if (_response.data != null) {
      districts.value = _response.data!;
    }
  }

  void loadMoreProvinces() async {
    if (_islastPage == true) return;
    page++;
    final _filter = {
      'page': page,
      'limit': 10,
    };

    final _response = await _utilityRepository.getProvinces(filter: FilterModel.fromJson(_filter));

    if (_response.data != null) {
      if (_response.data!.isEmpty) {
        _islastPage = true;
      } else {
        provinces.addAll(_response.data!);
      }
    }
  }

  void loadMoreRegency() async {
    if (_regencyIsLastPage == true) return;
    _regencyPage++;
    final _filter = {
      'page': _regencyPage,
      'limit': 10,
    };

    final _response = await _utilityRepository.getRegencyByProvinceId(
      filter: FilterModel.fromJson(_filter),
      proviceId: province.value.id ?? 0,
    );

    if (_response.data != null) {
      if (_response.data!.isEmpty) {
        _regencyIsLastPage = true;
      } else {
        regencies.addAll(_response.data!);
      }
    }
  }

  void loadMoreDistricts() async {
    if (_districtIsLastPage == true) return;
    _districtPage++;
    final _filter = {
      'page': _districtPage,
      'limit': 10,
    };

    final _response = await _utilityRepository.getDistrictsByRegency(
      filter: FilterModel.fromJson(_filter),
      id: district.value.id ?? 0,
    );

    if (_response.data != null) {
      if (_response.data!.isEmpty) {
        _districtIsLastPage = true;
      } else {
        districts.addAll(_response.data!);
      }
    }
  }

  onSelectProvince(idx) {
    province.value = provinces[idx];
    regencies.clear();
    districts.clear();
    regency.value = const BaseModel();
    district.value = const BaseModel();
    getRegency();
    Get.back();
  }

  onSelectRegency(idx) {
    regency.value = regencies[idx];
    districts.clear();
    district.value = const BaseModel();
    getDistrict();
    Get.back();
  }

  onSelectDistrict(idx) {
    district.value = districts[idx];
    Get.back();
  }

  void getBack() {
    forceSetAddress! ? Get.offAllNamed(Routes.HOME) : Get.back();
  }

  onSubmit() async {
    WispayDialog.showLoading();
    var _data = {
      "outlet_name": encrypt(outletName.value),
      "regency_id": regency.value.id,
      "district_id": district.value.id,
      "outlet_latitude": outletLocation.value.latitude,
      "outlet_longitude": outletLocation.value.longitude,
      "outlet_address": encrypt(outletLocation.value.address ?? ""),
    };
    final _response = await _accountRepository.updateOutlet(data: RequestBodyModel.fromJson(_data));
    if (_response.data != null) {
      categoriesController.getProfile();
    }

    WispayDialog.closeDialog();
    getBack();
  }
}
