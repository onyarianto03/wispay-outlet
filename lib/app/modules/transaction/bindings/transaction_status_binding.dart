// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/modules/transaction/controllers/transaction_status_controller.dart';

class TransactionStatusBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TransactionStatusController>(
      () => TransactionStatusController(),
    );
  }
}
