// Dart imports:
import 'dart:async';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/data/models/utility/filter_date_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';

class TransactionController extends GetxController with SingleGetTickerProviderMixin {
  final _repo = TransactionRepository(Get.find());
  final CategoriesController categoriesController = Get.find();
  final ScrollController sController = ScrollController();

  final trxStatus = TransactionStatusHelper.TRANSACTION_STATUS_LIST;
  late TabController tabController = TabController(
    length: trxStatus.length,
    vsync: this,
  );

  final _activeTab = 0.obs;
  final initialLoaded = false.obs;
  final isLoading = true.obs;
  final status = ''.obs;
  final selectedCategory = const BaseModel(name: 'Semua Kategori').obs;
  final transactionList = <TransactionDataModel>[].obs;
  final isLastPage = false.obs;

  String? _startDate;
  String? _endDate;
  String? _date;
  int _page = 1;
  final int _limit = 10;

  @override
  void onInit() {
    super.onInit();

    ever<int>(
      _activeTab,
      (val) async {
        _page = 1;
        isLastPage.value = false;
        if (val != 0) {
          status.value = trxStatus[val];
          await getTransactionList();
        } else {
          print('get on ever');
          status.value = '';
          await getTransactionList();
        }
      },
    );

    sController.addListener(_scrollListener);
    // tabController.addListener(_tabListener);
    tabController.animation?.addListener(_tabAnimationListener);
  }

  Future<void> getTransactionList() async {
    isLoading.value = true;
    var _filter = {
      'status': status.value,
      'created_at[from]': _startDate,
      'created_at[to]': _endDate,
      'date': _date,
      'product_category_id': selectedCategory.value.id,
    };
    final result = await _repo.getTransactionList(filter: FilterModel.fromJson(_filter));
    transactionList.assignAll(result.data?.data ?? []);
    isLastPage.value = false;
    isLoading.value = false;
  }

  // void _tabListener() {
  //   if (!tabController.indexIsChanging) {
  //     _activeTab.value = tabController.index;
  //   }
  // }

  void _tabAnimationListener() {
    if (_activeTab.value != tabController.animation?.value.round()) {
      _activeTab.value = tabController.animation!.value.round();
    }
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  void handleSelectCategory(idx) {
    selectedCategory.value = categoriesController.filterProductCategories[idx];
    getTransactionList();
  }

  void handleSelectDate(FilterDateModel valueDate) async {
    if (valueDate.dateRange != null) {
      final r = valueDate.dateRange;
      final String _s = DateHelper.formatDate(r!.start.toString(), format: 'dd-MM-yyyy');
      final String _e = DateHelper.formatDate(r.end.toString(), format: 'dd-MM-yyyy');
      _startDate = _s;
      _endDate = _e;
    } else if (valueDate.dateRange == null) {
      _startDate = null;
      _endDate = null;
    }

    _date = valueDate.date;
    getTransactionList();
  }

  void _scrollListener() async {
    if (sController.position.pixels == sController.position.maxScrollExtent) {
      if (isLastPage.value == true) return;

      _page++;
      var _filter = {
        'status': status.value,
        'created_at[from]': _startDate,
        'created_at[to]': _endDate,
        'date': _date,
        'product_category_id': selectedCategory.value.id,
        'page': _page,
        '_limit': _limit,
      };

      final result = await _repo.getTransactionList(filter: FilterModel.fromJson(_filter));
      if (result.success != false) {
        if (result.data!.data!.isEmpty) {
          isLastPage(true);
        } else {
          transactionList.addAll(result.data!.data!);
        }
      }
    }
  }
}
