// Package imports:
import 'dart:io';

import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image/image.dart' as img;
import 'package:path_provider/path_provider.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/utils/file_utils.dart';
import 'package:wispay_outlet/app/core/values/values.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/trx/trx_detail_item.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/helpers/transaction_detail_helper.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class TransactionDetailController extends GetxController {
  final int transactionId = Get.arguments["transactionId"];
  final String productCode = Get.arguments['productCode'];
  final String? productBrandCode = Get.arguments?['productBrandCode'];

  final transactionDataModel = const TransactionDataModel().obs;
  final CategoriesController categoriesController = Get.find();
  final ScreenshotController ssController = ScreenshotController();
  final TransactionRepository _transactionRepository = TransactionRepository(Get.find());
  final TextEditingController tComission = TextEditingController();
  final isLoading = false.obs;
  final comission = ''.obs;
  String pathImage = '';
  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;

  @override
  void onInit() {
    getInitialData();
    initSavetoPath();
    super.onInit();
  }

  void getInitialData() async {
    isLoading.value = true;
    final detail = await _transactionRepository.getDetail(id: transactionId);
    tComission.addListener(_onchangeComission);
    if (detail.data != null) {
      transactionDataModel.value = detail.data!;
      _setComission();
    }

    isLoading.value = false;
  }

  void _setComission() {
    tComission.clear();
    if (transactionDataModel.value.commission != null && transactionDataModel.value.commission != '0.0') {
      comission.value = transactionDataModel.value.commission;
      tComission.text = double.parse(transactionDataModel.value.commission).toInt().toString();
    }
  }

  void onCapture() async {
    WispayDialog.showLoading();
    final __dir = await getApplicationDocumentsDirectory();
    String fileName = DateTime.now().microsecondsSinceEpoch.toString() + '.png';
    final pixelRatio = MediaQuery.of(Get.context!).devicePixelRatio;
    final imageFile = await ssController.captureFromWidget(
      BaseCard(
        spreadRadius: 0,
        blurRadius: 0,
        margin: EdgeInsets.zero,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Assets.imagesWispayLogo.image(width: 137, height: 34),
            const SizedBox(height: Spacing.defaultSpacing),
            TransactionDetailHelper.getDetailPage(productCode, transactionDataModel.value, hideButton: true)
          ],
        ),
      ),
      pixelRatio: pixelRatio,
    );
    final assetFile = await FileUtils.getImageFileFromAssets(Assets.imagesWatermark.path);
    final _watermark = img.decodeImage(assetFile.readAsBytesSync())!;
    final _img = img.decodeImage(imageFile)!;
    final wm = img.drawImage(_img, _watermark, dstX: 1, dstY: 10, dstW: _img.width, dstH: _img.height);
    final _savedImage = File(__dir.path + '/' + fileName)..writeAsBytesSync(img.encodePng(wm));

    Share.shareFiles([_savedImage.path]).then((_) => Get.back());
  }

  void showComission() {
    WispayBottomSheet.show(
      onclose: () {
        _setComission();
      },
      title: 'Atur Komisi',
      children: [
        const InfoWidget(text: 'Komisi adalah keuntungan yang ditambahkan ke harga jual'),
        const SizedBox(height: Spacing.defaultSpacing),
        TrxDetailItem(title: 'Nama Produk', value: transactionDataModel.value.productName ?? '-'),
        TrxDetailItem(title: 'Harga Beli', value: NumberHelper.formatMoney(transactionDataModel.value.total)),
        Obx(
          () => TrxDetailItem(
            title: 'Besar Komisi',
            value: comission.value,
            isInput: true,
            textEditingController: tComission,
            crossAxisAlignment: CrossAxisAlignment.center,
          ),
        ),
        const Divider(thickness: 1),
        Obx(() => Row(
              children: [
                Expanded(
                  flex: 5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const CustomText(title: 'Tagihan Customer'),
                      CustomText(
                        title: NumberHelper.formatMoney((double.parse(transactionDataModel.value.total) +
                                double.parse(comission.value.isEmpty ? '0.0' : comission.value.replaceAll('.', '')))
                            .toString()),
                        size: FontSize.large,
                        color: WispayColors.cPrimary,
                        textType: TextType.Bold,
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: CustomButton(
                    title: 'Simpan',
                    onPress: () => _onSaveComission(),
                  ),
                )
              ],
            )),
        const SizedBox(height: Spacing.defaultSpacing),
      ],
    );
  }

  void _onSaveComission() async {
    Get.back();
    final comission = tComission.text.isEmpty ? '0' : tComission.text.replaceAll('.', '');
    await _transactionRepository.updateComission(
      trxId: transactionId,
      comission: comission,
    );
    getInitialData();
  }

  void _onchangeComission() => comission.value = tComission.text;

  void onPrint() async {
    final _connected = await bluetooth.isConnected;
    if (_connected == false) {
      Get.toNamed(Routes.CETAK_BUKTI);
    } else {
      TransactionDetailHelper.getPrintHeader(categoriesController.profile.value, pathImage);
      TransactionDetailHelper.getPrintBody(productCode, transactionDataModel.value);

      TransactionDetailHelper.getPrintFooter();
    }
  }

  initSavetoPath() async {
    const filename = 'wispay-bw.png';
    var bytes = await rootBundle.load("assets/images/wispay-bw.png");
    String dir = (await getApplicationDocumentsDirectory()).path;
    writeToFile(bytes, '$dir/$filename');
    pathImage = '$dir/$filename';
  }

  Future<void> writeToFile(ByteData data, String path) {
    final buffer = data.buffer;
    return File(path).writeAsBytes(buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }
}
