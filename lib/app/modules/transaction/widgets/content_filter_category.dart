// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/bottomsheet_title.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';

class ContentFilterCategory extends StatelessWidget {
  const ContentFilterCategory({
    Key? key,
    required this.c,
  }) : super(key: key);

  final CategoriesController c;

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      initialChildSize: 0.6,
      maxChildSize: 0.95,
      expand: false,
      builder: (ctx, sc) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
          child: Column(
            children: [
              const BottomSheetTitle(
                title: 'Pilih Kategori',
              ),
              const SizedBox(
                height: Spacing.defaultSpacing,
              ),
              SizedBox(
                height: 40,
                child: TextField(
                  style: const TextStyle(fontSize: FontSize.medium),
                  decoration: InputDecoration(
                    prefixIcon: const Icon(Icons.search),
                    isDense: true,
                    hintText: 'Cari Kategori',
                    hintStyle: const TextStyle(
                      color: WispayColors.cGrey99,
                      fontSize: FontSize.small,
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: Spacing.defaultSpacing,
              ),
              Expanded(
                child: ListView.separated(
                  shrinkWrap: true,
                  controller: sc,
                  separatorBuilder: (context, index) => const Divider(
                    thickness: 1,
                  ),
                  itemCount: c.productCategoryItems.length,
                  itemBuilder: (context, index) {
                    final item = c.productCategoryItems[index];
                    return InkWell(
                      onTap: () => Get.back(),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: Spacing.small,
                        ),
                        child: CustomText(
                          title: item.name ?? '',
                          size: FontSize.defaultSize,
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
