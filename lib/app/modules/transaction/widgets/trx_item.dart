// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

class TrxItem extends StatelessWidget {
  const TrxItem({Key? key, required this.data, required this.onTap}) : super(key: key);

  final TransactionDataModel data;
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(Spacing.defaultSpacing),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: WispayColors.cWhite,
          boxShadow: [buildShadow(blurRadius: 20, spreadRadius: 0)],
        ),
        child: Column(
          children: [
            _buildItem(
              leftWidget: CustomText(
                title: data.code ?? '-',
                size: FontSize.small,
                color: WispayColors.cBlack333,
              ),
              rightWidget: CustomText(
                title: DateHelper.formatDate(data.createdAt, format: 'dd MMM'),
                size: FontSize.small,
                color: WispayColors.cGreyBC,
              ),
            ),
            const SizedBox(height: 5),
            _buildItem(
              leftWidget: CustomText(
                title: data.productName ?? '-',
                textType: TextType.SemiBold,
                color: WispayColors.cBlack666,
              ),
              rightWidget: Visibility(
                child: CustomImage(
                  source: data.product?.image?.thumbnail?.url ?? '',
                  height: 20,
                ),
                visible: data.product?.image?.url != null,
              ),
            ),
            const SizedBox(height: 5),
            _buildItem(
              leftWidget: CustomText(
                title: data.customerId != null ? decrypt(data.customerId!) : '-',
                color: WispayColors.cGrey90,
              ),
              rightWidget: CustomText(
                title: TransactionStatusHelper.statusColorAndText(data.status!)['text'],
                textType: TextType.SemiBold,
                size: FontSize.small,
                color: TransactionStatusHelper.statusColorAndText(data.status!)['color'],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Row _buildItem({required Widget leftWidget, required Widget rightWidget}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [leftWidget, rightWidget],
    );
  }
}
