// Project imports:
import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/helpers/string_helper.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/data/models/profile/profile_model.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/modules/products/BPJS/views/bpjs_detail.dart';
import 'package:wispay_outlet/app/modules/products/PLN/views/pln_detail.dart';
import 'package:wispay_outlet/app/modules/products/asuransi_gadget/widgets/asuransi_gadget_detail.dart';
import 'package:wispay_outlet/app/modules/products/e-wallet/views/ewallet_detail.dart';
import 'package:wispay_outlet/app/modules/products/emoney/widgets/emoney_detail.dart';
import 'package:wispay_outlet/app/modules/products/modem/widgets/modem_detail.dart';
import 'package:wispay_outlet/app/modules/products/multifinance/views/multifinance_detail.dart';
import 'package:wispay_outlet/app/modules/products/omni/widgets/omni_detail.dart';
import 'package:wispay_outlet/app/modules/products/pdam/views/pdam_detail.dart';
import 'package:wispay_outlet/app/modules/products/pascabayar/views/pascabayar_detail.dart';
import 'package:wispay_outlet/app/modules/products/physical_voucer/widgets/physical_voucher_detail.dart';
import 'package:wispay_outlet/app/modules/products/pulsa_data/widgets/data_detail.dart';
import 'package:wispay_outlet/app/modules/products/pulsa_data/widgets/pulsa_detail.dart';
import 'package:wispay_outlet/app/modules/products/sim_card/widgets/sim_card_detail.dart';
import 'package:wispay_outlet/app/modules/products/streaming/widgets/streaming_card_detail.dart';
import 'package:wispay_outlet/app/modules/products/telkom/views/telkom_indihome_detail.dart';
import 'package:wispay_outlet/app/modules/products/telkom/views/telkom_telpon_detail.dart';
import 'package:wispay_outlet/app/modules/products/voucher_game/views/voucher_game_card_detail.dart';

class TransactionDetailHelper {
  static BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;
  static getDetailPage(
    String productCode,
    TransactionDataModel payload, {
    bool hideButton = false,
  }) {
    switch (productCode) {
      case ProductCode.TSEL_OMNI:
      case ProductCode.TSEL_OMNI_TELKOMSEL:
        return OmniDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.VOUCHER_GAME:
        return VoucherGameCardDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.STREAMING:
        return StreamingDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.EMONEY:
        return EmoneyDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.PHONE_CREDIT:
        return PulsaDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.MODEM:
        return ModemDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.DATA_PLAN:
        return DataDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.MULTIFINANCE:
        return MultiFinanceDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.PASCABAYAR:
        return PascabayarDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.PHYSICAL_VOUCHER:
        return PhysicalVoucherDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.INSURANCE:
        return AsuransiGadgetDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.PLN:
        return PLNDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.PDAM:
        return PDAMDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.BPJS:
        return BPJSDetail(payload: payload, status: payload.status ?? '', hidebutton: hideButton);
      case ProductCode.TELKOM_TAGIHAN_INDIHOME:
        return TelkomIndihomeDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.TELKOM_TAGIHAN_TELPON:
        return TelkomTelponDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.EWALLET:
        return EWalletDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      case ProductCode.SIM_CARD:
        return SimCardDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
      default:
        return MultiFinanceDetail(payload: payload, status: payload.status!, hidebutton: hideButton);
    }
  }

  static getPrintCustom(String title, String message, {int size = 0, int align = 0}) {
    bluetooth.printCustom("$title $message", size, align);
  }

  static getPrintSerial(String title, String message) {
    bluetooth.printCustom(title, 0, 1);
    bluetooth.printCustom(message, 1, 1);
  }

  static getPrintBorder() {
    bluetooth.printCustom("----------------------------------------", 0, 1);
  }

  static getPrintDefaultBody(TransactionDataModel payload) {
    getPrintCustom('Nama Produk    :', payload.product?.name ?? '-');
    getPrintCustom('Status         :', 'Sukses');
    getPrintCustom('No. TRX        :', payload.code ?? '-');
    getPrintCustom('Tanggal        :', DateHelper.formatDate(payload.createdAt, format: 'dd MMM yyyy HH:mm'));
  }

  static getPrintBody(String productCode, TransactionDataModel payload) {
    getPrintDefaultBody(payload);
    switch (productCode) {
      case ProductCode.VOUCHER_GAME:
        getPrintCustom('User Id        :', decrypt(payload.customerId));
        getPrintCustom('Tagihan        :',
            NumberHelper.formatMoney((double.parse(payload.total) + double.parse(payload.commission)).toString()));
        if (payload.serial != null) {
          getPrintBorder();
          getPrintSerial('Token', decrypt(payload.serial));
        }
        break;

      case ProductCode.EMONEY:
        getPrintCustom('Nama Kartu     :', decrypt(payload.customerId));
        getPrintCustom('Nomor Hp       :', decrypt(payload.customerPhone));
        getPrintCustom('Tagihan        :',
            NumberHelper.formatMoney((double.parse(payload.total) + double.parse(payload.commission)).toString()));
        break;

      case ProductCode.REMITTANCE:
        getPrintCustom('Rekening Tujuan:',
            '${payload.product?.productBrand?.name} - ${decrypt(payload.remittance?.accountNumber)}');
        getPrintCustom('Pemilik Rekening:', decrypt(payload.remittance?.accountName));
        getPrintCustom('Tagihan        :',
            NumberHelper.formatMoney((double.parse(payload.total) + double.parse(payload.commission)).toString()));
        break;

      case ProductCode.PHYSICAL_VOUCHER:
        getPrintCustom('Nomor Hp       :', decrypt(payload.customerPhone));
        getPrintCustom('Tagihan        :',
            NumberHelper.formatMoney((double.parse(payload.total) + double.parse(payload.commission)).toString()));
        getPrintBorder();
        getPrintSerial('No. Seri', payload.detailPayload?.iccid ?? '-');
        getPrintSerial('Kode Voucher', decrypt(payload.serial));
        break;

      case ProductCode.INSURANCE:
        getPrintCustom(
            'Tanggal Mulai  :', DateHelper.formatDate(payload.detailPayload?.startAt, format: 'dd MMM yyyy HH:mm'));
        getPrintCustom(
            'Tanggal Berakhir:', DateHelper.formatDate(payload.detailPayload?.finishAt, format: 'dd MMM yyyy HH:mm'));
        break;

      case ProductCode.PLN:
        getPrintCustom('Nomor Pelanggan:', decrypt(payload.customerId));
        getPrintCustom('Nama Pelanggan :', decrypt(payload.detailPayload?.name ?? '-'));
        if (payload.product?.productSubcategory?.name == 'Pra Bayar') {
          getPrintCustom('Tarik/Daya     :', payload.detailPayload?.power ?? '-');
          getPrintCustom('Tagihan        :',
              NumberHelper.formatMoney((double.parse(payload.total) + double.parse(payload.commission)).toString()));
          getPrintBorder();
          getPrintSerial('Token', decrypt(payload.serial));
        } else {
          getPrintCustom('Tagihan        :', '${payload.detailPayload?.bill ?? '-'} Bulan');
          getPrintCustom('Bulan/Tahun    :', payload.detailPayload?.period ?? '-');
          getPrintCustom('Tagihan        :',
              NumberHelper.formatMoney((double.parse(payload.total) + double.parse(payload.commission)).toString()));
        }
        break;

      case ProductCode.PDAM:
        getPrintCustom('Nama           :', decrypt(payload.detailPayload?.name));
        getPrintCustom('Nomor Hp       :', decrypt(payload.customerPhone));
        getPrintCustom('Tagihan        :',
            NumberHelper.formatMoney((double.parse(payload.total) + double.parse(payload.commission)).toString()));
        break;

      case ProductCode.BPJS:
        getPrintCustom('Nama           :', StringHelper.convertArrayToString(decrypt(payload.detailPayload?.name)));
        getPrintCustom('Jumlah Peserta :', payload.detailPayload?.numberParticipant ?? '-');
        getPrintCustom('Jumlah Bulan   :', payload.detailPayload?.period ?? '-');
        getPrintCustom('Tagihan        :',
            NumberHelper.formatMoney((double.parse(payload.total) + double.parse(payload.commission)).toString()));
        break;

      case ProductCode.TELKOM_TAGIHAN_INDIHOME:
        getPrintCustom('Nomor Pelanggan:', decrypt(payload.customerId));
        getPrintCustom('Nama Pelanggan :', decrypt(payload.detailPayload?.name));
        getPrintCustom('Nomor Hp       :', decrypt(payload.customerPhone));
        getPrintCustom('Tagihan        :',
            NumberHelper.formatMoney((double.parse(payload.total) + double.parse(payload.commission)).toString()));
        break;

      case ProductCode.TELKOM_TAGIHAN_TELPON:
        getPrintCustom('Nomor Hp       :', decrypt(payload.customerPhone));
        getPrintCustom('Tagihan        :',
            NumberHelper.formatMoney((double.parse(payload.total) + double.parse(payload.commission)).toString()));
        break;

      default:
        getPrintCustom('Nomor Hp       :', NumberHelper.formatPhoneRemove62(decrypt(payload.customerPhone)));
        getPrintCustom('Tagihan        :',
            NumberHelper.formatMoney((double.parse(payload.total) + double.parse(payload.commission)).toString()));
        if (payload.serial != null) {
          getPrintBorder();
          getPrintSerial('Nomor SN       :', decrypt(payload.serial));
        }
        break;
    }
  }

  static getPrintHeader(ProfileModel payload, String logo) {
    bluetooth.printImage(logo);
    bluetooth.printCustom(decrypt(payload.outletName), 0, 1);
    bluetooth.printCustom(decrypt(payload.outletAddress), 0, 1);
    getPrintBorder();
  }

  static getPrintFooter() {
    getPrintBorder();
    bluetooth.printCustom("Beli pulsa dan bayar tagihan semakin mudah di wis-pay.com", 0, 1);
    bluetooth.printCustom("Struk ini adalah bukti pembayaran yang sah", 0, 1);
    bluetooth.printNewLine();
    bluetooth.printNewLine();
    bluetooth.paperCut();
  }
}
