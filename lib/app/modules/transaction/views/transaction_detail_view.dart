// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:auto_size_text/auto_size_text.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/transaction/controllers/transaction_detail_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/helpers/transaction_detail_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class TransactionDetailView extends GetView<TransactionDetailController> {
  const TransactionDetailView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(controller.productCode);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Bukti Transaksi'),
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () => Get.back(),
        ),
      ),
      body: Obx(() {
        final code =
            controller.productCode == ProductCode.TELKOM ? controller.productBrandCode : controller.productCode;
        return controller.isLoading.value
            ? Center(
                child: Lottie.asset(Assets.lottieLoading, width: 100),
              )
            : SafeArea(
                child: SingleChildScrollView(
                  padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing, vertical: Spacing.large),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      TransactionDetailHelper.getDetailPage(
                        code!,
                        controller.transactionDataModel.value,
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      Visibility(
                        child: WispayOutlinedButton(
                          title: 'Atur Komisi',
                          onPress: () => controller.showComission(),
                        ),
                        visible: controller.productCode != ProductCode.INSURANCE &&
                            controller.productCode != ProductCode.SELL_OUT,
                      ),
                      const SizedBox(height: Spacing.large),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Expanded(
                            child: CustomButton(
                              isEnabled:
                                  controller.transactionDataModel.value.status == TransactionStatusHelper.SUCCESS,
                              leftIcon: Image.asset(Assets.iconsButtonPrint.path, width: 20, height: 20),
                              type: ButtonType.Secondary,
                              title: 'Cetak Bukti',
                              onPress: () => controller.onPrint(),
                            ),
                          ),
                          const SizedBox(width: Spacing.defaultSpacing),
                          Expanded(
                            child: CustomButton(
                              leftIcon: Image.asset(Assets.iconsButtonSent.path, width: 20, height: 20),
                              type: ButtonType.Secondary,
                              title: 'Kirim Bukti',
                              onPress: () => controller.onCapture(),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      const SizedBox(
                        width: double.maxFinite,
                        child: AutoSizeText.rich(
                          TextSpan(
                            text: 'Butuh bantuan? ',
                            style: TextStyle(fontSize: 14),
                            children: [
                              TextSpan(
                                text: 'Klik di sini',
                                style: TextStyle(color: WispayColors.cPrimary, decoration: TextDecoration.underline),
                              )
                            ],
                          ),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
              );
      }),
    );
  }
}
