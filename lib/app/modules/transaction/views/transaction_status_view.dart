// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class TransactionStatusView extends GetView {
  const TransactionStatusView({
    Key? key,
    required this.child,
    this.productCode,
    this.transactionId,
    required this.status,
    this.productBrandCode,
  }) : super(key: key);

  final Widget child;
  final String? productCode;
  final String? productBrandCode;
  final int? transactionId;
  final String status;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(''),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.close_sharp),
          onPressed: () => Get.offAllNamed(Routes.HOME),
        ),
      ),
      body: SafeArea(
        child: Container(
          color: WispayColors.cWhite,
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _title(),
              const SizedBox(
                height: Spacing.defaultSpacing,
              ),
              _buildIcon(context),
              const SizedBox(height: Spacing.defaultSpacing),
              _description(),
              const SizedBox(height: Spacing.large),
              child,
              const Spacer(),
              CustomButton(
                title: 'Lihat Rincian Transaksi',
                onPress: () => Get.toNamed(
                  Routes.DETAIL_TRANSACTION,
                  arguments: {
                    'productCode': productCode,
                    'transactionId': transactionId,
                    'productBrandCode': productBrandCode,
                  },
                ),
              ),
              const SizedBox(height: Spacing.defaultSpacing),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  CustomText(title: 'Butuh Bantuan? '),
                  CustomText(
                    title: 'Klik Di sini',
                    color: WispayColors.cPrimary,
                    decoration: TextDecoration.underline,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildIcon(BuildContext context) {
    return TransactionStatusHelper.getIcon(status);
  }

  Widget _description() => CustomText(
        title: TransactionStatusHelper.getDescription(status),
        textAlign: TextAlign.center,
      );

  Widget _title() {
    return CustomText(
      title: TransactionStatusHelper.paymentStatus(status),
      size: FontSize.medium,
      textType: TextType.Bold,
      textAlign: TextAlign.center,
    );
  }
}
