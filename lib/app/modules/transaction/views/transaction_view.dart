// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/empty_widget.dart';
import 'package:wispay_outlet/app/global_widgets/rounded_tab_bar.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/widgets/skeleton_widget.dart';
import 'package:wispay_outlet/app/modules/transaction/widgets/trx_item.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/app/views/filter_date_view.dart';
import '../controllers/transaction_controller.dart';

class TransactionView extends GetView<TransactionController> {
  TransactionView({Key? key}) : super(key: key);
  final CategoriesController c = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Transaksi'), centerTitle: true),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.only(top: 16.h),
          child: Column(
            children: [
              Obx(
                () => Padding(
                  padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                  child: Dropdown(
                    label: controller.selectedCategory.value.name,
                    value: controller.selectedCategory.value.name ?? "",
                    onTap: () => WispayBottomSheet.scrollableList<BaseModel>(
                      onSearch: (value) => c.searchFilterCategories(value),
                      items: c.filterProductCategories,
                      title: 'Pilih Kategori',
                      onClose: () => c.generateFilterCategories(),
                      selectedItem: controller.selectedCategory.value,
                      onSelect: (idx) {
                        controller.handleSelectCategory(idx);
                        c.generateFilterCategories();
                        Get.back();
                      },
                    ),
                  ),
                ),
              ),
              SizedBox(height: Spacing.defaultSpacing.h),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                child: FilterDate(onChangeDate: (value) {
                  controller.handleSelectDate(value);
                }),
              ),
              SizedBox(height: Spacing.small.h),
              Obx(() => Expanded(child: _buildTabBar()))
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTabBar() {
    return DefaultTabController(
      length: controller.trxStatus.length,
      child: Column(
        children: [
          _buildButtonTabBar(),
          Expanded(
            child: TabBarView(
              controller: controller.tabController,
              children: _buildTabBarView(controller),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildButtonTabBar() {
    List<Widget> items = [];
    for (var i = 0; i < controller.trxStatus.length; i++) {
      items.add(
        Tab(text: TransactionStatusHelper.TRANSACTION_STATUS_MAP[controller.trxStatus[i]]),
      );
    }
    return RoundedTabBar(tabs: items, controller: controller.tabController);
  }

  _buildTabBarView(TransactionController c) {
    List<Widget> list = [];
    for (var i = 0; i < c.trxStatus.length; i++) {
      list.add(_buildList(c, c.transactionList));
    }

    return list;
  }

  _buildList(TransactionController c, List<TransactionDataModel> transactionData) {
    if (transactionData.isEmpty && c.isLoading.value == false) {
      return const EmptyWidget(
        title: 'Belum Ada Riwayat',
        subTitle: 'Ayo mulai bertransaksi dan nikmati banyak keuntungannya',
      );
    }

    return RefreshIndicator(
      onRefresh: c.getTransactionList,
      child: ListView.separated(
        padding: EdgeInsets.symmetric(vertical: Spacing.defaultSpacing.h, horizontal: Spacing.defaultSpacing.w),
        itemCount: c.isLoading.value ? 5 : transactionData.length,
        separatorBuilder: (context, index) => const SizedBox(height: Spacing.small),
        controller: controller.sController,
        itemBuilder: (context, index) {
          if (c.isLoading.value) {
            return const SkeletonWidget();
          } else {
            var data = transactionData[index];
            return TrxItem(
              data: data,
              onTap: () => Get.toNamed(
                Routes.DETAIL_TRANSACTION,
                arguments: {
                  'transactionId': data.id,
                  'productCode': data.product?.productCategory?.code,
                  'productBrandCode': data.product?.productBrand?.code,
                },
              ),
            );
          }
        },
      ),
    );
  }
}
