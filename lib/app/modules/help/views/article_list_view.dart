// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/card_menu_widget.dart';
import 'package:wispay_outlet/app/modules/help/controllers/article_list_controller.dart';

class ArticleListView extends GetView<ArticleListController> {
  const ArticleListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String title = Get.arguments.title;

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: Spacing.defaultSpacing,
            vertical: Spacing.large,
          ),
          child: CardMenuWidget(menu: kArticleList),
        ),
      ),
    );
  }
}
