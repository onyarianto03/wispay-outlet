// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class ArticleView extends GetView {
  const ArticleView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Artikel detail'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: Spacing.defaultSpacing,
            vertical: Spacing.large,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.network(
                  'https://picsum.photos/id/1/200/300',
                  height: MediaQuery.of(context).size.width * 0.5,
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.cover,
                ),
              ).marginOnly(bottom: Spacing.defaultSpacing),
              const CustomText(
                title: 'Lorem Ipsum',
                textType: TextType.Bold,
                color: WispayColors.cBlack,
                size: FontSize.medium,
                margin: EdgeInsets.only(bottom: Spacing.small),
              ),
              const CustomText(
                title:
                    'Contoh penjelasan akun belum aktif: Bagi akun baru, pengguna perlu melengkapi data dan melakukan aktivasi sebelum bisa mulai berjualan. Proses aktivasi akun akan dibantu oleh Agen Sales kami sebagai perwakilan yang berada di tiap-tiap kota/kabupaten.',
                color: WispayColors.cBlack666,
                margin: EdgeInsets.only(bottom: Spacing.defaultSpacing),
              ),
              const CustomText(
                title: 'Apakah penjelasan ini membantu?',
                textType: TextType.Bold,
                color: WispayColors.cBlack,
                size: FontSize.defaultSize,
                margin: EdgeInsets.only(bottom: Spacing.small),
              ),
              Row(
                children: [
                  Row(
                    children: [
                      Assets.iconsAccountThumbUp
                          .image(height: Spacing.large, width: Spacing.large)
                          .marginOnly(right: Spacing.small),
                      const CustomText(title: 'Ya')
                    ],
                  ).marginOnly(right: Spacing.large),
                  Row(
                    children: [
                      Assets.iconsAccountThumbDown
                          .image(height: Spacing.large, width: Spacing.large)
                          .marginOnly(right: Spacing.small),
                      const CustomText(title: 'Tidak')
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
