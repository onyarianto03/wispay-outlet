// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/card_menu_widget.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import '../controllers/help_controller.dart';

class HelpView extends GetView<HelpController> {
  const HelpView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Bantuan'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Assets.imagesPusatBantuan
                    .image(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.width * 0.55,
                    )
                    .marginOnly(bottom: Spacing.defaultSpacing),
                CardMenuWidget(menu: kAccountHelp),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
