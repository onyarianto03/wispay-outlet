// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/modules/help/controllers/article_controller.dart';
import 'package:wispay_outlet/app/modules/help/controllers/article_list_controller.dart';
import 'package:wispay_outlet/app/modules/help/controllers/help_center_controller.dart';
import '../controllers/help_controller.dart';

class HelpBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ArticleController>(
      () => ArticleController(),
    );
    Get.lazyPut<ArticleListController>(
      () => ArticleListController(),
    );
    Get.lazyPut<HelpCenterController>(
      () => HelpCenterController(),
    );
    Get.lazyPut<HelpController>(
      () => HelpController(),
    );
  }
}
