import 'package:flutter/widgets.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/cetak_bukti/controllers/cetak_bukti_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class PrinterNotConfigured extends StatelessWidget {
  const PrinterNotConfigured({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final CetakBuktiController controller;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 60),
        Image.asset(
          Assets.imagesPrint.path,
          width: MediaQuery.of(context).size.width * 0.45,
        ),
        const SizedBox(
          height: Spacing.defaultSpacing,
        ),
        const CustomText(
          title: 'Hubungkan Printer',
          size: FontSize.medium,
          textType: TextType.SemiBold,
          color: WispayColors.cBlack333,
        ),
        const SizedBox(height: Spacing.small),
        const CustomText(
          textAlign: TextAlign.center,
          title: 'Hubungkan mesin printer melalui pengaturan menu bluetooth di perangkat kamu.',
        ),
        const Spacer(),
        CustomButton(
          title: 'Hubungkan Printer',
          onPress: controller.connectPrinter,
        )
      ],
    );
  }
}
