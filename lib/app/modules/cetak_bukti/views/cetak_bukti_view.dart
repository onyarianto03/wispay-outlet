import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/empty_widget.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/cetak_bukti/widgets/not_configurated.dart';

import '../controllers/cetak_bukti_controller.dart';

class CetakBuktiView extends GetView<CetakBuktiController> {
  const CetakBuktiView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text('Cetak Bukti'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () => Get.back(),
        ),
      ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          child: Obx(() {
            if (!controller.isConfigurated.value) return PrinterNotConfigured(controller: controller);
            if (controller.listPrinter.isEmpty) {
              return const EmptyWidget(title: 'Tidak dapat menemukan printer anda...', subTitle: '');
            }

            return BaseCard(
              margin: EdgeInsets.zero,
              child: ListView.separated(
                separatorBuilder: (_, __) => const SizedBox(height: Spacing.medium),
                shrinkWrap: true,
                itemCount: controller.listPrinter.length,
                itemBuilder: (_, idx) {
                  return _buildItem(controller.listPrinter[idx]);
                },
              ),
            );
          }),
        ),
      ),
    );
  }

  Widget _buildItem(CardMenu item) {
    return InkWell(
      onTap: () {
        item.onPress?.call();
      },
      child: Row(
        children: [
          item.icon != '' ? Image.asset(item.icon, width: 24, height: 24) : const SizedBox(),
          SizedBox(width: item.icon != '' ? Spacing.defaultSpacing : 0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildTitle(item),
                Visibility(
                  child: CustomText(
                    title: item.subTitle ?? "",
                    size: FontSize.small,
                    color: item.subTitleColor,
                  ),
                  visible: item.subTitle != null,
                )
              ],
            ),
          ),
          item.customRightIcon != null
              ? item.customRightIcon!
              : (item.route != null || item.onPress != null)
                  ? const Icon(Icons.arrow_forward_ios_rounded, size: 16)
                  : const SizedBox(),
        ],
      ),
    );
  }

  Row _buildTitle(CardMenu item) {
    return Row(
      children: [
        CustomText(
          title: item.title,
          size: FontSize.medium,
          textType: TextType.SemiBold,
          color: WispayColors.cBlack333,
        ),
        const SizedBox(width: Spacing.defaultSpacing),
        Visibility(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 1),
            decoration: BoxDecoration(
              color: WispayColors.cSuccess,
              borderRadius: BorderRadius.circular(16),
            ),
            child: const CustomText(
              title: 'Baru',
              color: WispayColors.cBlack333,
              size: FontSize.xSmall,
              textType: TextType.SemiBold,
            ),
          ),
          visible: item.isNew,
        )
      ],
    );
  }
}
