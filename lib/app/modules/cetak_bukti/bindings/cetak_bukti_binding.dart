import 'package:get/get.dart';

import '../controllers/cetak_bukti_controller.dart';

class CetakBuktiBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CetakBuktiController>(
      () => CetakBuktiController(),
    );
  }
}
