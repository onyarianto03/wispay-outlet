import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/card_menu.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import 'package:blue_thermal_printer/blue_thermal_printer.dart';

class CetakBuktiController extends GetxController {
  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;
  final RxBool isConfigurated = false.obs;
  final listPrinter = <CardMenu>[].obs;

  final devices = <BluetoothDevice>[].obs;
  final pathImage = ''.obs;
  final connected = false.obs;
  final pressed = false.obs;

  @override
  void onClose() {}

  connectPrinter() async {
    final isOn = await bluetooth.isOn;
    if (isOn == true) {
      await initPlatformState();
      isConfigurated.value = true;
    } else {
      WispaySnackbar.showError('Bluetooth tidak aktif');
    }
  }

  Future<void> initPlatformState() async {
    List<BluetoothDevice> _devices = [];

    try {
      _devices = await bluetooth.getBondedDevices();
      final List<CardMenu> _new = _devices.map((device) {
        return CardMenu(
          title: device.name ?? '',
          icon: device.connected ? Assets.iconsBluetoothActive.path : Assets.iconsBluetoothInactive.path,
          subTitle: device.connected ? 'Terhubung' : 'Tidak Terhubung',
          onPress: device.connected ? null : () => _connectToPrinter(device),
          customRightIcon: const SizedBox(),
        );
      }).toList();
      listPrinter.assignAll(_new);
    } on PlatformException catch (e) {
      print('Error $e');
    }
  }

  void _connectToPrinter(BluetoothDevice device) async {
    final existing = listPrinter.indexWhere((element) => element.title == device.name);

    if (existing != -1) {
      final val = CardMenu(
        title: device.name ?? '',
        icon: Assets.iconsBluetoothActive.path,
        subTitle: 'Sedang menghubungkan',
        customRightIcon: const CupertinoActivityIndicator(),
      );
      listPrinter[existing] = val;
      listPrinter.refresh();
    }
    try {
      final _isConnected = await bluetooth.isConnected;
      if (_isConnected == false) {
        bluetooth.connect(device).then((_) {
          if (existing != -1) {
            final val = CardMenu(
              title: device.name ?? '',
              icon: Assets.iconsBluetoothActive.path,
              subTitle: 'Terhubung',
              customRightIcon: const SizedBox(),
            );
            listPrinter[existing] = val;
            listPrinter.refresh();
            Get.back();
          }
        });
      }
    } on PlatformException catch (e) {
      print('Error $e');
    }
  }
}
