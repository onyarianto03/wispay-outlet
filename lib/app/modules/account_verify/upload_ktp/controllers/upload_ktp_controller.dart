// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/controllers/image_picker_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class UploadKtpController extends GetxController {
  ImagePickerController picker = Get.find();

  @override
  void onClose() {}

  void onUpload() {
    Get.toNamed(Routes.REGISTER_PIN);
  }
}
