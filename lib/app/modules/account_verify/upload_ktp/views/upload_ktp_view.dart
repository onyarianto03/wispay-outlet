// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:auto_size_text/auto_size_text.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/progress_title.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import '../controllers/upload_ktp_controller.dart';

class UploadKtpView extends GetView<UploadKtpController> {
  const UploadKtpView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(''),
        centerTitle: true,
        leading: IconButton(
          onPressed: () => Get.back(),
          icon: const Icon(Icons.chevron_left, size: 35),
        ),
      ),
      body: Container(
        color: WispayColors.cWhite,
        padding: const EdgeInsets.all(Spacing.defaultSpacing),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const ProgressTitle(
              title: 'Upload Foto KTP',
              description: 'Atur PIN',
              progressText: '2/3',
              percent: 0.70,
            ),
            const SizedBox(height: Spacing.large),
            const CustomText(
              title: 'Contoh foto:',
              textType: TextType.SemiBold,
              size: FontSize.small,
            ),
            _uploadDesc(),
            const CustomText(
              title: 'Foto KTP',
              margin: EdgeInsets.only(top: 26, bottom: Spacing.small),
              size: FontSize.small,
              textType: TextType.SemiBold,
            ),
            Obx(
              () {
                if (controller.picker.imageFile.value.path.isNotEmpty) return _imagePreview();

                return _imagePlaceholder();
              },
            ),
            const Spacer(),
            Obx(() {
              if (controller.picker.imageFile.value.path.isNotEmpty) {
                return Row(
                  children: [
                    Expanded(
                      child: WispayOutlinedButton(
                        title: 'Ganti Foto',
                        onPress: () => controller.picker.showUploadBottomsheet(),
                      ),
                    ),
                    const SizedBox(width: Spacing.defaultSpacing),
                    Expanded(
                      child: CustomButton(
                        title: 'Upload Foto',
                        onPress: () => controller.onUpload(),
                      ),
                    ),
                  ],
                );
              }
              return CustomButton(
                title: 'Upload Foto',
                onPress: () => controller.picker.showUploadBottomsheet(),
              );
            })
          ],
        ),
      ),
    );
  }

  Column _imagePreview() {
    return Column(
      children: [
        Container(
          clipBehavior: Clip.hardEdge,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
          width: double.maxFinite,
          child: Image.file(controller.picker.imageFile.value, fit: BoxFit.cover, height: 200),
        ),
        const SizedBox(height: Spacing.small),
        Row(
          children: const [
            Icon(Icons.check_circle, color: WispayColors.cPrimary, size: 16),
            CustomText(
              title: 'Berkas foto berhasil ditambahkan.',
              size: FontSize.small,
              textType: TextType.SemiBold,
              color: WispayColors.cPrimary,
              margin: EdgeInsets.only(left: Spacing.xSmall),
            ),
          ],
        )
      ],
    );
  }

  DottedBorder _imagePlaceholder() {
    return DottedBorder(
      borderType: BorderType.RRect,
      radius: const Radius.circular(8),
      dashPattern: const [4, 4],
      padding: EdgeInsets.zero,
      color: WispayColors.cPrimary,
      child: Container(
        height: 179,
        width: double.maxFinite,
        decoration: BoxDecoration(
          color: WispayColors.cPrimary.withOpacity(0.1),
          image: const DecorationImage(
            image: Assets.iconsMiscWave3,
            alignment: Alignment.bottomRight,
          ),
        ),
        child: Center(
          child: Container(
            clipBehavior: Clip.hardEdge,
            constraints: const BoxConstraints(maxHeight: 68),
            padding: const EdgeInsets.all(Spacing.small),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: WispayColors.cPrimary.withOpacity(0.15),
            ),
            child: Column(
              children: [
                Assets.iconsAddPhoto.image(width: 32, height: 32),
                const CustomText(
                  title: 'Ambil foto KTP',
                  size: FontSize.small,
                  color: WispayColors.cPrimary,
                  textType: TextType.SemiBold,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Row _uploadDesc() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Assets.iconsKtp.image(width: 100, height: 60),
        const SizedBox(width: Spacing.small),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AutoSizeText.rich(
                TextSpan(
                  text: '1. Gunakan ',
                  style: const TextStyle(fontSize: 14),
                  children: [
                    TextSpan(text: 'KTP asli ', style: _bold()),
                    const TextSpan(text: '(bukan fotokopi)'),
                  ],
                ),
              ),
              AutoSizeText.rich(
                TextSpan(
                  text: '2. Pastikan foto ',
                  style: const TextStyle(fontSize: 14),
                  children: [
                    TextSpan(text: 'KTP ', style: _bold()),
                    const TextSpan(text: 'tidak buram'),
                  ],
                ),
              ),
              AutoSizeText.rich(
                TextSpan(
                  text: '3. Pastikan foto ',
                  style: const TextStyle(fontSize: 14),
                  children: [
                    TextSpan(text: 'KTP ', style: _bold()),
                    const TextSpan(text: 'tidak terpotong'),
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  TextStyle _bold() {
    return const TextStyle(
      fontWeight: FontWeight.w600,
    );
  }
}
