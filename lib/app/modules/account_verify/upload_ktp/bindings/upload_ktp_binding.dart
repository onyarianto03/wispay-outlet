// Package imports:
import 'package:get/get.dart';

// Project imports:
import '../controllers/upload_ktp_controller.dart';

class UploadKtpBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<UploadKtpController>(UploadKtpController());
  }
}
