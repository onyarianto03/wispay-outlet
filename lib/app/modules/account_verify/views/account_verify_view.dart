// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/empty_widget.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import '../controllers/account_verify_controller.dart';

class AccountVerifyView extends GetView<AccountVerifyController> {
  const AccountVerifyView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Verifikasi Akun'),
        leading: IconButton(
          onPressed: () => Get.back(),
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(Spacing.defaultSpacing),
        child: Column(
          children: [
            const Spacer(),
            EmptyWidget(
              image: Assets.imagesUploadKtp.path,
              title: 'Kelengkapan Identitas Diri',
              subTitle: 'Mohon lengkapi foto KTP untuk keperluan verifikasi akunmu.',
            ),
            const Spacer(),
            CustomButton(
              title: 'Upload Foto KTP',
              onPress: () => Get.toNamed(Routes.UPLOAD_KTP),
            )
          ],
        ),
      ),
    );
  }
}
