// Package imports:
import 'package:get/get.dart';

// Project imports:
import '../controllers/account_verify_controller.dart';

class AccountVerifyBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AccountVerifyController>(
      () => AccountVerifyController(),
    );
  }
}
