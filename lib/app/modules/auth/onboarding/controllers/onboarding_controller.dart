import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/services/storage_service.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

List onboardingContents = [
  {
    "title": "Banyak Pilihan Layanan",
    "description": "Variasi Layanan kami mulai dari berbagai macam tagihan hingga layanan top up.",
    "image": Assets.imagesOnboarding1.path
  },
  {
    "title": "Bantuan Standby 24 / 7",
    "description": "Tidak perlu kuatir, Customer Service kami siap membantu 24 jam setiap saat.",
    "image": Assets.imagesOnboarding2.path
  },
  {
    "title": "Promo & Poin Berhadiah",
    "description": "Transaksi & dapatkan poin yang bisa ditukarkan dengan berbagai hadiah menarik.",
    "image": Assets.imagesOnboarding3.path
  },
];

class OnboardingController extends GetxController {
  final currentPage = 0.obs;
  PageController pageController = PageController(initialPage: 0);
  final StorageService _storage = Get.find();

  @override
  void onClose() {}

  void onPageChanged(index) {
    currentPage.value = index;
  }

  void next({skip = false}) {
    // currentPage.value = index;
    if (currentPage.value == onboardingContents.length - 1 || skip == true) {
      _storage.finishOnboarding();
      // Get.reset();
      Get.offAllNamed(Routes.LOGIN);
    } else {
      pageController.animateToPage(
        currentPage.value + 1,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeIn,
      );
    }
  }
}
