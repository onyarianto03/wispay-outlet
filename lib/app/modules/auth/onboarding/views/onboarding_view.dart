import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/rounded_only_icon_button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/auth/onboarding/controllers/onboarding_controller.dart';

class OnboardingView extends GetView<OnboardingController> {
  const OnboardingView({Key? key}) : super(key: key);

  AnimatedContainer dotIndicator(index) {
    return AnimatedContainer(
      margin: const EdgeInsets.symmetric(horizontal: 3),
      duration: const Duration(milliseconds: 400),
      height: 8,
      width: 8,
      decoration: BoxDecoration(
        color: controller.currentPage.value == index ? WispayColors.cPrimary : WispayColors.cGreyC,
        shape: BoxShape.circle,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () async {
        if (controller.currentPage.value > 0) {
          controller.currentPage.value -= 1;
          return false;
        }
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Stack(
            children: [
              Column(
                children: [
                  Expanded(
                    flex: 12,
                    child: Container(
                      alignment: Alignment.center,
                      child: PageView.builder(
                        itemCount: onboardingContents.length,
                        controller: controller.pageController,
                        onPageChanged: controller.onPageChanged,
                        itemBuilder: (ctx, index) => Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Center(
                              child: Image.asset(
                                onboardingContents[index]["image"],
                                width: width - 100,
                                fit: BoxFit.contain,
                              ),
                            ),
                            Container(
                              width: double.infinity,
                              margin: const EdgeInsets.symmetric(
                                horizontal: Spacing.large,
                                vertical: Spacing.defaultSpacing,
                              ),
                              child: Column(
                                children: [
                                  CustomText(
                                      title: onboardingContents[index]["title"],
                                      margin: const EdgeInsets.only(bottom: 10),
                                      size: FontSize.large,
                                      textType: TextType.SemiBold,
                                      color: WispayColors.cBlack333),
                                  CustomText(
                                    title: onboardingContents[index]["description"],
                                    textAlign: TextAlign.center,
                                    lineHeight: 1.6,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Obx(
                      () => Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: List.generate(
                          onboardingContents.length,
                          (index) => dotIndicator(index),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: Spacing.large),
                  RoundedOnlyIconButton(
                    icon: const Icon(Icons.chevron_right, size: 35),
                    onPressed: controller.next,
                    borderRadius: 50,
                  ),
                  const SizedBox(height: Spacing.large),
                ],
              ),
              Positioned(
                right: 0,
                child: InkWell(
                  onTap: () => controller.next(skip: true),
                  child: const CustomText(
                    title: 'Skip',
                    textType: TextType.SemiBold,
                    color: WispayColors.cPrimary,
                    margin: EdgeInsets.all(Spacing.defaultSpacing),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
