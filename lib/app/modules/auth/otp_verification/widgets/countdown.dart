// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/modules/auth/otp_verification/controllers/countdown_controller.dart';
import 'package:wispay_outlet/generated/locales.g.dart';

class Countdown extends GetView<CountdownController> {
  final VoidCallback? onResend;

  const Countdown({Key? key, this.onResend}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var textStyle = Theme.of(context).textTheme;
    return Obx(
      () {
        Duration timeLeft = Duration(seconds: controller.remainingTime.value.toInt().abs());
        var minutes = timeLeft.inMinutes.remainder(60).toString().padLeft(2, '0');
        var seconds = timeLeft.inSeconds.remainder(60).toString().padLeft(2, '0');

        return controller.isCountdownFinished.value
            ? GestureDetector(
                onTap: () {
                  controller.onReset();
                  onResend?.call();
                },
                child: Text(
                  LocaleKeys.resend_code.tr,
                  style: textStyle.bodyText1?.copyWith(
                    decoration: TextDecoration.underline,
                    color: WispayColors.cPrimary,
                  ),
                ),
              )
            : Text(
                LocaleKeys.resend.trParams({'time': '($minutes:$seconds)'}),
                style: textStyle.bodyText1?.copyWith(
                  color: WispayColors.cPrimary,
                ),
              );
      },
    );
  }
}
