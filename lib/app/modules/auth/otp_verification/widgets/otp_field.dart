// Dart imports:
import 'dart:async';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:pin_code_fields/pin_code_fields.dart';

class OTPField extends StatelessWidget {
  const OTPField({Key? key, this.errorController, this.onCompleted}) : super(key: key);

  final StreamController<ErrorAnimationType>? errorController;
  final ValueChanged<String>? onCompleted;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: PinCodeTextField(
        autoFocus: false,
        keyboardType: TextInputType.number,
        onCompleted: onCompleted,
        animationCurve: Curves.bounceIn,
        errorAnimationController: errorController,
        length: 5,
        onChanged: (value) {},
        pinTheme: PinTheme(
          shape: PinCodeFieldShape.box,
          borderRadius: BorderRadius.circular(8),
          fieldHeight: 48,
          fieldWidth: 48,
          activeFillColor: Colors.white,
          inactiveColor: Colors.black.withOpacity(0.3),
          selectedColor: Colors.black,
          activeColor: Colors.black,
        ),
        showCursor: true,
        cursorColor: Colors.black,
        cursorHeight: 20,
        animationType: AnimationType.fade,
        appContext: context,
      ),
    );
  }
}
