// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/modules/auth/otp_verification/controllers/countdown_controller.dart';
import '../controllers/otp_verification_controller.dart';

class OtpVerificationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OtpVerificationController>(
      () => OtpVerificationController(),
    );
    Get.lazyPut<CountdownController>(() => CountdownController());
  }
}
