// Flutter imports:
import 'package:flutter/animation.dart';

// Package imports:
import 'package:get/get.dart';

class CountdownController extends GetxController with SingleGetTickerProviderMixin {
  RxDouble remainingTime = DateTime.now().difference(Get.arguments['otp_expired']).inSeconds.toDouble().obs;

  RxBool isCountdownFinished = false.obs;

  late Animation<double> animation;
  late AnimationController animationController;

  @override
  void onInit() {
    animationController = _generateAnimationController();
    animation = _generateAnimation();
    animationController.forward();
    super.onInit();
  }

  void _animationListener() {
    remainingTime.value = animation.value;
  }

  void _statusListener(AnimationStatus status) {
    if (status == AnimationStatus.completed) {
      isCountdownFinished.value = true;
    }
  }

  void onReset() {
    isCountdownFinished.value = false;
    animationController.reset();
    remainingTime.value = 65;
    animationController = _generateAnimationController();
    animation = _generateAnimation();
    animationController.forward();
  }

  @override
  void onClose() {
    animationController.dispose();
    animation.removeListener(_animationListener);
    animation.removeStatusListener(_statusListener);
    super.onClose();
  }

  Animation<double> _generateAnimation() =>
      Tween<double>(begin: remainingTime.value.abs(), end: 0).animate(animationController)
        ..addListener(_animationListener)
        ..addStatusListener(_statusListener);

  AnimationController _generateAnimationController() => AnimationController(
        vsync: this,
        duration: Duration(seconds: remainingTime.value.toInt().abs()),
      );
}
