// Dart imports:
import 'dart:async';

// Package imports:
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

// Project imports:
import 'package:wispay_outlet/app/core/services/storage_service.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/core/values/utils.dart';
import 'package:wispay_outlet/app/data/models/auth/otp_model.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/models/profile/profile_model.dart';
import 'package:wispay_outlet/app/data/models/token/token_model.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';
import 'package:wispay_outlet/app/modules/auth/pin_input/views/update_pin_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class OtpVerificationController extends GetxController {
  final _authRepository = AccountRepository(Get.find());

  final StorageService _storage = Get.find();

  String phoneNumber = Get.arguments['phone_number'];
  String? otpKey = Get.arguments['otp_key'];
  bool isAuth = Get.arguments?['is_auth'] ?? true;
  String otpType = Get.arguments['otp_type'];

  RxString otpCode = ''.obs;

  final errorController = StreamController<ErrorAnimationType>();

  @override
  void onClose() {
    errorController.close();
    errorController.done;
    super.onClose();
  }

  void onComplete(String otp) {
    otpCode.value = otp;
  }

  void _next(String pin, String accessKey) async {
    WispayDialog.showLoading();
    BaseResponse<TokenModel> response = await _authRepository.signIn(pin, accessKey, _storage.getFCM());

    if (response.success != false) {
      WispayDialog.closeDialog();
      _storage.saveToken(response.data);
      Get.offAllNamed(Routes.HOME);
    } else {
      WispayDialog.closeDialog();
      WispaySnackbar.showError(response.message!);
    }
  }

  void onSubmit() async {
    if (otpCode.value.length != 5) {
      errorController.add(ErrorAnimationType.shake);
      return;
    }

    if (isAuth) {
      _handleIsAuth();
      return;
    }

    _handleIsNotAuth();
  }

  void onResend() async {
    BaseResponse<OTPModel> response = await _authRepository.resendOTP(
      phoneNumber,
      otpKey,
    );

    if (response.success != false) {
      otpKey = response.data?.otpKey;
    } else {
      WispaySnackbar.showError(response.message!);
    }
  }

  void _handleIsAuth() async {
    WispayDialog.showLoading();
    BaseResponse<ProfileModel> response = await _authRepository.otpAuthVerification(
      otpCode.value,
      otpKey,
    );
    WispayDialog.closeDialog();
    if (response.data != null) {
      String? accessKey = response.data?.accessKey;
      _otpTypeCallback(accessKey);
    } else {
      WispaySnackbar.showError(response.message!);
    }
  }

  void _handleIsNotAuth() async {
    WispayDialog.showLoading();
    BaseResponse<ProfileModel> response = await _authRepository.otpVerification(
      otpCode.value,
      otpKey,
    );
    WispayDialog.closeDialog();
    if (response.success != false) {
      _bottomsheetChangePin();
    } else {
      WispaySnackbar.showError(response.message!);
    }
  }

  void _bottomsheetChangePin() {
    WispayBottomSheet.show(
      withTitle: false,
      isDismissible: false,
      isScrollControlled: false,
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: Spacing.defaultSpacing * 2),
            Assets.imagesNoPin.image(width: Get.width * 0.7),
            const CustomText(
              title: 'PIN Keamananmu Berhasil Diubah',
              textType: TextType.SemiBold,
              size: FontSize.medium,
              color: WispayColors.cBlack333,
              textAlign: TextAlign.center,
              margin: EdgeInsets.only(bottom: Spacing.small),
            ),
            const CustomText(
              title: 'PIN ini bersifat rahasia.Jangan bagikan PIN keamananmu ke pihak mana pun.',
              textAlign: TextAlign.center,
            ),
            const Divider(thickness: 1, height: 32, indent: 16, endIndent: 16),
            CustomButton(
              title: 'Oke',
              onPress: () {
                Get.offAllNamed(Routes.HOME);
              },
            ),
            const SizedBox(height: Spacing.defaultSpacing)
          ],
        ),
      ],
    );
  }

  void _otpTypeCallback(String? accessKey) {
    switch (otpType) {
      case OTPType.SIGN_IN:
        Get.offNamed(Routes.PIN_INPUT, arguments: {
          'next': (pin) => _next(pin, accessKey!),
          'subtitle': 'Masukkan PIN keamananmu untuk lanjut login',
          'phone': phoneNumber,
        });
        break;
      case OTPType.SIGN_UP:
        Get.offNamed(Routes.REGISTER, arguments: {'accessKey': accessKey});
        break;
      case OTPType.FORGOT_PIN:
        Get.to(() => UpdatePinView(phone: phoneNumber, otpType: otpType));
    }
  }
}
