// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/auth/otp_verification/widgets/countdown.dart';
import 'package:wispay_outlet/app/modules/auth/otp_verification/widgets/otp_field.dart';
import 'package:wispay_outlet/app/modules/auth/otp_verification/widgets/otp_verification_widget.dart';
import 'package:wispay_outlet/generated/locales.g.dart';
import '../controllers/otp_verification_controller.dart';

class OtpVerificationEmailView extends GetView<OtpVerificationController> {
  const OtpVerificationEmailView({Key? key, required this.email}) : super(key: key);

  final String email;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OTPVerificationWidget.getAppBar,
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              OTPVerificationWidget.rowHeader(
                context,
                title: LocaleKeys.auth_otp_verification_title.tr,
                subtitle: 'Silakan masukan kode OTP yang dikirim ke email anda',
                phoneNumber: email,
              ),
              const SizedBox(height: 50),
              OTPField(
                errorController: controller.errorController,
                onCompleted: controller.onComplete,
              ),
              Align(
                alignment: Alignment.center,
                child: Countdown(
                  onResend: controller.onResend,
                ),
              ),
              const Spacer(),
              CustomButton(title: LocaleKeys.buttons_verify_number.tr, onPress: controller.onSubmit)
            ],
          ),
        ),
      ),
    );
  }
}
