// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

// Package imports:
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/number_helper.dart';
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/data/models/auth/otp_model.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class LoginController extends GetxController {
  final AccountRepository _authRepository = AccountRepository(Get.find());

  final _formKey = GlobalKey<FormState>();
  late TextEditingController phoneController;

  var phoneNumber = ''.obs;
  var isSubmitting = false.obs;

  final phoneNumberValidator = MultiValidator([
    RequiredValidator(errorText: 'Phone number is required'),
    IndonesiaPhoneNumber(errorText: 'Phone number is not valid'),
  ]);

  get getFormKey => _formKey;

  @override
  void onInit() {
    super.onInit();
    phoneController = TextEditingController()..addListener(onChangePhoneNumber);
  }

  @override
  void onClose() {
    super.onClose();
    phoneController.removeListener(onChangePhoneNumber);
  }

  void onChangePhoneNumber() {
    phoneNumber.value = phoneController.text;
  }

  void requestOTP() async {
    String phone = NumberHelper.formatPhoneToIndonesian(phoneNumber.value);
    final isValid = _formKey.currentState!.validate();

    if (isValid) {
      _formKey.currentState!.save();
      isSubmitting(true);

      BaseResponse<OTPModel> response = await _authRepository.otpAuth(phone);
      if (response.data != null) {
        isSubmitting(false);
        _formKey.currentState!.reset();
        Get.toNamed(
          Routes.OTP_VERIFICATION,
          arguments: {
            'otp_expired': response.data!.otpExpired,
            'otp_key': response.data!.otpKey,
            'phone_number': phone,
            'otp_type': response.data!.otpType,
          },
        );
      } else {
        _formKey.currentState!.reset();
        isSubmitting(false);
        WispaySnackbar.showError(response.message!);
      }
    }
  }
}
