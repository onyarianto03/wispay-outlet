// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import 'package:wispay_outlet/generated/locales.g.dart';
import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Obx(
            () => Form(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              key: controller.getFormKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 40),
                  Container(
                    alignment: Alignment.topLeft,
                    child: Assets.iconsWelcome.image(width: 78, height: 78),
                  ),
                  const SizedBox(height: 22),
                  CustomText(
                    title: LocaleKeys.auth_welcome.tr,
                    size: FontSize.xLarge,
                    color: WispayColors.cBlack333,
                    textType: TextType.SemiBold,
                  ),
                  const SizedBox(height: 8),
                  CustomText(
                    title: LocaleKeys.auth_welcome_subtitile.tr,
                  ),
                  const SizedBox(height: 50),
                  CustomTextInput(
                    keyboardType: TextInputType.number,
                    value: controller.phoneNumber.value,
                    hintStyle: theme.textTheme.bodyText1?.copyWith(
                      color: WispayColors.cBlack666,
                    ),
                    hintText: LocaleKeys.phone_number.tr,
                    isEnabled: !controller.isSubmitting.value,
                    onClear: controller.phoneController.clear,
                    validator: controller.phoneNumberValidator,
                    textEditingController: controller.phoneController,
                  ),
                  const Spacer(),
                  CustomButton(
                    isLoading: controller.isSubmitting.value,
                    isEnabled: !controller.isSubmitting.value,
                    onPress: controller.requestOTP,
                    title: LocaleKeys.next.tr,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
