// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/modules/auth/pin_input/controllers/update_pin_controller.dart';

// Project imports:
import '../controllers/pin_input_controller.dart';

class PinInputBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PinInputController>(() => PinInputController(), fenix: true);
    Get.lazyPut<UpdatePinController>(() => UpdatePinController(), fenix: true);
  }
}
