// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class PinViewHelper {
  static Row rowHeader(
    BuildContext context, {
    required String title,
    required String subtitle,
  }) {
    return Row(
      children: [
        Assets.iconsAuthPin.image(width: 78, height: 78),
        const SizedBox(width: 16),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                title: title,
                size: FontSize.large,
                color: WispayColors.cBlack333,
                textType: TextType.Bold,
              ),
              const SizedBox(height: 8),
              CustomText(
                title: subtitle,
                size: FontSize.defaultSize,
              ),
            ],
          ),
        ),
      ],
    );
  }

  static AppBar get getAppBar => AppBar(
        iconTheme: const IconThemeData(color: WispayColors.cBlack666),
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          icon: const Icon(
            Icons.chevron_left,
            size: 40,
          ),
          onPressed: () => Get.back(),
        ),
      );
}
