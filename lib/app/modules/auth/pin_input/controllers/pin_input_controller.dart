// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/modules/auth/otp_verification/bindings/otp_verification_binding.dart';
import 'package:wispay_outlet/app/modules/auth/otp_verification/views/otp_verification_email_view.dart';

class PinInputController extends GetxController {
  ValueChanged<String> next = Get.arguments['next'];
  String? subtitle = Get.arguments?['subtitle'];
  String? accessKey = Get.arguments['accessKey'];
  String? phone = Get.arguments?['phone'];
  TextEditingController pinController = TextEditingController();
  final AccountRepository _repo = AccountRepository(Get.find());

  @override
  void onClose() {}

  void onCompleteInputPin(String pin) async {
    next.call(encrypt(pin));
    Future.delayed(const Duration(milliseconds: 500), () {
      pinController.clear();
    });
  }

  void onChangePin(String pin) {}

  void forgotPin() async {
    WispayDialog.fullScreenLoading();
    final _response = await _repo.accountOTP(otpType: OTPType.FORGOT_PIN, phone: phone ?? '');
    if (_response.data != null) {
      WispayDialog.closeDialog();
      final data = _response.data;
      Get.to(
        () => OtpVerificationEmailView(email: decrypt(data?.email)),
        binding: OtpVerificationBinding(),
        arguments: {
          'phone_number': decrypt(data?.phone),
          'otp_key': data!.otpKey!,
          'is_auth': true,
          'otp_type': data.otpType,
          'otp_expired': data.otpExpired,
        },
      );
    }
  }
}
