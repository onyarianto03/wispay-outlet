// Package imports:

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/auth/otp_model.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class UpdatePinController extends GetxController {
  final _authRepository = AccountRepository(Get.find());
  TextEditingController pinController = TextEditingController();
  TextEditingController pinController2 = TextEditingController();
  late PageController pageController = PageController(initialPage: 0);

  final current = 0.obs;
  final title = 'PIN Keamanan Baru'.obs;
  final subtitle = 'Atur PIN keamanan yang baru'.obs;

  @override
  void onInit() {
    ever<int>(current, (val) {
      pageController.jumpToPage(val);
    });
    super.onInit();
  }

  @override
  void onClose() {
    pinController.dispose();
    pinController2.dispose();
    super.onClose();
  }

  void onCompletePin(String pin) {
    pin = pinController.text;
    current.value += 1;
  }

  void onCompletePin2(String pin, String phone, String otpType) async {
    if (pinController.text != pinController2.text) {
      WispaySnackbar.showError('PIN yang anda masukkan tidak sama');
      pinController2.clear();
      return;
    }

    _handleOtpType(otpType, phone);

    pinController2.clear();
  }

  void onBack() {
    if (current.value > 0) {
      current.value = current.value - 1;
      pinController2.clear();
    } else {
      Get.back();
      pinController.clear();
    }
  }

  _handleOtpType(String otpType, String phone) async {
    switch (otpType) {
      case OTPType.UPDATE_PIN:
        BaseResponse<OTPModel> res = await _authRepository.accountOTP(otpType: OTPType.UPDATE_PIN, phone: phone);
        if (res.data != null) {
          pinController2.clear();
          pinController.clear();
          Get.offNamedUntil(
            Routes.OTP_VERIFICATION,
            (routes) => routes.isFirst,
            arguments: {
              'phone_number': phone,
              'is_auth': false,
              'otp_key': res.data!.otpKey,
              'otp_expired': res.data!.otpExpired,
              'otp_type': OTPType.UPDATE_PIN,
            },
          );
        }
        break;
      case OTPType.FORGOT_PIN:
        _bottomsheetChangePin();
        break;
    }
  }

  void _bottomsheetChangePin() {
    WispayBottomSheet.show(
      withTitle: false,
      isDismissible: false,
      isScrollControlled: false,
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: Spacing.defaultSpacing * 2),
            Assets.imagesNoPin.image(width: Get.width * 0.7),
            const CustomText(
              title: 'PIN Keamananmu Berhasil Diubah',
              textType: TextType.SemiBold,
              size: FontSize.medium,
              color: WispayColors.cBlack333,
              textAlign: TextAlign.center,
              margin: EdgeInsets.only(bottom: Spacing.small),
            ),
            const CustomText(
              title: 'PIN ini bersifat rahasia.Jangan bagikan PIN keamananmu ke pihak mana pun.',
              textAlign: TextAlign.center,
            ),
            const Divider(thickness: 1, height: 32, indent: 16, endIndent: 16),
            CustomButton(
              title: 'Oke',
              onPress: () {
                Get.close(4);
              },
            ),
            const SizedBox(height: Spacing.defaultSpacing)
          ],
        ),
      ],
    );
  }
}
