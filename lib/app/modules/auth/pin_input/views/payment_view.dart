// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/pin_input_widget.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/auth/pin_input/controllers/pin_input_controller.dart';

class PaymentView extends GetView<PinInputController> {
  const PaymentView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Pembayaran'),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(Icons.chevron_left, size: 35),
            onPressed: () => Get.back(),
          ),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              children: [
                const CustomText(
                  title: 'Masukkan PIN untuk Membayar',
                  textType: TextType.SemiBold,
                  textAlign: TextAlign.center,
                  margin: EdgeInsets.only(top: Spacing.small),
                  size: FontSize.medium,
                ),
                const SizedBox(height: 68),
                PinInputWidget(
                  onComplete: controller.onCompleteInputPin,
                  onChange: controller.onChangePin,
                  controller: controller.pinController,
                ),
                const SizedBox(height: 24),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const CustomText(
                      title: 'Lupa pin?',
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    GestureDetector(
                      onTap: () => controller.forgotPin(),
                      child: const CustomText(
                        title: 'Reset',
                        color: WispayColors.cPrimary,
                        textType: TextType.SemiBold,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
