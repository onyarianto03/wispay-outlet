// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';

// Project imports:
import 'package:wispay_outlet/app/global_widgets/pin_input_widget.dart';
import 'package:wispay_outlet/app/modules/auth/pin_input/controllers/update_pin_controller.dart';
import 'package:wispay_outlet/app/modules/auth/pin_input/widget/pin_view_helper.dart';

class UpdatePinView extends GetView<UpdatePinController> {
  const UpdatePinView({Key? key, required this.phone, required this.otpType}) : super(key: key);

  final String phone;
  final String otpType;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (controller.current.value > 0) {
          controller.current.value -= 1;
          return false;
        }
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          iconTheme: const IconThemeData(color: WispayColors.cBlack666),
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            icon: const Icon(
              Icons.chevron_left,
              size: 40,
            ),
            onPressed: () => controller.onBack(),
          ),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: PageView(
              controller: controller.pageController,
              physics: const NeverScrollableScrollPhysics(),
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    PinViewHelper.rowHeader(
                      context,
                      title: 'PIN Keamanan Baru',
                      subtitle: 'Atur PIN keamanan yang baru',
                    ),
                    const SizedBox(height: 85),
                    PinInputWidget(
                      onComplete: (pin) {
                        controller.onCompletePin(pin);
                      },
                      onChange: (pin) => {},
                      controller: controller.pinController,
                      autoDisponseController: false,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    PinViewHelper.rowHeader(
                      context,
                      title: 'Konfirmasi PIN Baru',
                      subtitle: 'Masukkan sekali lagi PIN keamanan yang baru',
                    ),
                    const SizedBox(height: 85),
                    PinInputWidget(
                      onComplete: (pin) {
                        controller.onCompletePin2(pin, phone, otpType);
                      },
                      onChange: (pin) => {},
                      controller: controller.pinController2,
                      autoDisponseController: false,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
