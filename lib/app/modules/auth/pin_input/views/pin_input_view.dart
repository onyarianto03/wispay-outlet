// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/global_widgets/pin_input_widget.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/auth/pin_input/widget/pin_view_helper.dart';
import '../controllers/pin_input_controller.dart';

class PinInputView extends GetView<PinInputController> {
  const PinInputView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PinViewHelper.getAppBar,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              PinViewHelper.rowHeader(
                context,
                title: 'PIN',
                subtitle: controller.subtitle ?? 'Masukkan PIN keamananmu untuk lanjut login',
              ),
              const SizedBox(height: 85),
              PinInputWidget(
                onComplete: controller.onCompleteInputPin,
                onChange: controller.onChangePin,
                controller: controller.pinController,
              ),
              const SizedBox(height: 24),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const CustomText(
                    title: 'Lupa pin?',
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  GestureDetector(
                    onTap: () => controller.forgotPin(),
                    child: const CustomText(
                      title: 'Reset',
                      color: WispayColors.cPrimary,
                      textType: TextType.SemiBold,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
