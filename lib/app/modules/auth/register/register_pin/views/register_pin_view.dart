// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/pin_input_widget.dart';
import 'package:wispay_outlet/app/global_widgets/progress_title.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import '../controllers/register_pin_controller.dart';

class RegisterPinView extends GetView<RegisterPinController> {
  const RegisterPinView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(''),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () => Get.back(),
        ),
      ),
      body: Obx(
        () => Container(
          color: WispayColors.cWhite,
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              ProgressTitle(
                percent: 1,
                title: controller.isFilled.value ? 'Konfirmasi PIN' : 'Atur PIN',
                description: controller.isFilled.value ? 'Selesai' : 'Konfirmasi PIN',
                progressText: '3/3',
                descriptionColor: controller.isFilled.value ? WispayColors.cGreen2 : WispayColors.cBlack666,
              ),
              const SizedBox(height: 111),
              PinInputWidget(
                controller: controller.pinController,
                onChange: (val) => {},
                onComplete: controller.onComplete,
                focusNode: controller.focusNode,
              ),
              Visibility(
                child: const CustomText(
                  title: 'PIN tidak sama',
                  color: WispayColors.cRed2,
                  size: FontSize.small,
                  textType: TextType.SemiBold,
                ),
                visible: !controller.isValid.value,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
