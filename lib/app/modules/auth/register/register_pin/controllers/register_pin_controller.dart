// Flutter imports:
import 'package:dio/dio.dart' as dio;
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:http_parser/http_parser.dart';
import 'package:wispay_outlet/app/controllers/image_picker_controller.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/services/storage_service.dart';

import 'package:wispay_outlet/app/data/repositories/account_repository.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/modules/auth/register/controllers/register_controller.dart';

// Project imports:
import 'package:wispay_outlet/app/routes/app_pages.dart';

class RegisterPinController extends GetxController {
  final currentPin = ''.obs;
  final isFilled = false.obs;
  final isValid = true.obs;

  final StorageService _storage = Get.find();
  late TextEditingController pinController = TextEditingController();
  final focusNode = FocusNode();
  final AccountRepository _accountRepository = AccountRepository(Get.find());
  final RegisterController _registerController = Get.find();
  final ImagePickerController _imagePickerController = Get.find();

  @override
  void onClose() {}

  void onChange(String value) {
    pinController.text = value;
  }

  void onComplete(val) async {
    if (!isFilled.value) {
      currentPin.value = val;
      pinController.text = val;
      isFilled.value = true;
      pinController.clear();
      Future.delayed(const Duration(milliseconds: 100), () {
        focusNode.requestFocus();
      });
    } else if (currentPin.value == val) {
      isValid.value = true;
      onSignUp();
    } else {
      isValid.value = false;
    }
  }

  void onSignUp() async {
    WispayDialog.showLoading();

    dio.FormData _formData = dio.FormData.fromMap({
      'sign_up[outlet_name]': _registerController.outletName.value,
      'sign_up[name]': _registerController.fullName.value,
      'sign_up[email]': encrypt(_registerController.email.value),
      'sign_up[reference_code]': _registerController.referralCode.value,
      'sign_up[regency_id]': _registerController.regency.value.id,
      'sign_up[district_id]': _registerController.district.value.id,
      'sign_up[id_card_image]': await dio.MultipartFile.fromFile(
        _imagePickerController.imageFile.value.path,
        contentType: MediaType('image', 'png'),
      ),
      'sign_up[pin]': encrypt(currentPin.value),
      'sign_up[pin_confirmation]': encrypt(currentPin.value),
    });

    final _response = await _accountRepository.signUp(_registerController.accessKey, _formData, _storage.getFCM());
    if (_response.data != null) {
      _storage.saveToken(_response.data);
      WispayDialog.closeDialog();
      Get.offNamedUntil(Routes.HOME, (route) => false);
    } else {
      WispayDialog.closeDialog();
    }

    WispayDialog.closeDialog();
  }
}
