// Flutter imports:
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/progress_title.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/auth/register/widgets/list_district_widget.dart';

import '../controllers/register_controller.dart';
import '../widgets/list_regency_widget.dart';

class RegisterView extends GetView<RegisterController> {
  const RegisterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(''),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.chevron_left, size: 35),
          onPressed: () => Get.back(),
        ),
      ),
      body: Container(
        color: WispayColors.cWhite,
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          child: Obx(
            () => Form(
              key: controller.formKey,
              autovalidateMode: AutovalidateMode.disabled,
              child: Column(
                children: [
                  const ProgressTitle(
                    title: 'Daftar Akun Baru',
                    description: 'Upload KTP',
                    progressText: '1/3',
                    percent: 0.35,
                  ),
                  const SizedBox(height: Spacing.large),
                  CustomTextInput(
                    label: 'Nama Lengkap',
                    value: controller.fullName.value,
                    hintText: 'cth. Agus Mulyadi',
                    hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
                    textInputAction: TextInputAction.next,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    textEditingController: controller.fullNameController,
                    validator: RequiredValidator(errorText: 'Nama Lengkap tidak boleh kosong'),
                  ),
                  const SizedBox(height: Spacing.xMedium),
                  CustomTextInput(
                    label: 'Nama Outlet',
                    value: controller.outletName.value,
                    hintText: 'cth. Agus Link',
                    hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
                    textInputAction: TextInputAction.next,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    textEditingController: controller.outletNameController,
                    validator: RequiredValidator(errorText: 'Nama Lengkap tidak boleh kosong'),
                  ),
                  const SizedBox(height: Spacing.xMedium),
                  CustomTextInput(
                    label: 'Nomor Hp',
                    value: controller.loginController.phoneNumber.value,
                    hintText: controller.loginController.phoneNumber.value,
                    hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    isEnabled: false,
                  ),
                  const SizedBox(height: Spacing.xMedium),
                  CustomTextInput(
                    label: 'Alamat Email',
                    value: controller.email.value,
                    hintText: 'cth. agusmulyadi@gmail.com',
                    hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
                    textInputAction: TextInputAction.next,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    textEditingController: controller.emailController,
                    validator: MultiValidator([
                      RequiredValidator(errorText: 'Email tidak boleh kosong'),
                      EmailValidator(errorText: 'Email tidak valid'),
                    ]),
                  ),
                  const SizedBox(height: Spacing.xMedium),
                  CustomTextInput(
                    label: 'Kode Referensi (tidak wajib)',
                    value: controller.referralCode.value,
                    hintText: 'cth. ABC01234',
                    hintStyle: const TextStyle(color: WispayColors.cBlackBBB),
                    textInputAction: TextInputAction.done,
                    textEditingController: controller.referralCodeController,
                  ),
                  const SizedBox(height: Spacing.xMedium),
                  _dropdown(
                    label: 'Kota/Kabupaten',
                    selectedItem: controller.regency.value,
                    placeholder: 'Pilih Kota/Kabupaten',
                    key: controller.regencyKey,
                  ),
                  const SizedBox(height: Spacing.xMedium),
                  _dropdownDistrict(
                    label: 'Kecamatan',
                    placeholder: 'Pilih Kecamatan',
                    selectedItem: controller.district.value,
                  ),
                  const SizedBox(height: Spacing.xMedium),
                  CustomButton(
                    title: 'Lanjutkan',
                    onPress: () => controller.onSubmit(),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _dropdown({
    String label = '',
    String? placeholder,
    GlobalKey<FormFieldState<BaseModel>>? key,
    required BaseModel selectedItem,
  }) {
    return FormField(
      validator: (BaseModel? value) {
        if (value?.id == null) {
          return 'Silahkan pilih $label';
        }
        return null;
      },
      autovalidateMode: AutovalidateMode.onUserInteraction,
      builder: (FormFieldState<BaseModel> state) {
        return Dropdown(
          errorText: state.errorText,
          topLabel: label,
          label: placeholder,
          value: selectedItem.name ?? "",
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
            WispayBottomSheet.scrollable(
              title: 'Pilih $placeholder',
              withSearch: false,
              itemBuilder: (_, sc) => ListRegencyWidget(
                scrollController: sc,
                onSelect: (value) => state.didChange(value),
              ),
            );
          },
        );
      },
    );
  }

  Widget _dropdownDistrict({
    String label = '',
    String? placeholder,
    required BaseModel selectedItem,
  }) {
    return FormField(
      validator: (BaseModel? value) => value?.id == null ? 'Silahkan pilih $label' : null,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      builder: (state) => Dropdown(
          errorText: state.errorText,
          topLabel: label,
          enabled: controller.regency.value.id != null,
          label: placeholder,
          value: selectedItem.name ?? "",
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
            WispayBottomSheet.scrollable(
              title: 'Pilih $placeholder',
              withSearch: false,
              itemBuilder: (_, sc) => ListDistrictWidget(
                scrollController: sc,
                onSelect: (value) => state.didChange(value),
              ),
            );
          }),
    );
  }
}
