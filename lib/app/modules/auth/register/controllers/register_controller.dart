// Package imports:
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/services/storage_service.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/repositories/utility_repository.dart';
import 'package:wispay_outlet/app/modules/auth/login/controllers/login_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class RegisterController extends GetxController {
  final StorageService _storage = Get.find();
  final LoginController loginController = Get.find();
  final UtilityRepository _utilityRepository = UtilityRepository(Get.find());

  final formKey = GlobalKey<FormState>();
  final regencyKey = GlobalKey<FormFieldState<BaseModel>>();
  final accessKey = Get.arguments['accessKey'];

  final fullNameController = TextEditingController();
  final emailController = TextEditingController();
  final referralCodeController = TextEditingController();
  final outletNameController = TextEditingController();

  final fullName = ''.obs;
  final email = ''.obs;
  final referralCode = ''.obs;
  final outletName = ''.obs;
  final listRegency = <BaseModel>[].obs;
  final listDistrict = <BaseModel>[].obs;
  final regency = const BaseModel().obs;
  final district = const BaseModel().obs;
  final regencyTerm = ''.obs;
  final districtTerm = ''.obs;

  bool _regencyIsLastPage = false;
  bool _districtIsLastPage = false;
  int _regencyPage = 1;
  int _districtPage = 1;

  @override
  void onInit() async {
    super.onInit();
    fullNameController.addListener(_onchangeFullName);
    emailController.addListener(_onchangeEmail);
    referralCodeController.addListener(_onchangeReferralCode);
    outletNameController.addListener(_onchangeOutletName);

    referralCodeController.text = _storage.getReferralCode() ?? '';

    getRegency();

    debounce<String>(regencyTerm, (keyword) {
      getRegency();
    }, time: const Duration(milliseconds: 500));
  }

  @override
  void onClose() {
    super.onClose();
    fullNameController.removeListener(_onchangeFullName);
    emailController.removeListener(_onchangeEmail);
    referralCodeController.removeListener(_onchangeReferralCode);
  }

  void _onchangeFullName() => fullName.value = fullNameController.text;
  void _onchangeEmail() => email.value = emailController.text;
  void _onchangeReferralCode() => referralCode.value = referralCodeController.text;
  void _onchangeOutletName() => outletName.value = outletNameController.text;

  void getRegency() async {
    final _filter = {
      'page': _regencyPage,
      'q': regencyTerm.value,
    };

    final _response = await _utilityRepository.getRegency(filter: FilterModel.fromJson(_filter));

    if (_response.data != null) {
      listRegency.value = _response.data!;
    }
  }

  void getDistrict() async {
    final _response = await _utilityRepository.getDistrictsByRegency(id: regency.value.id!);

    if (_response.data != null) {
      listDistrict.value = _response.data!;
    }
  }

  void loadMoreRegency() async {
    if (_regencyIsLastPage == true) return;
    _regencyPage++;
    final _filter = {
      'page': _regencyPage,
      'limit': 10,
    };

    final _response = await _utilityRepository.getRegency(filter: FilterModel.fromJson(_filter));

    if (_response.data != null) {
      if (_response.data!.isEmpty) {
        _regencyIsLastPage = true;
      } else {
        listRegency.addAll(_response.data!);
      }
    }
  }

  void loadMoreDistricts() async {
    if (_districtIsLastPage == true) return;
    _districtPage++;
    final _filter = {
      'page': _districtPage,
      'limit': 10,
    };

    final _response = await _utilityRepository.getDistrictsByRegency(
      filter: FilterModel.fromJson(_filter),
      id: district.value.id ?? 0,
    );

    if (_response.data != null) {
      if (_response.data!.isEmpty) {
        _districtIsLastPage = true;
      } else {
        listDistrict.addAll(_response.data!);
      }
    }
  }

  Future<void> onSearchRegency(value) async {
    listRegency.clear();
    _regencyPage = 1;
    _regencyIsLastPage = false;
    regencyTerm.value = value;
  }

  Future<void> onSearchDistrict(value) async {
    listDistrict.clear();
    _districtPage = 1;
    _districtIsLastPage = false;
    districtTerm.value = value;
  }

  onSelectRegency(idx) {
    regency.value = listRegency[idx];
    listDistrict.clear();
    district.value = const BaseModel();
    getDistrict();
    Get.back();
  }

  onSelectDistrict(idx) {
    district.value = listDistrict[idx];
    Get.back();
  }

  onSubmit() {
    bool isValid = formKey.currentState!.validate();

    if (isValid) {
      Get.toNamed(Routes.UPLOAD_KTP);
    }
  }
}
