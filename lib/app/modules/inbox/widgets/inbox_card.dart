// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/widget_extensions.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/date_helper.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/inbox/inbox_item.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';

class InboxCard extends StatelessWidget {
  const InboxCard({
    Key? key,
    required this.item,
    this.onTap,
  }) : super(key: key);

  final InboxItem item;
  final void Function(InboxItem)? onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(Spacing.medium),
      decoration: BoxDecoration(
        boxShadow: const [shadow],
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.r),
      ),
      child: InkWell(
        onTap: () => onTap!(item),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            item.image?.thumbnail?.url != null
                ? ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.network(
                      item.image?.thumbnail?.url ?? '',
                      height: 55,
                      width: 70,
                      fit: BoxFit.cover,
                    ),
                  ).marginOnly(right: Spacing.medium)
                : const SizedBox(),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: CustomText(
                          title: item.title ?? '',
                          size: FontSize.defaultSize,
                          textType: TextType.Bold,
                        ),
                      ),
                      const SizedBox(width: Spacing.medium),
                      CustomText(
                        title: DateHelper.formatDate(item.createdAt, format: 'dd MMM yyyy'),
                        textAlign: TextAlign.right,
                        size: FontSize.small,
                        color: WispayColors.cGreyBC,
                      ),
                    ],
                  ),
                  const SizedBox(height: Spacing.small),
                  CustomText(
                    title: item.body ?? '',
                    color: WispayColors.cGrey90,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    minFontSize: FontSize.defaultSize,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
