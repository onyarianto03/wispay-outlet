// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:shimmer/shimmer.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';

class SkeletonWidget extends StatelessWidget {
  const SkeletonWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: WispayColors.cSecondary,
      highlightColor: WispayColors.cSecondary.withOpacity(0.3),
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: Spacing.xMedium, horizontal: Spacing.defaultSpacing),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: WispayColors.cSecondary.withOpacity(0.08),
        ),
        child: Column(
          children: [
            Stack(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: WispayColors.cSecondary.withOpacity(0.08),
                      ),
                    ),
                    const SizedBox(width: Spacing.defaultSpacing),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: Spacing.xSmall),
                        Container(
                          height: 16,
                          width: 87,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: WispayColors.cSecondary.withOpacity(0.08),
                          ),
                        ),
                        const SizedBox(height: Spacing.small),
                        Container(
                          height: 16,
                          width: 216,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: WispayColors.cSecondary.withOpacity(0.08),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    height: 16,
                    width: 30,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: WispayColors.cSecondary.withOpacity(0.08),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
