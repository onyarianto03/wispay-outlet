// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/inbox/inbox_item.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/inbox/widgets/terms_condition.dart';

class InboxDetailView extends GetView {
  const InboxDetailView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final InboxItem item = InboxItem.fromJson(Get.arguments);
    List<String> terms = [
      'Promo berlaku dengan minimal transaksi sejumlah Rp100.000,-',
      'Tiap pengguna Wispay dapat menggunakan promo ini sebanyak 1x',
      'Dapatkan cashback sebesar Rp50.000,- untuk setiap pembelian Kartu Perdana',
      'Maksimal nominal cashback yang bisa didapat sebesar Rp10.000,-'
    ];

    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail Promo'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(Spacing.defaultSpacing),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.network(
                  item.image?.thumbnail?.url ?? '',
                  height: MediaQuery.of(context).size.width * 0.5,
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.cover,
                ),
              ).marginOnly(bottom: Spacing.defaultSpacing),
              const CustomText(
                title: 'Berlaku:',
                textAlign: TextAlign.left,
              ),
              const CustomText(
                title: '1 Jan 2022 - 31 Des 2022',
                textType: TextType.SemiBold,
                color: WispayColors.cPrimary,
                textAlign: TextAlign.left,
                margin: EdgeInsets.only(bottom: Spacing.large),
              ),
              CustomText(
                title: item.title ?? '-',
                textType: TextType.Bold,
                color: WispayColors.cBlack,
                size: FontSize.medium,
                margin: const EdgeInsets.only(bottom: Spacing.small),
              ),
              CustomText(
                title: item.body ?? '-',
                color: WispayColors.cBlack,
                margin: const EdgeInsets.only(bottom: Spacing.large),
              ),
              const CustomText(
                title: 'Syarat dan Ketentuan',
                textType: TextType.Bold,
                color: WispayColors.cBlack,
                size: FontSize.medium,
                margin: EdgeInsets.only(bottom: Spacing.small),
              ),
              Expanded(
                child: TermsCondition(terms: terms),
              ),
              const CustomButton(title: 'Transaksi Sekarang')
            ],
          ),
        ),
      ),
    );
  }
}
