// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/inbox/inbox_item.dart';
import 'package:wispay_outlet/app/global_widgets/empty_widget.dart';
import 'package:wispay_outlet/app/global_widgets/top_tab_bar.dart';
import 'package:wispay_outlet/app/modules/inbox/widgets/inbox_card.dart';
import 'package:wispay_outlet/app/modules/inbox/widgets/skeleton_widget.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import '../controllers/inbox_controller.dart';

class InboxView extends GetWidget<InboxController> {
  const InboxView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pesan Masuk'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Obx(
          () => DefaultTabController(
            length: 2,
            child: Column(
              children: [
                TopTapBar(
                  controller: controller.tabController,
                  tabs: [
                    Tab(text: 'Notifikasi (${controller.notificationUnread.value})'),
                    const Tab(text: 'Promo'),
                  ],
                ),
                Expanded(
                  child: TabBarView(
                    controller: controller.tabController,
                    children: [
                      _buildItem(
                        item: controller.notificationList,
                        type: 'notification',
                        getData: controller.getNotificationList,
                        c: controller.notificationScrollController,
                      ),
                      _buildItem(
                        item: controller.promoList,
                        type: 'promo',
                        getData: controller.getPromoList,
                        c: controller.promoScrollController,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildItem({
    required RxList<dynamic> item,
    required String type,
    required Future<void> Function() getData,
    required ScrollController c,
  }) {
    onTap(InboxItem obj) {
      if (type == 'notification') {
        null;
      } else {
        Get.toNamed(Routes.INBOX_DETAIL, arguments: obj.toJson());
      }
    }

    if (item.isEmpty && !controller.notificationIsLoading.value && !controller.promoIsLoading.value) {
      return EmptyWidget(
        title: 'Belum Ada ${type == 'notification' ? 'Notifikasi' : 'Promo'}',
        subTitle: '',
        image: Assets.iconsEmptyList.path,
        imageWidth: Get.width * 0.3,
      );
    }

    return RefreshIndicator(
      onRefresh: getData,
      child: ListView.separated(
        padding: const EdgeInsets.symmetric(vertical: Spacing.defaultSpacing, horizontal: Spacing.defaultSpacing),
        itemCount: controller.promoIsLoading.value || controller.notificationIsLoading.value ? 5 : item.length,
        separatorBuilder: (context, index) => const SizedBox(height: Spacing.medium),
        controller: c,
        itemBuilder: (ctx, index) {
          if (controller.promoIsLoading.value || controller.notificationIsLoading.value) {
            return const SkeletonWidget();
          }
          return InboxCard(
            item: item[index],
            onTap: (obj) => onTap(obj),
          );
        },
      ),
    );
  }
}
