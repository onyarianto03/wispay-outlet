// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/modules/inbox/controllers/inbox_detail_controller.dart';
import '../controllers/inbox_controller.dart';

class InboxBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<InboxDetailController>(
      () => InboxDetailController(),
    );
    Get.lazyPut<InboxController>(
      () => InboxController(),
    );
  }
}
