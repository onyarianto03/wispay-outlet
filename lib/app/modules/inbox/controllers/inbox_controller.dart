// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/inbox/inbox_item.dart';
import 'package:wispay_outlet/app/data/models/inbox/promo_item.dart';
import 'package:wispay_outlet/app/data/repositories/notification_repository.dart';

class InboxController extends GetxController with SingleGetTickerProviderMixin {
  final _repo = NotificationRepository(Get.find());

  final notificationList = <InboxItem>[].obs;
  final notificationIsLastPage = false.obs;
  final notificationIsLoading = false.obs;
  final promoList = <PromoItem>[].obs;
  final promoIsLastPage = false.obs;
  final promoIsLoading = false.obs;

  final notificationUnread = 0.obs;

  final ScrollController notificationScrollController = ScrollController();
  final ScrollController promoScrollController = ScrollController();

  int _page = 1;
  final int _limit = 10;

  late TabController tabController = TabController(
    length: 2,
    vsync: this,
  );

  final _activeTab = 0.obs;
  final initialLoaded = false.obs;

  @override
  void onInit() {
    super.onInit();

    ever<int>(
      _activeTab,
      (val) async {
        _page = 1;
        notificationIsLastPage.value = false;
        promoIsLastPage.value = false;
        if (val != 0) {
          await getPromoList();
        } else {
          await getNotificationList();
        }
      },
    );

    tabController.addListener(_tabListener);
    tabController.animation?.addListener(_tabAnimationListener);
    notificationScrollController.addListener(_notificationScrollListener);
    promoScrollController.addListener(_promoScrollListener);
  }

  @override
  void onClose() {}

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  Future<void> getNotificationList() async {
    notificationIsLoading(true);
    final result = await _repo.getNotificationList();
    notificationList.assignAll(result.data?.data ?? []);
    final unread = await _repo.unreadNotification();
    notificationUnread.value = unread.data?.data?.unreadCount ?? 0;
    notificationIsLastPage(false);
    notificationIsLoading(false);
  }

  Future<void> getPromoList() async {
    promoIsLoading(true);
    final result = await _repo.getNotificationPromoList();
    promoList.assignAll(result.data?.data ?? []);
    promoIsLastPage(false);
    promoIsLoading(false);
  }

  void _notificationScrollListener() async {
    if (notificationScrollController.position.pixels == notificationScrollController.position.maxScrollExtent) {
      if (notificationIsLastPage.value == true) return;

      _page++;
      var _filter = {
        'page': _page,
        '_limit': _limit,
      };

      final result = await _repo.getNotificationList(filter: FilterModel.fromJson(_filter));
      if (result.success != false) {
        if (result.data!.data!.isEmpty) {
          notificationIsLastPage(true);
        } else {
          notificationList.addAll(result.data!.data!);
        }
      }
    }
  }

  void _promoScrollListener() async {
    if (promoScrollController.position.pixels == promoScrollController.position.maxScrollExtent) {
      if (promoIsLastPage.value == true) return;

      _page++;
      var _filter = {
        'page': _page,
        '_limit': _limit,
      };

      final result = await _repo.getNotificationPromoList(filter: FilterModel.fromJson(_filter));
      if (result.success != false) {
        if (result.data!.data!.isEmpty) {
          promoIsLastPage(true);
        } else {
          promoList.addAll(result.data!.data!);
        }
      }
    }
  }

  void _tabListener() {
    if (tabController.indexIsChanging) {
      _activeTab.value = tabController.index;
    }
  }

  void _tabAnimationListener() {
    if (_activeTab.value != tabController.animation?.value.round()) {
      _activeTab.value = tabController.animation!.value.round();
    }
  }
}
