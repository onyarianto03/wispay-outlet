import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/core/values/utils.dart';
import 'package:wispay_outlet/app/data/models/promo_model/promo_model.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/promo/widgets/terms_condition.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

import '../controllers/promo_controller.dart';

class PromoView extends GetView<PromoController> {
  const PromoView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail ${controller.type}'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.chevron_left, size: 35),
          onPressed: () => Get.back(),
        ),
      ),
      body: Obx(() {
        final promo = controller.data.value;
        if (controller.isLoading.value) {
          return Center(child: Lottie.asset(Assets.lottieLoading, width: 130));
        }

        return Stack(
          fit: StackFit.expand,
          children: [
            ListView(
              padding: const EdgeInsets.all(Spacing.defaultSpacing),
              shrinkWrap: true,
              children: [
                CustomImage(
                  borderRadius: 10,
                  source: promo.image?.url ?? "",
                  width: double.infinity,
                  height: 160,
                  fit: BoxFit.fill,
                ),
                const SizedBox(height: Spacing.defaultSpacing),
                _validTime(promo),
                CustomText(
                  title: promo.name ?? promo.title ?? '-',
                  textType: TextType.Bold,
                ),
                const SizedBox(height: Spacing.defaultSpacing),
                _buildDescription(promo),
                TermsCondition(terms: promo.terms)
              ],
            ),
            _buildButton(promo),
          ],
        );
      }),
    );
  }

  Widget _buildDescription(PromoModel promo) {
    return promo.description != null
        ? HtmlWidget(
            promo.description ?? "-",
            textStyle: const TextStyle(
              color: WispayColors.cBlack333,
              fontSize: FontSize.defaultSize,
            ),
          )
        : const SizedBox();
  }

  Widget _buildButton(PromoModel promo) {
    return promo.productCategory != null
        ? Positioned(
            bottom: 16,
            left: 16,
            right: 16,
            child: CustomButton(
              title: 'Transaksi Sekarang',
              onPress: () => Get.toNamed(
                ProductCategoryHelper.getProductNavigation(promo.productCategory?.code)!,
                arguments: promo.productCategory?.id,
              ),
            ),
          )
        : const SizedBox();
  }

  Widget _validTime(PromoModel promo) {
    return controller.type == bannerType[bannerEnum.ARTICLE]
        ? const SizedBox()
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CustomText(title: 'Berlaku: '),
              CustomText(
                color: WispayColors.cPrimary,
                textType: TextType.SemiBold,
                title: '${DateHelper.formatDate(
                  promo.startDate,
                  format: 'dd MMM',
                )} - ${DateHelper.formatDate(
                  promo.endDate,
                  format: 'dd MMM yyyy',
                )}',
              ),
              const SizedBox(height: Spacing.defaultSpacing)
            ],
          );
  }
}
