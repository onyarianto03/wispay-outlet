// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get_utils/src/extensions/widget_extensions.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';

class TermsCondition extends StatelessWidget {
  const TermsCondition({
    Key? key,
    required this.terms,
  }) : super(key: key);

  final List<String>? terms;

  @override
  Widget build(BuildContext context) {
    return terms == null
        ? const SizedBox()
        : Column(
            children: List.generate(
              terms!.length,
              (i) {
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      title: (i + 1).toString() + '.',
                      color: WispayColors.cBlack,
                      margin: const EdgeInsets.only(right: Spacing.small),
                    ),
                    Flexible(
                      child: CustomText(
                        title: terms![i],
                        color: WispayColors.cBlack,
                      ),
                    )
                  ],
                ).marginOnly(bottom: Spacing.xSmall);
              },
            ),
          );
  }
}
