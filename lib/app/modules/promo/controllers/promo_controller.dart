import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/utils.dart';
import 'package:wispay_outlet/app/data/models/promo_model/promo_model.dart';
import 'package:wispay_outlet/app/data/repositories/article_and_promo_repository.dart';

class PromoController extends GetxController {
  final ArticleAndPromoRepository _repository = ArticleAndPromoRepository(Get.find());

  final id = Get.arguments['id'];
  final type = Get.arguments['type'];

  final data = const PromoModel().obs;

  final isLoading = false.obs;

  @override
  void onInit() {
    super.onInit();

    _getDetail();
  }

  void _getDetail() async {
    isLoading.value = true;
    final _request =
        type == bannerType[bannerEnum.ARTICLE] ? _repository.getDetailArticle(id) : _repository.getDetailPromo(id);

    final _response = await _request;

    if (_response.success == true) {
      data.value = _response.data!;
    }

    isLoading.value = false;
  }
}
