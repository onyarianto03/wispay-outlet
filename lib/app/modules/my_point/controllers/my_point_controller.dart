// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/strings.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/gift_model/gift_model.dart';
import 'package:wispay_outlet/app/data/repositories/gift_repository.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';

class MyPointController extends GetxController with SingleGetTickerProviderMixin {
  final CategoriesController homeController = Get.find();
  final GiftRepository _giftRepository = GiftRepository(Get.find());
  final ScrollController scrollController = ScrollController();

  final giftKind = ['', BALANCE, CASHBACK, ITEM, LOTTERY];
  late TabController tabController = TabController(
    length: 5,
    vsync: this,
  );

  final _activeTab = 0.obs;
  final isLoading = true.obs;
  final listGifts = <GiftModel>[].obs;
  final kind = ''.obs;
  int _page = 1;
  bool _isLastPage = false;

  @override
  void onInit() {
    super.onInit();
    getGifts();

    ever<int>(_activeTab, (val) {
      // listGifts.clear();
      _page = 1;
      isLoading.value = true;

      if (val == 0) {
        kind.value = '';
      } else {
        kind.value = giftKind[val];
      }

      getGifts();
    });

    tabController.addListener(_tabListener);
    scrollController.addListener(_scrollListener);
    tabController.animation?.addListener(_tabAnimationListener);
  }

  @override
  void onClose() {
    tabController.removeListener(_tabListener);
    tabController.dispose();
  }

  void _tabListener() {
    if (!tabController.indexIsChanging) {
      _activeTab.value = tabController.index;
    }
  }

  void _tabAnimationListener() {
    if (_activeTab.value != tabController.animation!.value.round()) {
      _activeTab.value = tabController.animation!.value.round();
    }
  }

  void getGifts() async {
    isLoading.value = true;
    var _filter = {
      'kind': kind.value,
      'page': _page,
    };

    final _response = await _giftRepository.getGifts(filter: FilterModel.fromJson(_filter));

    if (_response.data != null) {
      listGifts.value = _response.data!;
      isLoading.value = false;
    }
  }

  Future<void> onRefresh() async {
    _page = 1;
    _isLastPage = false;
    getGifts();
  }

  void _scrollListener() async {
    if (scrollController.position.pixels == scrollController.position.maxScrollExtent) {
      if (_isLastPage) return;

      _page++;
      final _response = await _giftRepository.getGifts(
        filter: FilterModel.fromJson({
          'kind': kind.value,
          'page': _page,
        }),
      );

      if (_response.data != null) {
        if (_response.data!.isEmpty) {
          _isLastPage = true;
        } else {
          listGifts.addAll(_response.data!);
        }
      }
    }
  }
}
