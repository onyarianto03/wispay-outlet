// Package imports:
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/gift_model/gift_model.dart';
import 'package:wispay_outlet/app/data/repositories/gift_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/modules/my_point/widgets/success_tukar_modal.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class DetailVoucherController extends GetxController {
  final GiftRepository _giftRepository = GiftRepository(Get.find());

  final GiftModel data = Get.arguments['item'];
  final String kind = Get.arguments['kind'];

  final isLoading = false.obs;
  final voucher = const GiftModel().obs;

  final count = 0.obs;
  @override
  void onInit() {
    getDetailVoucher(data.id ?? 0);
    super.onInit();
  }

  void onRedeem() async {
    isLoading.value = true;
    final _response = await _giftRepository.redeemGift(giftId: data.id ?? 0);

    if (_response.success == true) {
      Get.back();
      WispayBottomSheet.show(
        withTitle: false,
        children: [
          SuccessTukarModal(item: data, giftExhangeId: _response.data?.id ?? 0),
          const SizedBox(height: Spacing.defaultSpacing),
        ],
      );
    }
    isLoading.value = false;
  }

  void showSetAddressModal() {
    Get.toNamed(Routes.SET_DELIVERY_ADDRESS, arguments: data.id);
  }

  void getDetailVoucher(int id) async {
    final _response = await _giftRepository.getDetailGift(giftId: id);
    if (_response.data != null) {
      voucher.value = _response.data!;
    }
  }
}
