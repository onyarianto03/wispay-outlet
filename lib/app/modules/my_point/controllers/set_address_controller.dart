import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:uuid/uuid.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/services/location_service.dart';
import 'package:wispay_outlet/app/core/utils/custom_form_validator.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/models/utility/prediction_model.dart';
import 'package:wispay_outlet/app/data/repositories/gift_repository.dart';
import 'package:wispay_outlet/app/data/repositories/google_repository.dart';
import 'package:wispay_outlet/app/data/repositories/utility_repository.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/detail_my_voucher_controller.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/my_point_controller.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/my_voucher_controller.dart';
import 'package:wispay_outlet/app/modules/my_point/widgets/confirm_delivery_address.dart';

final Dio api = Dio();

class SetAddressController extends GetxController {
  Uuid uuid = const Uuid();
  Completer<GoogleMapController> mapController = Completer();
  final CategoriesController homeController = Get.find();
  final LocationService _locationService = LocationService();
  final GoogleRepository _googleRepository = GoogleRepository(api);
  final UtilityRepository _utilityRepository = UtilityRepository(Get.find());
  final GiftRepository _giftRepository = GiftRepository(Get.find());

  final kGoogleApiKey = "AIzaSyBU6DA_zEcj4fLOgQ8uriTa8MDb7JFhysw";
  final int giftExhangeId = Get.arguments['id'];
  final bool isRedeem = Get.arguments['isRedeem'];

  String _sessionToken = '';
  final kCurrentPosition = const LatLng(-6.1753924, 106.8271528).obs;
  final marker = const Marker(markerId: MarkerId('user-position'), position: LatLng(0, 0)).obs;
  final completeAddress = ''.obs;
  final keyword = ''.obs;
  final listPlaces = <PredictionModel>[].obs;
  int _regencyPage = 1;
  bool _regencyIsLastPage = false;

  TextEditingController? addressController;
  TextEditingController? nameController;
  TextEditingController? phoneController;

  final formKey = GlobalKey<FormState>();

  var selectedLocation = const PredictionModel().obs;
  var address = ''.obs;
  var name = ''.obs;
  var phone = ''.obs;
  final regencyTerm = ''.obs;
  final listRegency = <BaseModel>[].obs;
  final selectedRegency = const BaseModel().obs;

  final phoneValidator = MultiValidator([
    RequiredValidator(errorText: 'Nomor HP wajib diisi'),
    IndonesiaPhoneNumber(errorText: 'Nomor HP tidak valid'),
  ]);
  final nameValidator = MultiValidator([RequiredValidator(errorText: 'Nama Penerima wajib diisi')]);
  final addressValidator = MultiValidator([RequiredValidator(errorText: 'Alamat Lengkap wajib diisi')]);

  @override
  void onInit() {
    super.onInit();
    getRegency();

    _sessionToken = uuid.v4();
    marker.value = Marker(
      markerId: const MarkerId('user-position'),
      position: LatLng(
        double.parse(decrypt(homeController.profile.value.outletLatitude)),
        double.parse(
          decrypt(homeController.profile.value.outletLongitude),
        ),
      ),
    );

    ever<LatLng>(kCurrentPosition, (position) {
      marker.value = Marker(markerId: const MarkerId('user-position'), position: position);
    });

    debounce(keyword, (_) {
      _getPlaces();
    }, time: const Duration(milliseconds: 300));

    debounce(
      regencyTerm,
      (_) => searchRegency(),
      time: const Duration(milliseconds: 300),
    );

    addressController = TextEditingController()..addListener(onChangeAddress);
    nameController = TextEditingController()..addListener(onChangeName);
    phoneController = TextEditingController()..addListener(onChangePhone);
  }

  @override
  void onClose() {
    addressController?.removeListener(onChangeAddress);
    nameController?.removeListener(onChangeName);
    phoneController?.removeListener(onChangePhone);
  }

  void onSelectLocation(PredictionModel place) async {
    FocusManager.instance.primaryFocus?.unfocus();
    final _location = await _locationService.getLocationFromAddress(place.description ?? "");
    selectedLocation(place);
    address.value = place.description ?? "";

    Get.back();
    _showConfirm(
      place: place,
      latitude: _location!.latitude,
      longitude: _location.longitude,
    );
  }

  void useCurrentLocation() async {
    Get.back();
    FocusManager.instance.primaryFocus?.unfocus();
    final GoogleMapController _c = await mapController.future;
    final _current = await _locationService.getCurrentLocation();
    final _place = await _locationService.getAddressFromLocation(_current);

    if (_place != null) {
      final String _address =
          '${_place.street}, ${_place.subLocality}, ${_place.locality}, ${_place.subAdministrativeArea}, ${_place.administrativeArea}';
      kCurrentPosition.value = LatLng(_current.latitude, _current.longitude);
      selectedLocation.value = PredictionModel(description: _address);
      await _c.moveCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(_current.latitude, _current.longitude), zoom: 14, tilt: 0),
        ),
      );

      _showConfirm(
        place: PredictionModel.fromJson({'description': _address}),
        latitude: _current.latitude,
        longitude: _current.longitude,
      );
    }
  }

  void _showConfirm({required PredictionModel place, required double latitude, required double longitude}) {
    Future.delayed(const Duration(milliseconds: 200), () {
      WispayBottomSheet.scrollable(
        title: '',
        withSearch: false,
        withTitle: false,
        initialChildSize: 0.7,
        maxChildSize: 0.85,
        itemBuilder: (_, scrollcontroller) => ConfirmDeliveryAddress(scrollController: scrollcontroller),
      );
    }).then((_) async {
      final GoogleMapController _c = await mapController.future;
      kCurrentPosition.value = LatLng(latitude, longitude);
      await _c.moveCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(latitude, longitude), zoom: 14, tilt: 0),
        ),
      );
    });
  }

  void onSetAddress() async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      Get.back();
      final _data = {
        'regency_id': selectedRegency.value.id,
        'recipient_name': encrypt(name.value),
        'recipient_phone': encrypt(NumberHelper.formatPhoneToIndonesian(phone.value)),
        'recipient_address': encrypt(address.value),
      };
      final _response =
          await _giftRepository.deliveryGift(giftId: giftExhangeId, data: RequestBodyModel.fromJson(_data));

      if (_response.data != null) {
        if (!isRedeem) {
          Get.find<DetailMyVoucherController>().getDetailVoucher(giftExhangeId);
          Get.find<MyVoucherController>().getVouchers();
        } else {
          Get.find<MyPointController>().getGifts();
        }
        Get.back();
        WispaySnackbar.showSuccess('Alamat pengiriman berhasil ditambahkan.');
      }
    }
  }

  void onChangeAddress() => address.value = addressController?.text ?? '';
  void onChangeName() => name.value = nameController?.text ?? '';
  void onChangePhone() => phone.value = phoneController?.text ?? '';
  void clearAddress() => address.value = '';
  void clearName() => name.value = '';
  void clearPhone() => phone.value = '';
  void onchangeSearch(value) => keyword.value = value;

  void _getPlaces() async {
    if (keyword.value.isNotEmpty) {
      final _res = await _googleRepository.getPlaces(keyword: keyword.value, sessionToken: _sessionToken);

      if (_res != null) {
        listPlaces.value = _res;
      }
    }
  }

  void onMapCreated(GoogleMapController c) async {
    final _locationService = LocationService();
    mapController.complete(c);
    Position _currentPosition = await _locationService.getCurrentLocation();
    kCurrentPosition.value = LatLng(_currentPosition.latitude, _currentPosition.longitude);
    c.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(_currentPosition.latitude, _currentPosition.longitude),
          zoom: 14,
        ),
      ),
    );
  }

  void getRegency() async {
    final _response = await _utilityRepository.getRegency();

    if (_response.data != null) {
      listRegency.value = _response.data!;
    }
  }

  void loadMoreRegency() async {
    if (_regencyIsLastPage == true) return;
    _regencyPage++;
    final _filter = {
      'page': _regencyPage,
      'limit': 10,
    };

    final _response = await _utilityRepository.getRegency(filter: FilterModel.fromJson(_filter));

    if (_response.data != null) {
      if (_response.data!.isEmpty) {
        _regencyIsLastPage = true;
      } else {
        listRegency.addAll(_response.data!);
      }
    }
  }

  void onSelectRegency(index) {
    Get.back();
    selectedRegency.value = listRegency[index];
  }

  void searchRegency() async {
    final _filter = {
      'page': _regencyPage,
      '_limit': 10,
      'q': regencyTerm.value,
    };

    final response = await _utilityRepository.getRegency(filter: FilterModel.fromJson(_filter));

    if (response.data != null) {
      listRegency.addAll(response.data!);
    }
  }

  void onPickedContact(String? phone) {
    if (phone != null) {
      phoneController?.text = phone;
    }
  }

  Future<void> onSearchRegency(value) async {
    listRegency.clear();
    _regencyPage = 1;
    _regencyIsLastPage = false;
    regencyTerm.value = value;
  }

  clearData() {
    keyword.value = '';
    listPlaces.clear();
  }

  clearRegencyState() {
    regencyTerm.value = '';
    listRegency.clear();
    getRegency();
  }
}
