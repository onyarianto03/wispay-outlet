// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/data/models/gift_model/gift_model.dart';
import 'package:wispay_outlet/app/data/repositories/gift_repository.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class DetailMyVoucherController extends GetxController {
  final GiftRepository _giftRepository = GiftRepository(Get.find());

  final GiftModel data = Get.arguments['item'];
  final String kind = Get.arguments['kind'];

  final voucher = const GiftModel().obs;

  @override
  void onInit() {
    getDetailVoucher(data.id ?? 0);
    super.onInit();
  }

  @override
  void onClose() {}

  void showSetAddressModal() {
    Get.toNamed(Routes.SET_DELIVERY_ADDRESS, arguments: {
      'id': data.id,
      'isRedeem': false,
    });
  }

  void getDetailVoucher(int id) async {
    final _response = await _giftRepository.getDetailExchangedGift(giftId: id);
    if (_response.data != null) {
      voucher.value = _response.data!;
    }
  }
}
