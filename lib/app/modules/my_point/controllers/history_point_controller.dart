// Package imports:
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/point/history_point_model.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';

class HistoryPointController extends GetxController {
  final AccountRepository _accountRepository = AccountRepository(Get.find());
  final ScrollController scrollController = ScrollController();

  int _page = 1;
  bool _isLastPage = false;
  final isLoading = false.obs;
  final listHistoryPoint = <HistoryPointModel>[].obs;

  @override
  void onInit() {
    _getHistoryPoints();

    scrollController.addListener(_scrollListener);
    super.onInit();
  }

  @override
  void onClose() {
    scrollController.removeListener(_scrollListener);
  }

  Future<void> onRefresh() async {
    _page = 1;
    _isLastPage = false;
    listHistoryPoint.clear();
    _getHistoryPoints();
  }

  void _getHistoryPoints() async {
    isLoading.value = true;
    final response = await _accountRepository.getHistoryPoints();

    if (response.data != null) {
      listHistoryPoint.addAll(response.data!);
    }

    isLoading.value = false;
  }

  void _scrollListener() async {
    if (scrollController.position.pixels == scrollController.position.maxScrollExtent) {
      if (_isLastPage) return;

      _page++;
      final _response = await _accountRepository.getHistoryPoints(filter: FilterModel(page: _page));
      if (_response.data != null) {
        if (_response.data!.isEmpty) {
          _isLastPage = true;
        } else {
          listHistoryPoint.addAll(_response.data!);
        }
      }
    }
  }
}
