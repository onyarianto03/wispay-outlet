// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/gift_model/gift_model.dart';
import 'package:wispay_outlet/app/data/repositories/gift_repository.dart';

class MyVoucherController extends GetxController with SingleGetTickerProviderMixin {
  late TabController tabController = TabController(length: 5, vsync: this);
  final GiftRepository _giftRepository = GiftRepository(Get.find());
  final ScrollController scrollController = ScrollController();
  final giftKind = ['', BALANCE, CASHBACK, ITEM, LOTTERY];

  final listVoucher = <GiftModel>[].obs;
  int _page = 1;
  bool _isLastPage = false;
  final kind = ''.obs;
  final isLoading = false.obs;

  final _activeTab = 0.obs;

  @override
  void onInit() {
    super.onInit();
    getVouchers();
    tabController.addListener(_tabListener);
    tabController.animation?.addListener(_tabAnimationListener);
    scrollController.addListener(_scrollListener);

    ever<int>(_activeTab, (val) {
      _page = 1;
      isLoading.value = true;
      listVoucher.clear();
      if (val == 0) {
        kind.value = '';
      } else {
        kind.value = giftKind[val];
      }

      getVouchers();
    });
  }

  @override
  void onClose() {}

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  void getVouchers() async {
    isLoading.value = true;
    var _filter = {
      'kind': kind.value,
      'page': _page,
    };

    final _response = await _giftRepository.getExchangedGift(filter: FilterModel.fromJson(_filter));

    if (_response.data != null) {
      listVoucher.value = _response.data!;
    }
    isLoading.value = false;
  }

  void _scrollListener() async {
    if (scrollController.position.pixels == scrollController.position.maxScrollExtent) {
      if (_isLastPage) return;

      _page++;
      final _response = await _giftRepository.getExchangedGift(
        filter: FilterModel.fromJson({
          'kind': kind.value,
          'page': _page,
        }),
      );

      if (_response.data != null) {
        if (_response.data!.isEmpty) {
          _isLastPage = true;
        } else {
          listVoucher.addAll(_response.data!);
        }
      }
    }
  }

  void _tabListener() {
    if (!tabController.indexIsChanging) {
      _activeTab.value = tabController.index;
    }
  }

  void _tabAnimationListener() {
    if (_activeTab.value != tabController.animation!.value.round()) {
      _activeTab.value = tabController.animation!.value.round();
    }
  }

  Future<void> onRefresh() async {
    _page = 1;
    _isLastPage = false;
    getVouchers();
  }
}
