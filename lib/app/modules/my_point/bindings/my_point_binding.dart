import 'package:get/get.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/detail_my_voucher_controller.dart';

import 'package:wispay_outlet/app/modules/my_point/controllers/detail_voucher_controller.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/history_point_controller.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/my_voucher_controller.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/set_address_controller.dart';

import '../controllers/my_point_controller.dart';

// Package imports:

// Project imports:

class MyPointBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SetAddressController>(
      () => SetAddressController(),
      fenix: true,
    );
    Get.lazyPut<HistoryPointController>(
      () => HistoryPointController(),
    );
    Get.lazyPut<MyVoucherController>(
      () => MyVoucherController(),
    );
    Get.lazyPut<DetailVoucherController>(
      () => DetailVoucherController(),
      fenix: true,
    );
    Get.lazyPut<MyPointController>(
      () => MyPointController(),
    );
    Get.lazyPut<DetailMyVoucherController>(
      () => DetailMyVoucherController(),
      fenix: true,
    );
  }
}
