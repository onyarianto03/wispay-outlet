// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/inbox/widgets/terms_condition.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/detail_my_voucher_controller.dart';

class DetailMyVoucherView extends GetView<DetailMyVoucherController> {
  const DetailMyVoucherView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          controller.kind != ITEM ? 'Detail Voucher' : controller.data.giftPaylod?.name ?? "",
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: [
            ListView(
              padding: const EdgeInsets.all(Spacing.defaultSpacing),
              shrinkWrap: true,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: CustomImage(
                    source: controller.data.giftPaylod?.image?.url ?? "",
                    height: MediaQuery.of(context).size.width * 0.5,
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                  ),
                ).marginOnly(bottom: Spacing.defaultSpacing),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const CustomText(
                            title: 'Poin Ditukarkan:',
                            textAlign: TextAlign.left,
                          ),
                          CustomText(
                            title: NumberHelper.formatMoneyWithoutSymbol(controller.data.point) + ' Poin',
                            textType: TextType.SemiBold,
                            color: WispayColors.cPrimary,
                            textAlign: TextAlign.left,
                            margin: const EdgeInsets.only(bottom: Spacing.large),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const CustomText(
                            title: 'Berlaku:',
                            textAlign: TextAlign.left,
                          ),
                          CustomText(
                            title: DateHelper.formatDate(controller.data.giftPaylod?.startDate, format: 'dd MMM') +
                                " - " +
                                DateHelper.formatDate(controller.data.giftPaylod?.startDate, format: 'dd MMM yyyy'),
                            textType: TextType.SemiBold,
                            color: WispayColors.cPrimary,
                            textAlign: TextAlign.left,
                            margin: const EdgeInsets.only(bottom: Spacing.large),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                CustomText(
                  title: controller.data.giftPaylod?.name ?? '',
                  textType: TextType.Bold,
                  color: WispayColors.cBlack,
                  size: FontSize.medium,
                  margin: const EdgeInsets.only(bottom: Spacing.small),
                ),
                const CustomText(
                  title: 'Dapatkan hadiah saldo Wispay lalu dilanjut sedikit penjalasan di sini..',
                  color: WispayColors.cBlack,
                  margin: EdgeInsets.only(bottom: Spacing.large),
                ),
                const CustomText(
                  title: 'Syarat dan Ketentuan',
                  textType: TextType.Bold,
                  color: WispayColors.cBlack,
                  size: FontSize.medium,
                  margin: EdgeInsets.only(bottom: Spacing.small),
                ),
                TermsCondition(terms: controller.data.gift?.terms ?? [""]),
                const SizedBox(height: Spacing.defaultSpacing * 4),
              ],
            ),
            Positioned(
              bottom: 0,
              right: 0,
              left: 0,
              child: Obx(
                () => Visibility(
                  visible: controller.voucher.value.giftExchangeDelivery?.isClaimed == false && controller.kind == ITEM,
                  child: Container(
                    color: WispayColors.cWhite,
                    padding: const EdgeInsets.all(Spacing.defaultSpacing),
                    child: CustomButton(
                      title: 'Atur alamat Pengiriman',
                      onPress: () {
                        controller.showSetAddressModal();
                      },
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
