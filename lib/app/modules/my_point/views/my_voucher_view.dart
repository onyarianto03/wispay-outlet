// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/gift_helper.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/empty_widget.dart';
import 'package:wispay_outlet/app/global_widgets/rounded_tab_bar.dart';
import 'package:wispay_outlet/app/modules/my_point/views/detail_my_voucher_view.dart';
import 'package:wispay_outlet/app/modules/my_point/widgets/voucher_card.dart';

import '../widgets/skeleton_widget.dart';
import '../controllers/my_voucher_controller.dart';

class MyVoucherView extends GetView<MyVoucherController> {
  const MyVoucherView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Voucher Saya'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.only(top: Spacing.small),
          child: _buildTabBar(),
        ),
      ),
    );
  }

  DefaultTabController _buildTabBar() {
    return DefaultTabController(
      length: controller.giftKind.length,
      child: Column(
        children: [
          _buildButtonTabBar(),
          Expanded(
            child: TabBarView(
              controller: controller.tabController,
              children: _buildTabBarView(),
            ),
          )
        ],
      ),
    );
  }

  Container _buildButtonTabBar() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
      child: RoundedTabBar(
        tabs: controller.giftKind.map((e) => Tab(text: GiftHelper.keyToName(e))).toList(),
        controller: controller.tabController,
      ),
    );
  }

  _buildTabBarView() {
    List<Widget> list = [];
    for (var i = 0; i < controller.giftKind.length; i++) {
      list.add(_buildList());
    }

    return list;
  }

  _buildList() {
    final itemData = controller.listVoucher;

    return RefreshIndicator(
      onRefresh: () => controller.onRefresh(),
      child: Obx(
        () => itemData.isEmpty && controller.isLoading.value == false
            ? const Center(
                child: EmptyWidget(title: 'Tidak ada voucher', subTitle: ''),
              )
            : ListView.separated(
                controller: controller.scrollController,
                padding: const EdgeInsets.symmetric(vertical: Spacing.defaultSpacing),
                itemCount: controller.isLoading.value ? 5 : itemData.length,
                separatorBuilder: (context, index) => const SizedBox(height: Spacing.small),
                itemBuilder: (context, index) {
                  if (controller.isLoading.value) {
                    return const Padding(
                      padding: EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                      child: SkeletonWidget(),
                    );
                  }
                  return VoucherCard(
                    item: itemData[index],
                    onTap: (item) => Get.to(() => const DetailMyVoucherView(), arguments: {
                      'item': item,
                      'kind': item.gift?.kind ?? "",
                    }),
                  );
                },
              ),
      ),
    );
  }
}
