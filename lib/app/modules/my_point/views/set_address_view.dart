import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/set_address_controller.dart';
import 'package:wispay_outlet/app/modules/my_point/widgets/search_location.dart';

class SetAddressView extends GetView<SetAddressController> {
  const SetAddressView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text('Cari Lokasi'),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.chevron_left,
              size: 35,
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: SafeArea(
          child: Stack(
            children: [
              Obx(() => GoogleMap(
                    mapType: MapType.normal,
                    zoomControlsEnabled: false,
                    myLocationButtonEnabled: true,
                    initialCameraPosition: CameraPosition(
                      target: LatLng(
                        double.parse(decrypt(controller.homeController.profile.value.outletLatitude)),
                        double.parse(decrypt(controller.homeController.profile.value.outletLongitude)),
                      ),
                      zoom: 15,
                    ),
                    markers: <Marker>{controller.marker.value},
                    onMapCreated: (c) => controller.onMapCreated(c),
                  )),
              Positioned(
                child: Padding(
                  padding: const EdgeInsets.all(Spacing.defaultSpacing),
                  child: SearchLocation(controller: controller),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
