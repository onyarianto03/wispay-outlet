// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/history_point_controller.dart';
import 'package:wispay_outlet/app/modules/my_point/widgets/history_point_card.dart';
import 'package:wispay_outlet/app/modules/saldo/widget/skeleton_widget.dart';

class HistoryPointView extends GetView<HistoryPointController> {
  const HistoryPointView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Riwayat Poin'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: controller.onRefresh,
          child: Obx(() => ListView.separated(
                controller: controller.scrollController,
                padding: const EdgeInsets.all(Spacing.defaultSpacing),
                itemCount: controller.isLoading.value ? 5 : controller.listHistoryPoint.length,
                separatorBuilder: (context, index) => const SizedBox(height: Spacing.small),
                itemBuilder: (context, index) {
                  if (controller.isLoading.value) {
                    return const Padding(
                      padding: EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                      child: SkeletonWidget(),
                    );
                  }
                  return HistoryPointCard(item: controller.listHistoryPoint[index]);
                },
              )),
        ),
      ),
    );
  }
}
