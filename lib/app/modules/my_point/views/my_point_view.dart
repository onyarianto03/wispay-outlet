// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/gift_helper.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/gift_model/gift_model.dart';
import 'package:wispay_outlet/app/global_widgets/empty_widget.dart';
import 'package:wispay_outlet/app/global_widgets/rounded_tab_bar.dart';
import 'package:wispay_outlet/app/modules/my_point/widgets/my_point_header.dart';
import 'package:wispay_outlet/app/modules/my_point/widgets/point_card.dart';

import 'package:wispay_outlet/app/routes/app_pages.dart';
import '../controllers/my_point_controller.dart';
import '../widgets/skeleton_widget.dart';

class MyPointView extends GetView<MyPointController> {
  const MyPointView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Poin Saya'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              color: WispayColors.cWhite,
              padding: const EdgeInsets.symmetric(
                horizontal: Spacing.defaultSpacing,
                vertical: Spacing.large,
              ),
              child: Obx(
                () => MyPointHeader(
                  poin: controller.homeController.profile.value.outletPoint?.total ?? "0",
                ),
              ),
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.only(top: Spacing.small),
                child: _buildTabBar(),
              ),
            )
          ],
        ),
      ),
    );
  }

  DefaultTabController _buildTabBar() {
    return DefaultTabController(
      length: 4,
      child: Column(
        children: [
          _buildButtonTabBar(),
          Expanded(
            child: TabBarView(
              controller: controller.tabController,
              children: _buildTabBarView(controller),
            ),
          )
        ],
      ),
    );
  }

  Container _buildButtonTabBar() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
      child: RoundedTabBar(
        tabs: controller.giftKind.map((e) => Tab(text: GiftHelper.keyToName(e))).toList(),
        controller: controller.tabController,
      ),
    );
  }

  _buildTabBarView(MyPointController c) {
    List<Widget> list = [];
    for (var i = 0; i < controller.giftKind.length; i++) {
      list.add(_buildList(c));
    }

    return list;
  }

  _buildList(MyPointController c) {
    List<GiftModel> itemData = controller.listGifts;
    return RefreshIndicator(
      onRefresh: () => controller.onRefresh(),
      child: Obx(
        () => itemData.isEmpty && c.isLoading.value == false
            ? const Center(
                child: EmptyWidget(title: 'Tidak ada voucher', subTitle: ''),
              )
            : ListView.separated(
                controller: controller.scrollController,
                padding: const EdgeInsets.symmetric(vertical: Spacing.defaultSpacing),
                itemCount: controller.isLoading.value ? 5 : itemData.length,
                separatorBuilder: (context, index) => const SizedBox(height: Spacing.defaultSpacing),
                itemBuilder: (context, index) {
                  if (controller.isLoading.value == true) {
                    return const Padding(
                      padding: EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
                      child: SkeletonWidget(),
                    );
                  }
                  return PointCard(
                    item: itemData[index],
                    onTap: (item) => Get.toNamed(Routes.DETAIL_VOUCHER, arguments: {
                      'item': item,
                      'kind': item.kind,
                    }),
                  );
                },
              ),
      ),
    );
  }
}
