// Flutter imports:
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/gift_model/gift_model.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/detail_voucher_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class TukarPoinModal extends GetView<DetailVoucherController> {
  const TukarPoinModal({
    Key? key,
    required this.item,
  }) : super(key: key);

  final GiftModel item;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Assets.imagesTukarGift.image(
          height: MediaQuery.of(context).size.width * 0.6,
          fit: BoxFit.cover,
        ),
        const CustomText(
          title: 'Tukarkan Poin?',
          textType: TextType.Bold,
          size: FontSize.medium,
          margin: EdgeInsets.only(bottom: Spacing.small),
        ),
        AutoSizeText.rich(
          wording(),
          style: const TextStyle(fontSize: 14, color: WispayColors.cBlack666),
          textAlign: TextAlign.center,
        ),
        const Divider(thickness: 1, height: 32),
        Row(
          children: [
            Expanded(
              child: WispayOutlinedButton(
                title: 'Batal',
                onPress: () => Get.back(),
              ),
            ),
            const SizedBox(width: Spacing.defaultSpacing),
            Expanded(
              child: Obx(() => CustomButton(
                    isLoading: controller.isLoading.value,
                    isEnabled: !controller.isLoading.value,
                    title: 'Tukar',
                    onPress: () => controller.onRedeem(),
                  )),
            )
          ],
        )
      ],
    );
  }

  TextSpan wording() {
    return TextSpan(
      text: 'Apakah Anda yakin ingin menukar ',
      children: [
        TextSpan(
          text: NumberHelper.formatMoneyWithoutSymbol(item.point),
          style: const TextStyle(fontWeight: FontWeight.w600),
        ),
        const TextSpan(text: ' poin dengan '),
        TextSpan(
          text: '${item.name} ?',
          style: const TextStyle(fontWeight: FontWeight.w600),
        ),
      ],
    );
  }
}
