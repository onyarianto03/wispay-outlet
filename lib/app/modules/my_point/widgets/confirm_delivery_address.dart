import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/set_address_controller.dart';
import 'package:wispay_outlet/app/modules/my_point/widgets/list_regency_widget.dart';

class ConfirmDeliveryAddress extends GetWidget<SetAddressController> {
  const ConfirmDeliveryAddress({Key? key, required this.scrollController}) : super(key: key);

  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    return ListView(
      controller: scrollController,
      children: [
        Obx(
          () => Form(
            autovalidateMode: AutovalidateMode.disabled,
            key: controller.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  title: decrypt(controller.homeController.profile.value.outletName),
                  textType: TextType.SemiBold,
                  size: FontSize.medium,
                  color: WispayColors.cBlack,
                  margin: EdgeInsets.zero,
                ),
                const SizedBox(height: Spacing.xSmall),
                CustomText(
                  title: controller.selectedLocation.value.description ?? "",
                  color: WispayColors.cBlack,
                ),
                const SizedBox(height: Spacing.xMedium),
                CustomTextInput(
                  label: 'Alamat Lengkap',
                  hintText: ' ',
                  value: controller.address.value,
                  keyboardType: TextInputType.multiline,
                  validator: controller.addressValidator,
                  textEditingController: controller.addressController,
                  margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
                  maxLines: 3,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                ),
                _dropdown(),
                const SizedBox(height: Spacing.defaultSpacing),
                CustomTextInput(
                  label: 'Nama Penerima',
                  hintText: ' ',
                  value: controller.name.value,
                  keyboardType: TextInputType.name,
                  validator: controller.nameValidator,
                  textEditingController: controller.nameController,
                  onClear: controller.nameController?.clear,
                  margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                ),
                CustomTextInput(
                  label: 'Nomor HP',
                  hintText: ' ',
                  value: controller.phone.value,
                  isGSM: false,
                  showContact: true,
                  onContactPicked: (value) => controller.onPickedContact(value),
                  validator: controller.phoneValidator,
                  textEditingController: controller.phoneController,
                  onClear: controller.phoneController?.clear,
                  margin: const EdgeInsets.only(bottom: Spacing.large),
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                ),
                CustomButton(
                  title: 'Atur Alamat Pengiriman',
                  onPress: controller.onSetAddress,
                ),
                const SizedBox(height: Spacing.defaultSpacing),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _dropdown() {
    return Obx(
      () => Dropdown(
        value: controller.selectedRegency.value.name ?? "",
        label: "Nama Kotamu",
        topLabel: "Kota/Kabupaten",
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
          WispayBottomSheet.scrollable(
            maxChildSize: 0.9,
            onclose: () => controller.clearRegencyState(),
            withSearch: false,
            title: 'Pilih kota/kabupaten',
            itemBuilder: (context, scrollController) => ListRegencyWidget(scrollController: scrollController),
          );
        },
      ),
    );
  }
}
