// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';

// Project imports:
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class MyPointHeader extends StatelessWidget {
  const MyPointHeader({
    Key? key,
    required this.poin,
  }) : super(key: key);

  final String poin;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      decoration: BoxDecoration(
        boxShadow: const [shadow],
        color: Colors.white,
        image: DecorationImage(
          image: AssetImage(Assets.iconsMiscWave1.path),
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.circular(10.r),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const CustomText(title: 'Jumlah Poin', size: FontSize.small),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () => Get.toNamed(Routes.HISTORY_POINT),
                child: Row(
                  children: [
                    CustomText(
                      title: NumberHelper.formatMoneyWithoutSymbol(poin) + ' Poin',
                      margin: const EdgeInsets.only(right: Spacing.small),
                      size: FontSize.medium,
                      color: WispayColors.cPrimary,
                      textType: TextType.SemiBold,
                    ),
                    const Icon(Icons.chevron_right),
                  ],
                ),
              ),
              const Spacer(flex: 1),
              Expanded(
                flex: 2,
                child: CustomButton(
                  title: 'Voucher Saya',
                  onPress: () => Get.toNamed(Routes.MY_VOUCHER),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
