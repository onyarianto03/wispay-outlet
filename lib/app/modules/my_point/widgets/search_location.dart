import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/set_address_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class SearchLocation extends GetView<SetAddressController> {
  const SearchLocation({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final SetAddressController controller;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: const CustomTextInput(
        fillColor: WispayColors.cWhite,
        filled: true,
        isEnabled: false,
        prefixIcon: Icon(Icons.search, size: 24),
        hintText: 'Cari Lokasi...',
        value: '',
      ),
      onTap: () => WispayBottomSheet.scrollable(
        onclose: () => controller.clearData(),
        initialChildSize: 0.9,
        withTitle: false,
        title: 'Lokasi',
        onSearch: (value) => controller.onchangeSearch(value),
        itemBuilder: (_, __) => Column(
          children: [
            const SizedBox(height: Spacing.defaultSpacing),
            GestureDetector(
              onTap: () => controller.useCurrentLocation(),
              child: BaseCard(
                decorationImage: Assets.iconsMiscWave1,
                margin: EdgeInsets.zero,
                spreadRadius: 0,
                blurRadius: 20,
                padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing, vertical: Spacing.small),
                child: Row(
                  children: const [
                    Icon(Icons.my_location, color: WispayColors.cPrimary),
                    SizedBox(width: Spacing.small),
                    CustomText(
                      title: 'Gunakan Lokasi Saat Ini',
                      color: WispayColors.cPrimary,
                      textType: TextType.SemiBold,
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Obx(
                () => ListView.separated(
                  shrinkWrap: true,
                  separatorBuilder: (context, index) => const Divider(thickness: 1),
                  itemBuilder: (ctx, idx) {
                    return InkWell(
                      onTap: () => controller.onSelectLocation(controller.listPlaces[idx]),
                      child: CustomText(title: controller.listPlaces[idx].description ?? ""),
                    );
                  },
                  itemCount: controller.listPlaces.length,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
