// Flutter imports:
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';

// Project imports:

import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/gift_model/gift_model.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class SuccessTukarModal extends StatelessWidget {
  const SuccessTukarModal({
    Key? key,
    required this.item,
    required this.giftExhangeId,
  }) : super(key: key);

  final GiftModel item;
  final int giftExhangeId;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Assets.imagesTukarBerhasil.image(
          height: MediaQuery.of(context).size.width * 0.6,
          fit: BoxFit.contain,
        ),
        const CustomText(
          title: 'Penukaran Berhasil',
          textType: TextType.Bold,
          size: FontSize.medium,
          margin: EdgeInsets.only(bottom: Spacing.small),
        ),
        AutoSizeText.rich(
          successWording(),
          style: const TextStyle(fontSize: 14, color: WispayColors.cBlack666),
          textAlign: TextAlign.center,
        ),
        const Divider(thickness: 1),
        CustomButton(
          title: item.kind != ITEM ? 'Lihat Voucher' : 'Atur Alamat Pengiriman',
          onPress: () {
            Get.back();
            if (item.kind != ITEM) {
              Get.toNamed(Routes.MY_VOUCHER);
            } else {
              Get.toNamed(Routes.SET_DELIVERY_ADDRESS, arguments: {
                'id': giftExhangeId,
                'isRedeem': true,
              });
            }
          },
        ),
      ],
    );
  }

  TextSpan successWording() {
    if (item.kind != ITEM) {
      return TextSpan(
        text: 'Kamu berhasil menukarkan ',
        children: [
          TextSpan(
            text: NumberHelper.formatMoneyWithoutSymbol(item.point),
            style: const TextStyle(fontWeight: FontWeight.w600),
          ),
          const TextSpan(text: ' poinmu dengan '),
          TextSpan(
            text: '${item.name ?? item.gift?.name}.',
            style: const TextStyle(fontWeight: FontWeight.w600),
          ),
        ],
      );
    }
    return TextSpan(
      text: 'Yeay! ',
      children: [
        TextSpan(
          text: 'Hadiah ${item.name} ',
          style: const TextStyle(fontWeight: FontWeight.w600),
        ),
        const TextSpan(text: 'akan dikirim ke alamatmu sekitar 14 hari lagi. Tunggu ya.'),
      ],
    );
  }
}
