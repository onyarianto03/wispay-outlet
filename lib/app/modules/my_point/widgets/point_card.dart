// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';

// Project imports:
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/gift_model/gift_model.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';

class PointCard extends StatelessWidget {
  const PointCard({
    Key? key,
    required this.item,
    this.onTap,
  }) : super(key: key);

  final GiftModel item;
  final void Function(GiftModel)? onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
      decoration: BoxDecoration(
        boxShadow: const [shadow],
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.r),
      ),
      child: InkWell(
        onTap: () => onTap!(item),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              alignment: Alignment.bottomLeft,
              padding: const EdgeInsets.all(Spacing.defaultSpacing),
              height: MediaQuery.of(context).size.width * 0.4,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8.r),
                  topRight: Radius.circular(8.r),
                ),
                image: DecorationImage(
                  image: NetworkImage(item.image?.url ?? ''),
                  colorFilter: ColorFilter.mode(WispayColors.cBlack.withOpacity(0.5), BlendMode.darken),
                  fit: BoxFit.cover,
                ),
              ),
              child: CustomText(
                title: item.name ?? '',
                size: FontSize.medium,
                textType: TextType.SemiBold,
                color: WispayColors.cWhite,
              ),
            ),
            Container(
              padding: const EdgeInsets.all(Spacing.defaultSpacing),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8.r),
              ),
              child: CustomText(
                title: NumberHelper.formatMoneyWithoutSymbol(item.point) + ' Poin',
                textType: TextType.SemiBold,
                color: WispayColors.cBlack,
              ),
            )
          ],
        ),
      ),
    );
  }
}
