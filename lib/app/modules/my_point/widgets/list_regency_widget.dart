import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/my_point/controllers/set_address_controller.dart';

class ListRegencyWidget extends StatelessWidget {
  final SetAddressController _controller = Get.find<SetAddressController>();
  final ScrollController scrollController;
  ListRegencyWidget({Key? key, required this.scrollController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 40,
          child: TextField(
            onChanged: (terms) => _controller.onSearchRegency(terms),
            style: const TextStyle(fontSize: FontSize.medium),
            decoration: InputDecoration(
              prefixIcon: const Icon(Icons.search),
              isDense: true,
              hintText: 'Cari kota/kabupaten',
              hintStyle: const TextStyle(
                color: WispayColors.cGrey99,
                fontSize: FontSize.small,
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
              ),
            ),
          ),
        ),
        const SizedBox(height: Spacing.small),
        Expanded(
          child: NotificationListener(
            onNotification: (notification) {
              if (notification is ScrollEndNotification) {
                if (notification.metrics.pixels == notification.metrics.maxScrollExtent) {
                  _controller.loadMoreRegency();
                  return true;
                }
              }
              return true;
            },
            child: Obx(
              () => ListView.separated(
                shrinkWrap: true,
                controller: scrollController,
                itemCount: _controller.listRegency.length,
                physics: const BouncingScrollPhysics(),
                separatorBuilder: (context, index) => const Divider(thickness: 1),
                itemBuilder: (context, index) => ListTile(
                  dense: true,
                  contentPadding: EdgeInsets.zero,
                  onTap: () => _controller.onSelectRegency(index),
                  visualDensity: const VisualDensity(vertical: -2),
                  trailing: Visibility(
                    child: const Icon(
                      Icons.check_circle,
                      color: WispayColors.cGreen2,
                      size: 20,
                    ),
                    visible: _controller.listRegency[index].id == _controller.selectedRegency.value.id,
                  ),
                  title: CustomText(
                    title: _controller.listRegency[index].name ?? "",
                    color: WispayColors.cBlack333,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
