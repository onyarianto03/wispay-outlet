// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/point/history_point_model.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';

class HistoryPointCard extends StatelessWidget {
  const HistoryPointCard({
    Key? key,
    required this.item,
  }) : super(key: key);

  final HistoryPointModel item;

  @override
  Widget build(BuildContext context) {
    bool isIn = item.inOut == 'IN';
    String plusMin = isIn ? '+' : '-';

    return BaseCard(
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      margin: EdgeInsets.zero,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 9,
                child: CustomText(
                  minFontSize: FontSize.small,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  size: FontSize.defaultSize,
                  title: item.description ?? '-',
                  color: WispayColors.cBlack333,
                ),
              ),
              const Spacer(),
              CustomText(
                title: DateHelper.formatDate(item.createdAt, format: 'dd MMM'),
                color: WispayColors.cGreyBC,
                size: FontSize.small,
              ),
            ],
          ),
          SizedBox(
            height: Spacing.small.w,
          ),
          CustomText(
            title: plusMin + NumberHelper.formatMoneyWithoutSymbol(item.point!) + ' Poin',
            color: isIn ? WispayColors.cGreen : WispayColors.cRed,
            textType: TextType.SemiBold,
            textAlign: TextAlign.left,
          ),
        ],
      ),
    );
  }
}
