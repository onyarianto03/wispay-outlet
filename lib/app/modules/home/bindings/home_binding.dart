// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/modules/account/controllers/account_controller.dart';

// Project imports:
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/inbox/controllers/inbox_controller.dart';
import 'package:wispay_outlet/app/modules/saldo/controllers/detail_topup_request_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/controllers/transaction_controller.dart';
import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<TransactionController>(() => TransactionController());
    Get.lazyPut<InboxController>(() => InboxController());
    Get.lazyPut<AccountController>(() => AccountController(), fenix: true);
    Get.lazyPut<DetailTopupRequestController>(() => DetailTopupRequestController(), fenix: true);
    Get.lazyPut<CategoriesController>(() => CategoriesController(), fenix: true);
  }
}
