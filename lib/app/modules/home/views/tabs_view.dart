// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import 'package:wispay_outlet/generated/locales.g.dart';
import '../controllers/home_controller.dart';

class TabView extends GetView<HomeController> {
  const TabView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Obx(
          () => IndexedStack(
            index: controller.tabIndex.value,
            children: controller.listScreen,
          ),
        ),
      ),
      bottomNavigationBar: Obx(
        () => BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: controller.tabIndex.value,
          onTap: controller.setTabIndex,
          selectedFontSize: FontSize.small,
          unselectedFontSize: FontSize.small,
          items: [
            BottomNavigationBarItem(
              icon: _tabIcon(Assets.iconsTabsHome.path),
              activeIcon: _tabIcon(Assets.iconsTabsHomeActive.path),
              label: LocaleKeys.tabs_home.tr,
            ),
            BottomNavigationBarItem(
              icon: _tabIcon(Assets.iconsTabsTransaction.path),
              activeIcon: _tabIcon(Assets.iconsTabsTransactionActive.path),
              label: LocaleKeys.tabs_transaction.tr,
            ),
            BottomNavigationBarItem(
              icon: _tabIcon(Assets.iconsTabsInbox.path),
              activeIcon: _tabIcon(Assets.iconsTabsInboxActive.path),
              label: LocaleKeys.tabs_inbox.tr,
            ),
            BottomNavigationBarItem(
              icon: _tabIcon(Assets.iconsTabsProfile.path),
              activeIcon: _tabIcon(Assets.iconsTabsProfileActive.path),
              label: LocaleKeys.tabs_my_account.tr,
            ),
          ],
        ),
      ),
    );
  }

  Image _tabIcon(String path) {
    return Image.asset(
      path,
      height: 24,
      width: 24,
    );
  }
}
