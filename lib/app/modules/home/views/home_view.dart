// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/banner/banner_item/banner_item_model.dart';
import 'package:wispay_outlet/app/data/models/profile/profile_model.dart';
import 'package:wispay_outlet/app/global_widgets/product_category_item.dart';
import 'package:wispay_outlet/app/global_widgets/shimmer/shimmer_widget.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/home/widgets/button_wismart.dart';
import 'package:wispay_outlet/app/modules/home/widgets/payment_slider.dart';
import 'package:wispay_outlet/app/modules/home/widgets/promo_slider_widget.dart';
import '../controllers/home_controller.dart';
import '../widgets/card_wallet.dart';
import '../widgets/header_content.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;
    ever(controller.categoriesController.profile, (ProfileModel value) => controller.showModalSetAddress(value));

    var boxDecoration = BoxDecoration(
      gradient: LinearGradient(
        stops: const [0.05, 0.3],
        tileMode: TileMode.decal,
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [
          WispayColors.cPrimary.withAlpha(40),
          WispayColors.cPrimary.withAlpha(1),
        ],
      ),
    );

    return Scaffold(
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: () => controller.onRefresh(),
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: Spacing.defaultSpacing.w),
              width: MediaQuery.of(context).size.width,
              decoration: boxDecoration,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const HeaderContent().paddingSymmetric(horizontal: Spacing.defaultSpacing.w),
                  SizedBox(height: Spacing.large.h),
                  const CardWallet().paddingSymmetric(horizontal: Spacing.defaultSpacing.w),
                  SizedBox(height: Spacing.defaultSpacing.h),
                  Obx(() => Visibility(child: const PaymentSlider(), visible: controller.topupRequests.isNotEmpty)),
                  Obx(
                    () => Visibility(
                      child: SizedBox(height: Spacing.medium.h),
                      visible: controller.topupRequests.isNotEmpty,
                    ),
                  ),
                  Obx(() => controller.isWismartCampaign.value ? const ButtonWismart() : const SizedBox()),
                  Obx(() => SizedBox(height: controller.isWismartCampaign.value ? Spacing.small.h : 0)),
                  _titleSection(textTheme, title: 'Produk'),
                  SizedBox(height: Spacing.small.h),
                  _buildCategoryMenu(),
                  SizedBox(height: Spacing.large.h),
                  _titleSection(textTheme, title: 'Promo Terbaru'),
                  SizedBox(height: Spacing.small.h),
                  _buildBanner(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBanner() {
    return Obx(() {
      bool? isLoading = controller.isLoading.value;
      List<BannerItemModel>? _items = controller.homeBanner;
      if (isLoading) {
        return SizedBox(
          height: 140.h,
          child: ShimmerWidget.circle(
            height: 140.h,
            width: double.infinity,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ),
          ),
        );
      }
      return SizedBox(
        height: 140.h,
        child: PageView.builder(
          padEnds: true,
          physics: const BouncingScrollPhysics(),
          itemCount: _items.length,
          controller: PageController(viewportFraction: 0.9, keepPage: true),
          itemBuilder: (context, index) => PromoSliderWidget(
            image: _items[index].image!.url!,
            id: _items[index].id!,
            type: _items[index].type!,
          ),
        ),
      );
    });
  }

  Widget _buildCategoryMenu() {
    return Obx(
      () {
        bool isLoading = controller.categoriesController.isLoading.value;
        final data = controller.categoriesController.homeCategories;
        return StaggeredGridView.countBuilder(
          padding: EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing.w),
          shrinkWrap: true,
          crossAxisCount: 4,
          crossAxisSpacing: Spacing.defaultSpacing.h,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: isLoading ? 8 : data.length,
          mainAxisSpacing: Spacing.xSmall.w,
          staggeredTileBuilder: (index) => const StaggeredTile.fit(1),
          itemBuilder: (context, index) {
            return ProductCategoryItem(
              image: isLoading ? '' : data[index].imagePath ?? '',
              title: isLoading ? '' : data[index].name ?? '',
              isLoading: isLoading,
              onTap: isLoading
                  ? null
                  : () => Get.toNamed(
                        data[index].routeName!,
                        arguments: data[index].code == ProductCode.PHONE_CREDIT
                            ? [data[index].id, controller.categoriesController.dataPlanId]
                            : data[index].id,
                      ),
            );
          },
        );
      },
    );
  }

  Padding _titleSection(TextTheme textTheme, {required String title}) => Padding(
        padding: EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing.w),
        child: CustomText(
          title: title,
          size: FontSize.large,
          textType: TextType.Bold,
          color: WispayColors.cBlack333,
        ),
      );
}
