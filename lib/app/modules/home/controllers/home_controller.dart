// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/services/storage_service.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/banner/banner_item/banner_item_model.dart';
import 'package:wispay_outlet/app/data/models/profile/profile_model.dart';
import 'package:wispay_outlet/app/data/models/topup/topup_model.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';
import 'package:wispay_outlet/app/data/repositories/home_repository.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/app/modules/account/views/account_view.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/modules/home/views/home_view.dart';
import 'package:wispay_outlet/app/modules/inbox/controllers/inbox_controller.dart';
import 'package:wispay_outlet/app/modules/inbox/views/inbox_view.dart';
import 'package:wispay_outlet/app/modules/saldo/controllers/detail_topup_request_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/controllers/transaction_controller.dart';
import 'package:wispay_outlet/app/modules/transaction/views/transaction_view.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class HomeController extends GetxController {
  StorageService storageService = Get.find<StorageService>();
  final PageController slideController = PageController(keepPage: true);

  CategoriesController categoriesController = Get.find();
  TransactionController transactionController = Get.find();
  InboxController inboxController = Get.find();

  final AccountRepository _accountRepository = AccountRepository(Get.find());
  final HomeRepository _homeRepository = HomeRepository(Get.find());
  final DetailTopupRequestController _reqTopupController = Get.find<DetailTopupRequestController>();
  final TransactionRepository _trxRepo = TransactionRepository(Get.find());

  final isLoading = true.obs;
  final isError = false.obs;
  final isSuccess = false.obs;
  final homeBanner = <BannerItemModel>[].obs;
  final topupRequests = <TopupModel>[].obs;
  final isWismartCampaign = false.obs;
  final isModalShown = false.obs;

  List<Widget> listScreen = <Widget>[const HomeView(), TransactionView(), const InboxView(), const AccountView()];

  Rx<int> tabIndex = 0.obs;

  final sliderIdx = 0.obs;

  @override
  void onInit() async {
    getTopupRequests();
    await getInitialData();

    ever<int>(tabIndex, (val) {
      if (val == 1 && transactionController.initialLoaded.value == false) {
        transactionController.getTransactionList();
        transactionController.initialLoaded.value = true;
      } else if (val == 2 && inboxController.initialLoaded.value == false) {
        inboxController.getNotificationList();
        inboxController.initialLoaded.value = true;
      } else if (val == 3) {
        categoriesController.getProfile();
      }
    });

    super.onInit();
  }

  @override
  void onClose() {}

  void setTabIndex(int index) {
    tabIndex.value = index;
  }

  void getTopupRequests() async {
    final _res = await _homeRepository.getHomeSection('topup_requests_and_remittances');
    if (_res.success != false) {
      final _t = List<TopupModel>.from(_res.data.map((e) => TopupModel.fromJson(e)));
      topupRequests.assignAll(_t);
    }
  }

  void showModalSetAddress(ProfileModel profile) {
    if ((profile.outletLatitudeNotSet! || profile.outletLongitudeNotSet!) && !isModalShown.value) {
      isModalShown.value = true;
      WispayBottomSheet.show(
        children: [
          Column(
            children: [
              const SizedBox(height: Spacing.defaultSpacing),
              Image.asset(
                Assets.imagesForceSetAddress.path,
                height: 100,
              ),
              const SizedBox(height: Spacing.defaultSpacing),
              const CustomText(
                title: 'Isi Alamat Outlet',
                size: FontSize.large,
                textAlign: TextAlign.center,
                textType: TextType.SemiBold,
                color: WispayColors.cBlack,
              ),
              const SizedBox(height: Spacing.small),
              const CustomText(
                title: 'Yuk, isi alamat sekarang supaya outletmu mudah ditemukan oleh pembeli.',
                textAlign: TextAlign.center,
                textType: TextType.Medium,
              ),
              const SizedBox(height: Spacing.defaultSpacing),
              CustomButton(
                title: 'Atur Alamat',
                onPress: () {
                  final _args = {
                    'forceSetAddress': true,
                  };
                  Get.back();
                  isModalShown.value = false;
                  Future.delayed(const Duration(milliseconds: 300), () {
                    Get.toNamed(Routes.DATA_OUTLET, arguments: _args);
                  });
                },
              ),
              const SizedBox(height: Spacing.large),
            ],
          ),
        ],
        isDismissible: false,
        withTitle: false,
        isScrollControlled: false,
        enableDrag: false,
        enableBackButton: false,
      );
    }
  }

  Future<void> getInitialData() async {
    isLoading(true);
    var _responses = await Future.wait([
      _accountRepository.getProfile(),
      _homeRepository.getHomeSection('promos_and_articles'),
      _homeRepository.getHomeData(),
    ]);

    if (_responses.every((element) => element.success != false)) {
      final _bannerData = List<BannerItemModel>.from(_responses[1].data.map((e) => BannerItemModel.fromJson(e)));
      homeBanner.assignAll(_bannerData);
      isWismartCampaign.value = _responses[2].data['summary']['is_wismart_campaign'];

      isLoading(false);
    } else {
      isLoading(false);
      isError(true);
    }
  }

  Future<void> gotoDetilTopup(int index, String type) async {
    if (type == 'Transaction') {
      final _res = await _trxRepo.getDetail(id: topupRequests[index].id);
      if (_res.success == true) {
        Get.back();
        Get.toNamed(Routes.SEND_MONEY_PAYMENT_INSTRUCTIONS, arguments: _res.data);
      } else {
        Get.close(2);
      }
    } else {
      WispayDialog.fullScreenLoading();
      final _r = await _reqTopupController.getDetail(topupRequests[index].id!);
      if (_r) {
        Get.back();
        Get.toNamed(Routes.HOW_TO_PAY_TOPUP);
      } else {
        Get.close(2);
      }
    }
  }

  Future<void> onRefresh() async {
    await getInitialData();
    getTopupRequests();
  }
}
