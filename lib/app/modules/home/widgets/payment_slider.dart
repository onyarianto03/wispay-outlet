// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/topup/topup_model.dart';
import 'package:wispay_outlet/app/global_widgets/countdown/countdown.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/home/controllers/home_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class PaymentSlider extends GetWidget<HomeController> {
  const PaymentSlider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Obx(
          () => Visibility(
            child: GestureDetector(
              onTap: () => Get.toNamed(Routes.WAITING_PAYMENT),
              child: CustomText(
                title: 'Lihat Semua',
                textAlign: TextAlign.right,
                textType: TextType.SemiBold,
                color: WispayColors.cPrimary,
                margin: EdgeInsets.only(
                  right: Spacing.defaultSpacing.w,
                ),
              ),
            ),
            visible: controller.topupRequests.length > 1,
          ),
        ),
        SizedBox(height: Spacing.xSmall.h),
        Container(
          height: 60.h,
          padding: EdgeInsets.only(right: Spacing.defaultSpacing.w),
          child: _buildPageView(),
        ),
        SizedBox(height: Spacing.small.h),
        Obx(
          () => Visibility(
            child: _buildPageIndicator(),
            visible: controller.topupRequests.length > 1,
          ),
        ),
      ],
    );
  }

  Widget _buildPageView() {
    var boxDecoration = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      border: Border.all(
        color: const Color(0XFF6c94f9),
        width: 1.5,
      ),
      color: WispayColors.cWhite,
    );

    return Obx(
      () => PageView.builder(
        padEnds: true,
        controller: controller.slideController,
        itemCount: controller.topupRequests.length,
        physics: const BouncingScrollPhysics(),
        onPageChanged: (value) => controller.sliderIdx.value = value,
        itemBuilder: (_, idx) {
          return GestureDetector(
            onTap: () => controller.gotoDetilTopup(idx, controller.topupRequests[idx].type ?? ""),
            child: Container(
              padding: EdgeInsets.only(left: Spacing.defaultSpacing.w),
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: Spacing.defaultSpacing.w,
                  vertical: Spacing.medium.h,
                ),
                decoration: boxDecoration,
                child: _buildCard(controller.topupRequests[idx]),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildCard(TopupModel topup) {
    return Row(
      children: [
        CustomImage(
          source: topup.logo!.url!,
          width: 32,
          height: 32,
          borderRadius: 0,
          fit: BoxFit.contain,
        ),
        SizedBox(width: Spacing.defaultSpacing.w),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: 'Menunggu Pembayaran',
              size: FontSize.xSmall.sp,
              color: WispayColors.cBlack333,
              textType: TextType.Regular,
            ),
            SizedBox(height: Spacing.xSmall.h),
            CustomText(
              title: NumberHelper.formatMoney(topup.amount.toString()),
              size: FontSize.xSmall.sp,
              textType: TextType.SemiBold,
              color: WispayColors.cBlack333,
            ),
          ],
        ),
        const Spacer(),
        Align(
          alignment: Alignment.topCenter,
          child: CountDownTimer(
            secondsRemaining: DateTime.now().difference(DateTime.parse(topup.expiredAt!)).inSeconds.toInt().abs(),
            whenTimeExpires: () => controller.getTopupRequests(),
            countDownTimerStyle: TextStyle(
              fontSize: FontSize.xSmall.sp,
              color: WispayColors.cPrimary,
              fontWeight: FontWeight.w600,
            ),
            countDownFormatter: _formatter,
          ),
        )
      ],
    );
  }

  Widget _indicator(bool isActive) {
    return SizedBox(
      height: 10,
      child: AnimatedContainer(
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
        margin: const EdgeInsets.symmetric(horizontal: 2.0),
        height: isActive ? 8 : 8.0,
        width: isActive ? 18 : 8.0,
        decoration: ShapeDecoration(
          shape: !isActive
              ? const CircleBorder()
              : RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
          color: isActive ? WispayColors.cPrimary : const Color(0XFFEAEAEA),
        ),
      ),
    );
  }

  Widget _buildPageIndicator() {
    return Obx(() {
      List<Widget> list = [];
      for (int i = 0; i < controller.topupRequests.length; i++) {
        list.add(i == controller.sliderIdx.value ? _indicator(true) : _indicator(false));
      }
      return Row(
        children: list,
        mainAxisAlignment: MainAxisAlignment.center,
      );
    });
  }

  _formatter(sec) {
    final timeleft = Duration(seconds: sec);
    String hours = timeleft.inHours.remainder(24).toString().padLeft(2, '0');
    String minutes = timeleft.inMinutes.remainder(60).toString().padLeft(2, '0');
    String seconds = timeleft.inSeconds.remainder(60).toString().padLeft(2, '0');

    if (hours == '00') {
      return '$minutes:$seconds detik';
    }

    return '$hours:$minutes:$seconds detik';
  }
}
