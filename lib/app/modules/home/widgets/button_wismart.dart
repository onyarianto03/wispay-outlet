// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:auto_size_text/auto_size_text.dart';
import 'package:get/get.dart';
import 'package:device_apps/device_apps.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:url_launcher/url_launcher.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class ButtonWismart extends StatelessWidget {
  const ButtonWismart({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
      child: Material(
        borderRadius: BorderRadius.circular(Spacing.defaultSpacing),
        clipBehavior: Clip.hardEdge,
        child: InkWell(
          onTap: () => _handleOnTap(),
          splashColor: WispayColors.cGreen2.withOpacity(0.2),
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: Spacing.xSmall, horizontal: Spacing.defaultSpacing),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              gradient: LinearGradient(
                colors: [WispayColors.cGreen2.withOpacity(0.1), WispayColors.cGreen2.withOpacity(0.05)],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
              image: const DecorationImage(
                image: Assets.iconsMiscWave4,
                alignment: Alignment.topRight,
              ),
            ),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          const CustomText(
                            title: 'Wismart  ',
                            size: FontSize.small,
                            textType: TextType.SemiBold,
                            color: WispayColors.cBlack333,
                          ),
                          Assets.iconsBroccoli.image(width: 12)
                        ],
                      ),
                      const CustomText(
                        title: 'Belanja kebutuhan harian bisa sambil rebahan di rumah. Yuk, coba sekarang!',
                        size: FontSize.xSmall,
                      )
                    ],
                  ),
                ),
                Assets.imagesWismartHero.image(width: 72),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _handleOnTap() async {
    bool isInstalled = await DeviceApps.isAppInstalled(dotenv.get('WISMART_APP_NAME'));

    if (isInstalled) {
      DeviceApps.openApp(dotenv.get('WISMART_APP_NAME'));
    } else {
      WispayBottomSheet.show(withTitle: false, children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: Spacing.defaultSpacing * 2),
            Assets.imagesWismartPhone.image(width: Get.width * 0.7),
            const CustomText(
              title: 'Daftar Akun Wismart',
              color: WispayColors.cBlack,
              textType: TextType.SemiBold,
              margin: EdgeInsets.only(top: 31, bottom: Spacing.small),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
              child: AutoSizeText.rich(
                TextSpan(
                  text: 'Yuk, daftarin nomor ',
                  children: [
                    TextSpan(
                      text: 'hp outlet ',
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                    TextSpan(text: 'di aplikasi Wismart untuk bisa belanja pakai saldo Outlet ✨')
                  ],
                ),
                textAlign: TextAlign.center,
                maxLines: 2,
                minFontSize: 12,
                style: TextStyle(height: 1.5, fontSize: FontSize.small),
              ),
            ),
            const Divider(thickness: 1, height: 32),
            CustomButton(
                title: 'Mulai Belanja',
                onPress: () {
                  String url = 'https://play.google.com/store/apps/details?id=com.wispay';
                  launch(url);
                  Get.back();
                }),
            const SizedBox(height: Spacing.defaultSpacing)
          ],
        ),
      ]);
    }
  }
}
