// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/number_helper.dart';
import 'package:wispay_outlet/app/core/theme/common_theme.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/rounded_icon_button.dart';
import 'package:wispay_outlet/app/global_widgets/shimmer/shimmer_widget.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';
import 'package:wispay_outlet/generated/locales.g.dart';

class CardWallet extends GetWidget<CategoriesController> {
  const CardWallet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        boxShadow: const [shadow],
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.r),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.r),
        child: Column(
          children: [
            _buildTopSection(),
            const Divider(height: 1, thickness: 1, indent: Spacing.defaultSpacing),
            _buildBottomSection()
          ],
        ),
      ),
    );
  }

  Container _buildTopSection() {
    return Container(
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(Assets.iconsMiscWave1.path),
          alignment: Alignment.topRight,
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () => Get.toNamed(Routes.SALDO),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  size: FontSize.small,
                  title: LocaleKeys.my_saldo.tr,
                ),
                const SizedBox(height: Spacing.xSmall),
                Obx(() {
                  bool isLoading = controller.isLoading.value;
                  String? saldo = controller.profile.value.outletWallet?.total;
                  if (isLoading) {
                    return ShimmerWidget.rectangular(height: 20, width: 100);
                  } else {
                    return CustomText(
                      title: NumberHelper.formatMoney(saldo),
                      textType: TextType.SemiBold,
                      color: WispayColors.cPrimary,
                    );
                  }
                }),
              ],
            ),
          ),
          const Spacer(),
          RoundedIconButton(
            icon: Assets.iconsButtonPaperPlane.path,
            title: LocaleKeys.transfer.tr,
            onPressed: () => Get.toNamed(Routes.TRANSFER),
          ),
          const SizedBox(width: 37),
          RoundedIconButton(
            icon: Assets.iconsButtonTopup.path,
            title: LocaleKeys.topup.tr,
            onPressed: () => Get.toNamed(Routes.REQUEST_TOPUP),
          ),
        ],
      ),
    );
  }

  Padding _buildBottomSection() {
    return Padding(
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      child: InkWell(
        onTap: () => Get.toNamed(Routes.MY_POINT),
        child: Row(
          children: [
            Assets.iconsCoin.image(width: 24, height: 24),
            const SizedBox(width: Spacing.small),
            const CustomText(
              title: 'Poin Saya',
              size: FontSize.defaultSize,
              textType: TextType.SemiBold,
            ),
            const Spacer(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Obx(() {
                  bool isLoading = controller.isLoading.value;
                  String? point = controller.profile.value.outletPoint?.total;

                  if (isLoading) {
                    return ShimmerWidget.rectangular(height: 20, width: 80);
                  } else {
                    return CustomText(
                      title: LocaleKeys.point.trParams({
                        'point': NumberHelper.formatMoneyWithoutSymbol(point),
                      }),
                      size: FontSize.defaultSize,
                      textType: TextType.SemiBold,
                      color: WispayColors.cPrimary,
                    );
                  }
                }),
                const SizedBox(width: Spacing.defaultSpacing / 2),
                Icon(Icons.chevron_right, size: ScreenUtil().setSp(20)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
