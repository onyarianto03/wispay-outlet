// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:livechatt/livechatt.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/models/profile/profile_model.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class HeaderContent extends GetView<CategoriesController> {
  const HeaderContent({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _buildGreeting(),
        _buildButton(),
      ],
    );
  }

  Obx _buildGreeting() {
    return Obx(
      () {
        ProfileModel? _profile = controller.profile.value;
        return CustomText(
          title: 'Halo, ${_profile.name ?? ''}',
          size: FontSize.xLarge,
          textType: TextType.Bold,
          color: WispayColors.cBlack333,
        );
      },
    );
  }

  Widget _buildButton() {
    return GestureDetector(
      onTap: () => Livechat.beginChat(
        dotenv.get('LIVECHAT_API_KEY'),
        '',
        controller.profile.value.name!,
        decrypt(controller.profile.value.email),
      ),
      child: Column(
        children: [
          Container(
            width: 32.w,
            height: 32.h,
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            padding: const EdgeInsets.all(Spacing.small),
            child: Image.asset(Assets.iconsCs.path),
          ),
          const CustomText(
            title: 'Bantuan',
            size: FontSize.small,
          ),
        ],
      ),
    );
  }
}
