// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class PromoSliderWidget extends StatelessWidget {
  const PromoSliderWidget({
    Key? key,
    required this.image,
    required this.id,
    required this.type,
  }) : super(key: key);

  final String image;
  final int id;
  final String type;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.toNamed(Routes.PROMO, arguments: {
        'id': id,
        'type': type,
      }),
      child: Padding(
        padding: EdgeInsets.only(right: Spacing.defaultSpacing.w),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(12),
          child: FittedBox(
            fit: BoxFit.fill,
            child: Image.network(image),
          ),
        ),
      ),
    );
  }
}
