// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/modules/sales/widgets/connected_widget.dart';
import 'package:wispay_outlet/app/modules/sales/widgets/not_connected_widget.dart';
import '../controllers/sales_controller.dart';

class SalesView extends GetView<SalesController> {
  const SalesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Agen Wispay Sales'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.chevron_left,
            size: 35,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Obx(() {
        if (controller.homeController.profile.value.upline != null) {
          return const ConnectedWidget();
        }
        return const NotConnectedWidget();
      }),
    );
  }
}
