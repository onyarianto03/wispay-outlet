// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:auto_size_text/auto_size_text.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class ConnectedWidget extends GetView<CategoriesController> {
  const ConnectedWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.all(Spacing.defaultSpacing),
      child: Column(
        children: [
          const SizedBox(height: Spacing.defaultSpacing),
          CustomImage(
            source: controller.profile.value.upline?.avatar?.url ?? "",
            width: 100,
            height: 100,
            defaultImage: Assets.iconsSales.image(width: 100, height: 100),
          ),
          Obx(
            () => CustomText(
              title: controller.profile.value.upline?.name ?? "",
              size: FontSize.xLarge,
              textType: TextType.Bold,
              color: WispayColors.cBlack333,
              margin: const EdgeInsets.only(bottom: Spacing.xSmall, top: Spacing.defaultSpacing),
            ),
          ),
          AutoSizeText.rich(
            TextSpan(
              text: 'Kode Agen: ',
              children: [
                TextSpan(
                  text: controller.profile.value.referenceCode ?? "",
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
            style: const TextStyle(
              fontSize: FontSize.defaultSize,
            ),
          ),
          const SizedBox(height: 31),
          CustomButton(title: 'Hubungi Agen', onPress: () => _contactSales()),
        ],
      ),
    );
  }

  void _contactSales() async {
    String message = 'Halo Wispay, saya ingin bertanya tentang layanan Wispay Outlet.\n\n';
    String _url = 'https://wa.me/${decrypt(controller.profile.value.upline?.phone)}?text=${Uri.encodeFull(message)}';

    if (await canLaunch(_url)) {
      await launch(_url);
    } else {
      throw 'Could not launch $_url';
    }
  }
}
