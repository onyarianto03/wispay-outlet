// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/sales/controllers/sales_controller.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class NotConnectedWidget extends GetView<SalesController> {
  const NotConnectedWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Stack(
        children: [
          SingleChildScrollView(
            reverse: true,
            padding: const EdgeInsets.all(Spacing.defaultSpacing),
            physics: const NeverScrollableScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(height: 50),
                Assets.imagesSales.image(width: Get.width * 0.8),
                const CustomText(
                  title: 'Hubungkan Agen Wispay',
                  size: FontSize.medium,
                  color: WispayColors.cBlack333,
                  textType: TextType.SemiBold,
                  margin: EdgeInsets.only(top: Spacing.large, bottom: Spacing.xSmall),
                ),
                const CustomText(
                  title: 'Agen sales Wispay adalah yang akan membantumu untuk berjualan di Wispay',
                  textAlign: TextAlign.center,
                  margin: EdgeInsets.only(bottom: Spacing.large),
                ),
                CustomTextInput(
                  value: controller.code.value,
                  label: 'Kode Agen',
                  isEnabled: true,
                  hintText: 'cth. ABC01234',
                  textEditingController: controller.tController,
                  onClear: controller.onClear,
                  hintStyle: const TextStyle(
                    color: WispayColors.cBlackBBB,
                  ),
                ),
                const SizedBox(height: 50)
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(Spacing.defaultSpacing),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: CustomButton(
                title: 'Simpan',
                onPress: controller.onSubmit,
                isEnabled: controller.code.value.isNotEmpty,
              ),
            ),
          )
        ],
      ),
    );
  }
}
