// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';

// Project imports:
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';

class SalesController extends GetxController {
  final AccountRepository _accountRepo = AccountRepository(Get.find());
  final CategoriesController homeController = Get.find();

  final isConnected = false.obs;
  final code = ''.obs;
  final TextEditingController tController = TextEditingController();

  final count = 0.obs;
  @override
  void onInit() {
    tController.addListener(_onChange);
    super.onInit();
  }

  @override
  void onClose() {
    tController.removeListener(_onChange);
  }

  void onSubmit() async {
    WispayDialog.fullScreenLoading();
    final _response = await _accountRepo.updateReferenceCode(referenceCode: tController.text);
    if (_response.success != false) {
      homeController.getProfile().then((_) => WispayDialog.closeDialog());
    } else {
      WispayDialog.closeDialog();
    }
  }

  void _onChange() {
    code.value = tController.text;
  }

  void onClear() {
    tController.clear();
  }
}
