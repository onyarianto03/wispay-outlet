// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/utils/input_price_formatter.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/clickable_widget.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/info_widget.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/commision_setting/widgets/skeleton_widget.dart';
import '../controllers/commision_setting_controller.dart';

class CommisionSettingView extends GetView<CommisionSettingController> {
  const CommisionSettingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pengaturan Komisi'),
        leading: IconButton(onPressed: () => Get.back(), icon: const Icon(Icons.arrow_back_ios)),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing, vertical: Spacing.large),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const InfoWidget(
                text: 'Komisi adalah besar keuntungan yang akan ditambahkan ke tagihan pembeli saat bertransaksi.',
                margin: EdgeInsets.only(bottom: Spacing.large),
              ),
              const CustomText(
                title: 'Atur Otomatis',
                size: FontSize.medium,
                color: WispayColors.cBlack333,
                textType: TextType.SemiBold,
                margin: EdgeInsets.only(bottom: Spacing.defaultSpacing),
              ),
              Obx(() => _buildMenuItem(controller.isLoading.value)),
              const Divider(
                height: 32,
                thickness: 1,
                color: WispayColors.cBorderE,
              ),
              const CustomText(
                title: 'Atur per Produk',
                size: FontSize.medium,
                color: WispayColors.cBlack333,
                textType: TextType.SemiBold,
              ),
              Obx(() {
                var _comissions = controller.homeController.profile.value.comissionSetting!.toJson();
                return ListView(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    for (var item in _comissions.keys)
                      Container(
                        margin: const EdgeInsets.only(bottom: Spacing.defaultSpacing),
                        child: _buildMenuItem(controller.isLoading.value, key: item),
                      ),
                  ],
                );
              })
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildMenuItem(bool loading, {String? key}) {
    var _comissions = controller.homeController.profile.value.comissionSetting!.toJson();
    if (loading) {
      return const SkeletonWidget();
    }

    if (key == 'all') return Container(height: 0);

    return ClickableWidget(
      onTap: () => WispayBottomSheet.show(
        onclose: () => controller.resetField(),
        title: "Atur Komisi",
        children: [
          CustomText(
            title: GetUtils.capitalize(key?.replaceAll('_', " ") ?? "Semua Produk") ?? "Semua Produk",
            margin: const EdgeInsets.only(bottom: Spacing.xSmall, top: Spacing.defaultSpacing),
            size: FontSize.small,
            textType: TextType.SemiBold,
            textAlign: TextAlign.start,
          ),
          Obx(
            () => CustomTextInput(
              value: controller.nominal.value,
              hintText: 'nominal komisi',
              textEditingController: controller.tEdititingController,
              keyboardType: TextInputType.number,
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                InputPriceFormatter(),
              ],
            ),
          ),
          const SizedBox(height: Spacing.defaultSpacing),
          CustomButton(title: 'Simpan', onPress: () => controller.updateComissionSettings(key: key ?? 'all')),
          const SizedBox(height: Spacing.defaultSpacing),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(Spacing.defaultSpacing + 1),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(title: GetUtils.capitalize(key?.replaceAll('_', " ") ?? "Semua Produk") ?? "Semua Produk"),
            Row(
              children: [
                CustomText(
                  title: NumberHelper.formatMoney(_comissions[key ?? 'all']),
                  textType: TextType.SemiBold,
                  color: WispayColors.cBlack333,
                ),
                const SizedBox(width: Spacing.defaultSpacing),
                const Icon(Icons.arrow_forward_ios_sharp, size: 16)
              ],
            )
          ],
        ),
      ),
    );
  }
}
