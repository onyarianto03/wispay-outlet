// Package imports:
import 'package:get/get.dart';

// Project imports:
import '../controllers/commision_setting_controller.dart';

class CommisionSettingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CommisionSettingController>(
      () => CommisionSettingController(),
    );
  }
}
