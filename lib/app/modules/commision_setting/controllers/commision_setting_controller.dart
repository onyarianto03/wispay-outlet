// Package imports:
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/data/models/comission_settings_model/comission_settings_model.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/product_category/product_category_item/product_category_item_model.dart';
import 'package:wispay_outlet/app/data/repositories/account_repository.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';

class CommisionSettingController extends GetxController {
  final isLoading = false.obs;
  final CategoriesController homeController = Get.find();
  final AccountRepository _accountRepository = AccountRepository(Get.find());
  final TextEditingController tEdititingController = TextEditingController();

  final productCategory = <ProductCategoryItemModel>[].obs;

  final nominal = ''.obs;
  @override
  void onInit() {
    tEdititingController.addListener(_onchangeText);
    super.onInit();
  }

  @override
  void onClose() {}

  void updateComissionSettings({required String key}) async {
    Get.back();
    isLoading.value = true;
    Map<String, dynamic> _settings = {
      key: nominal.value.replaceAll('.', ''),
    };
    final _res = await _accountRepository.updateComissionSettings(settings: ComissionSettingsModel.fromJson(_settings));
    if (_res.success == true) {
      homeController.getProfile().then((value) => isLoading.value = false);
    }
  }

  void resetField() {
    tEdititingController.clear();
  }

  void _onchangeText() {
    nominal.value = tEdititingController.text;
  }
}
