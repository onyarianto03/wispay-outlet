import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/helpers.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/countdown/countdown.dart';
import 'package:wispay_outlet/app/global_widgets/custom_image.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';

import '../controllers/waiting_payment_controller.dart';

class WaitingPaymentView extends GetView<WaitingPaymentController> {
  const WaitingPaymentView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Menunggu Pembayaran'),
        leading: IconButton(
          icon: const Icon(Icons.chevron_left, size: 35),
          onPressed: () => Get.back(),
        ),
        centerTitle: true,
      ),
      body: Obx(() => ListView.builder(
            itemCount: controller.dataList.length,
            padding: const EdgeInsets.all(Spacing.defaultSpacing),
            itemBuilder: (_, idx) => PaymentCard(
              source: controller.dataList[idx].logo?.url ?? '',
              expiredAt: controller.dataList[idx].expiredAt ?? '',
              totalTransfer: NumberHelper.formatMoney(controller.dataList[idx].amount),
              vaName: decrypt(controller.dataList[idx].accountName),
              onTap: () => controller.gotoDetail(idx, controller.dataList[idx].type),
            ),
          )),
    );
  }
}

class PaymentCard extends StatelessWidget {
  const PaymentCard({
    Key? key,
    required this.expiredAt,
    required this.totalTransfer,
    required this.vaName,
    required this.source,
    required this.onTap,
  }) : super(key: key);

  final String expiredAt;
  final String totalTransfer;
  final String vaName;
  final String source;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return BaseCard(
      spreadRadius: 0,
      blurRadius: 20,
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(Spacing.defaultSpacing),
            width: double.infinity,
            color: WispayColors.cPrimary.withOpacity(0.1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const CustomText(
                  title: 'Batas Bayar ',
                  color: WispayColors.cPrimary,
                  textType: TextType.SemiBold,
                  textAlign: TextAlign.center,
                ),
                CountDownTimer(
                  secondsRemaining: DateTime.now().difference(DateTime.parse(expiredAt)).inSeconds.toInt().abs(),
                  whenTimeExpires: () => {},
                  countDownTimerStyle: const TextStyle(
                    color: WispayColors.cPrimary,
                    fontWeight: FontWeight.w600,
                  ),
                  countDownFormatter: _formatter,
                )
              ],
            ),
          ),
          const SizedBox(height: Spacing.defaultSpacing),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
            child: Row(
              children: [
                CustomImage(
                  source: source,
                  width: 32,
                  height: 32,
                  borderRadius: 0,
                  fit: BoxFit.contain,
                ),
                const SizedBox(width: Spacing.defaultSpacing),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      title: totalTransfer,
                      textType: TextType.SemiBold,
                    ),
                    CustomText(title: vaName),
                  ],
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(Spacing.defaultSpacing),
            child: CustomButton(
              title: 'Cara Bayar',
              onPress: onTap,
            ),
          )
        ],
      ),
    );
  }

  _formatter(sec) {
    final timeleft = Duration(seconds: sec);
    String hours = timeleft.inHours.remainder(24).toString().padLeft(2, '0');
    String minutes = timeleft.inMinutes.remainder(60).toString().padLeft(2, '0');
    String seconds = timeleft.inSeconds.remainder(60).toString().padLeft(2, '0');

    if (hours == '00') {
      return ' $minutes : $seconds detik';
    }

    return ' $hours : $minutes : $seconds detik';
  }
}
