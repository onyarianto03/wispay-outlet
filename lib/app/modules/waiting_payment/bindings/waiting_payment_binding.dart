import 'package:get/get.dart';

import '../controllers/waiting_payment_controller.dart';

class WaitingPaymentBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WaitingPaymentController>(
      () => WaitingPaymentController(),
    );
  }
}
