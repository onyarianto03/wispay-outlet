import 'package:get/get.dart';
import 'package:wispay_outlet/app/data/models/topup/topup_model.dart';
import 'package:wispay_outlet/app/data/repositories/home_repository.dart';
import 'package:wispay_outlet/app/data/repositories/transaction_repository.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/modules/saldo/controllers/detail_topup_request_controller.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class WaitingPaymentController extends GetxController {
  final HomeRepository _homeRepository = HomeRepository(Get.find());
  final TransactionRepository _trxRepo = TransactionRepository(Get.find());
  final DetailTopupRequestController _reqTopupController = Get.find<DetailTopupRequestController>();

  final isLoading = true.obs;
  final dataList = <TopupModel>[].obs;
  @override
  void onInit() {
    super.onInit();
    _getData();
  }

  @override
  void onClose() {}

  void gotoDetail(index, type) async {
    if (type == 'Transaction') {
      final _res = await _trxRepo.getDetail(id: dataList[index].id);
      if (_res.success == true) {
        Get.back();
        Get.toNamed(Routes.SEND_MONEY_PAYMENT_INSTRUCTIONS, arguments: _res.data);
      } else {
        Get.close(2);
      }
    } else {
      WispayDialog.fullScreenLoading();
      final _r = await _reqTopupController.getDetail(dataList[index].id!);
      if (_r) {
        Get.back();
        Get.toNamed(Routes.HOW_TO_PAY_TOPUP);
      } else {
        Get.close(2);
      }
    }
  }

  void _getData() async {
    isLoading.value = true;
    final _response = await _homeRepository.getHomeSection('topup_requests_and_remittances');
    if (_response.success == true) {
      final _t = List<TopupModel>.from(_response.data.map((e) => TopupModel.fromJson(e)));
      dataList.assignAll(_t);
    }
    isLoading.value = false;
  }
}
