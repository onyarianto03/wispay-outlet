// Package imports:
import 'package:dio/dio.dart';
import 'package:get/get.dart';

// Project imports:

import 'package:wispay_outlet/app/controllers/filter_date_controller.dart';
import 'package:wispay_outlet/app/controllers/image_picker_controller.dart';
import 'package:wispay_outlet/app/controllers/map_controller.dart';
import 'package:wispay_outlet/app/controllers/product_controller.dart';
import 'package:wispay_outlet/app/controllers/qr_scanner_controller.dart';
import 'package:wispay_outlet/app/core/services/api_service.dart';

class InitialBinding implements Bindings {
  Dio api() {
    Dio _dio = apiService();
    return _dio;
  }

  @override
  void dependencies() async {
    Get.put(api());
    Get.lazyPut(() => ImagePickerController(), fenix: true);
    Get.lazyPut(() => QrScannerController(), fenix: true);
    Get.create(() => FilterDateController());
    Get.create(() => ProductController());
    Get.lazyPut(() => MapController(), fenix: true);
  }
}
