import 'package:get/get.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';
import 'package:wispay_outlet/app/data/repositories/product_repository.dart';

class ProductController extends GetxController {
  final ProductRepository _productRepository = ProductRepository(Get.find());
  final productBrand = const <ProductBrandModel>[].obs;
  final productSubCategory = const <BaseModel>[].obs;
  final products = <ProductModel>[].obs;

  Future<List<ProductBrandModel>?> getProductBrands(categoryId, {FilterModel? filter}) async {
    final Map<String, dynamic> newFilter = {};
    if (filter != null) {
      final _c = Map<String, dynamic>.from(filter.toJson());
      newFilter.addAll(_c);
    }
    final _filter = {
      "_limit": 14,
    };
    newFilter.addAll(_filter);
    final res = await _productRepository.getProductBrands(categoryId, filter: FilterModel.fromJson(newFilter));
    if (res.data != null) {
      productBrand.value = res.data!;
      return res.data;
    }
    return null;
  }

  Future<List<BaseModel>?> getSubCategories(brandId, {FilterModel? filter}) async {
    final Map<String, dynamic> newFilter = {};
    if (filter != null) {
      final _c = Map<String, dynamic>.from(filter.toJson());
      newFilter.addAll(_c);
    }
    final _filter = {
      "_limit": 14,
    };
    newFilter.addAll(_filter);
    final res = await _productRepository.getSubCategories(brandId, filter: FilterModel.fromJson(newFilter));
    if (res.data != null) {
      productSubCategory.value = res.data!;
      return res.data;
    }
    return null;
  }

  Future<List<ProductModel>?> getProducts(subCategoryId, {FilterModel? filter}) async {
    final Map<String, dynamic> newFilter = {};
    if (filter != null) {
      final _c = Map<String, dynamic>.from(filter.toJson());
      newFilter.addAll(_c);
    }
    final _filter = {
      "product_subcategory_id": subCategoryId,
      "_limit": 14,
    };
    newFilter.addAll(_filter);
    final res = await _productRepository.getProducts(filter: FilterModel.fromJson(newFilter));
    if (res.data != null) {
      products.value = res.data!;
      return res.data;
    }
    return null;
  }
}
