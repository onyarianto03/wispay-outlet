import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/helpers/date_helper.dart';
import 'package:wispay_outlet/app/data/enums/datefilter_enum.dart';
import 'package:wispay_outlet/app/data/models/utility/filter_date_model.dart';

String kLastThirtyDays = 'last-month';
String kLastNintyDays = 'last-3month';
String kAllDate = '';
String kSelectCustomDate =
    DateTimeRange(start: DateTime.now(), end: DateTime.now().add(const Duration(days: 1))).toString();

class FilterDateController extends GetxController {
  Map<dateFilter, Map<String, dynamic>> dateValue = {
    dateFilter.allDate: {
      'title': 'Semua Tanggal',
      'value': kAllDate,
    },
    dateFilter.lastThirtyDays: {
      'title': '30 Hari Terakhir',
      'value': kLastThirtyDays,
    },
    dateFilter.lastNintyDays: {
      'title': '90 Hari Terakhir',
      'value': kLastNintyDays,
    },
    dateFilter.selectCustomDate: {
      'title': 'Pilih Tanggal Sendiri',
      'value': kSelectCustomDate,
    },
  };

  final filterDateTitle = 'Pilih Tanggal'.obs;
  final selectedDateFilter = dateFilter.allDate.obs;
  final dateRangeValue = DateTimeRange(
    start: DateTime.now().subtract(const Duration(days: 30)),
    end: DateTime.now(),
  ).obs;

  final filterValue = kAllDate.obs;

  final count = 0.obs;

  @override
  void onClose() {}

  void onSaveFilterDate({String type = 'default', ValueChanged<FilterDateModel>? cb}) {
    if (type == 'range') {
      filterDateTitle.value =
          '${_formatDate(dateRangeValue.value.start.toString())} - ${_formatDate(dateRangeValue.value.end.toString())}';
      cb?.call(FilterDateModel(
        dateRange: dateRangeValue.value,
        date: null,
      ));
    } else {
      filterDateTitle.value = dateValue[selectedDateFilter.value]!['title'];
      cb?.call(FilterDateModel(
        dateRange: null,
        date: dateValue[selectedDateFilter.value]!['value'],
      ));
    }

    Get.back();
  }

  String _formatDate(String date) {
    return DateHelper.formatDate(date, format: 'dd MMM yyyy');
  }
}
