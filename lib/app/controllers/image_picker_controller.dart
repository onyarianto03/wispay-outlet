// Dart imports:
import 'dart:io';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';

class ImagePickerController extends GetxController {
  final ImageCropper _cropper = ImageCropper();
  ImagePickerController();
  final ImagePicker _picker = ImagePicker();
  final imageFile = File('').obs;
  final isPicked = false.obs;

  captureImage(ImageSource source) async {
    final XFile? photo = await _picker.pickImage(source: source);
    if (photo != null) {
      File? crp = await _cropper.cropImage(
        sourcePath: photo.path,
        aspectRatio: const CropAspectRatio(ratioX: 3, ratioY: 2),
      );

      print(crp);
      print(photo);

      if (crp != null) {
        isPicked.value = true;
        imageFile.value = crp;
        Get.back();
      }
    }
  }

  void showUploadBottomsheet() {
    WispayBottomSheet.show(
      title: 'Ambil Foto KTP',
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              child: Container(
                padding: const EdgeInsets.only(bottom: Spacing.defaultSpacing, top: Spacing.small),
                width: double.maxFinite,
                child: const CustomText(
                  title: 'Pilih dari galeri',
                  color: WispayColors.cBlack333,
                ),
              ),
              onTap: () => captureImage(ImageSource.gallery),
            ),
            const Divider(thickness: 1),
            InkWell(
              child: Container(
                padding: const EdgeInsets.only(bottom: Spacing.defaultSpacing, top: Spacing.small),
                width: double.maxFinite,
                child: const CustomText(
                  title: 'Ambil dengan kamera',
                  color: WispayColors.cBlack333,
                ),
              ),
              onTap: () => captureImage(ImageSource.camera),
            ),
          ],
        ),
      ],
    );
  }
}
