import 'dart:async';

import 'package:dio/dio.dart' as _d;
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:uuid/uuid.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/services/location_service.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/data/models/utility/prediction_model.dart';
import 'package:wispay_outlet/app/data/repositories/google_repository.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/custom_text_input.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';
import 'package:wispay_outlet/app/global_widgets/typography/typography_helper.dart';
import 'package:wispay_outlet/app/modules/categories/controllers/categories_controller.dart';

_d.Dio api() {
  _d.Dio api = _d.Dio();

  api.interceptors.add(PrettyDioLogger(
    compact: true,
    responseBody: true,
    responseHeader: false,
    request: true,
    requestBody: true,
  ));

  return api;
}

class MapController extends GetxController {
  final LocationService _locationService = LocationService();
  final GoogleRepository _googleRepository = GoogleRepository(api());
  final CategoriesController homeController = Get.find();
  Uuid uuid = const Uuid();
  String _sessionToken = '';
  Completer<GoogleMapController> mapController = Completer();

  final kGoogleApiKey = "AIzaSyBU6DA_zEcj4fLOgQ8uriTa8MDb7JFhysw";
  final TextEditingController _addressController = TextEditingController();

  final kCurrentPosition = const LatLng(-6.1753924, 106.8271528).obs;
  final marker = const Marker(markerId: MarkerId('user-position'), position: LatLng(0, 0)).obs;
  final completeAddress = ''.obs;
  final keyword = ''.obs;
  final listPlaces = <PredictionModel>[].obs;

  @override
  void onInit() async {
    _sessionToken = uuid.v4();
    marker.value = Marker(
      markerId: const MarkerId('user-position'),
      position: LatLng(
        double.parse(decrypt(homeController.profile.value.outletLatitude)),
        double.parse(
          decrypt(homeController.profile.value.outletLongitude),
        ),
      ),
    );
    ever<LatLng>(kCurrentPosition, (position) {
      marker.value = Marker(markerId: const MarkerId('user-position'), position: position);
    });

    debounce(keyword, (_) {
      _getPlaces();
    }, time: const Duration(milliseconds: 300));
    _addressController.addListener(_onchangeAddress);

    super.onInit();
  }

  @override
  void onClose() {
    _addressController.removeListener(_onchangeAddress);
    super.onClose();
  }

  void onSearch(value) {
    keyword.value = value;
  }

  void _getPlaces() async {
    if (keyword.value.isNotEmpty) {
      List<PredictionModel>? _res =
          await _googleRepository.getPlaces(keyword: keyword.value, sessionToken: _sessionToken);

      if (_res != null) {
        listPlaces.value = _res;
      }
    }
  }

  void onMapCreated(GoogleMapController c) async {
    final _locationService = LocationService();
    mapController.complete(c);
    Position _currentPosition = await _locationService.getCurrentLocation();
    kCurrentPosition.value = LatLng(_currentPosition.latitude, _currentPosition.longitude);
    c.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(_currentPosition.latitude, _currentPosition.longitude),
          zoom: 14,
        ),
      ),
    );
  }

  void onSelectPlace(PredictionModel place) async {
    Get.back();
    FocusManager.instance.primaryFocus?.unfocus();
    final GoogleMapController _c = await mapController.future;
    final _location = await _locationService.getLocationFromAddress(place.description ?? "");

    if (_location != null) {
      kCurrentPosition.value = LatLng(_location.latitude, _location.longitude);
      _addressController.text = place.description ?? '';
      await _c.moveCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(_location.latitude, _location.longitude), zoom: 14, tilt: 0),
        ),
      );
      showConfirm(place: place, latitude: _location.latitude.toString(), longitude: _location.longitude.toString());
    }
  }

  void useCurrentLocation() async {
    Get.back();
    FocusManager.instance.primaryFocus?.unfocus();
    final GoogleMapController _c = await mapController.future;
    final _current = await _locationService.getCurrentLocation();
    final _place = await _locationService.getAddressFromLocation(_current);

    if (_place != null) {
      final String _address =
          '${_place.street}, ${_place.subLocality}, ${_place.locality}, ${_place.subAdministrativeArea}, ${_place.administrativeArea}';
      kCurrentPosition.value = LatLng(_current.latitude, _current.longitude);
      _addressController.text = _address;
      await _c.moveCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(_current.latitude, _current.longitude), zoom: 14, tilt: 0),
        ),
      );
      showConfirm(
        place: PredictionModel.fromJson({'description': _address}),
        latitude: _current.latitude.toString(),
        longitude: _current.longitude.toString(),
      );
    }
  }

  void showConfirm({required PredictionModel place, required String latitude, required String longitude}) {
    WispayBottomSheet.show(
      withTitle: false,
      children: [
        CustomText(
          title: place.description ?? "",
          margin: const EdgeInsets.only(top: Spacing.defaultSpacing),
          textAlign: TextAlign.start,
          textType: TextType.SemiBold,
          color: WispayColors.cBlack333,
        ),
        const SizedBox(height: Spacing.defaultSpacing),
        CustomTextInput(
          value: _addressController.text,
          label: 'Alamat Lengkap',
          hintText: '',
          maxLines: 3,
          textEditingController: _addressController,
        ),
        const SizedBox(height: Spacing.defaultSpacing),
        Obx(
          () => CustomButton(
            title: 'Simpan Lokasi',
            isEnabled: completeAddress.value.isNotEmpty,
            onPress: () {
              Get.back();
              WispayDialog.showLoading();
              Future.delayed(const Duration(milliseconds: 1500), () {
                WispayDialog.closeDialog();
                Get.back<LocationDataModel>(
                  result: LocationDataModel(
                    latitude: latitude,
                    longitude: longitude,
                    address: completeAddress.value,
                  ),
                );
              });
            },
          ),
        ),
        const SizedBox(height: Spacing.defaultSpacing),
      ],
    );
  }

  void resetState() {
    keyword.value = '';
    completeAddress.value = '';
    _addressController.clear();
    listPlaces.clear();
  }

  _onchangeAddress() {
    completeAddress.value = _addressController.text;
  }
}
