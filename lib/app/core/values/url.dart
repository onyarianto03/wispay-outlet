// ignore_for_file: non_constant_identifier_names

const String REFRESH_TOKEN_URL = '/outlet-tokens/refresh-token';

// Home
const String URL_HOME = '/home';
const String URL_HOME_PROMOS = '$URL_HOME?section=promos_and_articles';

// account
const String URL_ACCOUNT = '/account';
const String URL_OTP_AUTH = '$URL_ACCOUNT/otp-auth';
const String URL_OTP_AUTH_VERIFICATION = '$URL_ACCOUNT/otp-auth-verification';
const String URL_OTP = '$URL_ACCOUNT/otp';
const String URL_OTP_VERIFICATION = '$URL_ACCOUNT/otp-verification';
const String URL_OTP_AUTH_RESEND = '$URL_ACCOUNT/resend-otp';
const String URL_SIGN_IN = '$URL_ACCOUNT/sign-in';
const String URL_PROFILE = '$URL_ACCOUNT/profile';
const String URL_UPDATE_PIN = '$URL_ACCOUNT/update-pin';
const String URL_UPDATE_REFERENCE_CODE = '$URL_ACCOUNT/update-reference-code';
const String URL_COMISSION_SETTING = '$URL_ACCOUNT/commission-setting';
const String URL_UPDATE_OUTLET = '$URL_ACCOUNT/update-outlet';
const String URL_UPDATE_AVATAR = '$URL_ACCOUNT/update-avatar';
const String URL_UPDATE_PROFILE = '$URL_ACCOUNT/update-profile';
const String URL_SIGN_UP = '$URL_ACCOUNT/sign-up';
const String URL_REFERRAL_COMISSION = '$URL_ACCOUNT/referral-commission';

// product
const String URL_PRODUCTS = '/products';
const String URL_PRODUCT_CATEGORY = '/product-categories';
const String URL_PRODUCT_BRAND = '/product-brands';
const String URL_PRODUCT_SUBCATEGORY = '/product-subcategories';
const String URL_PRODUCT_CATEGORY_BRAND = '$URL_PRODUCT_CATEGORY/%s$URL_PRODUCT_BRAND';
const String URL_PRODUCT_BRAND_SUBCATEGORY = '$URL_PRODUCT_BRAND/%s$URL_PRODUCT_SUBCATEGORY';

// transactions
const String URL_TRANSACTION = '/transaction';
const String URL_TRANSACTIONS = '/transactions';
const String URL_TRANSACTION_INQUIRY = '$URL_TRANSACTION/%s/%s/inquiry';
const String URL_TRANSACTION_ORDER = '$URL_TRANSACTION/%s/%s/order';
const String URL_TRANSACTION_CONFIRM = '$URL_TRANSACTION/%s/%s/confirm';
const String URL_DETAIL_TRANSACTION = '$URL_TRANSACTIONS/%s';
const String URL_TRANSACTION_UPDATE_COMISSION = '$URL_TRANSACTIONS/%s/commission';

// TOP up
const String URL_TOP_UP_REQUEST = '/topup-requests';
const String URL_OUTLET_WALLET = '/outlet-wallets';

// Wispay Bank
const String URL_WISPAY_BANK = '/wispay-banks';
const String URL_WISPAY_BANK_TOUPUP = '$URL_WISPAY_BANK/topup';
const String URL_WISPAY_BANK_REMITTANCE = '$URL_WISPAY_BANK/remittance';

// Notifications
const String URL_NOTIFICATION = '/notifications';
const String URL_UNREAD_NOTIFICATIONS = '$URL_NOTIFICATION/unread-count';

// Promo and aricles
const String URL_PROMOS = '/promos';
const String URL_ARTICLES = '/articles';

// Wallet
const String URL_TRANSFER_WALLET_OUTLET = '/transfer-wallets';
const String URL_TRANSFER_WALLET_PARTNER = '/partner/transfer-wallets';
const String URL_TRANSFER_WALLET_QR = '/transfer-wallets/qr-code';
const String URL_INQUIRY_TRANSFER_WALLET_PARTNER = '/partner/transfer-wallets/inquiry';

// validate
const String URL_VALIDATE_PIN = '/validate/outlet/pin';

// gmaps
const String URL_GMAPS = 'https://maps.googleapis.com/maps/api/place/autocomplete/json';

// area
const String URL_PROVINCE = '/provinces';
const String URL_DISTRICT = '/districts';
const String URL_REGENCY = '/regencies';

// gifts
const String URL_GIFTS = '/gifts';
const String URL_GIFT_EXCHANGE = '/gift-exchanges';
const String URL_REEDEM_GIFT = '$URL_GIFTS/%s/gift-exchanges';
const String URL_DELIVERY_GIFT = '$URL_GIFT_EXCHANGE/%s/gift-exchange-delivery';

// points
const String URL_HISTORY_POINTS = '/outlet-points';

// insurance
const String URL_INSURANCE = '/insurances';
const String URL_INSURANCE_PROVINCE = URL_INSURANCE + '/provinces';
const String URL_INSURANCE_CITY = URL_INSURANCE_PROVINCE + '/%s/cities';
const String URL_INSURANCE_BRANDS = URL_INSURANCE + '/gadgets/brands';
const String URL_INSURANCE_DEVICES = URL_INSURANCE + '/gadgets/devices';
const String URL_INSURANCE_DEVICE_CATEGORY = '/insurance-device-categories';
const String URL_INSURANCE_IMAGE = '/insurance-images';

// sellout
const String URL_SELL_OUT = '/sell-outs';
