// Flutter imports:
import 'package:flutter/material.dart';

abstract class WispayColors {
  static const cDisabledColor = Color(0xFFDDDDDD);
  static const cBorderColor = Color(0xFFBDBDBD);
  static const cPrimary = Color(0xFF0A4DF4);
  static const cBlack = Color(0xFF373737);
  static const cBlack333 = Color(0xFF333333);
  static const cBlack666 = Color(0xFF666666);
  static const cBlackBBB = Color(0xFFBBBBBB);
  static const cGreen = Color(0xFF48BA27);
  static const cGreen2 = Color(0xFF4EB125);
  static const cSuccess = Color(0xFF65D835);
  static const cWhite = Color(0xFFFFFFFF);
  static const cWhite2 = Color(0xFFF7F9FA);
  static const cSecondary = Color(0xFF447EF9);
  static const cRed = Color(0xFFDB0112);
  static const cRed2 = Color(0xFFFE0000);
  static const cBorderE = Color(0xFFEEEEEE);
  static const cGrey90 = Color(0xFF909090);
  static const cGrey99 = Color(0xFF999999);
  static const cGreyBC = Color(0xFFBCBCBC);
  static const cGreyC = Color(0xFFC4C4C4);
  static const cWarning = Color(0xFFD88501);
  static const cGrey82 = Color(0xFF828282);
  static const cGrey4f = Color(0xFF4F4F4F);
  static const bgColor = Color(0xFFFBFBFB);
}
