class FontSize {
  const FontSize._();

  /// 14.0
  static const double defaultSize = 14;

  /// 10
  static const double xxSmall = 10;

  /// 11
  static const double xSmall = 11;

  /// 12.0
  static const double small = 12;

  /// 16.0
  static const double medium = 16;

  /// 18.0
  static const double large = 18;

  /// 20.0
  static const double xLarge = 20;

  /// 24.0
  static const double xxLarge = 24;
}
