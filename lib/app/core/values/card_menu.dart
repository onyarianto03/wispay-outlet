// Project imports:
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class CardMenu {
  final String title;
  final String? subTitle;
  final Color? subTitleColor;
  final String? nextScreenTitle;
  final String icon;
  final String? route;
  final void Function()? onPress;
  final bool isNew;
  final Widget? customRightIcon;

  const CardMenu({
    required this.title,
    this.subTitle,
    this.subTitleColor,
    this.nextScreenTitle,
    required this.icon,
    this.route,
    this.onPress,
    this.isNew = false,
    this.customRightIcon,
  });
}

List<CardMenu> kWispayLaterMenu = [
  CardMenu(
    title: 'Wispay-Later',
    subTitle: "Pinjaman modal mudah tanpa bunga",
    icon: Assets.iconsAccountWispayLater.path,
    isNew: true,
  )
];

List<CardMenu> kOutletMenu = [
  CardMenu(
      title: "Data Outlet",
      subTitle: "Nama outlet, alamat outlet",
      icon: Assets.iconsAccountOutlet.path,
      onPress: () => Get.toNamed(Routes.DATA_OUTLET, arguments: {
            'forceSetAddress': false,
          })),
  CardMenu(
    title: "Agen Sales Wispay",
    subTitle: "Kode agen, no. hp",
    icon: Assets.iconsAccountAgent.path,
    route: Routes.SALES,
  )
];

List<CardMenu> kSettingMenu = [
  CardMenu(
    title: "Pengaturan Keuntungan",
    subTitle: "Atur keuntungan kios",
    icon: Assets.iconsAccountCommision.path,
    route: Routes.COMMISION_SETTING,
  ),
  CardMenu(
    title: "Cetak Bukti",
    subTitle: "Pencetakan Bukti Transaksi",
    icon: Assets.iconsAccountPrint.path,
    route: Routes.CETAK_BUKTI,
  )
];

List<CardMenu> kReferralMenu = [
  CardMenu(
    title: "Kode Undangan",
    subTitle: "Ajak teman, dapat uang  💸",
    icon: Assets.iconsAccountCommision.path,
    route: Routes.REFERRAL,
  ),
];

List<CardMenu> kPinMenu = [
  CardMenu(
    title: 'Ubah PIN',
    icon: Assets.iconsAccountPin.path,
  ),
];

List<CardMenu> kHelpMenu = [
  CardMenu(
    title: 'Bantuan',
    icon: Assets.iconsAccountBantuan.path,
    route: Routes.HELP,
  ),
];

List<CardMenu> kLogoutMenu = [
  CardMenu(
    title: 'Logout',
    icon: Assets.iconsAccountLogout.path,
  ),
];

List<CardMenu> kAccountHelp = [
  CardMenu(
    title: 'Pusat Bantuan',
    icon: Assets.iconsAccountPusatBantuan.path,
    subTitle: 'Kumpulan topik yang sering ditanyakan',
    route: Routes.HELP_CENTER,
  ),
  CardMenu(
    title: 'Telephone',
    icon: Assets.iconsAccountTelepone.path,
    subTitle: '081234567890',
  ),
  CardMenu(
    title: 'Email',
    icon: Assets.iconsAccountEmail.path,
    subTitle: 'helpdesk@wis-pay.com',
  ),
  CardMenu(
    title: 'Live Chat',
    icon: Assets.iconsAccountLiveChat.path,
    subTitle: 'Chat langsung dengan Customer Service',
  ),
];

List<CardMenu> kHelpCenter = [
  CardMenu(
    title: 'Login',
    icon: Assets.iconsAccountLogin.path,
    route: Routes.ARTICLE_LIST,
  ),
  CardMenu(
    title: 'Akun Saya',
    icon: Assets.iconsAccountAkunSaya.path,
    route: Routes.ARTICLE_LIST,
  ),
  CardMenu(
    title: 'Top-up Saldo',
    icon: Assets.iconsAccountTopUp.path,
    route: Routes.ARTICLE_LIST,
  ),
  CardMenu(
    title: 'Transaksi Saya',
    icon: Assets.iconsAccountTransaksiSaya.path,
    route: Routes.ARTICLE_LIST,
  ),
];

List<CardMenu> kArticleList = [
  const CardMenu(
    title: 'Artikel 1',
    icon: '',
    route: Routes.ARTICLE,
  ),
  const CardMenu(
    title: 'Artikel 2',
    icon: '',
    route: Routes.ARTICLE,
  ),
  const CardMenu(
    title: 'Artikel 3',
    icon: '',
    route: Routes.ARTICLE,
  ),
  const CardMenu(
    title: 'Artikel 4',
    icon: '',
    route: Routes.ARTICLE,
  ),
];

List<CardMenu> kTransferMenuOutlet = [
  CardMenu(
    title: 'Sesama Outlet',
    subTitle: 'Transfer Saldo Wispay ke sesama Outlet',
    icon: Assets.iconsOutletRounded.path,
    route: Routes.TRANSFER_FORM,
    nextScreenTitle: 'OUTLET',
  ),
];

List<CardMenu> kTransferMenuCustomer = [
  CardMenu(
    title: 'Nomor Pelanggan',
    subTitle: 'Transfer Saldo Wispay ke Pelanggan',
    isNew: true,
    icon: Assets.iconsAccountRounded.path,
    route: Routes.TRANSFER_FORM,
    nextScreenTitle: 'PARTNER',
  ),
];

// ASURANSI GADGET
List<CardMenu> kListBenefitGadget = [
  CardMenu(
    title: 'Cari dan Pilih',
    subTitle: 'Cari dan pilih jenis asuransi yang paling sesuai dengan kebutuhan Anda',
    icon: Assets.iconsInsuranceCari.path,
  ),
  CardMenu(
    title: 'Isi Data',
    subTitle: 'Isi data dengan cepat dan mudah, semua proses hanya dengan beberapa klik',
    icon: Assets.iconsInsuranceIsidata.path,
  ),
  CardMenu(
    title: 'Terlindungi',
    subTitle: 'Kini tidak perlu khawatir Perlindungan maksimal dalam genggaman Anda',
    icon: Assets.iconsInsuranceTerlindungi.path,
  ),
];

List<CardMenu> kListClaimGadget = [
  CardMenu(
    title: 'Polis Asuransi',
    subTitle: 'Pastikan polis asuransi kamu dalam keadaan aktif saat pengajuan klaim.',
    icon: Assets.iconsInsuranceInvoice.path,
  ),
  CardMenu(
    title: 'Ikuti Petunjuk Pengajuan',
    subTitle:
        'Setiap perusahaan Asuransi memiliki prosedur yang berbeda-beda, ikuti petunjuk pengajuan klaim seperti yang tertera di dalam polis asuransi kamu.',
    icon: Assets.iconsInsurancePetunjuk.path,
  ),
  CardMenu(
    title: 'Batas Waktu',
    subTitle:
        'Perhatikan batas waktu maksimum yang telah ditentukan antara terjadinya peristiwa dan tanggal klaim kamu.',
    icon: Assets.iconsInsuranceBataswaktu.path,
  ),
  CardMenu(
    title: 'Formulir Klaim',
    subTitle: 'Isi formulir klaim dan siapkan dokumen pendukung yang dibutuhkan.',
    icon: Assets.iconsInsuranceFormulir.path,
  ),
];
