const String ACCESS_TOKEN = "access_token";
const String FCM_TOKEN = "FCM_TOKEN";
const String REFRESH_TOKEN = "refresh_token";
const String FINISH_ONBOARDING = "finish_onboarding";
const String PHONE_NUMBER = "phone_number";
const String DELIVERY_ADDRESS = "delivery_address";
const String REFERRAL_CODE = "referral_code";
const String ALL = 'ALL';

const String TELEKOMUNIKASI = "TELEKOMUNIKASI";
const String TAGIHAN = "TAGIHAN";
const String UANG_ELEKTRONIK = "UANG_ELEKTRONIK";
const String HIBURAN = "HIBURAN";
const String LAYANAN_FINANSIAL = "LAYANAN_FINANSIAL";

const String UPDATE_PIN = "UPDATE_PIN";

const String OUTLET_TO_SELLER = 'OUTLET_TO_SELLER';
const String OUTLET_TO_ADMIN = 'OUTLET_TO_ADMIN';

const String PENDING = 'PENDING';
const String DECLINE = 'DECLINE';
const String ACCEPT = 'ACCEPT';

const String WAITING_PAYMENT = 'WAITING_PAYMENT';

// gift kind
const String BALANCE = 'BALANCE';
const String CASHBACK = 'CASHBACK';
const String ITEM = 'ITEM';
const String LOTTERY = 'LOTTERY';

// sellout kinds
const String SELLOUT_KIND_MODEM = 'MODEM';
const String SELLOUT_KIND_HANDPHONE = 'HANDPHONE';
