abstract class ComissionKeys {
  static const String VOUCHER_FISIK = "voucher_fisik";
  static const String ALL = "all";
  static const String PULSA = "pulsa";
  static const String PLN = "pln";
  static const String BPJS = "bpjs";
  static const String PDAM = "pdam";
  static const String PAKET_DATA = "paket_data";
  static const String VOUCHER_GAME = "voucher_game";
  static const String UANG_ELEKTRONIK = "uang_elektronik";
  static const String TELKOM = "telkom";
  static const String DOMPET_ELEKTRONIK = "dompet_elektronik";
  static const String TELEVISI = "televisi";
  static const String MULTI_FINANCE = "multi_finance";
  static const String STREAMING = "streaming";
  static const String KIRIM_UANG = "kirim_uang";
  static const String SIM_CARD = "sim_card";
  static const String MODEM = "modem";
  static const String PULSA_PASCABAYAR = "pulsa_pascabayar";
}
