class Spacing {
  const Spacing._();

  /// 2
  static const double xxSmall = 2.0;

  /// 4
  static const double xSmall = 4.0;

  /// 8
  static const double small = 8.0;

  /// 12
  static const double medium = 12.0;

  /// 16
  static const double defaultSpacing = 16.0;

  /// 20
  static const double xMedium = 20.0;

  /// 24
  static const double large = 24.0;
}
