abstract class ProductCode {
  static const PHONE_CREDIT = 'PHONE_CREDIT';
  static const DATA_PLAN = 'DATA_PLAN';
  static const PLN = 'PLN';
  static const PDAM = 'PDAM';
  static const BPJS = 'BPJS';
  static const EWALLET = 'EWALLET';
  static const VOUCHER_GAME = 'VOUCHER_GAME';
  static const EMONEY = 'EMONEY';
  static const PASCABAYAR = 'PASCABAYAR';
  static const TELKOM = 'TELKOM';
  static const TELKOM_TAGIHAN_INDIHOME = 'TELKOM_TAGIHAN_INDIHOME';
  static const TELKOM_TAGIHAN_TELPON = 'TELKOM_TAGIHAN_TELPON';
  static const PHYSICAL_VOUCHER = 'PHYSICAL_VOUCHER';
  static const REMITTANCE = 'REMITTANCE';
  static const INSURANCE = 'INSURANCE';
  static const MODEM = 'MODEM';
  static const SIM_CARD = 'SIM_CARD';
  static const STREAMING = 'STREAMING';
  static const MULTIFINANCE = 'MULTIFINANCE';
  static const TSEL_OMNI = 'TSEL_OMNI';
  static const SELL_OUT = 'SELL_OUT';
  static const TSEL_OMNI_TELKOMSEL = 'TSEL_OMNI_TELKOMSEL';
}
