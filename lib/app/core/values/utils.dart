const SALDO_WALLET_INOUT = ["OUT", "IN"];

class OTPType {
  static const String SIGN_UP = "SIGN_UP";
  static const String SIGN_IN = "SIGN_IN";
  static const String FORGOT_PIN = "FORGOT_PIN";
  static const String UPDATE_PIN = "UPDATE_PIN";
  static const String EMAIL_VERIFICATION = "EMAIL_VERIFICATION ";
  static const String UPDATE_PHONE = "UPDATE_PHONE ";
}

enum bannerEnum { ARTICLE, PROMO }

const bannerType = {
  bannerEnum.ARTICLE: 'Article',
  bannerEnum.PROMO: 'Promo',
};
