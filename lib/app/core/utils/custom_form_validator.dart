// Package imports:
import 'package:form_field_validator/form_field_validator.dart';

class IndonesiaPhoneNumber extends TextFieldValidator {
  IndonesiaPhoneNumber({String errorText = 'enter a valid phone number!'}) : super(errorText);

  @override
  bool get ignoreEmptyValues => false;

  @override
  bool isValid(String? value) {
    if (value == null || value.isEmpty) return false;

    return hasMatch(r'(^(\+?62|0)([0-9]{2,3})?[0-9]{9,10}$)', value);
  }
}
