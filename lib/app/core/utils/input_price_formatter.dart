// Flutter imports:
import 'package:flutter/services.dart';

// Package imports:
import 'package:intl/intl.dart';

class InputPriceFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.compareTo(oldValue.text) != 0) {
      int selectionIndexFromTheRight = newValue.text.length - newValue.selection.end;
      NumberFormat formatter = NumberFormat('#,###,###', 'ID_id');

      int? number = int.tryParse(newValue.text.replaceAll(formatter.symbols.GROUP_SEP, '')) ?? 0;
      String newString = formatter.format(number);
      int offset = newString == '0' && newString.length == 1 ? 1 : newString.length - selectionIndexFromTheRight;

      return TextEditingValue(
        text: newString,
        selection: TextSelection.collapsed(offset: offset),
      );
    } else {
      return newValue;
    }
  }
}
