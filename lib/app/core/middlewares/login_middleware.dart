// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/services/storage_service.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class LoginMiddleware extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    StorageService _storage = Get.find<StorageService>();
    bool? finishOnboarding = _storage.getfinishOnboarding();
    return finishOnboarding == true ? null : const RouteSettings(name: Routes.ONBOARDING);
  }
}
