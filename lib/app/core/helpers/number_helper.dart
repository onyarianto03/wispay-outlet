// Package imports:
// ignore_for_file: valid_regexps

import 'package:intl/intl.dart';

class NumberHelper {
  // convert phone number with country code +62
  static String formatPhoneToIndonesian(String phone) {
    String newPhone = phone.replaceFirst(RegExp(r"(0|620|62)"), '+62');
    return newPhone;
  }

  // convert phone number with country code +62
  static String formatPhoneRemove62(String phone) {
    if (phone.startsWith('+62')) {
      String newPhone = phone.replaceAll('+62', '0');
      return newPhone;
    }
    return phone;
  }

  static String formatMoney(dynamic value) {
    double numberToFormat = double.parse(value?.toString() ?? '0.0');
    NumberFormat formatter = NumberFormat(
      'Rp###,###,###',
      'ID_id',
    );

    return formatter.format(numberToFormat);
  }

  static String formatMoneyWithoutSymbol(String? value) {
    double numberToFormat = double.parse(value ?? '0.0');
    NumberFormat formatter = NumberFormat(
      '###,###,###',
      'ID_id',
    );

    return formatter.format(numberToFormat);
  }
}
