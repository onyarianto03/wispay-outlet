// Project imports:
import 'package:wispay_outlet/app/core/values/comission_keys.dart';
import 'package:wispay_outlet/app/core/values/product_code.dart';
import 'package:wispay_outlet/app/core/values/strings.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class ProductCategoryHelper {
  static String? getProductNavigation(String? code) {
    switch (code) {
      case ProductCode.SELL_OUT:
        return Routes.SELL_OUT;
      case ProductCode.TSEL_OMNI:
        return Routes.OMNI;
      case ProductCode.VOUCHER_GAME:
        return Routes.VOUCHER_GAME;
      case ProductCode.STREAMING:
        return Routes.STREAMING;
      case ProductCode.MODEM:
        return Routes.MODEM;
      case ProductCode.EMONEY:
        return Routes.EMONEY;
      case ProductCode.EWALLET:
        return Routes.E_WALLET;
      case ProductCode.SIM_CARD:
        return Routes.SIM_CARD;
      case ProductCode.PLN:
        return Routes.PLN;
      case ProductCode.PDAM:
        return Routes.PDAM;
      case ProductCode.MULTIFINANCE:
        return Routes.MULTIFINANCE;
      case ProductCode.PASCABAYAR:
        return Routes.PASCABAYAR;
      case ProductCode.PHYSICAL_VOUCHER:
        return Routes.PHYSICAL_VOUCER;
      case ProductCode.INSURANCE:
        return Routes.ASURANSI_GADGET;
      case ProductCode.PHONE_CREDIT:
        return Routes.PULSA_DATA;
      case ProductCode.BPJS:
        return Routes.BPJS;
      case ProductCode.TELKOM:
        return Routes.TELKOM;
      case ProductCode.TELKOM_TAGIHAN_INDIHOME:
        return Routes.TELKOM_INDIHOME;
      case ProductCode.TELKOM_TAGIHAN_TELPON:
        return Routes.TELKOM_TELPON;
      case ProductCode.REMITTANCE:
        return Routes.SEND_MONEY;
      default:
        return Routes.CATEGORIES;
    }
  }

  static String getComissionKey(String code) {
    switch (code) {
      case ProductCode.BPJS:
        return ComissionKeys.BPJS;
      case ProductCode.TELKOM:
      case ProductCode.TELKOM_TAGIHAN_INDIHOME:
      case ProductCode.TELKOM_TAGIHAN_TELPON:
        return ComissionKeys.TELKOM;
      case ProductCode.PHONE_CREDIT:
        return ComissionKeys.PULSA;
      case ProductCode.PLN:
        return ComissionKeys.PLN;
      case ProductCode.PDAM:
        return ComissionKeys.PDAM;
      case ProductCode.MULTIFINANCE:
        return ComissionKeys.MULTI_FINANCE;
      case ProductCode.PASCABAYAR:
        return ComissionKeys.PULSA_PASCABAYAR;
      case ProductCode.PHYSICAL_VOUCHER:
        return ComissionKeys.VOUCHER_FISIK;
      case ProductCode.INSURANCE:
        return ComissionKeys.ALL;
      case ProductCode.EMONEY:
        return ComissionKeys.UANG_ELEKTRONIK;
      case ProductCode.EWALLET:
        return ComissionKeys.DOMPET_ELEKTRONIK;
      case ProductCode.SIM_CARD:
        return ComissionKeys.SIM_CARD;
      case ProductCode.STREAMING:
        return ComissionKeys.STREAMING;
      case ProductCode.VOUCHER_GAME:
        return ComissionKeys.VOUCHER_GAME;
      case ProductCode.TSEL_OMNI:
        return ComissionKeys.PAKET_DATA;
      case ProductCode.SELL_OUT:
        return ComissionKeys.ALL;
      case ProductCode.MODEM:
        return ComissionKeys.MODEM;
      case ProductCode.REMITTANCE:
        return ComissionKeys.KIRIM_UANG;
      default:
        return ComissionKeys.ALL;
    }
  }

  String? getProductGroupFromCode(String code) {
    switch (code) {
      case ProductCode.PHONE_CREDIT:
        return TELEKOMUNIKASI;
      case ProductCode.DATA_PLAN:
        return TELEKOMUNIKASI;
      case ProductCode.PLN:
        return TAGIHAN;
      case ProductCode.PDAM:
        return TAGIHAN;
      case ProductCode.BPJS:
        return TAGIHAN;
      case ProductCode.EWALLET:
        return UANG_ELEKTRONIK;
      case ProductCode.VOUCHER_GAME:
        return HIBURAN;
      case ProductCode.EMONEY:
        return UANG_ELEKTRONIK;
      case ProductCode.PASCABAYAR:
        return TELEKOMUNIKASI;
      case ProductCode.TELKOM:
        return TAGIHAN;
      case ProductCode.PHYSICAL_VOUCHER:
        return TELEKOMUNIKASI;
      case ProductCode.REMITTANCE:
        return UANG_ELEKTRONIK;
      case ProductCode.INSURANCE:
        return LAYANAN_FINANSIAL;
      case ProductCode.MODEM:
        return TELEKOMUNIKASI;
      case ProductCode.SIM_CARD:
        return TELEKOMUNIKASI;
      case ProductCode.STREAMING:
        return HIBURAN;
      case ProductCode.MULTIFINANCE:
        return TAGIHAN;
      case ProductCode.TSEL_OMNI:
        return TELEKOMUNIKASI;
      case ProductCode.SELL_OUT:
        return TELEKOMUNIKASI;
      default:
        return null;
    }
  }

  static String? productCodeToProductName(String productCode) {
    switch (productCode) {
      case ProductCode.PHONE_CREDIT:
        return 'Pulsa & Paket Data';
      case ProductCode.DATA_PLAN:
        return 'Paket Data';
      case ProductCode.PLN:
        return 'PLN';
      case ProductCode.PDAM:
        return 'PDAM';
      case ProductCode.BPJS:
        return 'BPJS';
      case ProductCode.EWALLET:
        return 'Dompet Elektronik';
      case ProductCode.VOUCHER_GAME:
        return 'Voucher Game';
      case ProductCode.EMONEY:
        return 'Uang Elektronik';
      case ProductCode.PASCABAYAR:
        return 'Pascabayar';
      case ProductCode.TELKOM:
        return 'Telkom';
      case ProductCode.PHYSICAL_VOUCHER:
        return 'Voucher Fisik';
      case ProductCode.REMITTANCE:
        return 'Kirim Tunai';
      case ProductCode.INSURANCE:
        return 'Asuransi';
      case ProductCode.MODEM:
        return 'Modem';
      case ProductCode.SIM_CARD:
        return 'Kartu Perdana';
      case ProductCode.STREAMING:
        return 'Streaming';
      case ProductCode.MULTIFINANCE:
        return 'Tagihan Kredit';
      case ProductCode.TSEL_OMNI:
        return 'Tsel Omni';
      case ProductCode.SELL_OUT:
        return 'Sell Out';
      default:
        return null;
    }
  }

  static String? groupKeyToName(String groupKey) {
    switch (groupKey) {
      case TELEKOMUNIKASI:
        return 'Telekomunikasi';
      case TAGIHAN:
        return 'Tagihan';
      case UANG_ELEKTRONIK:
        return 'Uang Elektronik';
      case HIBURAN:
        return 'Hiburan';
      case LAYANAN_FINANSIAL:
        return 'Layanan Finansial';
      default:
        return null;
    }
  }
}
