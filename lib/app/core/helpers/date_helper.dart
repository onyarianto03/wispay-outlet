// Package imports:
import 'package:jiffy/jiffy.dart';

class DateHelper {
  static String formatDate(String? date, {String format = 'dd/MM/yyyy'}) {
    if (date != null) {
      return Jiffy(date).format(format);
    }
    return '-';
  }
}
