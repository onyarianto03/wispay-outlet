// Package imports:
import 'package:encrypt/encrypt.dart';

Map<String, dynamic> options = {
  'iv': IV.fromUtf8('1614556800000000'),
  'mode': AESMode.cbc,
  'key': Key.fromUtf8('89B4FF7EFAE3EF1811DF9A1CB18774D5'),
};

String encrypt(String text) {
  final encrypter = Encrypter(AES(options['key'], mode: options['mode']));
  final encrypted = encrypter.encrypt(text, iv: options['iv']);
  return encrypted.base64;
}

String decrypt(String? text) {
  if (text == null) {
    return '';
  }
  final Encrypted encrypted = Encrypted.fromBase64(text);
  final encrypter = Encrypter(AES(options['key'], mode: options['mode']));
  final decrypted = encrypter.decrypt(encrypted, iv: options['iv']);
  return decrypted;
}
