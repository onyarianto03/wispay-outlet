class TransactionKind {
  static const String kGENERAL = "GENERAL";
  static const String kTRANSACTION = "TRANSACTION";
  static const String kTOPUP = "TOPUP";
  static const String kREFERRAL = "REFERRAL";
  static const String kREFUND = "REFUND";
  static const String kREDUCE = "REDUCE";
  static const String kGIFT = "GIFT";
  static const String kTRANSFER = "TRANSFER";
  static const String kIN = 'IN';
  static const String kOUT = 'OUT';

  String humanizeTransactionKind(String? kind, {String? inOut}) {
    switch (kind) {
      case TransactionKind.kGENERAL:
        return "Umum";
      case TransactionKind.kTRANSACTION:
        return "Pembayaran";
      case TransactionKind.kTOPUP:
        return "Top-Up Saldo";
      case TransactionKind.kREFERRAL:
        return "Komisi Wismart";
      case TransactionKind.kREFUND:
        return "Pengembalian Dana";
      case TransactionKind.kREDUCE:
        return "Pengurangan Saldo o/ Admin";
      case TransactionKind.kGIFT:
        return "Hadiah";
      case TransactionKind.kTRANSFER:
        if (inOut == kIN) {
          return "Transfer Masuk";
        }
        return "Transfer Keluar";

      default:
        return "";
    }
  }
}
