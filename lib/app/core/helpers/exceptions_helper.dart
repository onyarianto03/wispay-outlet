// Package imports:

import 'package:dio/dio.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/http.dart';

class DioExceptions implements Exception {
  DioExceptions.fromDioError(DioError dioError) {
    switch (dioError.type) {
      case DioErrorType.cancel:
        message = "Request to API server was cancelled";
        break;
      case DioErrorType.connectTimeout:
        message = "Connection timeout with API server";
        break;
      case DioErrorType.receiveTimeout:
        message = "Receive timeout in connection with API server";
        break;
      case DioErrorType.response:
        message = _handleError(
          dioError.response?.statusCode,
          dioError.response?.data,
        );
        break;
      case DioErrorType.sendTimeout:
        message = "Send timeout in connection with API server";
        break;
      default:
        message = _handleError(
          dioError.response?.statusCode,
          dioError.response?.data,
        );
        break;
    }
  }

  late String message;

  String _handleError(int? statusCode, dynamic error) {
    switch (statusCode) {
      case HTTP_BAD_REQUEST:
        return 'Bad request';
      case HTTP_NOT_FOUND:
        return error["data"]["message"];
      case HTTP_INTERNAL_SERVER_ERROR:
        return 'Internal server error';
      case 422:
        if (error["data"]["message"].runtimeType != String) {
          var _d = Map<String, dynamic>.from(error["data"]['message']);

          return _d['base'][0];
        }
        return error["data"]["message"];
      case 410:
        return error["data"]["message"];
      case HTTP_UNAUTHORIZED:
        return 'Unauthorized';
      default:
        return 'Oops something went wrong';
    }
  }

  @override
  String toString() => message;
}
