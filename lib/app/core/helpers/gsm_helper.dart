// Project imports:
import 'package:wispay_outlet/generated/assets.gen.dart';

List<String> telkomsel = ['0811', '0812', '0813', '0821', '0822', '0823', '0851', '0852', '0853'];
List<String> indosat = ['0814', '0815', '0816', '0855', '0856', '0857', '0858'];
List<String> xl = ['0817', '0818', '0819', '0859', '0877', '0878', '0879'];
List<String> three = ['0890', '0891', '0892', '0893', '0894', '0895', '0896', '0897', '0898', '0899'];
List<String> smartfren = ['0881', '0882', '0883', '0884', '0885', '0886', '0887', '0888', '0889'];
List<String> axis = ['0831', '0832', '0833', '0834', '0835', '0836', '0837', '0838', '0839'];

const TELKOMSEL = 'telkomsel';
const INDOSAT = 'indosat';
const XL = 'xl';
const THREE = 'three';
const SMARTFREN = 'smartfren';
const AXIS = 'axis';

List _operatorLogo = [
  Assets.iconsGsmTelkomsel.image(height: 45),
  Assets.iconsGsmIndosat.image(height: 45),
  Assets.iconsGsmXl.image(height: 45),
  Assets.iconsGsm3.image(height: 45),
  Assets.iconsGsmSmartfren.image(height: 45),
  Assets.iconsGsmAxis.image(height: 45),
];

class GSMHelper {
  static String getOperatorName(String phoneNumber) {
    if (telkomsel.contains(phoneNumber.substring(0, 4))) {
      return TELKOMSEL;
    } else if (indosat.contains(phoneNumber.substring(0, 4))) {
      return INDOSAT;
    } else if (xl.contains(phoneNumber.substring(0, 4))) {
      return XL;
    } else if (three.contains(phoneNumber.substring(0, 4))) {
      return THREE;
    } else if (smartfren.contains(phoneNumber.substring(0, 4))) {
      return SMARTFREN;
    } else if (axis.contains(phoneNumber.substring(0, 4))) {
      return AXIS;
    } else {
      return "";
    }
  }

  static getOperatorLogo(String number) {
    if (number.length >= 4) {
      if (telkomsel.contains(number.substring(0, 4))) {
        return _operatorLogo[0];
      } else if (indosat.contains(number.substring(0, 4))) {
        return _operatorLogo[1];
      } else if (xl.contains(number.substring(0, 4))) {
        return _operatorLogo[2];
      } else if (three.contains(number.substring(0, 4))) {
        return _operatorLogo[3];
      } else if (smartfren.contains(number.substring(0, 4))) {
        return _operatorLogo[4];
      } else {
        return null;
      }
    }
    return null;
  }
}
