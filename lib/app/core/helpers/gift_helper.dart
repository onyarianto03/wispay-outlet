import 'package:wispay_outlet/app/core/values/strings.dart';

class GiftHelper {
  static keyToName(String key) {
    switch (key) {
      case BALANCE:
        return 'Saldo';
      case ITEM:
        return 'Barang';
      case CASHBACK:
        return 'Cashback';
      case LOTTERY:
        return 'Voucher Undian';
      default:
        return 'Semua';
    }
  }
}
