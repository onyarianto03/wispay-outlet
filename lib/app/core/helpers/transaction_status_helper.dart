// Project imports:
import 'package:lottie/lottie.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/strings.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class TransactionStatusHelper {
  static const PENDING = 'PENDING';
  static const SUCCESS = 'SUCCESS';
  static const FAILED = 'FAILED';
  static const CANCELED = 'CANCELED';
  static const WAITING_PAYMENT = 'WAITING_PAYMENT';
  static const ALL = 'ALL';

  static const TRANSACTION_STATUS_LIST = [ALL, SUCCESS, PENDING, FAILED, CANCELED, WAITING_PAYMENT];

  static const TRANSACTION_STATUS_MAP = {
    ALL: 'Semua',
    SUCCESS: 'Berhasil',
    PENDING: 'Diproses',
    FAILED: 'Gagal',
    CANCELED: 'Dibatalkan',
    WAITING_PAYMENT: 'Menunggu Pembayaran',
  };

  static statusColorAndText(String status) {
    switch (status) {
      case SUCCESS:
        return {
          'color': WispayColors.cGreen,
          'text': 'BERHASIL',
        };
      case FAILED:
        return {
          'color': WispayColors.cRed,
          'text': 'GAGAL',
        };
      case CANCELED:
        return {
          'color': WispayColors.cRed,
          'text': 'DIBATALKAN',
        };
      case DECLINE:
        return {
          'color': WispayColors.cRed,
          'text': 'DIBATALKAN',
        };
      case ACCEPT:
        return {
          'color': WispayColors.cGreen,
          'text': 'SELESAI',
        };
      case WAITING_PAYMENT:
        return {
          'color': WispayColors.cWarning,
          'text': 'MENUNGGU PEMBAYARAN',
        };

      default:
        return {
          'color': WispayColors.cPrimary,
          'text': 'SEDANG DIPROSES',
        };
    }
  }

  static String paymentStatus(String status) {
    switch (status) {
      case PENDING:
        return 'Pembayaran Diproses';
      case SUCCESS:
        return 'Pembayaran Berhasil!';
      case FAILED:
        return 'Pembayaran Gagal';
      default:
        return 'Pembayaran Gagal';
    }
  }

  static String getDescription(String status) {
    switch (status) {
      case PENDING:
        return 'Terima kasih, pembayaranmu sedang kami proses. Untuk memantau status pembayaran, cek rincian transaksi.';
      case SUCCESS:
        return 'Terima kasih sudah bertransaksi di WisPay.';
      case FAILED:
        return 'Maaf, pembayaranmu gagal diproses. Silakan coba lagi.';
      default:
        return 'Pembayaran gagal';
    }
  }

  static LottieBuilder getIcon(String status) {
    switch (status) {
      case PENDING:
        return Lottie.asset(Assets.lottiePayPending, width: 150);
      case SUCCESS:
        return Lottie.asset(Assets.lottiePaySuccess, width: 150);
      case FAILED:
        return Lottie.asset(Assets.lottiePayFailed, width: 150);
      default:
        return Lottie.asset(Assets.lottiePayPending, width: 150);
    }
  }
}
