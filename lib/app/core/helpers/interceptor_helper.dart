// ignore_for_file: unnecessary_new

// Package imports:
import 'package:dio/dio.dart' as dio;
import 'package:get/get.dart';

// Project imports:
import 'package:wispay_outlet/app/core/services/api_service.dart';
import 'package:wispay_outlet/app/core/services/storage_service.dart';
import 'package:wispay_outlet/app/core/values/http.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/routes/app_pages.dart';

class InterceptorHelper {
  onRequest(dio.RequestOptions request, dio.RequestInterceptorHandler handler) {
    StorageService _storage = Get.find();

    String? token = _storage.getAccessToken();
    if (request.method == 'GET' || request.method == 'delete' || request.data == null) {
      request.data = true;
    }

    if (token != null) {
      request.headers['X-AUTH-TOKEN'] = token;
    }
    request.headers['Content-Type'] = 'application/json';
    return handler.next(request);
  }

  onResponse(dio.Response response, dio.ResponseInterceptorHandler handler) async {
    dio.Dio api = apiService();
    if (response.statusCode! >= HTTP_BAD_REQUEST) {
      if (response.statusCode == HTTP_UNAUTHORIZED) {
        var token = await _getAccess();
        if (token != null) {
          response.requestOptions.headers['X-AUTH-TOKEN'] = token;
          final opts = new dio.Options(
            method: response.requestOptions.method,
            headers: response.requestOptions.headers,
          );
          final cloneReq = await api.request(
            response.requestOptions.path,
            options: opts,
            data: response.requestOptions.data,
            queryParameters: response.requestOptions.queryParameters,
          );
          return handler.resolve(cloneReq);
        } else {
          Get.offNamed(Routes.LOGIN);
        }
      }

      final error = dio.DioError(
        requestOptions: response.requestOptions,
        type: dio.DioErrorType.response,
        response: response,
      );

      return handler.reject(error);
    }

    return handler.next(response);
  }

  _getAccess() async {
    // final StorageService _storage = StorageService(_getStorage);
    StorageService _storage = Get.find();
    var refreshToken = _storage.getRefreshToken();
    dio.Dio api = apiService();

    Map<String, dynamic> consumerToken = {
      'refresh_token': refreshToken,
    };

    api.post(REFRESH_TOKEN_URL, data: {
      'outlet_token': consumerToken,
    }).then((value) {
      _storage.saveToken(value.data);
      return value.data;
    }).catchError((_) {
      _storage.removeToken();
      return null;
    });
  }
}
