// Flutter imports:
import 'package:flutter/material.dart';

const BoxShadow shadow = BoxShadow(
  color: Colors.black12,
  blurRadius: 5,
  spreadRadius: 0.5,
);

BoxShadow buildShadow({
  double blurRadius = 5,
  double spreadRadius = 0.5,
  Offset offset = Offset.zero,
}) {
  return BoxShadow(
    color: Colors.black.withOpacity(0.06),
    blurRadius: blurRadius,
    spreadRadius: spreadRadius,
    offset: offset,
  );
}
