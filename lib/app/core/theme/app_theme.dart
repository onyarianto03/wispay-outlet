// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Project imports:
import 'package:wispay_outlet/app/core/theme/text_theme.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';

class MyThemes {
  static final light = ThemeData(
      fontFamily: 'SourceSansPro',
      appBarTheme: AppBarTheme(
        color: Colors.white,
        toolbarTextStyle: const TextStyle(fontFamily: 'SourceSansPro'),
        titleTextStyle: kHeadline4Style,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: WispayColors.cSecondary.withAlpha(50),
        ),
        elevation: 0.1,
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      primaryColor: WispayColors.cPrimary,
      inputDecorationTheme: InputDecorationTheme(
        fillColor: WispayColors.cDisabledColor,
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.red, width: 2.0),
          borderRadius: BorderRadius.circular(8),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: WispayColors.cBorderColor, width: 1),
          borderRadius: BorderRadius.circular(8),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.black, width: 2),
          borderRadius: BorderRadius.circular(8),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: WispayColors.cBorderColor, width: 1),
          borderRadius: BorderRadius.circular(8),
        ),
      ),
      textTheme: const TextTheme().copyWith(
        bodyText1: kBodyText1Style,
        bodyText2: kBodyText2Style,
        headline4: kHeadline4Style,
        headline5: kHeadline5Style,
      ));

  static final dark = ThemeData.dark()
      .copyWith(primaryColor: Colors.black, scaffoldBackgroundColor: Colors.black, cardColor: Colors.black);
}
