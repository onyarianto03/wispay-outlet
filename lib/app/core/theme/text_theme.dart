// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';

const kHeadline4Style = TextStyle(
  fontSize: FontSize.xLarge,
  fontWeight: FontWeight.w600,
  color: WispayColors.cBlack,
  height: 1.5,
  fontFamily: 'SourceSansPro',
);

const kHeadline5Style = TextStyle(
  fontSize: FontSize.large,
  fontWeight: FontWeight.bold,
  color: WispayColors.cBlack333,
  height: 1,
  fontStyle: FontStyle.normal,
);

const kBodyText2Style = TextStyle(
  fontSize: FontSize.small,
  fontWeight: FontWeight.w400,
  color: WispayColors.cBlack666,
  height: 1.25,
);

const kBodyText1Style = TextStyle(
  fontSize: FontSize.defaultSize,
  fontWeight: FontWeight.w400,
  color: WispayColors.cBlack666,
  height: 1.25,
  fontFamily: 'SourceSansPro',
);
