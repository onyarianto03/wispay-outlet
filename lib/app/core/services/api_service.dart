// Package imports:
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/interceptor_helper.dart';

var options = BaseOptions(
  baseUrl: dotenv.get('BASE_URL', fallback: 'https://staging-api.wis-pay.com/outlet/v1'),
  connectTimeout: 50000,
  receiveTimeout: 50000,
  contentType: 'application/json',
  receiveDataWhenStatusError: true,
  validateStatus: (status) {
    return status! > 0;
  },
);

Dio apiService() {
  Dio api = Dio(options);
  InterceptorHelper interceptorHelper = InterceptorHelper();

  api.interceptors.add(PrettyDioLogger(
    compact: true,
    responseBody: true,
    responseHeader: false,
    request: true,
    requestBody: true,
    requestHeader: true,
  ));

  api.interceptors.add(
    InterceptorsWrapper(
      onRequest: (r, h) => interceptorHelper.onRequest(r, h),
      onResponse: (r, h) => interceptorHelper.onResponse(r, h),
    ),
  );

  return api;
}
