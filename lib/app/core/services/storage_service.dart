// Package imports:
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/strings.dart';
import 'package:wispay_outlet/app/data/models/token/token_model.dart';
import 'package:wispay_outlet/app/data/models/utility/prediction_model.dart';

class StorageService extends GetxService {
  final GetStorage _storage = GetStorage();

  Future<StorageService> init() async {
    return this;
  }

  void savePhoneNumber(String phoneNumber) {
    _storage.write(PHONE_NUMBER, phoneNumber);
  }

  String getPhoneNumber() {
    return _storage.read(PHONE_NUMBER);
  }

  void saveToken(TokenModel? data) async {
    String? accessToken = data?.token;
    String? refreshToken = data?.refreshToken;

    await _storage.write(ACCESS_TOKEN, accessToken);
    await _storage.write(REFRESH_TOKEN, refreshToken);
  }

  void saveFCM(String fcm) async {
    await _storage.write(FCM_TOKEN, fcm);
  }

  String? getFCM() => _storage.read(FCM_TOKEN);

  String? getRefreshToken() {
    return _storage.read(REFRESH_TOKEN);
  }

  String? getAccessToken() {
    return _storage.read(ACCESS_TOKEN);
  }

  void removeToken() async {
    await _storage.remove(ACCESS_TOKEN);
    await _storage.remove(REFRESH_TOKEN);
  }

  void finishOnboarding() async {
    await _storage.write(FINISH_ONBOARDING, true);
  }

  bool? getfinishOnboarding() {
    return _storage.read(FINISH_ONBOARDING);
  }

  void saveDeliveryAddress(LocationDataModel deliveryAddress) async {
    await _storage.write(DELIVERY_ADDRESS, deliveryAddress.toJson());
  }

  LocationDataModel? getDeliveryAddress() {
    final Map<String, dynamic>? _address = _storage.read(DELIVERY_ADDRESS);
    if (_address != null) {
      return LocationDataModel.fromJson(_address);
    } else {
      return null;
    }
  }

  void saveReferralCode(String? code) async {
    await _storage.write(REFERRAL_CODE, code);
  }

  String? getReferralCode() {
    return _storage.read(REFERRAL_CODE);
  }
}
