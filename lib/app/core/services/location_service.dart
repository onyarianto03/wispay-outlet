import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';

class LocationService {
  Future<bool> needToRequestPermission() async {
    LocationPermission _permission = await Geolocator.checkPermission();
    if (_permission == LocationPermission.denied ||
        _permission == LocationPermission.deniedForever ||
        _permission == LocationPermission.unableToDetermine) {
      return true;
    }

    return false;
  }

  Future<Position> getCurrentLocation() async {
    bool _needToRequestPermission = await needToRequestPermission();
    if (_needToRequestPermission) {
      await Geolocator.requestPermission().then(
        (value) async {
          if (value == LocationPermission.always || value == LocationPermission.whileInUse) {
            return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
          }
        },
      );
    }

    return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  Future<Location?> getLocationFromAddress(String address) async {
    try {
      List<Location> locations = await locationFromAddress(address);
      return locations.first;
    } catch (err) {
      WispaySnackbar.showError(err.toString());
      return null;
    }
  }

  Future<Placemark?> getAddressFromLocation(Position position) async {
    try {
      List<Placemark> locations = await placemarkFromCoordinates(position.latitude, position.longitude);
      return locations.first;
    } catch (err) {
      WispaySnackbar.showError(err.toString());
      return null;
    }
  }
}
