import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/services/storage_service.dart';
import 'package:wispay_outlet/app/core/values/colors.dart';

String channel = 'high_importance_channel';

class NotificationService {
  static final NotificationService _notificationService = NotificationService._internal();
  static final FirebaseMessaging _messaging = FirebaseMessaging.instance;
  static final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  factory NotificationService() {
    return _notificationService;
  }

  NotificationService._internal();

  Future<void> init() async {
    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    await _flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(
          AndroidNotificationChannel(
            channel,
            channel,
            showBadge: true,
            importance: Importance.high,
            enableLights: true,
          ),
        );

    const AndroidInitializationSettings _androidSettings = AndroidInitializationSettings('@drawable/ic_notif');
    const initializationSettings = InitializationSettings(android: _androidSettings);

    if (Platform.isAndroid) {
      await _flutterLocalNotificationsPlugin.initialize(initializationSettings);
      await _requestPermission(settings.authorizationStatus);
      await setupInteractedMessage();
    }
  }

  Future<void> _requestPermission(AuthorizationStatus status) async {
    if (status != AuthorizationStatus.authorized || status != AuthorizationStatus.provisional) {
      await _messaging.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );
    } else {
      print('User granted permission: $status');
    }
  }

  static Future<void> showLocalNotificationAlert({required RemoteMessage? message}) async {
    Get.snackbar(
      message?.notification?.title ?? 'Notification',
      message?.notification?.body ?? '',
      duration: const Duration(seconds: 5),
      dismissDirection: SnackDismissDirection.HORIZONTAL,
    );
  }

  void watchNotification() {
    print('Firebase Messaging: Watch incoming notification');

    // On Launch
    FirebaseMessaging.onMessage.listen(
      (RemoteMessage message) async {
        print('Got a message whilst in the foreground!');
        print('Message data: ${message.data}');

        if (message.notification != null) {
          RemoteNotification notification = message.notification!;

          _flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel,
                channel,
                icon: '@drawable/ic_notif',
                color: WispayColors.cPrimary,
                autoCancel: true,
              ),
            ),
          );
          // await showLocalNotificationAlert(message: message);
        }
      },
    );
  }

  Future<void> setupInteractedMessage() async {
    // Get any messages which caused the application to open from
    // a terminated state.
    RemoteMessage? initialMessage = await FirebaseMessaging.instance.getInitialMessage();

    // If the message also contains a data property with a "type" of "chat",
    // navigate to a chat screen
    if (initialMessage != null) {
      _handleMessage(initialMessage);
    }

    // Also handle any interaction when the app is in the background via a
    // Stream listener
    FirebaseMessaging.onMessageOpenedApp.listen(_handleMessage);
  }

  void _handleMessage(RemoteMessage message) {
    print('message');
  }

  void saveToken() async {
    StorageService _storage = Get.find();
    String? token = await _messaging.getToken();
    if (token != null) {
      _storage.saveFCM(token);
      print(token);
    }
  }
}
