import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/core/services/storage_service.dart';

class FirebaseDynamicLinkService {
  static final FirebaseDynamicLinkService _instance = FirebaseDynamicLinkService._internal();
  static final FirebaseDynamicLinks _dynamicLinks = FirebaseDynamicLinks.instance;

  factory FirebaseDynamicLinkService() {
    return _instance;
  }

  FirebaseDynamicLinkService._internal();

  static Future<Uri> generateLink({bool short = true, String? code = ''}) async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      // The Dynamic Link URI domain. You can view created URIs on your Firebase console
      uriPrefix: 'https://wispayoutlet.page.link',
      // The deep Link passed to your application which you can use to affect change
      link: Uri.parse('https://outlet.wis-pay.com?code=$code'),
      // Android application details needed for opening correct app on device/Play Store
      androidParameters: AndroidParameters(
        packageName: dotenv.get('PACKAGE_NAME'),
        minimumVersion: 0,
      ),
      socialMetaTagParameters: SocialMetaTagParameters(
        imageUrl: Uri.parse(
          'https://d12esd8hu6ksp7.cloudfront.net/storages/store/articles/original/7bfb4701-cccd-41c1-9da4-525f504b2c35.png',
        ),
        title: 'Dapatkan Saldo wispay 7.000',
        description:
            'Yuk, unduh aplikasi 𝘄𝗶𝘀𝗽𝗮𝘆 dan gunakan kode $code untuk mendapatkan lebih banyak keuntungannya.',
      ),
    );

    Uri _link = await _dynamicLinks.buildLink(parameters);
    if (short) {
      final ShortDynamicLink shortDynamicLink = await _dynamicLinks.buildShortLink(parameters);
      _link = shortDynamicLink.shortUrl;
    }

    return _link;
  }

  static Future<void> initialize() async {
    final PendingDynamicLinkData? initialLink = await _dynamicLinks.getInitialLink();
    final StorageService _storage = Get.find();
    if (initialLink != null) {
      final Uri deepLink = initialLink.link;
      // Example of using the dynamic link to push the user to a different screen
      print('initial link: $deepLink');
    }

    _dynamicLinks.onLink.listen((event) {
      final Uri deepLink = event.link;
      final code = deepLink.queryParameters['code'];
      if (code != null) {
        _storage.saveReferralCode(code);
      }
    });
  }
}
