import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';
part 'detail_payload_model.g.dart';

@JsonSerializable(explicitToJson: true)
class DetailPayloadModel extends BaseModel {
  DetailPayloadModel({
    String? name,
    String? phone,
    this.numberParticipant,
    this.bill,
    this.period,
    this.power,
    this.iccid,
    this.allowanceDesc,
    this.productLengthDateId,
    this.productName,
    this.email,
    this.finishAt,
    this.orderCode,
    this.partnerName,
    this.startAt,
    this.trxId,
  }) : super(
          name: name,
          phone: phone,
        );

  @JsonKey(name: 'number_participant')
  dynamic numberParticipant;
  @JsonKey(name: 'bill')
  dynamic bill;
  @JsonKey(name: 'period')
  dynamic period;
  @JsonKey(name: 'power')
  dynamic power;
  @JsonKey(name: 'iccid')
  dynamic iccid;
  @JsonKey(name: 'allowance_desc')
  String? allowanceDesc;
  @JsonKey(name: 'product_length_date_id')
  String? productLengthDateId;
  @JsonKey(name: 'product_name')
  String? productName;
  @JsonKey(name: 'trx_id')
  String? trxId;
  @JsonKey(name: 'order_code')
  String? orderCode;
  @JsonKey(name: 'partner_name')
  String? partnerName;
  @JsonKey(name: 'email')
  String? email;
  @JsonKey(name: 'start_at')
  String? startAt;
  @JsonKey(name: 'finish_at')
  String? finishAt;

  factory DetailPayloadModel.fromJson(Map<String, dynamic> json) => _$DetailPayloadModelFromJson(json);
  @override
  Map<String, dynamic> toJson() => _$DetailPayloadModelToJson(this);
}
