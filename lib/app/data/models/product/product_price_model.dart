import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';
part 'product_price_model.g.dart';

@JsonSerializable(explicitToJson: true)
class ProductPriceModel extends BaseModel {
  ProductPriceModel({
    String? point,
    this.price,
    this.hpp,
    this.hppAdminFee,
    this.adminFee,
    dynamic total,
  }) : super(
          point: point,
          total: total,
        );
  @JsonKey(name: 'hpp')
  dynamic hpp;
  @JsonKey(name: 'price')
  dynamic price;
  @JsonKey(name: 'hpp_admin_fee')
  dynamic hppAdminFee;
  @JsonKey(name: 'admin_fee')
  dynamic adminFee;

  factory ProductPriceModel.fromJson(Map<String, dynamic> json) => _$ProductPriceModelFromJson(json);
  @override
  Map<String, dynamic> toJson() => _$ProductPriceModelToJson(this);
}
