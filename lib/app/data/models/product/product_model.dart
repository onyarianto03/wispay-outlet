// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_price_model.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';
import 'package:wispay_outlet/app/data/models/product_category/product_category_item/product_category_item_model.dart';

import 'detail_payload_model.dart';

part 'product_model.g.dart';

@JsonSerializable(explicitToJson: true)
class ProductModel extends BaseModel {
  ProductModel({
    int? id,
    String? name,
    ImageModel? image,
    this.productCategory,
    this.productBrand,
    this.productSubcategory,
    String? description,
    BaseModel? meta,
    BaseModel? productSupplier,
    dynamic defaultPrice,
    dynamic total,
    dynamic defaultPoint,
    this.defaultHpp,
    this.defaultAdminFee,
    this.defaultHppAdminFee,
    this.price,
    this.adminFee,
    this.customerId,
    this.customerPhone,
    this.productName,
    this.isPurchaseable,
    this.isDigipos,
    this.insurance,
  }) : super(
          id: id,
          name: name,
          image: image,
          description: description,
          meta: meta,
          productSupplier: productSupplier,
          defaultPrice: defaultPrice,
          total: total,
          defaultPoint: defaultPoint,
        );

  @JsonKey(name: 'product_category')
  ProductCategoryItemModel? productCategory;
  @JsonKey(name: 'product_name')
  String? productName;
  @JsonKey(name: 'customer_phone')
  String? customerPhone;
  @JsonKey(name: 'product_brand')
  ProductBrandModel? productBrand;
  @JsonKey(name: 'product_price')
  ProductPriceModel? productPrice;
  @JsonKey(name: 'detail_payload')
  DetailPayloadModel? detailPayload;
  @JsonKey(name: 'product_subcategory')
  BaseModel? productSubcategory;

  @JsonKey(name: 'price', defaultValue: '0.0')
  dynamic price;
  @JsonKey(name: 'admin_fee', defaultValue: '0.0')
  dynamic adminFee;
  @JsonKey(name: 'customer_id')
  String? customerId;
  @JsonKey(name: 'default_hpp', defaultValue: '0.0')
  dynamic defaultHpp;
  @JsonKey(name: 'default_admin_fee', defaultValue: '0.0')
  dynamic defaultAdminFee;
  @JsonKey(name: 'default_hpp_admin_fee', defaultValue: '0.0')
  dynamic defaultHppAdminFee;
  @JsonKey(name: 'is_purchasable')
  bool? isPurchaseable;
  @JsonKey(name: 'is_digipos', defaultValue: false)
  bool? isDigipos;
  Premi? insurance;

  factory ProductModel.fromJson(Map<String, dynamic> json) => _$ProductModelFromJson(json);
  @override
  Map<String, dynamic> toJson() => _$ProductModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Premi {
  const Premi({this.premi});
  final String? premi;

  factory Premi.fromJson(Map<String, dynamic> json) => _$PremiFromJson(json);
  Map<String, dynamic> toJson() => _$PremiToJson(this);
}
