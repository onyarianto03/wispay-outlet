import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';
part 'wispay_bank_model.g.dart';

@JsonSerializable(explicitToJson: true)
class WispayBankModel extends BaseModel {
  const WispayBankModel({
    int? id,
    String? name,
    this.bankName,
    this.number,
    ImageModel? logo,
    String? code,
  }) : super(name: name, id: id, logo: logo, code: code);

  @JsonKey(name: 'bank_name')
  final String? bankName;
  final String? number;

  factory WispayBankModel.fromJson(Map<String, dynamic> json) => _$WispayBankModelFromJson(json);
  @override
  Map<String, dynamic> toJson() => _$WispayBankModelToJson(this);
}
