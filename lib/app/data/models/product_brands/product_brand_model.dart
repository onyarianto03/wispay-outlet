// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';

part 'product_brand_model.g.dart';

@JsonSerializable(explicitToJson: true)
class ProductBrandModel extends BaseModel {
  const ProductBrandModel(
      {int? id,
      String? code,
      String? name,
      bool? isActive,
      ImageModel? image,
      this.column3rdPlaceholder,
      this.isPhoneOnly,
      this.isTopup,
      this.isWithSerial})
      : super(
          id: id,
          name: name,
          isActive: isActive,
          image: image,
          code: code,
        );

  @JsonKey(name: 'is_topup', defaultValue: false)
  final bool? isTopup;
  @JsonKey(name: 'is_with_serial', defaultValue: false)
  final bool? isWithSerial;
  @JsonKey(name: 'is_phone_only', defaultValue: false)
  final bool? isPhoneOnly;
  @JsonKey(name: 'column3rd_placeholder', defaultValue: '')
  final String? column3rdPlaceholder;

  factory ProductBrandModel.fromJson(Map<String, dynamic> json) => _$ProductBrandModelFromJson(json);
}

@JsonSerializable(explicitToJson: true)
class ProductBrandListModel extends BaseModel {
  ProductBrandListModel(this.data);

  final List<ProductBrandModel>? data;

  factory ProductBrandListModel.fromJson(Map<String, dynamic> json) => _$ProductBrandListModelFromJson(json);

  Object? get first => null;
}
