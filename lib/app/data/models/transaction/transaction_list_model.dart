// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';

part 'transaction_list_model.g.dart';

@JsonSerializable()
class TransactionListModel {
  const TransactionListModel({this.data});

  final List<TransactionDataModel>? data;

  factory TransactionListModel.fromJson(Map<String, dynamic> json) =>
      _$TransactionListModelFromJson(json);

  Map<String, dynamic> toJson() => _$TransactionListModelToJson(this);
}
