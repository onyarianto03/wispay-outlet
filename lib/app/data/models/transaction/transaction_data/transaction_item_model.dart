// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';

part 'transaction_item_model.g.dart';

@JsonSerializable(explicitToJson: true, createToJson: false)
class TransactionItemModel extends BaseModel {
  const TransactionItemModel({
    String? createdAt,
    String? updatedAt,
    int? id,
    this.status,
    this.quantity,
    this.totalPrice,
    this.address,
    this.latitude,
    this.longitude,
  }) : super(createdAt: createdAt, updatedAt: updatedAt, id: id);

  final String? status;
  final int? quantity;
  @JsonKey(name: 'total_price')
  final String? totalPrice;
  final String? address;
  final String? latitude;
  final String? longitude;

  factory TransactionItemModel.fromJson(Map<String, dynamic> json) => _$TransactionItemModelFromJson(json);
}
