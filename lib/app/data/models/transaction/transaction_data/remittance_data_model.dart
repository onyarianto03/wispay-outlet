import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/wispay_bank_model/wispay_bank_model.dart';
part 'remittance_data_model.g.dart';

@JsonSerializable(explicitToJson: true, createToJson: false)
class RemittanceDataModel extends BaseModel {
  RemittanceDataModel({
    int? id,
    String? createdAt,
    this.accountNumber,
    this.accountName,
    this.status,
    this.expiredAt,
    this.wispayBank,
  }) : super(id: id, createdAt: createdAt);

  @JsonKey(name: 'account_number')
  final String? accountNumber;
  @JsonKey(name: 'account_name')
  final String? accountName;
  @JsonKey(name: 'expired_at')
  final String? expiredAt;
  @JsonKey(name: 'wispay_bank')
  final WispayBankModel? wispayBank;
  final String? status;

  factory RemittanceDataModel.fromJson(Map<String, dynamic> json) => _$RemittanceDataModelFromJson(json);
}
