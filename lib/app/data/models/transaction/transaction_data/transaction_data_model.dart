// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/product/detail_payload_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/remittance_data_model.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_item_model.dart';

part 'transaction_data_model.g.dart';

@JsonSerializable(explicitToJson: true, createToJson: false)
class TransactionDataModel extends BaseModel {
  const TransactionDataModel({
    String? createdAt,
    int? id,
    String? code,
    this.stock,
    this.productName,
    this.customerId,
    this.customerPhone,
    this.serial,
    this.status,
    this.product,
    this.detailPayload,
    dynamic total,
    this.adminFee,
    this.commission,
    this.remittance,
    this.transactionItem,
    this.subtotal,
  }) : super(createdAt: createdAt, id: id, code: code, total: total);

  final String? stock;
  @JsonKey(name: 'product_name')
  final String? productName;
  @JsonKey(name: 'customer_id')
  final String? customerId;
  @JsonKey(name: 'customer_phone')
  final String? customerPhone;
  final String? serial;
  final String? status;
  final ProductModel? product;
  @JsonKey(name: 'detail_payload')
  final DetailPayloadModel? detailPayload;
  @JsonKey(name: 'commission')
  final dynamic commission;
  final RemittanceDataModel? remittance;
  @JsonKey(name: 'transaction_item')
  final TransactionItemModel? transactionItem;
  @JsonKey(name: 'admin_fee')
  final dynamic adminFee;
  final dynamic subtotal;

  factory TransactionDataModel.fromJson(Map<String, dynamic> json) => _$TransactionDataModelFromJson(json);
}
