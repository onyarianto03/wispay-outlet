import 'package:json_annotation/json_annotation.dart';

part 'remittance_inquiry_model.g.dart';

@JsonSerializable(explicitToJson: true, createToJson: false)
class RemittanceInquiryModel {
  final String? accountName;
  final String? accountNo;
  final String? bankShortCode;

  RemittanceInquiryModel({this.accountName, this.accountNo, this.bankShortCode});

  factory RemittanceInquiryModel.fromJson(Map<String, dynamic> json) => _$RemittanceInquiryModelFromJson(json);
}
