import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/wispay_bank_model/wispay_bank_model.dart';
part 'detail_request_topup_model.g.dart';

@JsonSerializable()
class DetailRequestTopupModel extends BaseModel {
  const DetailRequestTopupModel({
    int? id,
    String? amount,
    String? createdAt,
    this.expiredAt,
    this.pgAccountName,
    this.pgAccountNumber,
    this.wispayBank,
    String? total,
    this.adminFee,
    String? code,
    this.status,
    String? kind,
  }) : super(
          amount: amount,
          id: id,
          createdAt: createdAt,
          total: total,
          code: code,
          kind: kind,
        );

  @JsonKey(name: 'pg_account_name')
  final String? pgAccountName;
  @JsonKey(name: 'pg_account_number')
  final String? pgAccountNumber;
  @JsonKey(name: 'expired_at')
  final String? expiredAt;
  @JsonKey(name: 'wispay_bank')
  final WispayBankModel? wispayBank;

  @JsonKey(name: 'admin_fee')
  final String? adminFee;

  final String? status;

  factory DetailRequestTopupModel.fromJson(Map<String, dynamic> json) => _$DetailRequestTopupModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$DetailRequestTopupModelToJson(this);
}
