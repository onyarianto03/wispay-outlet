import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';
part 'topup_model.g.dart';

@JsonSerializable()
class TopupModel extends BaseModel {
  const TopupModel({
    int? id,
    String? amount,
    this.type,
    this.accountName,
    this.accountNumber,
    ImageModel? logo,
    this.expiredAt,
  }) : super(id: id, amount: amount, logo: logo);

  @JsonKey(name: 'type')
  final String? type;
  @JsonKey(name: 'account_number')
  final String? accountNumber;
  @JsonKey(name: 'account_name')
  final String? accountName;
  @JsonKey(name: 'expired_at')
  final String? expiredAt;

  factory TopupModel.fromJson(Map<String, dynamic> json) => _$TopupModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$TopupModelToJson(this);
}
