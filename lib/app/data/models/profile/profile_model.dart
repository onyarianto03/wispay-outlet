// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';
import 'package:wispay_outlet/app/data/models/regency/regency_model.dart';
import 'package:wispay_outlet/app/data/models/wallet/wallet_model.dart';
import 'package:wispay_outlet/app/data/models/comission_settings_model/comission_settings_model.dart';

part 'profile_model.g.dart';

@JsonSerializable(explicitToJson: true)
class ProfileModel extends BaseModel {
  ProfileModel({
    String? name,
    int? id,
    String? phone,
    ImageModel? avatar,
    this.email,
    this.accessKey,
    this.referenceCode,
    this.outletAddress,
    this.outletName,
    this.outletLatitude,
    this.outletLongitude,
    this.regency,
    this.outletWallet,
    this.outletPoint,
    this.upline,
    this.comissionSetting,
    this.district,
    this.status,
    this.isTselPartner,
    this.outletLatitudeNotSet,
    this.outletLongitudeNotSet,
  }) : super(name: name, id: id, phone: phone, avatar: avatar);

  final String? email;
  @JsonKey(name: 'reference_code')
  final String? referenceCode;
  @JsonKey(name: 'access_key')
  final String? accessKey;
  @JsonKey(name: 'outlet_name')
  final String? outletName;
  @JsonKey(name: 'outlet_latitude', defaultValue: 'BpLCudZBbetvYDvhFf9/jg==')
  final String? outletLatitude;
  @JsonKey(name: 'outlet_longitude', defaultValue: 'BpLCudZBbetvYDvhFf9/jg==')
  final String? outletLongitude;
  @JsonKey(name: 'outlet_address', defaultValue: 'BpLCudZBbetvYDvhFf9/jg==')
  final String? outletAddress;
  final RegencyModel? regency;
  @JsonKey(name: 'outlet_wallet')
  final WalletModel? outletWallet;
  @JsonKey(name: 'outlet_point')
  final WalletModel? outletPoint;
  BaseModel? upline;
  @JsonKey(name: 'commission_setting')
  ComissionSettingsModel? comissionSetting;
  @JsonKey(name: 'district')
  final District? district;
  final String? status;
  @JsonKey(name: 'is_tsel_partner', defaultValue: false)
  final bool? isTselPartner;
  @JsonKey(name: 'outlet_latitude_not_set', defaultValue: false)
  final bool? outletLatitudeNotSet;
  @JsonKey(name: 'outlet_longitude_not_set', defaultValue: false)
  final bool? outletLongitudeNotSet;

  factory ProfileModel.fromJson(Map<String, dynamic> json) => _$ProfileModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ProfileModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class District extends BaseModel {
  District({int? id, String? name, this.regency}) : super(id: id, name: name);

  Regency? regency;
  factory District.fromJson(Map<String, dynamic> json) => _$DistrictFromJson(json);
}

@JsonSerializable(explicitToJson: true)
class Regency extends BaseModel {
  Regency({int? id, String? name, this.province}) : super(id: id, name: name);
  Province? province;
  factory Regency.fromJson(Map<String, dynamic> json) => _$RegencyFromJson(json);
}

@JsonSerializable(explicitToJson: true)
class Province extends BaseModel {
  Province({int? id, String? name}) : super(id: id, name: name);

  factory Province.fromJson(Map<String, dynamic> json) => _$ProvinceFromJson(json);
}



// {
//     "response": {
//         "status": 200,
//         "message": "OK",
//         "url": "https://staging-api.wis-pay.com/outlet/v1/account/profile"
//     },
//     "data": {
//         "id": 4,
//         "created_at": "2021-03-02 11:45:28 +0700",
//         "updated_at": "2021-12-08 12:15:59 +0700",
//         "name": "Dhian Jois",
//         "phone": "ZGULc59k4+NuP/Ole7Dm5g==",
//         "email": "ac751KcZa2slzEKv3246fmCOWEfPDchBa3DGv7VI+kI=",
//         "referral_code": "DHIAN222-WP",
//         "avatar": {
//             "url": "https://d12esd8hu6ksp7.cloudfront.net/storages/store/outlets/original/6a11e4f4-199f-4a73-8c87-389c13f008e9.png",
//             "thumbnail": {
//                 "url": "https://d12esd8hu6ksp7.cloudfront.net/storages/store/outlets/thumbnail/thumbnail_6a11e4f4-199f-4a73-8c87-389c13f008e9.png"
//             }
//         },
//         "is_active": true,
//         "reference_code": "SLB887DB",
//         "outlet_name": "rr5Gngl8A6u+uXf6ZTUI7A==",
//         "outlet_latitude": "gMO78V/6PKjS9UP4gbnUiA==",
//         "outlet_longitude": "BTi3MTURBvK4nwWHG4EYag==",
//         "outlet_address": "pKqXitfMR35RjAY7X/vKkKyyEz8EocW+7leFOAVZl+mrHLpGcO3YRG1hCuG/gQJRGt3habLgp5Sh27ecFSPOTiIc7esjRrWDRvRrMJZFunWtFQGlatqKy9xuVMP3iuzV25F7NQD2woxDmRxCPh3IlA==",
//         "suspend_at": null,
//         "topup_limit": "4417500.0",
//         "id_card_status": "PENDING",
//         "referral_commission": null,
//         "referral_expired_day": null,
//         "is_tsel_partner": null,
//         "outlet_latitude_already_set": false,
//         "outlet_longitude_already_set": false,
//         "regency": {
//             "id": 165,
//             "name": "KABUPATEN GARUT",
//             "is_internal": true,
//             "links": {},
//             "province": {
//                 "id": 12,
//                 "name": "JAWA BARAT",
//                 "links": {}
//             },
//             "cluster": {
//                 "id": 2,
//                 "created_at": "2021-03-01 00:54:36 +0700",
//                 "updated_at": "2021-03-01 00:54:38 +0700",
//                 "name": "Garut",
//                 "links": {}
//             }
//         },
//         "commission_setting": {
//             "id": 4,
//             "created_at": "2021-03-02 11:45:28 +0700",
//             "updated_at": "2021-03-22 17:57:41 +0700",
//             "all": "2000.0",
//             "pulsa": "0.0",
//             "pln": "20000.0",
//             "bpjs": "5000.0",
//             "pdam": "0.0",
//             "paket_data": "0.0",
//             "voucher_game": "0.0",
//             "uang_elektronik": "0.0",
//             "pulsa_pascabayar": "0.0",
//             "telkom": "25555.0",
//             "dompet_elektronik": "2500.0",
//             "televisi": "0.0",
//             "multi_finance": "0.0",
//             "streaming": "0.0",
//             "voucher_fisik": null,
//             "kirim_uang": null,
//             "sim_card": null,
//             "modem": null,
//             "links": {}
//         },
//         "upline": {
//             "id": 283,
//             "name": "Dhian Jois",
//             "phone": "ZGULc59k4+NuP/Ole7Dm5g==",
//             "email": "ac751KcZa2slzEKv3246fmCOWEfPDchBa3DGv7VI+kI=",
//             "referral_code": "SLB887DB",
//             "avatar": {
//                 "url": "https://d12esd8hu6ksp7.cloudfront.net/storages/store/sellers/original/f0d6c7cf-a8c6-4cda-b112-0a2c9cad4a74.png",
//                 "thumbnail": {
//                     "url": "https://d12esd8hu6ksp7.cloudfront.net/storages/store/sellers/thumbnail/thumbnail_f0d6c7cf-a8c6-4cda-b112-0a2c9cad4a74.png"
//                 }
//             }
//         },
//         "status": "VERIFIED",
//         "progress": "1.0",
//         "outlet_wallet": {
//             "id": 2043,
//             "created_at": "2021-11-30T20:06:17.649+07:00",
//             "updated_at": "2021-11-30T20:06:17.649+07:00",
//             "kind": "REFUND",
//             "in_out": "IN",
//             "description": "Pengembalian dana Rp10.800,- dari transaksi produk PLN Pasca Bayar - #TRX-263-2021-09-000000902",
//             "amount": "10800.0",
//             "total": "949562.0",
//             "product_category_id": 5
//         },
//         "outlet_point": {
//             "id": 437,
//             "created_at": "2021-11-17T13:28:11.792+07:00",
//             "updated_at": "2021-11-17T13:28:11.792+07:00",
//             "kind": "CASHBACK",
//             "in_out": "IN",
//             "description": "Cashback poin Wispay 500 dari transaksi produk Kirim Uang BRI - #TRX-746-2021-06-000000479 🟡",
//             "point": "500.0",
//             "total": "7355.0"
//         },
//         "remaining_daily_limit": 4417500,
//         "links": {},
//         "district": {
//             "id": 2671,
//             "name": "KALIBENING",
//             "regency": {
//                 "id": 191,
//                 "name": "KABUPATEN BANJARNEGARA",
//                 "is_internal": true,
//                 "links": {},
//                 "province": {
//                     "id": 13,
//                     "name": "JAWA TENGAH",
//                     "links": {}
//                 },
//                 "cluster": {
//                     "id": 5,
//                     "created_at": "2021-03-01 00:54:36 +0700",
//                     "updated_at": "2021-03-01 00:54:38 +0700",
//                     "name": "Purwokerto",
//                     "links": {}
//                 }
//             },
//             "links": {}
//         }
//     }
// }
