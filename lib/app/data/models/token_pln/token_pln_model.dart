import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
part 'token_pln_model.g.dart';

@JsonSerializable(explicitToJson: true)
class TokenPLNModel extends BaseModel {
  TokenPLNModel(
    int? id,
    String? createdAt,
    String? updatedAt,
    String? name,
    String? amount,
  ) : super(
          id: id,
          createdAt: createdAt,
          updatedAt: updatedAt,
          name: name,
          amount: amount,
        );

  factory TokenPLNModel.fromJson(Map<String, dynamic> json) => _$TokenPLNModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$TokenPLNModelToJson(this);
}
