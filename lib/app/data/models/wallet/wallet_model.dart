// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';

part 'wallet_model.g.dart';

@JsonSerializable(explicitToJson: true, createToJson: false)
class WalletModel extends BaseModel {
  WalletModel({
    int? id,
    dynamic amount,
    dynamic point,
    String? kind,
    String? description,
    dynamic total,
    ImageModel? image,
    this.inOut,
    this.productCategoryId,
  }) : super(id: id, amount: amount, point: point, kind: kind, description: description, total: total, image: image);

  @JsonKey(name: 'in_out')
  String? inOut;

  @JsonKey(name: 'product_category_id')
  int? productCategoryId;

  factory WalletModel.fromJson(Map<String, dynamic> json) => _$WalletModelFromJson(json);
}
