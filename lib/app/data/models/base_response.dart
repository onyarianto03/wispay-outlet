// Package imports:
import 'package:json_annotation/json_annotation.dart';

part 'base_response.g.dart';

@JsonSerializable(
  explicitToJson: true,
  genericArgumentFactories: true,
)
class BaseResponse<T> {
  Res? response;
  T? data;
  bool? success;
  String? message;

  BaseResponse({
    this.response,
    this.data,
    this.success = true,
    this.message,
  });

  factory BaseResponse.fromJson(Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$BaseResponseFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object Function(T? value) toJsonT) => _$BaseResponseToJson(this, toJsonT);
}

@JsonSerializable()
class Res {
  int? status;
  String? message;
  String? url;

  Res({this.status, this.message, this.url});

  factory Res.fromJson(Map<String, dynamic> json) => _$ResFromJson(json);

  Map<String, dynamic> toJson() => _$ResToJson(this);
}
