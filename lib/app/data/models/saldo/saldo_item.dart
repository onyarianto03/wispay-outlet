// Package imports:
import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';

part 'saldo_item.g.dart';

@JsonSerializable(explicitToJson: true)
class SaldoItem extends BaseModel {
  @JsonKey(name: 'in_out')
  final String? inOUT;

  SaldoItem({
    String? createdAt,
    String? amount,
    int? id,
    String? kind,
    this.inOUT,
    String? description,
    final String? total,
  }) : super(
          amount: amount,
          createdAt: createdAt,
          id: id,
          kind: kind,
          description: description,
          total: total,
        );

  factory SaldoItem.fromJson(Map<String, dynamic> json) => _$SaldoItemFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$SaldoItemToJson(this);
}
