// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';

part 'topup_history_model.g.dart';

@JsonSerializable(explicitToJson: true)
class TopUpHistoryModel extends BaseModel {
  final String? status;
  @JsonKey(name: 'transaction_id')
  final String? transactionId;

  TopUpHistoryModel(
    int? id,
    String? name,
    this.status,
    String? amount,
    this.transactionId,
    String? createdAt,
    String? updatedAt,
  ) : super(id: id, name: name, amount: amount, createdAt: createdAt, updatedAt: updatedAt);

  factory TopUpHistoryModel.fromJson(Map<String, dynamic> json) => _$TopUpHistoryModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$TopUpHistoryModelToJson(this);
}
