import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';
part 'gift_model.g.dart';

@JsonSerializable()
class GiftModel extends BaseModel {
  const GiftModel({
    int? id,
    String? name,
    ImageModel? image,
    String? point,
    String? description,
    String? endDate,
    String? startDate,
    String? kind,
    List<String>? terms,
    this.cashbackAmount,
    this.cashbackNominal,
    this.cashbackValidity,
    this.itemName,
    this.lotteryName,
    this.redeemQuota,
    this.giftPaylod,
    this.gift,
    this.giftExchangeDelivery,
  }) : super(
          id: id,
          name: name,
          image: image,
          point: point,
          description: description,
          startDate: startDate,
          endDate: endDate,
          terms: terms,
          kind: kind,
        );

  @JsonKey(name: 'redeem_quota')
  final int? redeemQuota;
  @JsonKey(name: 'cashback_nominal')
  final int? cashbackNominal;
  @JsonKey(name: 'cashback_amount')
  final int? cashbackAmount;
  @JsonKey(name: 'cashback_validity')
  final int? cashbackValidity;
  @JsonKey(name: 'item_name')
  final String? itemName;
  @JsonKey(name: 'lottery_name')
  final String? lotteryName;
  @JsonKey(name: 'gift_payload')
  final BaseModel? giftPaylod;
  final BaseModel? gift;
  @JsonKey(name: 'gift_exchange_delivery')
  final GiftExchangeDeliveryItem? giftExchangeDelivery;

  factory GiftModel.fromJson(Map<String, dynamic> json) => _$GiftModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$GiftModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GiftExchangeDeliveryItem extends BaseModel {
  GiftExchangeDeliveryItem({int? id, this.isClaimed}) : super(id: id);
  @JsonKey(name: 'is_claimed')
  bool? isClaimed;

  factory GiftExchangeDeliveryItem.fromJson(Map<String, dynamic> json) => _$GiftExchangeDeliveryItemFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$GiftExchangeDeliveryItemToJson(this);
}


// {
//       "id": 10,
//       "created_at": "2021-02-10 18:28:30 +0700",
//       "updated_at": "2021-02-10 18:28:30 +0700",
//       "name": "Dorla Reichel",
//       "description": "Soluta repellendus ab. Tempora eos aut. Omnis incidunt rerum.",
//       "image": {
//         "url": "http://localhost:3000/storages/development/store/gifts/original/c8d13869-8f4f-4a43-be68-6b23064612d6.png",
//         "thumbnail": {
//           "url": "http://localhost:3000/storages/development/store/gifts/thumbnail/thumbnail_c8d13869-8f4f-4a43-be68-6b23064612d6.png"
//         }
//       },
//       "point": "80725.0",
//       "kind": "LOTTERY",
//       "wallet_amount": 468106,
//       "cashback_nominal": 684206,
//       "cashback_amount": 21,
//       "cashback_validity": 82,
//       "item_name": "Lightweight Concrete Clock",
//       "lottery_name": "Rustic Granite Chair",
//       "start_date": "2021-02-10 18:28:29 +0700",
//       "end_date": "2021-02-17 18:28:29 +0700",
//       "redeem_quota": 100,
//       "is_fcm": false,
//       "is_active": true,
//       "terms": [
//         "Ut nemo esse autem.",
//         "Voluptatem dolores qui cumque.",
//         "Et architecto natus cumque."
//       ],
//       "links": {},
//       "product_category": {
//         "id": 1,
//         "created_at": "2021-02-10 17:46:58 +0700",
//         "updated_at": "2021-02-10 17:46:58 +0700",
//         "name": "Junita Kovacek II",
//         "image": {
//           "url": "http://localhost:3000/storages/development/store/product-categories/original/f3359004-d2c0-4db7-8743-22a756275cfb.png",
//           "thumbnail": {
//             "url": "http://localhost:3000/storages/development/store/product-categories/thumbnail/thumbnail_f3359004-d2c0-4db7-8743-22a756275cfb.png"
//           }
//         },
//         "position": 22,
//         "is_active": true,
//         "links": {}
//       }
//     }
