import 'package:json_annotation/json_annotation.dart';
part 'filter_model.g.dart';

@JsonSerializable()
class FilterModel {
  const FilterModel({
    this.date,
    this.from,
    this.productCategoryId,
    this.status,
    this.to,
    this.limit,
    this.page,
    this.inOut,
    this.kind,
    this.inRemittance,
    this.inTopup,
    this.statusList,
    this.q,
    this.productSubcategoryId,
    this.isDigipos,
    this.isPhysicalVoucher,
    this.isSimCard,
    this.isItem,
  }) : super();
  @JsonKey(name: 'created_at[from]', includeIfNull: false)
  final String? from;
  @JsonKey(name: 'created_at[to]', includeIfNull: false)
  final String? to;
  @JsonKey(name: 'date', includeIfNull: false)
  final String? date;
  @JsonKey(name: 'product_category_id', includeIfNull: false)
  final int? productCategoryId;
  @JsonKey(name: 'status', includeIfNull: false)
  final String? status;
  @JsonKey(name: 'page', includeIfNull: false)
  final int? page;
  @JsonKey(name: '_limit', includeIfNull: false)
  final int? limit;
  @JsonKey(name: 'kind', includeIfNull: false)
  final String? kind;
  @JsonKey(name: 'in_out', includeIfNull: false)
  final String? inOut;
  @JsonKey(name: 'in_topup', includeIfNull: false)
  final bool? inTopup;
  @JsonKey(name: 'in_remittance', includeIfNull: false)
  final bool? inRemittance;
  @JsonKey(name: 'status[]', includeIfNull: false)
  final List<String>? statusList;
  @JsonKey(name: 'q', includeIfNull: false)
  final String? q;
  @JsonKey(name: 'product_subcategory_id', includeIfNull: false)
  final int? productSubcategoryId;
  @JsonKey(name: 'is_digipos', includeIfNull: false)
  final bool? isDigipos;
  @JsonKey(name: 'is_physical_voucher', includeIfNull: false)
  final bool? isPhysicalVoucher;
  @JsonKey(name: 'is_sim_card', includeIfNull: false)
  final bool? isSimCard;
  @JsonKey(name: 'is_item', includeIfNull: false)
  final bool? isItem;

  factory FilterModel.fromJson(Map<String, dynamic> json) => _$FilterModelFromJson(json);

  Map<String, dynamic> toJson() => _$FilterModelToJson(this);
}
