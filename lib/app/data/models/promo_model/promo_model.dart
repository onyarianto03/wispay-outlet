import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';
import 'package:wispay_outlet/app/data/models/product_category/product_category_item/product_category_item_model.dart';
part 'promo_model.g.dart';

@JsonSerializable()
class PromoModel extends BaseModel {
  const PromoModel({
    int? id,
    String? name,
    String? code,
    String? description,
    String? startDate,
    String? endDate,
    ImageModel? image,
    String? title,
    List<String>? terms,
    this.cashbackAmount,
    this.cashbackMax,
    this.cashbackType,
    this.discountType,
    this.productCategory,
    this.promoType,
    this.quotaPerUser,
    this.quotaUsage,
  }) : super(
          id: id,
          code: code,
          description: description,
          startDate: startDate,
          endDate: endDate,
          image: image,
          terms: terms,
          name: name,
          title: title,
        );

  @JsonKey(name: 'promo_type')
  final String? promoType;
  @JsonKey(name: 'cashback_type')
  final String? cashbackType;
  @JsonKey(name: 'cashback_amount')
  final dynamic cashbackAmount;
  @JsonKey(name: 'cashback_max')
  final dynamic cashbackMax;
  @JsonKey(name: 'discount_type')
  final String? discountType;
  @JsonKey(name: 'quota_usage')
  final dynamic quotaUsage;
  @JsonKey(name: 'quota_per_user')
  final dynamic quotaPerUser;
  @JsonKey(name: 'product_category')
  final ProductCategoryItemModel? productCategory;

  factory PromoModel.fromJson(Map<String, dynamic> json) => _$PromoModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$PromoModelToJson(this);
}
