// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/product_category/product_category_item/product_category_item_model.dart';

part 'product_category_model.g.dart';

@JsonSerializable(explicitToJson: true)
class ProductCategoryModel {
  const ProductCategoryModel({this.data});

  final List<ProductCategoryItemModel>? data;

  factory ProductCategoryModel.fromJson(Map<String, dynamic> json) =>
      _$ProductCategoryModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProductCategoryModelToJson(this);
}
