// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';

part 'product_category_item_model.g.dart';

@JsonSerializable(explicitToJson: true)
class ProductCategoryItemModel extends BaseModel {
  ProductCategoryItemModel({
    String? name,
    int? id,
    bool? isActive,
    ImageModel? image,
    this.position,
    String? code,
    this.groupKey,
    this.routeName,
    this.imagePath,
  }) : super(
          name: name,
          id: id,
          isActive: isActive,
          image: image,
          code: code,
        );

  int? position;
  @JsonKey(name: 'group_key')
  String? groupKey;
  String? routeName;
  String? imagePath;

  factory ProductCategoryItemModel.fromJson(Map<String, dynamic> json) => _$ProductCategoryItemModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ProductCategoryItemModelToJson(this);
}
