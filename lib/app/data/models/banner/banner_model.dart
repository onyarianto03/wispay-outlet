// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/banner/banner_item/banner_item_model.dart';

part 'banner_model.g.dart';

@JsonSerializable()
class BannerModel {
  BannerModel({this.data});

  final List<BannerItemModel>? data;

  factory BannerModel.fromJson(Map<String, dynamic> json) =>
      _$BannerModelFromJson(json);

  Map<String, dynamic> toJson() => _$BannerModelToJson(this);
}
