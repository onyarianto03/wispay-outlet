// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';

part 'banner_item_model.g.dart';

@JsonSerializable(explicitToJson: true)
class BannerItemModel extends BaseModel {
  BannerItemModel({
    int? id,
    String? name,
    String? description,
    this.type,
    ImageModel? image,
  }) : super(id: id, name: name, description: description, image: image);

  String? type;

  factory BannerItemModel.fromJson(Map<String, dynamic> json) => _$BannerItemModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$BannerItemModelToJson(this);
}
