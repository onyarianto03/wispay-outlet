import 'package:json_annotation/json_annotation.dart';
part 'insurance_gadget_model.g.dart';

@JsonSerializable()
class InsuranceGadgetModel {
  @JsonKey(name: 'gadget_name')
  String? gadgetName;
  @JsonKey(name: 'gadget_brand')
  String? gadgetBrand;
  @JsonKey(name: 'gadget_year')
  String? gadgetYear;
  @JsonKey(name: 'gadget_price')
  int? gadgetPrice;
  @JsonKey(name: 'nominal')
  dynamic nominal;

  InsuranceGadgetModel({
    this.gadgetName,
    this.gadgetBrand,
    this.gadgetYear,
    this.gadgetPrice,
    this.nominal,
  });

  factory InsuranceGadgetModel.fromJson(Map<String, dynamic> json) => _$InsuranceGadgetModelFromJson(json);

  Map<String, dynamic> toJson() => _$InsuranceGadgetModelToJson(this);
}
