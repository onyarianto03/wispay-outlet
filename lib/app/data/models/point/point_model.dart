// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';

part 'point_model.g.dart';

@JsonSerializable(explicitToJson: true)
class PointModel extends BaseModel {
  @JsonKey(name: 'image_url')
  final String? imageUrl;
  final String? type;

  PointModel(
    int? id,
    String? createdAt,
    String? title,
    String? updatedAt,
    String? point,
    this.imageUrl,
    this.type,
  ) : super(id: id, createdAt: createdAt, updatedAt: updatedAt, point: point, title: title);

  factory PointModel.fromJson(Map<String, dynamic> json) => _$PointModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$PointModelToJson(this);
}
