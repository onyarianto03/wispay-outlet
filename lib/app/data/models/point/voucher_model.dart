// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';

part 'voucher_model.g.dart';

@JsonSerializable(explicitToJson: true)
class VoucherModel extends BaseModel {
  @JsonKey(name: 'expired_at')
  final String? expiredAt;
  @JsonKey(name: 'image_url')
  final String? imageUrl;

  VoucherModel(
    int? id,
    String? createdAt,
    String? updatedAt,
    String? title,
    this.expiredAt,
    this.imageUrl,
  ) : super(
          id: id,
          createdAt: createdAt,
          updatedAt: updatedAt,
          title: title,
        );

  factory VoucherModel.fromJson(Map<String, dynamic> json) => _$VoucherModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$VoucherModelToJson(this);
}
