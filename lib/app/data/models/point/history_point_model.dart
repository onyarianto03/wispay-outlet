// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';

part 'history_point_model.g.dart';

@JsonSerializable(explicitToJson: true)
class HistoryPointModel extends BaseModel {
  @JsonKey(name: 'in_out')
  final String? inOut;

  HistoryPointModel(
    int? id,
    String? description,
    this.inOut,
    String? point,
    String? createdAt,
    String? updatedAt,
  ) : super(id: id, description: description, point: point, createdAt: createdAt, updatedAt: updatedAt);

  factory HistoryPointModel.fromJson(Map<String, dynamic> json) => _$HistoryPointModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$HistoryPointModelToJson(this);
}
