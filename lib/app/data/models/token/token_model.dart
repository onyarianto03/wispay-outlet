// Package imports:
import 'package:json_annotation/json_annotation.dart';

part 'token_model.g.dart';

@JsonSerializable(createToJson: false)
class TokenModel {
  String? token;
  @JsonKey(name: 'refresh_token')
  String? refreshToken;

  TokenModel({
    this.token,
    this.refreshToken,
  });

  factory TokenModel.fromJson(Map<String, dynamic> json) =>
      _$TokenModelFromJson(json);
}
