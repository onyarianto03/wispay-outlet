// Package imports:
import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/inbox/promo_item.dart';

part 'promo_list_model.g.dart';

@JsonSerializable()
class PromoListModel {
  const PromoListModel({this.data});

  final List<PromoItem>? data;

  factory PromoListModel.fromJson(Map<String, dynamic> json) => _$PromoListModelFromJson(json);

  Map<String, dynamic> toJson() => _$PromoListModelToJson(this);
}
