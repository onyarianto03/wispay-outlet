// Package imports:
import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';

part 'promo_item.g.dart';

@JsonSerializable(explicitToJson: true)
class PromoItem extends BaseModel {
  @JsonKey(name: 'promo_type')
  final String? promoType;
  @JsonKey(name: 'cashback_type')
  final String? cashbackType;
  @JsonKey(name: 'cashback_amount')
  final double? cashbackAmount;
  @JsonKey(name: 'cashback_max')
  final double? cashbackMax;
  @JsonKey(name: 'discount_type')
  final String? discountType;
  @JsonKey(name: 'discount_amount')
  final double? discountAmount;
  @JsonKey(name: 'minimum_transaction')
  final double? minimumTransaction;
  @JsonKey(name: 'quota_usage')
  final int? quotaUsage;
  @JsonKey(name: 'quota_per_user')
  final int? quotePerUser;

  PromoItem({
    int? id,
    String? createdAt,
    String? updatedAt,
    String? name,
    String? description,
    String? code,
    String? startDate,
    String? endDate,
    this.promoType,
    this.cashbackType,
    this.cashbackAmount,
    this.cashbackMax,
    this.discountType,
    this.discountAmount,
    this.minimumTransaction,
    this.quotaUsage,
    this.quotePerUser,
    ImageModel? image,
  }) : super(
          name: name,
          description: description,
          createdAt: createdAt,
          updatedAt: updatedAt,
          id: id,
          image: image,
          code: code,
          startDate: startDate,
          endDate: endDate,
        );

  factory PromoItem.fromJson(Map<String, dynamic> json) => _$PromoItemFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$PromoItemToJson(this);
}
