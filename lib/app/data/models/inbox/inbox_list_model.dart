// Package imports:
import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/inbox/inbox_item.dart';

part 'inbox_list_model.g.dart';

@JsonSerializable()
class InboxListModel {
  const InboxListModel({this.data});

  final List<InboxItem>? data;

  factory InboxListModel.fromJson(Map<String, dynamic> json) => _$InboxListModelFromJson(json);

  Map<String, dynamic> toJson() => _$InboxListModelToJson(this);
}
