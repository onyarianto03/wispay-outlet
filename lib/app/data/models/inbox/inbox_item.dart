// Package imports:
import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';

part 'inbox_item.g.dart';

@JsonSerializable(explicitToJson: true)
class InboxItem extends BaseModel {
  final String? body;

  InboxItem({
    int? id,
    String? createdAt,
    String? updatedAt,
    String? title,
    this.body,
    ImageModel? image,
  }) : super(title: title, createdAt: createdAt, updatedAt: updatedAt, id: id, image: image);

  factory InboxItem.fromJson(Map<String, dynamic> json) => _$InboxItemFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$InboxItemToJson(this);
}

@JsonSerializable(explicitToJson: true)
class InboxUnread {
  @JsonKey(name: 'unread_count')
  final int? unreadCount;

  InboxUnread({
    this.unreadCount,
  });

  factory InboxUnread.fromJson(Map<String, dynamic> json) => _$InboxUnreadFromJson(json);

  Map<String, dynamic> toJson() => _$InboxUnreadToJson(this);
}

@JsonSerializable(explicitToJson: true)
class InboxUnreadModel {
  final InboxUnread? data;

  InboxUnreadModel({
    this.data,
  });

  factory InboxUnreadModel.fromJson(Map<String, dynamic> json) => _$InboxUnreadModelFromJson(json);

  Map<String, dynamic> toJson() => _$InboxUnreadModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class NotificationReadModel extends BaseModel {
  @JsonKey(name: 'read_at')
  final String? readAt;

  NotificationReadModel({
    int? id,
    this.readAt,
  }) : super(id: id);

  factory NotificationReadModel.fromJson(Map<String, dynamic> json) => _$NotificationReadModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$NotificationReadModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class InboxDetail extends BaseModel {
  final String? body;
  @JsonKey(name: 'notification_read')
  final NotificationReadModel? notificationRead;
  final dynamic links;
  final dynamic assignable;

  InboxDetail({
    int? id,
    String? createdAt,
    String? updatedAt,
    String? title,
    String? kind,
    this.body,
    this.notificationRead,
    ImageModel? image,
    this.links,
    this.assignable,
  }) : super(title: title, createdAt: createdAt, updatedAt: updatedAt, id: id, image: image, kind: kind);

  factory InboxDetail.fromJson(Map<String, dynamic> json) => _$InboxDetailFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$InboxDetailToJson(this);
}
