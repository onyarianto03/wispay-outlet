import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';
part 'bank_model.g.dart';

@JsonSerializable()
class BankModel extends BaseModel {
  const BankModel({
    int? id,
    ImageModel? logo,
    String? name,
    this.bankName,
  }) : super(id: id, logo: logo, name: name);

  @JsonKey(name: 'bank_name')
  final String? bankName;

  factory BankModel.fromJson(Map<String, dynamic> json) => _$BankModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$BankModelToJson(this);
}
