// Package imports:
import 'package:json_annotation/json_annotation.dart';

part 'otp_model.g.dart';

@JsonSerializable()
class OTPModel {
  final String? message;
  @JsonKey(name: 'otp_key')
  final String? otpKey;
  @JsonKey(name: 'otp_type')
  final String? otpType;
  @JsonKey(name: 'otp_expired')
  final DateTime? otpExpired;
  final String? email;
  final String? phone;

  const OTPModel({this.message, this.otpKey, this.otpType, this.otpExpired, this.email, this.phone});

  factory OTPModel.fromJson(Map<String, dynamic> json) => _$OTPModelFromJson(json);

  Map<String, dynamic> toJson() => _$OTPModelToJson(this);
}
