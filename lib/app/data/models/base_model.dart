// Package imports:
import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';

part 'base_model.g.dart';

@JsonSerializable(explicitToJson: true)
class BaseModel {
  final int? id;
  final String? name;
  final String? code;
  final String? title;
  final dynamic amount;
  @JsonKey(name: 'point', defaultValue: '0.0')
  final dynamic point;
  final String? description;
  final String? token;
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @JsonKey(name: 'updated_at')
  final String? updatedAt;
  @JsonKey(name: 'is_active')
  final bool? isActive;
  final ImageModel? image;
  final ImageModel? avatar;
  final ImageModel? attachment;
  final ImageModel? logo;
  final String? phone;
  @JsonKey(name: 'start_date')
  final String? startDate;
  @JsonKey(name: 'end_date')
  final String? endDate;
  @JsonKey(name: "terms")
  final List<String>? terms;
  final String? kind;
  final BaseModel? meta;
  @JsonKey(name: 'product_supplier')
  final BaseModel? productSupplier;
  @JsonKey(name: 'default_price')
  final dynamic defaultPrice;
  final dynamic total;
  @JsonKey(name: 'default_point')
  final dynamic defaultPoint;

  const BaseModel({
    this.id,
    this.code,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.title,
    this.amount,
    this.point,
    this.isActive,
    this.description,
    this.image,
    this.avatar,
    this.phone,
    this.logo,
    this.startDate,
    this.endDate,
    this.terms,
    this.kind,
    this.token,
    this.meta,
    this.productSupplier,
    this.defaultPrice,
    this.total,
    this.defaultPoint,
    this.attachment,
  });

  factory BaseModel.fromJson(Map<String, dynamic> json) => _$BaseModelFromJson(json);

  Map<String, dynamic> toJson() => _$BaseModelToJson(this);
}
