// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';

part 'regency_model.g.dart';

@JsonSerializable(explicitToJson: true)
class RegencyModel {
  RegencyModel({
    this.isInternal,
    this.province,
    this.cluster,
  }) : super();

  final bool? isInternal;
  final BaseModel? province;
  final BaseModel? cluster;

  factory RegencyModel.fromJson(Map<String, dynamic> json) =>
      _$RegencyModelFromJson(json);

  Map<String, dynamic> toJson() => _$RegencyModelToJson(this);
}
