import 'package:json_annotation/json_annotation.dart';
part 'request_body_model.g.dart';

@JsonSerializable()
class RequestBodyModel {
  const RequestBodyModel({
    this.pin,
    this.token,
    this.amount,
    this.kind,
    this.wispayBankId,
    this.phone,
    this.to,
    this.districtId,
    this.outletAddress,
    this.outletLatitude,
    this.outletLongitude,
    this.outletName,
    this.regencyId,
    this.recipientName,
    this.recipientAddress,
    this.recipientPhone,
    this.email,
    this.customerId,
    this.period,
    this.productId,
    this.promoCode,
    this.accountNo,
    this.bankShortCode,
    this.nominal,
    this.note,
    this.gadgetBrand,
    this.gadgetName,
    this.insuranceDeviceCategoryId,
    this.address,
    this.city,
    this.dateOfBirth,
    this.gadgetImei,
    this.gadgetSerialNumber,
    this.gender,
    this.identityNumber,
    this.insuranceImageBackId,
    this.insuranceImageFrontId,
    this.province,
    this.name,
    this.latitude,
    this.longitude,
    this.paymentCode,
    this.simCard,
    this.imei,
    this.quantity,
    this.statusChannel,
  });
  @JsonKey(name: 'pin', includeIfNull: false)
  final String? pin;
  @JsonKey(name: 'token', includeIfNull: false)
  final String? token;
  @JsonKey(name: 'amount', includeIfNull: false)
  final String? amount;
  @JsonKey(name: 'kind', includeIfNull: false)
  final String? kind;
  @JsonKey(name: 'wispay_bank_id', includeIfNull: false)
  final int? wispayBankId;
  @JsonKey(name: 'phone', includeIfNull: false)
  final String? phone;
  @JsonKey(name: 'to', includeIfNull: false)
  final String? to;
  @JsonKey(name: 'outlet_name', includeIfNull: false)
  final String? outletName;
  @JsonKey(name: 'regency_id', includeIfNull: false)
  final int? regencyId;
  @JsonKey(name: 'district_id', includeIfNull: false)
  final int? districtId;
  @JsonKey(name: 'outlet_latitude', includeIfNull: false)
  final String? outletLatitude;
  @JsonKey(name: 'outlet_longitude', includeIfNull: false)
  final String? outletLongitude;
  @JsonKey(name: 'outlet_address', includeIfNull: false)
  final String? outletAddress;
  @JsonKey(name: 'recipient_name', includeIfNull: false)
  final String? recipientName;
  @JsonKey(name: 'recipient_phone', includeIfNull: false)
  final String? recipientPhone;
  @JsonKey(name: 'recipient_address', includeIfNull: false)
  final String? recipientAddress;
  @JsonKey(name: 'email', includeIfNull: false)
  final String? email;
  @JsonKey(name: 'customer_id', includeIfNull: false)
  final String? customerId;
  @JsonKey(name: 'period', includeIfNull: false)
  final int? period;
  @JsonKey(name: 'product_id', includeIfNull: false)
  final int? productId;
  @JsonKey(name: 'promo_code', includeIfNull: false)
  final String? promoCode;
  @JsonKey(name: 'accountNo', includeIfNull: false)
  final String? accountNo;
  @JsonKey(name: 'bankShortCode', includeIfNull: false)
  final String? bankShortCode;
  @JsonKey(name: 'nominal', includeIfNull: false)
  final String? nominal;
  @JsonKey(name: 'note', includeIfNull: false)
  final String? note;
  @JsonKey(name: 'insurance_device_category_id', includeIfNull: false)
  final int? insuranceDeviceCategoryId;
  @JsonKey(name: 'gadget_brand', includeIfNull: false)
  final String? gadgetBrand;
  @JsonKey(name: 'gadget_name', includeIfNull: false)
  final String? gadgetName;
  @JsonKey(name: 'address', includeIfNull: false)
  final String? address;
  @JsonKey(name: 'gender', includeIfNull: false)
  final String? gender;
  @JsonKey(name: 'date_of_birth', includeIfNull: false)
  final String? dateOfBirth;
  @JsonKey(name: 'identity_number', includeIfNull: false)
  final String? identityNumber;
  @JsonKey(name: 'province', includeIfNull: false)
  final String? province;
  @JsonKey(name: 'city', includeIfNull: false)
  final String? city;
  @JsonKey(name: 'gadget_imei', includeIfNull: false)
  final String? gadgetImei;
  @JsonKey(name: 'gadget_serial_number', includeIfNull: false)
  final String? gadgetSerialNumber;
  @JsonKey(name: 'insurance_image_front_id', includeIfNull: false)
  final String? insuranceImageFrontId;
  @JsonKey(name: 'insurance_image_back_id', includeIfNull: false)
  final String? insuranceImageBackId;
  @JsonKey(name: 'name', includeIfNull: false)
  final String? name;
  @JsonKey(name: 'payment_code', includeIfNull: false)
  final String? paymentCode;
  @JsonKey(name: 'latitude', includeIfNull: false)
  final String? latitude;
  @JsonKey(name: 'longitude', includeIfNull: false)
  final String? longitude;
  @JsonKey(name: 'imei', includeIfNull: false)
  final String? imei;
  @JsonKey(name: 'sim_card', includeIfNull: false)
  final String? simCard;
  @JsonKey(name: 'quantity', includeIfNull: false)
  final int? quantity;
  @JsonKey(name: 'status_channel', includeIfNull: false)
  final String? statusChannel;

  factory RequestBodyModel.fromJson(Map<String, dynamic> json) => _$RequestBodyModelFromJson(json);

  Map<String, dynamic> toJson() => _$RequestBodyModelToJson(this);
}
