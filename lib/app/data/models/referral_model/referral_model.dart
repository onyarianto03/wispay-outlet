import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
part 'referral_model.g.dart';

@JsonSerializable()
class ReferralModel extends BaseModel {
  const ReferralModel({
    int? id,
    String? createdAt,
    this.commission,
    this.expiredDay,
    this.isFinished,
    this.consumer,
  }) : super(
          id: id,
          createdAt: createdAt,
        );

  @JsonKey(name: 'is_finished', defaultValue: false)
  final bool? isFinished;
  @JsonKey(name: 'expired_day')
  final int? expiredDay;
  @JsonKey(name: 'commission')
  final int? commission;
  final BaseModel? consumer;

  factory ReferralModel.fromJson(Map<String, dynamic> json) => _$ReferralModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ReferralModelToJson(this);
}
