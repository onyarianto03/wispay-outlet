// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/image/image_model.dart';

part 'category_item.g.dart';

@JsonSerializable(explicitToJson: true, createToJson: false)
class CategoryItem extends BaseModel {
  final String position;
  @JsonKey(name: 'group_key')
  final String groupKey;

  CategoryItem(
    int? id,
    String? name,
    String code,
    bool isActive,
    this.position,
    this.groupKey,
    ImageModel? image,
  ) : super(id: id, name: name, isActive: isActive, image: image, code: code);

  factory CategoryItem.fromJson(Map<String, dynamic> json) => _$CategoryItemFromJson(json);
}
