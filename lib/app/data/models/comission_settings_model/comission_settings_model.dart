import 'package:json_annotation/json_annotation.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
part 'comission_settings_model.g.dart';

@JsonSerializable()
class ComissionSettingsModel extends BaseModel {
  ComissionSettingsModel({
    this.all,
    this.pulsa,
    this.pln,
    this.bpjs,
    this.dompetElektronik,
    this.kirimUang,
    this.pdam,
    this.modem,
    this.multiFinance,
    this.paketData,
    this.pulsaPascabayar,
    this.simCard,
    this.streaming,
    this.televisi,
    this.telkom,
    this.uangElektronik,
    this.voucherFisik,
    this.voucherGame,
  }) : super();

  @JsonKey(name: "all", includeIfNull: false)
  final String? all;
  @JsonKey(name: "pulsa", includeIfNull: false)
  final String? pulsa;
  @JsonKey(name: "pln", includeIfNull: false)
  final String? pln;
  @JsonKey(name: "bpjs", includeIfNull: false)
  final String? bpjs;
  @JsonKey(name: "pdam", includeIfNull: false)
  final String? pdam;
  @JsonKey(name: "paket_data", includeIfNull: false)
  final String? paketData;
  @JsonKey(name: "voucher_game", includeIfNull: false)
  final String? voucherGame;
  @JsonKey(name: "uang_elektronik", includeIfNull: false)
  final String? uangElektronik;
  @JsonKey(name: "pulsa_pascabayar", includeIfNull: false)
  final String? pulsaPascabayar;
  @JsonKey(name: "telkom", includeIfNull: false)
  final String? telkom;
  @JsonKey(name: "dompet_elektronik", includeIfNull: false)
  final String? dompetElektronik;
  @JsonKey(name: "televisi", includeIfNull: false)
  final String? televisi;
  @JsonKey(name: "multi_finance", includeIfNull: false)
  final String? multiFinance;
  @JsonKey(name: "streaming", includeIfNull: false)
  final String? streaming;
  @JsonKey(name: "voucher_fisik", includeIfNull: false)
  final String? voucherFisik;
  @JsonKey(name: "kirim_uang", includeIfNull: false)
  final String? kirimUang;
  @JsonKey(name: "sim_card", includeIfNull: false)
  final String? simCard;
  @JsonKey(name: "modem", includeIfNull: false)
  final String? modem;

  factory ComissionSettingsModel.fromJson(Map<String, dynamic> json) => _$ComissionSettingsModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ComissionSettingsModelToJson(this);
}
