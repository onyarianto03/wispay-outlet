import 'package:json_annotation/json_annotation.dart';
part 'prediction_model.g.dart';

@JsonSerializable(explicitToJson: true)
class PredictionModel {
  const PredictionModel({
    this.description,
  });
  final String? description;

  factory PredictionModel.fromJson(Map<String, dynamic> json) => _$PredictionModelFromJson(json);

  Map<String, dynamic> toJson() => _$PredictionModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class LocationDataModel {
  const LocationDataModel({this.latitude, this.longitude, this.address});
  final String? latitude;
  final String? longitude;
  final String? address;

  factory LocationDataModel.fromJson(Map<String, dynamic> json) => _$LocationDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$LocationDataModelToJson(this);
}
