import 'package:json_annotation/json_annotation.dart';
part 'dynamic_link_model.g.dart';

@JsonSerializable()
class DynamicLinkModel {
  const DynamicLinkModel({
    this.referralCode,
    this.promoCode,
  });

  @JsonKey(name: 'referral_code', includeIfNull: false)
  final String? referralCode;
  @JsonKey(name: 'promo_code', includeIfNull: false)
  final String? promoCode;

  factory DynamicLinkModel.fromJson(Map<String, dynamic> json) => _$DynamicLinkModelFromJson(json);

  Map<String, dynamic> toJson() => _$DynamicLinkModelToJson(this);
}
