import 'package:flutter/material.dart';

class FilterDateModel {
  String? date;
  DateTimeRange? dateRange;

  FilterDateModel({
    this.date,
    this.dateRange,
  });
}
