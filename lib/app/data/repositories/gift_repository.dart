import 'package:dio/dio.dart';
import 'package:sprintf/sprintf.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/exceptions_helper.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/gift_model/gift_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';

class GiftRepository {
  final Dio _apiClient;

  GiftRepository(this._apiClient);

  Future<BaseResponse<List<GiftModel>>> getGifts({
    FilterModel? filter,
  }) async {
    try {
      final Response response = await _apiClient.get(
        URL_GIFTS,
        queryParameters: filter?.toJson(),
      );

      final BaseResponse<List<GiftModel>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<GiftModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => GiftModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<BaseModel>> redeemGift({
    required int giftId,
  }) async {
    try {
      String url = sprintf(URL_REEDEM_GIFT, [giftId]);
      final Response response = await _apiClient.post(url);

      final BaseResponse<BaseModel> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => BaseModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse> deliveryGift({
    required int giftId,
    required RequestBodyModel data,
  }) async {
    try {
      String url = sprintf(URL_DELIVERY_GIFT, [giftId]);
      final Response response = await _apiClient.patch(
        url,
        data: data.toJson(),
      );

      final BaseResponse result = BaseResponse.fromJson(
        response.data,
        (jsonT) => jsonT,
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<List<GiftModel>>> getExchangedGift({
    FilterModel? filter,
  }) async {
    try {
      final Response response = await _apiClient.get(
        URL_GIFT_EXCHANGE,
        queryParameters: filter?.toJson(),
      );

      final BaseResponse<List<GiftModel>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<GiftModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => GiftModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<GiftModel>> getDetailExchangedGift({
    required int giftId,
  }) async {
    try {
      final Response response = await _apiClient.get(
        URL_GIFT_EXCHANGE + '/' + giftId.toString(),
      );

      final BaseResponse<GiftModel> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => GiftModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<GiftModel>> getDetailGift({
    required int giftId,
  }) async {
    try {
      final Response response = await _apiClient.get(
        URL_GIFTS + '/' + giftId.toString(),
      );

      final BaseResponse<GiftModel> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => GiftModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }
}
