// Package imports:
import 'package:dio/dio.dart';
import 'package:sprintf/sprintf.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/exceptions_helper.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/models/insurance_model/insurance_gadget_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';

class InsuranceRepository {
  final Dio _apiClient;

  InsuranceRepository(this._apiClient);

  Future<BaseResponse<List<BaseModel>>> getDeviceCategory() async {
    try {
      final Response response = await _apiClient.get(URL_INSURANCE_DEVICE_CATEGORY);

      final BaseResponse<List<BaseModel>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<BaseModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => BaseModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<List<BaseModel>>> getProvinces() async {
    try {
      final Response response = await _apiClient.get(URL_INSURANCE_PROVINCE);

      final BaseResponse<List<BaseModel>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<BaseModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => BaseModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<List<BaseModel>>> getCities(int proviceId) async {
    try {
      final url = sprintf(URL_INSURANCE_CITY, [proviceId]);
      final Response response = await _apiClient.get(url);
      final BaseResponse<List<BaseModel>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<BaseModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => BaseModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<List<String>>> getBrands(String gadgetType) async {
    try {
      final Response response = await _apiClient.get(URL_INSURANCE_BRANDS, queryParameters: {
        'gadget_type': gadgetType,
      });

      final BaseResponse<List<String>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<String>.from(
          (jsonT as List<dynamic>).map(
            (_t) => _t,
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<List<InsuranceGadgetModel>>> getDevices(String gadgetType, String gadgetBrand) async {
    try {
      final Response response = await _apiClient.get(URL_INSURANCE_DEVICES, queryParameters: {
        'gadget_type': gadgetType,
        'gadget_brand': gadgetBrand,
      });

      final BaseResponse<List<InsuranceGadgetModel>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<InsuranceGadgetModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => InsuranceGadgetModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<List<ProductModel>>> inquiry({
    required String supplier,
    required String productName,
    required RequestBodyModel body,
  }) async {
    try {
      final url = sprintf(URL_TRANSACTION_INQUIRY, [supplier.toLowerCase(), productName]);
      final Response response = await _apiClient.post(url, data: body.toJson());

      final BaseResponse<List<ProductModel>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<ProductModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => ProductModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<BaseModel>> uploadImage(FormData data) async {
    try {
      final Response response = await _apiClient.post(
        URL_INSURANCE_IMAGE,
        data: data,
      );

      final BaseResponse<BaseModel> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => BaseModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }
}
