import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:wispay_outlet/app/core/values/http.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/data/models/utility/prediction_model.dart';

class GoogleRepository {
  final Dio _api;

  GoogleRepository(this._api) {
    _api.options.baseUrl = URL_GMAPS;
  }

  Future<List<PredictionModel>?> getPlaces({
    required String keyword,
    required String sessionToken,
  }) async {
    final kGoogleApiKey = dotenv.get('GOOGLE_API_KEY');
    try {
      Response _res = await _api.get(
        URL_GMAPS,
        queryParameters: {
          'sessiontoken': sessionToken,
          'input': keyword,
          'key': kGoogleApiKey,
          'language': 'id',
          'components': 'country:id',
        },
      );

      if (_res.statusCode == HTTP_OK) {
        List<PredictionModel> _list =
            (_res.data['predictions'] as List).map((e) => PredictionModel.fromJson(e)).toList();
        return _list;
      }
    } catch (e) {
      return null;
    }
    return null;
  }
}
