// Package imports:
import 'package:dio/dio.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/exceptions_helper.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/profile/profile_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/models/saldo/saldo_item.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';

class WalletRepository {
  final Dio _apiService;

  WalletRepository(this._apiService);

  Future<BaseResponse<List<SaldoItem>>> getHistory({FilterModel? filter}) async {
    try {
      final Response _res = await _apiService.get(URL_OUTLET_WALLET, queryParameters: filter?.toJson());

      final BaseResponse<List<SaldoItem>> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => List<SaldoItem>.from(
          (jsonT as List<dynamic>).map(
            (_t) => SaldoItem.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<ProfileModel>> getOutletByPhone({required RequestBodyModel data}) async {
    try {
      final Response _res = await _apiService.post(URL_TRANSFER_WALLET_QR, data: {'qr_code': data.toJson()});

      final BaseResponse<ProfileModel> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => ProfileModel.fromJson(jsonT as Map<String, dynamic>),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<TransactionDataModel>> transferToOutlet(
      {required RequestBodyModel data, required String pin}) async {
    try {
      final Response _res = await _apiService.post(
        URL_TRANSFER_WALLET_OUTLET,
        data: {
          'transfer_wallet': data.toJson(),
          'pin': pin,
        },
      );

      final BaseResponse<TransactionDataModel> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => TransactionDataModel.fromJson(jsonT as Map<String, dynamic>),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<TransactionDataModel>> transferToPartner(
      {required RequestBodyModel data, required String pin}) async {
    try {
      final Response _res = await _apiService.post(
        URL_TRANSFER_WALLET_PARTNER,
        data: {
          'transfer_wallet': data.toJson(),
          'pin': pin,
        },
      );

      final BaseResponse<TransactionDataModel> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => TransactionDataModel.fromJson(jsonT as Map<String, dynamic>),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<ProfileModel>> inquiryTransferPartner({required RequestBodyModel data}) async {
    try {
      final Response _res = await _apiService.post(
        URL_INQUIRY_TRANSFER_WALLET_PARTNER,
        data: {
          'inquiry': data.toJson(),
        },
      );

      final BaseResponse<ProfileModel> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => ProfileModel.fromJson(jsonT as Map<String, dynamic>),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }
}
