// Package imports:
import 'package:dio/dio.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/exceptions_helper.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';

class HomeRepository {
  final Dio _apiService;

  HomeRepository(this._apiService);

  Future<BaseResponse> getHomeSection(String section, {String? page, int? limit}) async {
    try {
      final Response _res = await _apiService.get(
        URL_HOME,
        queryParameters: {
          'section': section,
          'page': page,
          '_limit': limit,
        },
      );

      final BaseResponse result = BaseResponse.fromJson(
        _res.data,
        (json) => json,
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse> getHomeData() async {
    try {
      final Response _res = await _apiService.get(URL_HOME);

      final BaseResponse result = BaseResponse.fromJson(
        _res.data,
        (json) => json,
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }
}
