// Package imports:
import 'package:dio/dio.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/exceptions_helper.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/wispay_bank_model/wispay_bank_model.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';

class UtilityRepository {
  final Dio _apiService;

  UtilityRepository(this._apiService);

  Future<BaseResponse<List<WispayBankModel>>> getWispayBanks(String url, {FilterModel? filter}) async {
    try {
      final Response _res = await _apiService.get(url, queryParameters: filter?.toJson());

      final BaseResponse<List<WispayBankModel>> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => List<WispayBankModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => WispayBankModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<List<BaseModel>>> getProvinces({FilterModel? filter}) async {
    try {
      final Response _res = await _apiService.get(URL_PROVINCE, queryParameters: filter?.toJson());

      final BaseResponse<List<BaseModel>> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => List<BaseModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => BaseModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<List<BaseModel>>> getRegencyByProvinceId({FilterModel? filter, required int proviceId}) async {
    try {
      final Response _res =
          await _apiService.get(URL_PROVINCE + '/$proviceId' + '/regencies', queryParameters: filter?.toJson());

      final BaseResponse<List<BaseModel>> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => List<BaseModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => BaseModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<List<BaseModel>>> getDistrictsByRegency({FilterModel? filter, required int id}) async {
    try {
      final Response _res =
          await _apiService.get(URL_REGENCY + '/$id' + '/districts', queryParameters: filter?.toJson());

      final BaseResponse<List<BaseModel>> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => List<BaseModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => BaseModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<List<BaseModel>>> getRegency({FilterModel? filter}) async {
    try {
      final Response _res = await _apiService.get(URL_REGENCY, queryParameters: filter?.toJson());

      final BaseResponse<List<BaseModel>> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => List<BaseModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => BaseModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<List<BaseModel>>> getDistricts({FilterModel? filter}) async {
    try {
      final Response _res = await _apiService.get(URL_DISTRICT, queryParameters: filter?.toJson());

      final BaseResponse<List<BaseModel>> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => List<BaseModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => BaseModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }
}
