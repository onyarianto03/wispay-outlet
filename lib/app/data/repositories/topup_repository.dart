// Package imports:
import 'package:dio/dio.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/exceptions_helper.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/models/topup/detail_request_topup_model.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';

class TopupRepository {
  final Dio _apiService;

  TopupRepository(this._apiService);

  Future<BaseResponse<DetailRequestTopupModel>> getTopupRequestDetail(int id) async {
    try {
      final Response _res = await _apiService.get(URL_TOP_UP_REQUEST + '/' + id.toString());

      final BaseResponse<DetailRequestTopupModel> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => DetailRequestTopupModel.fromJson(jsonT as Map<String, dynamic>),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<DetailRequestTopupModel>> createTopupRequest({required RequestBodyModel data}) async {
    try {
      final Response _res = await _apiService.post(URL_TOP_UP_REQUEST, data: {
        "topup_request": data.toJson(),
      });

      final BaseResponse<DetailRequestTopupModel> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => DetailRequestTopupModel.fromJson(jsonT as Map<String, dynamic>),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<List<DetailRequestTopupModel>>> getTopupRequestList({FilterModel? filter}) async {
    try {
      final Response _res = await _apiService.get(URL_TOP_UP_REQUEST, queryParameters: filter?.toJson());

      final BaseResponse<List<DetailRequestTopupModel>> result = BaseResponse.fromJson(
        _res.data,
        (jsonT) => List<DetailRequestTopupModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => DetailRequestTopupModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }
}
