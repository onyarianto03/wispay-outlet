// Package imports:
import 'package:dio/dio.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/exceptions_helper.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/inbox/inbox_item.dart';
import 'package:wispay_outlet/app/data/models/inbox/inbox_list_model.dart';
import 'package:wispay_outlet/app/data/models/inbox/promo_list_model.dart';

class NotificationRepository {
  final Dio _apiClient;

  NotificationRepository(this._apiClient);

  Future<BaseResponse<InboxListModel>> getNotificationList({
    FilterModel? filter,
  }) async {
    try {
      final Response response = await _apiClient.get(
        URL_NOTIFICATION,
        queryParameters: filter?.toJson(),
      );

      final Map<String, dynamic> data = response.data;
      final BaseResponse<InboxListModel> result = BaseResponse.fromJson(
        response.data,
        (json) => InboxListModel.fromJson(data),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<PromoListModel>> getNotificationPromoList({
    FilterModel? filter,
  }) async {
    try {
      final Response response = await _apiClient.get(
        URL_PROMOS,
        queryParameters: filter?.toJson(),
      );

      final Map<String, dynamic> data = response.data;
      final BaseResponse<PromoListModel> result = BaseResponse.fromJson(
        response.data,
        (json) => PromoListModel.fromJson(data),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<InboxDetail>> readNotification({
    int? id,
  }) async {
    try {
      final Response response = await _apiClient.get(
        '$URL_NOTIFICATION/$id',
      );

      final Map<String, dynamic> data = response.data;
      final BaseResponse<InboxDetail> result = BaseResponse.fromJson(
        response.data,
        (json) => InboxDetail.fromJson(data),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<InboxUnreadModel>> unreadNotification({
    int? id,
  }) async {
    try {
      final Response response = await _apiClient.get(
        URL_UNREAD_NOTIFICATIONS,
      );

      final Map<String, dynamic> data = response.data;
      final BaseResponse<InboxUnreadModel> result = BaseResponse.fromJson(
        response.data,
        (json) {
          return InboxUnreadModel.fromJson(data);
        },
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }
}
