// Package imports:
import 'package:dio/dio.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/exceptions_helper.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/models/promo_model/promo_model.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';

class ArticleAndPromoRepository {
  final Dio _apiClient;

  ArticleAndPromoRepository(this._apiClient);

  Future<BaseResponse<PromoModel>> getDetailPromo(int id) async {
    try {
      final Response response = await _apiClient.get(URL_PROMOS + '/' + id.toString());

      final BaseResponse<PromoModel> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => PromoModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<PromoModel>> getDetailArticle(int id) async {
    try {
      final Response response = await _apiClient.get(URL_ARTICLES + '/' + id.toString());

      final BaseResponse<PromoModel> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => PromoModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }
}
