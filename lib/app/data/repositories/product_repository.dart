// Package imports:
import 'package:dio/dio.dart';
import 'package:sprintf/sprintf.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/exceptions_helper.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/data/models/base_model.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/product_brands/product_brand_model.dart';
import 'package:wispay_outlet/app/data/models/product_category/product_category_model.dart';

class ProductRepository {
  final Dio _apiClient;

  ProductRepository(this._apiClient);

  Future<BaseResponse<ProductCategoryModel>> getProductCategories() async {
    try {
      final Response response = await _apiClient.get(URL_PRODUCT_CATEGORY, queryParameters: {
        '_limit': 30,
      });

      final Map<String, dynamic> data = response.data;
      final BaseResponse<ProductCategoryModel> result = BaseResponse.fromJson(
        response.data,
        (json) => ProductCategoryModel.fromJson(data),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<List<ProductBrandModel>>> getProductBrands(id, {FilterModel? filter}) async {
    try {
      final url = sprintf(URL_PRODUCT_CATEGORY_BRAND, [id]);
      final Response response = await _apiClient.get(url, queryParameters: filter?.toJson());

      final BaseResponse<List<ProductBrandModel>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<ProductBrandModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => ProductBrandModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<List<BaseModel>>> getSubCategories(id, {FilterModel? filter}) async {
    try {
      final url = sprintf(URL_PRODUCT_BRAND_SUBCATEGORY, [id]);
      final Response response = await _apiClient.get(url, queryParameters: filter?.toJson());

      final BaseResponse<List<BaseModel>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<BaseModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => BaseModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<List<ProductModel>>> getProducts({FilterModel? filter}) async {
    try {
      final Response response = await _apiClient.get(URL_PRODUCTS, queryParameters: filter?.toJson());

      final BaseResponse<List<ProductModel>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<ProductModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => ProductModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }
}
