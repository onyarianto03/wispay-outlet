// Package imports:
import 'package:dio/dio.dart';
import 'package:sprintf/sprintf.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/exceptions_helper.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/product/product_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/remittance_inquiry_model.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_data/transaction_data_model.dart';
import 'package:wispay_outlet/app/data/models/transaction/transaction_list_model.dart';
import 'package:wispay_outlet/app/global_widgets/dialog/wispay_dialog.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';

class TransactionRepository {
  final Dio _apiClient;

  TransactionRepository(this._apiClient);

  Future<BaseResponse<TransactionListModel>> getTransactionList({
    FilterModel? filter,
  }) async {
    try {
      final Response response = await _apiClient.get(
        URL_TRANSACTIONS,
        queryParameters: filter?.toJson(),
      );

      final Map<String, dynamic> data = response.data;
      final BaseResponse<TransactionListModel> result = BaseResponse.fromJson(
        response.data,
        (json) => TransactionListModel.fromJson(data),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<RemittanceInquiryModel>> inquiry({
    required String supplier,
    required String productName,
    required RequestBodyModel body,
  }) async {
    try {
      final url = sprintf(URL_TRANSACTION_INQUIRY, [supplier.toLowerCase(), productName]);
      final Response response = await _apiClient.post(url, data: body.toJson());

      final Map<String, dynamic> data = response.data;
      final BaseResponse<RemittanceInquiryModel> result = BaseResponse.fromJson(
        data,
        (json) => RemittanceInquiryModel.fromJson(json as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<ProductModel>> order({
    required String supplier,
    required String productName,
    required RequestBodyModel body,
  }) async {
    WispayDialog.showLoading();
    try {
      final url = sprintf(URL_TRANSACTION_ORDER, [supplier.toLowerCase(), productName]);
      final Response response = await _apiClient.post(url, data: body.toJson());

      WispayDialog.closeDialog();
      final Map<String, dynamic> data = response.data;
      final BaseResponse<ProductModel> result = BaseResponse.fromJson(
        data,
        (json) => ProductModel.fromJson(json as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispayDialog.closeDialog();
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<TransactionDataModel>> confirm({
    required String? supplier,
    required String? productName,
    required RequestBodyModel body,
    bool withPaymentDialog = true,
    bool withLoadingDialog = true,
  }) async {
    withPaymentDialog
        ? WispayDialog.showPaymentDialog()
        : withLoadingDialog
            ? WispayDialog.showLoading()
            : null;
    try {
      final url = sprintf(URL_TRANSACTION_CONFIRM, [supplier?.toLowerCase(), productName]);
      final Response response = await _apiClient.post(url, data: body.toJson());

      (withPaymentDialog || withLoadingDialog) ? WispayDialog.closeDialog() : null;
      final BaseResponse<TransactionDataModel> result = BaseResponse.fromJson(
        response.data,
        (json) => TransactionDataModel.fromJson(json as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      (withPaymentDialog || withLoadingDialog) ? WispayDialog.closeDialog() : null;
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<TransactionDataModel>> getDetail({
    required int? id,
  }) async {
    try {
      final url = sprintf(URL_DETAIL_TRANSACTION, [id]);
      final Response response = await _apiClient.get(url);

      final BaseResponse<TransactionDataModel> result = BaseResponse.fromJson(
        response.data,
        (json) => TransactionDataModel.fromJson(json as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<TransactionDataModel>> createSellOut({
    required RequestBodyModel data,
  }) async {
    try {
      final Response response = await _apiClient.post(URL_SELL_OUT, data: data.toJson());
      final BaseResponse<TransactionDataModel> result = BaseResponse.fromJson(
        response.data,
        (json) => TransactionDataModel.fromJson(json as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<TransactionDataModel>> updateComission({required String comission, required int trxId}) async {
    try {
      final url = sprintf(URL_TRANSACTION_UPDATE_COMISSION, [trxId]);
      final Response response = await _apiClient.patch(url, data: {
        'transaction': {'commission': comission}
      });
      final BaseResponse<TransactionDataModel> result = BaseResponse.fromJson(
        response.data,
        (json) => TransactionDataModel.fromJson(json as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }
}
