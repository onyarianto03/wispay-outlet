// Package imports:
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';

// Project imports:
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/helpers/exceptions_helper.dart';
import 'package:wispay_outlet/app/core/values/url.dart';
import 'package:wispay_outlet/app/data/models/auth/otp_model.dart';
import 'package:wispay_outlet/app/data/models/base_response.dart';
import 'package:wispay_outlet/app/data/models/comission_settings_model/comission_settings_model.dart';
import 'package:wispay_outlet/app/data/models/filter_model.dart';
import 'package:wispay_outlet/app/data/models/point/history_point_model.dart';
import 'package:wispay_outlet/app/data/models/profile/profile_model.dart';
import 'package:wispay_outlet/app/data/models/referral_model/referral_model.dart';
import 'package:wispay_outlet/app/data/models/request_body_model.dart';
import 'package:wispay_outlet/app/data/models/token/token_model.dart';
import 'package:wispay_outlet/app/global_widgets/wispay_snackbar.dart';

class AccountRepository {
  final Dio _apiClient;

  AccountRepository(this._apiClient);

  Future<BaseResponse<OTPModel>> otpAuth(String phone) async {
    var data = {
      "otp_auth": {"phone": encrypt(phone)}
    };

    try {
      Response response = await _apiClient.post(
        URL_OTP_AUTH,
        data: data,
      );

      BaseResponse<OTPModel>? result = BaseResponse.fromJson(
        response.data,
        (jsonT) => OTPModel.fromJson(jsonT as Map<String, dynamic>),
      );
      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<ProfileModel>> otpAuthVerification(
    String otpCode,
    String? otpKey,
  ) async {
    var data = {
      "otp_auth_verification": {"otp_code": otpCode}
    };

    try {
      Response response = await _apiClient.post(
        URL_OTP_AUTH_VERIFICATION,
        data: data,
        queryParameters: {
          "otp_key": otpKey,
        },
      );

      BaseResponse<ProfileModel> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => ProfileModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<OTPModel>> resendOTP(
    String phone,
    String? otpKey,
  ) async {
    var data = {
      "resend_otp": {
        "phone": encrypt(phone),
      }
    };

    try {
      Response response = await _apiClient.post(
        URL_OTP_AUTH_RESEND,
        data: data,
        queryParameters: {
          "otp_key": otpKey,
        },
      );

      BaseResponse<OTPModel>? result = BaseResponse.fromJson(
        response.data,
        (jsonT) => OTPModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<TokenModel>> signIn(
    String pin,
    String accessKey,
    String? fcmToken,
  ) async {
    var headers = {"Content-Type": "application/json", 'FCM-Device': 'ANDROID', 'FCM-Token': fcmToken};

    var data = {
      "sign_in": {
        "pin": pin,
      }
    };

    try {
      Response response = await _apiClient.post(
        URL_SIGN_IN,
        data: data,
        queryParameters: {
          "access_key": accessKey,
        },
        options: Options(headers: headers),
      );

      BaseResponse<TokenModel> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => TokenModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<TokenModel>> signUp(String accessKey, FormData data, String? fcmToken) async {
    var headers = {"Content-Type": "application/json", 'FCM-Device': 'ANDROID', 'FCM-Token': fcmToken};

    try {
      Response response = await _apiClient.post(
        URL_SIGN_UP,
        data: data,
        queryParameters: {
          "access_key": accessKey,
        },
        options: Options(headers: headers),
      );

      BaseResponse<TokenModel> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => TokenModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<ProfileModel>> getProfile() async {
    try {
      Response response = await _apiClient.get(
        URL_PROFILE,
      );

      BaseResponse<ProfileModel> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => ProfileModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<OTPModel>> accountOTP({required String otpType, required String phone}) async {
    try {
      Response response = await _apiClient.post(URL_OTP, data: {
        "otp": {
          "otp_type": otpType,
          "phone": encrypt(phone),
        }
      });

      BaseResponse<OTPModel> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => OTPModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse> validatePIN({
    required String pin,
  }) async {
    try {
      Response response = await _apiClient.post(URL_VALIDATE_PIN, data: {
        "pin": {"pin": pin}
      });

      BaseResponse result = BaseResponse.fromJson(
        response.data,
        (jsonT) => jsonT,
      );

      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      // WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse> updatePIN({
    required String accessKey,
    required String phone,
    required String currentPin,
    required String newPin,
  }) async {
    try {
      Response response = await _apiClient.post(URL_UPDATE_PIN, data: {
        "current_pin": currentPin,
        "update_pin": {
          "pin": newPin,
          "pin_confirmation": newPin,
        }
      });

      BaseResponse result = BaseResponse.fromJson(
        response.data,
        (jsonT) => jsonT,
      );

      return result;
    } on DioError catch (error) {
      final message = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(message.message);
      return BaseResponse(
        success: false,
        message: message.message,
      );
    }
  }

  Future<BaseResponse<ProfileModel>> otpVerification(
    String otpCode,
    String? otpKey,
  ) async {
    var data = {
      "otp_verification": {"otp_code": otpCode}
    };

    try {
      Response response = await _apiClient.post(
        URL_OTP_VERIFICATION,
        data: data,
        queryParameters: {
          "otp_key": otpKey,
        },
      );

      BaseResponse<ProfileModel> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => ProfileModel.fromJson(jsonT as Map<String, dynamic>),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse> updateReferenceCode({
    required String referenceCode,
  }) async {
    var data = {
      "update_reference_code": {"reference_code": referenceCode}
    };

    try {
      Response response = await _apiClient.patch(
        URL_UPDATE_REFERENCE_CODE,
        data: data,
        options: Options(headers: {"Content-Type": "application/json"}),
      );

      BaseResponse result = BaseResponse.fromJson(
        response.data,
        (jsonT) => jsonT as Map<String, dynamic>,
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse> updateComissionSettings({
    required ComissionSettingsModel settings,
  }) async {
    var data = {"commission_setting": settings.toJson()};

    try {
      Response response = await _apiClient.patch(
        URL_COMISSION_SETTING,
        data: data,
        options: Options(headers: {"Content-Type": "application/json"}),
      );

      BaseResponse result = BaseResponse.fromJson(
        response.data,
        (jsonT) => jsonT as Map<String, dynamic>,
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse> updateOutlet({
    required RequestBodyModel data,
  }) async {
    var _data = {"update_outlet": data.toJson()};

    try {
      Response response = await _apiClient.patch(
        URL_UPDATE_OUTLET,
        data: _data,
        options: Options(headers: {"Content-Type": "application/json"}),
      );

      BaseResponse result = BaseResponse.fromJson(
        response.data,
        (jsonT) => jsonT as Map<String, dynamic>,
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<List<HistoryPointModel>>> getHistoryPoints({
    FilterModel? filter,
  }) async {
    try {
      final Response response = await _apiClient.get(
        URL_HISTORY_POINTS,
        queryParameters: filter?.toJson(),
      );

      final BaseResponse<List<HistoryPointModel>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<HistoryPointModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => HistoryPointModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse> updateProfile({
    required RequestBodyModel data,
  }) async {
    var _data = {"update_profile": data.toJson()};

    try {
      Response response = await _apiClient.patch(
        URL_UPDATE_PROFILE,
        data: _data,
        options: Options(headers: {"Content-Type": "application/json"}),
      );

      BaseResponse result = BaseResponse.fromJson(
        response.data,
        (jsonT) => jsonT as Map<String, dynamic>,
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse> updateAvatar({
    required String filePath,
  }) async {
    FormData _formData = FormData.fromMap({
      'update_avatar[avatar]': await MultipartFile.fromFile(
        filePath,
        contentType: MediaType('image', 'png'),
      )
    });

    try {
      Response response = await _apiClient.patch(
        URL_UPDATE_AVATAR,
        data: _formData,
        options: Options(headers: {"Content-Type": "multipart/form-data"}),
      );

      BaseResponse result = BaseResponse.fromJson(
        response.data,
        (jsonT) => jsonT as Map<String, dynamic>,
      );

      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }

  Future<BaseResponse<List<ReferralModel>>> referralComission() async {
    try {
      Response response = await _apiClient.get(URL_REFERRAL_COMISSION);

      final BaseResponse<List<ReferralModel>> result = BaseResponse.fromJson(
        response.data,
        (jsonT) => List<ReferralModel>.from(
          (jsonT as List<dynamic>).map(
            (_t) => ReferralModel.fromJson(_t as Map<String, dynamic>),
          ),
        ),
      );
      return result;
    } on DioError catch (error) {
      final err = DioExceptions.fromDioError(error);
      WispaySnackbar.showError(err.message);
      return BaseResponse(
        success: false,
        message: err.message,
      );
    }
  }
}
