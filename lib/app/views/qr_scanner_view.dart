import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/qr_scanner_controller.dart';

class QrScannerView extends GetView<QrScannerController> {
  const QrScannerView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(child: controller.buildQRview(context)),
        ],
      ),
    );
  }
}
