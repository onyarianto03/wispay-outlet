// Flutter imports:
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wispay_outlet/app/controllers/filter_date_controller.dart';
import 'package:wispay_outlet/app/core/helpers/date_helper.dart';

// Project imports:
import 'package:wispay_outlet/app/core/values/colors.dart';
import 'package:wispay_outlet/app/core/values/font_size.dart';
import 'package:wispay_outlet/app/core/values/spacing.dart';
import 'package:wispay_outlet/app/data/enums/datefilter_enum.dart';
import 'package:wispay_outlet/app/data/models/utility/filter_date_model.dart';
import 'package:wispay_outlet/app/global_widgets/bottom_sheet/wispay_bottomsheet.dart';
import 'package:wispay_outlet/app/global_widgets/button/button.dart';
import 'package:wispay_outlet/app/global_widgets/dropdown.dart';
import 'package:wispay_outlet/app/global_widgets/typography/custom_text.dart';

class FilterDate extends GetWidget<FilterDateController> {
  const FilterDate({
    Key? key,
    required this.onChangeDate,
  }) : super(key: key);

  final ValueChanged<FilterDateModel> onChangeDate;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Dropdown(
        value: controller.filterDateTitle.value,
        onTap: () => WispayBottomSheet.show(
          title: 'Pilih Tanggal',
          children: [_bottomsheet()],
        ),
      ),
    );
  }

  _bottomsheet() {
    return Obx(
      () => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          for (dateFilter i in controller.dateValue.keys)
            RadioListTile<dateFilter>(
              value: i,
              dense: true,
              contentPadding: EdgeInsets.zero,
              visualDensity: const VisualDensity(horizontal: -2, vertical: -2),
              groupValue: controller.selectedDateFilter.value,
              onChanged: (dateFilter? val) {
                controller.selectedDateFilter.value = val!;
              },
              title: CustomText(
                title: controller.dateValue[i]!['title'],
                color: WispayColors.cBlack666,
              ),
            ),
          if (controller.selectedDateFilter.value == dateFilter.selectCustomDate) const DateRangePick(),
          const Divider(thickness: 1, height: 32),
          CustomButton(
            title: 'Terapkan',
            onPress: () {
              if (controller.selectedDateFilter.value == dateFilter.selectCustomDate) {
                controller.onSaveFilterDate(type: 'range', cb: onChangeDate);
              } else {
                controller.onSaveFilterDate(type: 'default', cb: onChangeDate);
              }
            },
          ),
          const SizedBox(height: Spacing.defaultSpacing)
        ],
      ),
    );
  }
}

class DateRangePick extends GetWidget<FilterDateController> {
  const DateRangePick({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: Spacing.defaultSpacing),
      child: Row(
        children: [
          Expanded(
            child: InkWell(
              onTap: () async {
                final picked = await showDateRangePicker(
                  context: context,
                  firstDate: DateTime(2010),
                  lastDate: DateTime.now(),
                  builder: (context, child) => Theme(
                    child: child!,
                    data: ThemeData.light(),
                  ),
                  initialEntryMode: DatePickerEntryMode.calendarOnly,
                );
                _handlePick(picked);
              },
              child: Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(
                    color: WispayColors.cBorderE,
                    width: 1,
                  ),
                ),
                child: Obx(
                  () {
                    var data = controller.dateRangeValue.value;
                    return CustomText(
                      title: '${_formatDate(data.start.toString())} - ${_formatDate(data.end.toString())}',
                      textAlign: TextAlign.center,
                      size: FontSize.small,
                      color: WispayColors.cBlack666,
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _handlePick(DateTimeRange? picked) {
    if (picked == null) return;

    controller.dateRangeValue.value = picked;
  }

  _formatDate(String date) {
    return DateHelper.formatDate(date, format: 'dd MMM yyyy');
  }
}
