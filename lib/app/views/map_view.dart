// import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wispay_outlet/app/controllers/map_controller.dart';
import 'package:wispay_outlet/app/core/helpers/encryption_helper.dart';
import 'package:wispay_outlet/app/core/values/values.dart';
import 'package:wispay_outlet/app/global_widgets/card/base.dart';
import 'package:wispay_outlet/app/global_widgets/typography/wispay_typography.dart';
import 'package:wispay_outlet/app/global_widgets/widget.dart';
import 'package:wispay_outlet/generated/assets.gen.dart';

class MapView extends GetView<MapController> {
  const MapView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Cari Lokasi'),
        leading: IconButton(
          icon: const Icon(Icons.chevron_left, size: 35),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: Stack(
          children: [
            Obx(
              () => GoogleMap(
                compassEnabled: false,
                mapToolbarEnabled: false,
                zoomControlsEnabled: false,
                mapType: MapType.normal,
                initialCameraPosition: CameraPosition(
                  target: LatLng(
                    double.parse(decrypt(controller.homeController.profile.value.outletLatitude)),
                    double.parse(decrypt(controller.homeController.profile.value.outletLongitude)),
                  ),
                  zoom: 15,
                ),
                myLocationButtonEnabled: false,
                markers: <Marker>{controller.marker.value},
                myLocationEnabled: true,
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.1),
                onMapCreated: (c) => controller.onMapCreated(c),
              ),
            ),
            Positioned(
              top: Spacing.defaultSpacing,
              left: Spacing.defaultSpacing,
              right: Spacing.defaultSpacing,
              child: InkWell(
                child: const CustomTextInput(
                  fillColor: WispayColors.cWhite,
                  filled: true,
                  isEnabled: false,
                  prefixIcon: Icon(Icons.search, size: 24),
                  hintText: 'Cari Lokasi...',
                  value: '',
                ),
                onTap: () => WispayBottomSheet.scrollable(
                  withTitle: false,
                  onclose: () => controller.resetState(),
                  title: 'Lokasi',
                  onSearch: (term) => controller.onSearch(term),
                  itemBuilder: (context, scrollController) => Column(
                    children: [
                      const SizedBox(height: Spacing.defaultSpacing),
                      InkWell(
                        onTap: controller.useCurrentLocation,
                        child: BaseCard(
                          padding: const EdgeInsets.symmetric(
                            vertical: Spacing.small,
                            horizontal: Spacing.defaultSpacing,
                          ),
                          blurRadius: 20,
                          spreadRadius: 0,
                          margin: EdgeInsets.zero,
                          decorationImage: Assets.iconsMiscWave1,
                          child: Row(
                            children: const [
                              Icon(Icons.my_location, color: WispayColors.cPrimary),
                              SizedBox(width: Spacing.small),
                              CustomText(
                                title: 'Gunakan Lokasi Saat Ini',
                                color: WispayColors.cPrimary,
                                textType: TextType.SemiBold,
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: Spacing.defaultSpacing),
                      Obx(
                        () => Expanded(
                          child: ListView.separated(
                            shrinkWrap: true,
                            separatorBuilder: (context, index) => const Divider(thickness: 1),
                            itemBuilder: (ctx, idx) {
                              return InkWell(
                                onTap: () => controller.onSelectPlace(controller.listPlaces[idx]),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(vertical: Spacing.small),
                                  child: CustomText(title: controller.listPlaces[idx].description ?? ""),
                                ),
                              );
                            },
                            itemCount: controller.listPlaces.length,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
