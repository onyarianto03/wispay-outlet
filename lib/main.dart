// Flutter imports:
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:wispay_outlet/app/core/services/firebase_dynamic_list_service.dart';
import 'package:wispay_outlet/app/core/services/notification_service.dart';

// Package imports:
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

// Project imports:
import 'package:wispay_outlet/app/core/services/storage_service.dart';
import 'package:wispay_outlet/app/core/theme/app_theme.dart';
import 'package:wispay_outlet/app/global_bindings/initial_binding.dart';
import 'package:wispay_outlet/generated/locales.g.dart';
import 'app/routes/app_pages.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initServices();

  runApp(ScreenUtilInit(
    designSize: const Size(360, 640),
    builder: () => GetMaterialApp(
      title: "Application",
      initialRoute: Routes.HOME,
      translationsKeys: AppTranslation.translations,
      locale: const Locale('id', "ID"),
      defaultTransition: Transition.cupertino,
      transitionDuration: const Duration(milliseconds: 500),
      themeMode: ThemeMode.light,
      theme: MyThemes.light,
      darkTheme: MyThemes.dark,
      getPages: AppPages.routes,
      debugShowCheckedModeBanner: false,
      debugShowMaterialGrid: false,
      initialBinding: InitialBinding(),
      builder: (context, widget) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          child: widget!,
        );
      },
    ),
  ));
}

Future<void> initServices() async {
  await GetStorage.init();
  Get.putAsync(() => StorageService().init());
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage((_firebaseMessagingBackgroundHandler));
  FirebaseDynamicLinkService.initialize();
  final notif = NotificationService();
  notif.init();
  notif.saveToken();
  notif.watchNotification();

  await dotenv.load(fileName: ".env");
}
