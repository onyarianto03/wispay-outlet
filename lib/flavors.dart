enum Flavor {
  PRODUCTION,
  STAGING,
  DEV,
}

class F {
  static Flavor? appFlavor;

  static String get name => appFlavor?.name ?? '';

  static String get title {
    switch (appFlavor) {
      case Flavor.PRODUCTION:
        return 'Wispay Outlet';
      case Flavor.STAGING:
        return 'Wispay Outlet Staging';
      case Flavor.DEV:
        return 'Wispay Outlet Dev';
      default:
        return 'title';
    }
  }

}
